<?php 
class Saved_model extends Base_model {

    protected $table;

	public function __construct() {
		parent::__construct();	
		$this->table = "tbl_saved_model";		
    }
    
    public function saveData($data){
        $saved_data = $this->get($this->table, array('user_id' => $data['user_id'], 'index' => $data['index'], "model_name" => $data["model_name"]), 1);
        if(is_null($saved_data)){
            return $this->add($this->table, $data);
        }else{
            $id = $saved_data['id'];
            return $this->update($this->table, $data, array('id' => $id));
        }
    }

    public function getSavedData($data){
        $result = $this->get($this->table, $data); 
        return $result;
    }
    
}