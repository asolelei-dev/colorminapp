<?php 
class ShopingCart_model extends Base_model {

    protected $table;

	public function __construct() {
		parent::__construct();	
		$this->table = "tbl_shoping_cart";		
    }
    
    public function saveItem($data){
        $saved_data = $this->get($this->table, array('user_id' => $data['user_id']), 1);
        if(is_null($saved_data)){
            return $this->add($this->table, $data);
        }else{
            $user_id = $saved_data['user_id'];
            return $this->update($this->table, $data, array('user_id' => $user_id));
        }
    }

    public function getShopingCartItems($data){
        $result = $this->get($this->table, $data, 1);
        return $result;
    }

    public function resetShoppingCart($data){
        $result = $this->delete($this->table, $data);
        return $result;
    }
    
}