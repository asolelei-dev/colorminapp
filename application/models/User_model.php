<?php 
class User_model extends Base_model {
	
	protected $table;

	public function __construct() {
		parent::__construct();	
		$this->table = "tbl_user";		
	}

	public function authenticate($user_email, $password) {
		$user = $this->getUserByUserEmail($user_email);
		if( !empty($user) ) {			
			if(sha1($this->config->item('encryption_key').$password) == $user['password']) {
				if($user['status'] == 'active'){
					return array("success"=>true, "msg"=>"success", "data"=>$user);
				}else{
					return array("success"=>false, "msg"=>"INACTIVE USER. PLEASE CONTACT THE ADMINITSTRATOR!");
				}
			} else {
				return array("success"=>false, "msg"=>"INCORRECT PASSWORD!");
			}
		} else {
			return array("success"=>false, "msg"=>"NO USER!");
		}
	}
	

	public function isUserExist($user_email, $except=false) {
		$this->db->select('*');
		$this->db->where('user_email', $user_email);
		if($except)
			$this->db->where('id !=', $except);
			
		$query = $this->db->get($this->table);
		
		if( $query->num_rows() > 0 ) {
			return true;
		}
		else {
			return false;
		}
	}

	public function addUser($data, $signup = false)
	{
		if(isset($data['password']))
			$data['password'] = sha1($this->config->item('encryption_key').$data['password']);
		
		return $this->add($this->table, $data);
	}

	public function getUserByUserID($name) {
		$this->db->select('*');
		$this->db->from($this->table);
		
		$this->db->where('name', $name);
		
		$query = $this->db->get();
		
		return $query->row_array();
	}

	public function getUserByUserEmail($user_email) {
		$this->db->select('*');
		$this->db->from($this->table);
		
		$this->db->where('user_email', $user_email);
		
		$query = $this->db->get();
		
		return $query->row_array();
	}


	public function updatePassword($data){

		$matching =$this->checkPasswordMatching($data);
		if($matching){
			unset($data['current_password']);	
			return $this->update_admin($data, $data['id']);
		}else{
			return false;
		}
	}

	public function checkPasswordMatching($data){
		$this->db->select("*");
		$this->db->where("id", $data["id"]);
		$this->db->from($this->table);
		$query = $this->db->get();
		$result = $query->row_array();
		if(is_array($result)){
			if($result['password'] == sha1($data["current_password"])){
				return true;
			}else{
				return false;
			}
		}
		return false;
	}

	public function getAdminDetail($user_id){

		$this->db->select("*");
		$this->db->where("id", $user_id);
		$this->db->from($this->table);
		$query = $this->db->get();
		return $query->row_array();
	}

}
