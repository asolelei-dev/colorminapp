<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" type="text/css" rel="stylesheet">

<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>
<script src="https://sdks.shopifycdn.com/js-buy-sdk/2.0.1/index.unoptimized.umd.min.js"></script>

<script src = "<?php echo site_url(); ?>/assets/js/shoping-cart-item.js"></script>
<script src = "<?php echo site_url(); ?>/assets/js/shoping-cart.js"></script>

<div class="shoping-cart">
    <div class = "shoping-cart-header">
        <div class = "memu-bar">
            <div class = "btn-exit">
                <img src = "./assets/img/button_shop_close_v2.png"/>
            </div>
            <div class = "btn-bucket" id = "btn-bucket">
                <span></span>
                <img src = "./assets/img/button_shop_cart_v2.png"/>
            </div>
        </div>
        <div class = "banner-image">
            <img src = "./assets/img/product_image.png" class = "product-image"/>
        </div>
    </div>
    
    <div class = "shoping-cart-body">
        <div id = "product-title">
            <h4>Product title</h4>
            <div>
                COLORMINIS SHOP
            </div>
        </div>

        <div id = "cost-div">
            <div id = "old-cost">30</div>
            <div id = "new-cost">20</div>
        </div>

        <div id = "size-finish">SIZE &amp; FINISH</div>

        <div>
            <select class="selectpicker" id = "product-options" data-width="100%">
                <option title="Combo 1">Hot Dog</option>
                <option title="Combo 2">Burger</option>
                <option title="Combo 3">Sugar</option>
            </select>
        </div>

        <div>
            <div class = "row">
                <div class = "col">
                    <button type="button" class="btn btn-primary btn-block btn-sm" id = "btn_add_to_cart">ADD TO CART</button>
                </div>
                <div class = "col">
                    <button type="button" class="btn btn-primary btn-block btn-sm" id = "btn_buy_now">BUY NOW</button>
                </div>
            </div>
        </div>

        <div>
            <p style = "font-weight:bold;">
                Natural
            </p>
        </div>
        <div>
            <p>
                This finish has a slightly rough surface with matte colors.
            </p>
        </div>
        <div>
            <p>
                This finish is for decorative models, and is not well suited for handling.
            </p>
        </div>
        <div>
            <p style = "font-weight:bold;">
                Glossy
            </p>
        </div>
        <div>
            <p>
                A glossy finish that provides good UV and moisture resistance.
            </p>
        </div>
        <div>
            <p>
                This finish has a high sheen and is great for board game figures &amp; other uses that require handling.
            </p>
        </div>

    </div>

    <div class="accordion" id="faq-accordian">
        <div class="card">
            <div class="card-header" id="headingOne">
                <div class="mb-0">
                    <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        <i class = "fa fa-caret-down"></i>
                        <span>FAQ</span>
                    </button>
                </div>
            </div>
    
            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne">
                <div class="card-body" id = "faq-list-body">
                
                </div>
            </div>

        </div>

        <div class="card">
            <div class="card-header" id="headingTwo">
                <div class="mb-0">
                    <button class="btn btn-link btn-block collapsed text-left" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        <i class = "fa fa-caret-down"></i>
                        <span>Contact Us</span>
                    </button>
                </div>
            </div>
            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo">
                <div class="card-body">
                    Contact@ColorMinis.com
                </div>
            </div>
            </div>
        </div>
    </div>
</div>

<div class = "shoping-cart-bucket">
    <div class = "check-out-header">
        <div class = "btn-exit">
            <img src = "./assets/img/button_shop_close_v2.png"/>
        </div>
    </div>
    <div class = "check-out-body container">
        <h1>Your Cart</h1>
        <div id = "item-list">
        </div>
        <div id = "total-summary">
            <div id = "sub-total"></div>
            <div style = "font-style:italic;">Shipping & taxes caclulated at checkout</div>
            <button type="button" class="btn btn-primary btn-block" id = "btn_check_out">CHECKOUT</button>
        </div>
    </div>
</div>