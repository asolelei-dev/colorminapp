<script src = "<?php echo base_url()?>assets/js/login-signup.js"></script>
<div class = "nav-bar-header" style = "display:none;" align="right">
    <?php
        if($isLogin){
            ?>
            <a id="btn-login" style="display:none;">Log in</a>
            <a id="btn-logout" href = "#">Log out</a>
        <?php
        }else{
        ?>
            <a id="btn-login" ">Log in</a>
            <a id="btn-logout" style="display:none;" href = "#">Log out</a>
        <?php
        }
    ?>
</div>

<div id = "login-page" style = "display:none;">
        <a href = "#" class = "logo-image-link"><img src = "<?php echo site_url() ?>/assets/img/Logo.png"></a>
    <div class = "login-form">
        <div class = "login-signup-toggle">
            <div class = "active" data-target = "#login-form">Login</div>
            <div data-target = "#signup-form">Signup</div>
        </div>

        <div class = "login-signup-body">

            <div id = "login-form">

                <div id = "server-response-message">
                </div>

                <div class="form-group">
                    <input id="user_email" class="form-control" type="email" name="email"  placeholder = "*Email Address">
                </div>

                <div class="form-group">
                    <input id="user_password" class="form-control" type="password" name="password" placeholder = "*Password">
                </div>

                <div class="form-group">
                    <button class="btn btn-primary" id = "btn-sign-in">Sign In</button>
                </div>

                <div class = "or-text" align="center">
                    <span class = "line"></span>
                    OR
                    <span class = "line"></span>
                </div>

                <h2>Sign In With ...</h2>

                <div class="form-group">
                    <button id="btn-facebook" class="btn btn-primary btn-anchor" style = "width:100%" data-href="<?php echo site_url()?>auth/facebook_login" target="_facebookauth">FACEBOOK</button>
                </div>

                <div class="form-group">
                    <button id="btn-google" class="btn btn-primary btn-anchor" style = "width:100%" data-href="<?php echo site_url()?>auth/google_login" target="_googleauth">GOOGLE</button>
                </div>
            </div>

            <div id = "signup-form" style = "display:none;">

                <h2>Sign Up With ...</h2>

                <div class="form-group">
                    <button id="btn-facebook" class="btn btn-primary btn-anchor" style = "width:100%" data-href="<?php echo site_url()?>auth/facebook_login" target="_facebookauth">FACEBOOK</button>
                </div>

                <div class="form-group">
                    <button id="btn-google" class="btn btn-primary btn-anchor" style = "width:100%" data-href="<?php echo site_url()?>auth/google_login" target="_googleauth">GOOGLE</button>
                </div>

                <div class = "or-text" align="center">
                    <span class = "line"></span>
                    OR
                    <span class = "line"></span>
                </div>

                <div class = 'form-group'>
                    <input class="form-control" type="email" name="email" placeholder="Email Address *">
                    <p class = "error-text">&nbsp;</p>
                </div>

                <div class = 'form-group'>
                    <input class="form-control" type="password" name="password" placeholder="Password *">
                    <p class = "error-text">&nbsp;</p>
                </div>

                <div class = 'form-group'>
                    <input class="form-control" type="password" name="confirm_password" placeholder="Confirm Password *">
                    <p class = "error-text">&nbsp;</p>
                </div>

                <div class="form-group">
                    <button class="btn btn-primary" id = "btn-sign-up">Sign Up</button>
                </div>

                <div class="form-group" align="center">
                    <div class="g-recaptcha" data-callback="recaptchaCallback" data-sitekey="6Ld0d7gUAAAAADu22s3kyr2vkE3Sgqy4q5qRMdoT"></div>
                </div>
            </div>
            
        </div>
    </div>
</div>

