<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="manifest" href="manifest.json">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="0" />
    <meta name="google-signin-client_id" content="527345867759-416ifafdbfiqnciigpo3iesm23r35k9q.apps.googleusercontent.com">

    <style media='screen' type='text/css'>
        @font-face {
            font-family: "Nevis";
            src: url('<?php echo base_url()?>/assets/fonts/nevis.ttf');
            font-weight:400;
            font-weight:normal;
        }

        #game-canvas {
            display:flex;
        }
        

    </style>
    <link rel="stylesheet" href="<?php echo base_url()?>/assets/css/style.css" crossorigin="anonymous">

    <div style="font-family: Nevis; position: absolute; top:100px; color: transparent">.</div>
    <script src="<?php echo base_url()?>assets/js/phaser.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/papaparse.min.js"></script>
    <title>Color Minis</title>
    <script src="<?php echo base_url()?>assets/js/Phaser/output.js"></script>
    <link rel="stylesheet" href="<?php echo base_url()?>assets/fonts/pe/css/pe-icon-set-weather.min.css">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.4.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script>
        var user_id = `<?php echo $user_id ?>`;
    </script>
    <script src = "<?php echo site_url(); ?>/assets/cvs/APP-LINK.js"></script>
    <script src = "<?php echo site_url(); ?>/assets/js/api.js"></script>

    <script src="https://www.google.com/recaptcha/api.js" async defer></script>



</head>
<body>
	
    <div id = "game-canvas"></div>
	<?php $this->load->view('shoping-cart'); ?>
    <?php $this->load->view('color-header', array("isLogin"=>$isLogin)); ?>
    <?php $this->load->view('consent') ?>

<script>
if ("serviceWorker" in navigator) {
  if (navigator.serviceWorker.controller) {
    console.log("Color active service worker found, no need to register");
  } else {
    // Register the service worker
    navigator.serviceWorker
      .register("sw.js", {
        scope: "./"
      })
      .then(function (reg) {
        console.log("Service worker has been registered for scope: " + reg.scope);
      }); 
  }
}

var flag = 1;
setInterval(() => {
	if(flag){
		if(!navigator.onLine) {
			flag = 0;
			$('body').append("<div id='network' class='alert alert-warning alert-dismissible fade show' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button><strong>Network Problem!</strong> You are currently offline. Some data will not be available.</div>")
		}
	}
	
	if(navigator.onLine){
    flag = 1;
    $('#network').remove();
	}
  
}, 2000);

if (!('indexedDB' in window)) {
    console.warn('IndexedDB not supported')

}else{
  const employeeData = [
            { id: "01", name: "Adam", age: 35 },
            { id: "02", name: "Eve", age: 32 }
         ];
  var db;
  var request = window.indexedDB.open("colorminis", 1);

  request.onerror = function(event) {
    console.log("error: ");
  };
  
  request.onsuccess = function(event) {
    db = request.result;
    console.log("success: "+ db);
  };
  
  request.onupgradeneeded = function(event) {
    var db = event.target.result;
    var objectStore = db.createObjectStore("user", {keyPath: "id"});
    
    for (var i in employeeData) {
        objectStore.add(employeeData[i]);
    }
  }


}
</script>
</html>