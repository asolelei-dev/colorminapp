<div class="consent-layout">
    <div>
        By using this site, you agree to our updated <a class = "btn-privacy-policy" href = "#">Privacy Policy</a> and our <a class = "btn-terms -of-use" href="#">Terms of Use</a>
    </div>
    <div>
        <div class = "btn-exit" id = "btn-term-service-accept">
            <img src = "./assets/img/button_shop_close_v2.png"/>
        </div>
    </div>
</div>
<script>
    $("#btn-term-service-accept").click(()=>{
        $(".consent-layout").removeClass("active");
        localStorage.setItem("bottom-height", 0);
        localStorage.setItem("accepted", true);
    })

    $(".btn-privacy-policy").click(()=>{
        window.location.href = LINKS.PRIVACY;
    })

    $("#btn-terms -of-use").click(()=>{
        window.location.href = LINKS.PRIVACY;
    })
    
</script>