<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Color extends CI_Controller {

	public function __construct() {	
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('saved_model');
		$this->load->model('shopingcart_model');
	}

	public function index() {
		$isLogin = false;
		$user_id = 0;
		if(isset($_SESSION['id'])){
			$isLogin = true;
			$user_id = $_SESSION['id'];
		}
		$this->load->view('index', array('isLogin'=>$isLogin, 'user_id'=>$user_id));
	}

	public function uploadSavedData(){

		$data = array(
			"user_id" => $_POST['user_id'],
			"model_name" => $_POST['model_name'],
			"saved_data" => $_POST['saved_data'],
			"index" => $_POST['index']
		);
		$result_code = $this->saved_model->saveData($data);
		echo json_encode(array("success" => $result_code)); 

	}

	public function getSavedData(){

		if (isset($_GET['slot_index'])) {
			$data = array(
				// "user_id" => $_GET['user_id'],
				"model_name" => $_GET['model_name'],
				"index" => $_GET['slot_index']
			);
		} else {
			$data = array(
				"user_id" => $_GET['user_id'],
				"model_name" => $_GET['model_name']
			);
		}
		
		$result = $this->saved_model->getSavedData($data);
		echo json_encode($result);

	}


	public function getSavedShopItems(){
		$data = array(
			"user_id" => $_GET['user_id']
		);
		$result = $this->shopingcart_model->getShopingCartItems($data);
		echo json_encode($result);
	}

	public function addShopingCart(){
		$data = array(
			"user_id" => (isset($_SESSION['id'])?$_SESSION['id']:$_POST['user_id']),
			"jsonData" => $_POST['jsonData']
		);
		$result = $this->shopingcart_model->saveItem($data);
		echo json_encode($result);
	}

	public function restShoppingCart(){
		$data = array(
			"user_id" => (isset($_SESSION['id'])?$_SESSION['id']:$_POST['user_id']),
		);
		$result = $this->shopingcart_model->resetShoppingCart($data);
		echo json_encode($result);
	}

}
