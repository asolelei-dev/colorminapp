<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Auth extends CI_Controller {

	private $redirect_url = 'http://localhost/color/';

	public function __construct() {	
		parent::__construct();
		header("Access-Control-Allow-Origin: *");
		header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding");
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		$this->load->model("user_model");
		
	}	

	public function google_login() {
		$client = new Google_Client();
		$client->setAuthConfig('./google-credentials.json');
		$client->addScope("https://www.googleapis.com/auth/userinfo.email");
		$redirect_uri = 'https://colorminis.com/color/Auth/google_login_callback/';
		$client->setRedirectUri($redirect_uri);
		$objOAuthService = new Google_Service_Oauth2($client);

		// Get User Data from Google and store them in $data
		$authUrl = $client->createAuthUrl();
		$data['authUrl'] = $authUrl;
		redirect($authUrl);
	}

	public function google_login_callback(){
		$client = new Google_Client();
		$client->setAuthConfig('./google-credentials.json');
		$client->addScope("https://www.googleapis.com/auth/userinfo.email");
		$redirect_uri = 'https://colorminis.com/color/Auth/google_login_callback/';
		$client->setRedirectUri($redirect_uri);

		$objOAuthService = new Google_Service_Oauth2($client);

		// Add Access Token to Session
		if (isset($_GET['code'])) {
			$client->authenticate($_GET['code']);
			$accessToken = $client->getAccessToken();
			$userData = $objOAuthService->userinfo->get();

			$email = $userData['email'];
			$id = $userData['id'];

			$this->setSession("google", $accessToken, $email, $id);
			$this->saveSocialProfile();
			redirect("https://colorminis.com/color/");

		}
	}

	//
	public function facebook_login() {

		$fb = new Facebook\Facebook([
		  	'app_id' => FACEBOOK_APP_ID, // Replace {app-id} with your app id
			'app_secret' => FACEBOOK_APP_SECRET,
			'default_graph_version' => 'v3.2',
		]);

		$helper = $fb->getRedirectLoginHelper();
		$permissions = ['email']; // Optional permissions
		$loginUrl = $helper->getLoginUrl('https://figuromomodels.com/color/Auth/facebook_callback', $permissions);

		redirect($loginUrl);

	}


	public function facebook_callback() {

		$fb = new Facebook\Facebook([
		  	'app_id' => FACEBOOK_APP_ID, // Replace {app-id} with your app id
			'app_secret' => FACEBOOK_APP_SECRET,
			'default_graph_version' => 'v3.2',
		]);

		$helper = $fb->getRedirectLoginHelper();

		if (isset($_GET['state'])) {
			$helper->getPersistentDataHandler()->set('state', $_GET['state']);
		}

		try {
			$accessToken = $helper->getAccessToken();
		} catch(Facebook\Exceptions\FacebookResponseException $e) {
		  // When Graph returns an error
			echo 'Graph returned an error: ' . $e->getMessage();
			exit;
		} catch(Facebook\Exceptions\FacebookSDKException $e) {
		  // When validation fails or other local issues
			echo 'Facebook SDK returned an error: ' . $e->getMessage();
			exit;
		}

		if (! isset($accessToken)) {
			if ($helper->getError()) {
				header('HTTP/1.0 401 Unauthorized');
				echo "Error: " . $helper->getError() . "\n";
				echo "Error Code: " . $helper->getErrorCode() . "\n";
				echo "Error Reason: " . $helper->getErrorReason() . "\n";
				echo "Error Description: " . $helper->getErrorDescription() . "\n";
			} else {
				header('HTTP/1.0 400 Bad Request');
				echo 'Bad request';
			}
			exit;
		}

		$fb->setDefaultAccessToken((string) $accessToken);

		$response = $fb->get('/me?locale=en_US&fields=name,email');
		$userNode = $response->getGraphUser();
		$id = $userNode['id'];
		$email = $userNode['email'];

		$this->setSession("google", $accessToken, $email, $id);
		$this->saveSocialProfile();

		redirect($this->redirect_url);

	}

	function logout(){
		session_destroy();
		redirect($this->redirect_url);
	}

	function saveSocialProfile(){

		$email = $_SESSION['email'];
		$data = array(
			"user_email"=>$email,
		);

		if(!$this->user_model->isUserExist($email)){
			$id = $this->user_model->addUser($data);
		}
		
	}

	function setSession($oauthProvider, $accessToken, $user_email, $user_id){
		$_SESSION['oauth_provider'] = $oauthProvider;
		$_SESSION['access_token'] = $accessToken;
		$_SESSION['email'] = $user_email;
		$_SESSION['id'] = $user_id;
	}

	function getSession(){

		var_dump($this->user_model);

	}

	function loginByEmail(){ 
		$email = $_POST['email'];
		$password = $_POST['password'];
		$result_code = $this->user_model->authenticate($email, $password);
		if($result_code['success']){
			$this->setSession("myserver", "token", $email, $result_code["data"]["id"]);
		}
		echo json_encode($result_code);
	}

	function signup(){
		$user_name = "user1";
		$email = $_POST['email'];
		$password = $_POST['password'];
		$data = array(
			"user_name"=>$user_name,
			"user_email"=>$email,
			"password"=>$password
		);

		if($this->user_model->isUserExist($email)){
			$result_code = false;
			$msg = "Duplicated user!";
		} else {
			$result_code = $this->user_model->addUser($data);
			$this->setSession("myserver", "token", $email, $result_code);
			$msg = "success";
		}
		echo json_encode(array("success"=>$result_code, "msg"=>$msg, "data" => array("id"=>$result_code)));
	}
}
