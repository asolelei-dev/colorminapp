<?php
class moment {

	protected $hour;
	protected $today_time;
	protected $yesterday_time;
	protected $dayBeforeYesterday_time;

	function __construct() {
		$this->hour = 12;
		$this->today_time              = strtotime("today $this->hour:00");
		$this->yesterday_time          = strtotime("yesterday $this->hour:00");
		$this->dayBeforeYesterday_time = strtotime("yesterday -1 day $this->hour:00");
	}

	function today() {
		return date("Y-m-d", $this->today_time);
	}

	function yesterday() {		
		return date("Y-m-d", $this->yesterday_time);
	}

	function beforeYesterday() {
		return date("Y-m-d", $this->dayBeforeYesterday_time);
	}

	public function convertDateToDB($from) {
		$date = DateTime::createFromFormat('m/d/Y',$from);
		return $date->format("Y-m-d");
	}
}
