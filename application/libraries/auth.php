<?php
class auth{

	function __construct(){
		$this->ci = &get_instance();
		$this->ci->load->library('session');
	}
    /**
    * Sets login information
    * 
    * @param array $user_info
    */
	function setLoginInfo($user_info){
		$this->ci->session->set_userdata('user', $user_info);
		$this->ci->session->set_userdata('logged_in', 1);		
	}

	function setLoginPassword($password) {
		$this->ci->session->set_userdata('password', $password);
	}

	function setRandomInfo($data) {
		$this->ci->session->set_userdata('data', $data);
	}
    /**
    * Logs a user out
    * 
    * @param mixed 
    */
	function logout($user_info=false) {
		$this->ci->session->unset_userdata('user');
		$this->ci->session->unset_userdata('logged_in');
		$this->ci->session->sess_destroy();
		redirect('/', 'refresh');
	}

    /**
    * Gets current user array
    * 
    */
	function getUser(){
		return $this->ci->session->userdata('user');
	}

	function getPassword() {
		return $this->ci->session->userdata('password');
	}
    
	/**
	* Check the loggin state
	*
	*/

	function checkAuth() {
		return $this->getUser() != null ? AUTH_SUCCESS : AUTH_NO_FOUND_NAME;
	}

    /**
    * Checks if user is logged in
    * 
    */
	function isLoggedIn(){
		return $this->ci->session->userdata('logged_in');
	}

	function getPermission(){
		if ($this->isAboveAdmin())
			return "ADMIN";
		else if ($this->isAboveCenter())
			return "CENTER";
		else if ($this->isAboveTrainer())
			return "TRAINER";

		return "";
	}

	function isAboveAdmin(){
		$user = $this->ci->session->userdata('hl_user');
		if ($user != null && $user->type == 0)
			return true;
		return false;
	}

	function isAboveCenter(){
		$user = $this->ci->session->userdata('hl_user');
		if ($user != null && ($user->type == 0 || $user->type == 1))
			return true;
		return false;
	}

	function isAboveTrainer(){
		$user = $this->ci->session->userdata('hl_user');
		if ($user != null && ($user->type == 0 || $user->type == 1 || $user->type == 2))
			return true;
		return false;
	}
}	