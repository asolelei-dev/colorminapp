var SELECTED_RECAPTCHA = false;
$(function(){

    // hideCanvas();

    $("#btn-login").click(()=>{
        hideCanvas();
    })

    $("#btn-logout").click(()=>{
        // hideCanvas();
        window.location.href = `${CST.HOST_ADDRESS}/Auth/logout`;
    })

    $(".login-signup-toggle>div").click((e)=>{
        $(e.target).siblings().removeClass("active");
        $(e.target).addClass("active");
        $($(e.target).siblings().data("target")).hide()
        $($(e.target).data("target")).show();
    })

    $("#btn-sign-up").click((e)=>{
        signup();
    })

    $("#btn-sign-in").click((e)=>{
        login();
    })

    $(".logo-image-link").click(e=>{
        showCanvas();
    });

    $(".btn-anchor").click(e=>{
        window.location.href = $(e.target).data('href');
    });

    function showCanvas(){
        $("#login-page").hide();
        if(CURRENT_SCENE == CST.SCENES.MAIN){
            $(".nav-bar-header").show();
        }
        $("canvas").show();
        if(parseInt(user_id) > 0){
            $(".nav-bar-header #btn-login").hide();
            $(".nav-bar-header #btn-logout").show();
        }else{
            $(".nav-bar-header #btn-login").show();
            $(".nav-bar-header #btn-logout").hide();
        }
    }

    function hideCanvas(){
        $("#login-page").show();
        $(".nav-bar-header").hide();
        $("canvas").hide();
    }

    

    function login(){
        const email = $("#login-form input[name='email']").val();
        const password = $("#login-form input[name='password']").val();
        $.ajax({
            url:`${CST.HOST_ADDRESS}/Auth/loginByEmail`,
            method:"POST",
            data:{
                email,
                password
            },
            success:(res)=>{
                var resObj = JSON.parse(res);
                if(!resObj.success){
                    $("#server-response-message").html(`<p>${resObj.msg}</p>`)
                }else{
                    // user_id = resObj.data.id;
                    // showCanvas();
                    // if(pendingFunc) pendingFunc();
                    // pendingFunc = undefined;
                    location.reload();
                }
            }
        })

    }

    function signup(){

        if(!SELECTED_RECAPTCHA){
            alert("Are you bot?");
            return;
        }

        //validation
        const email = $("#signup-form input[name='email']").val();
        const password = $("#signup-form input[name='password']").val();
        const confirm_password = $("#signup-form input[name='confirm_password']").val();

        var error = false;

        error = !ValidateEmail(email);
        if(error) return;
        error = !ValidationPassword(password, confirm_password);
        if(error) return;
        
        $.ajax({
            url:`${CST.HOST_ADDRESS}/Auth/signup`,
            method:"POST",
            data:{
                email,
                password
            },
            success:(res)=>{
                var resObj = JSON.parse(res);
                if(!resObj.success){
                    $("#signup-form input[name='email']").siblings("p").html(resObj.msg);
                }else{
                    user_id = resObj.data.id;
                    showCanvas();
                }
            }
        })

        function ValidateEmail(mail) 
        {
        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))
        {
            $("#signup-form input[name='email']").siblings("p").html("&nbsp;");
            return (true)
        }
            $("#signup-form input[name='email']").siblings("p").html("You have entered an invalid email address!");
            return (false)
        }

        function ValidationPassword(password, confirm_password){
            if(password.length > 0){
                $("#signup-form input[name='password']").siblings("p").html("&nbsp;");
                if(password == confirm_password){
                    $("#signup-form input[name='confirm_password']").siblings("p").html("&nbsp;");
                    return true;
                } else {
                    $("#signup-form input[name='confirm_password']").siblings("p").html("Two password fields did not match");
                    return false;
                }
            }else{
                $("#signup-form input[name='password']").siblings("p").html("Password field is empty");
                return false;
            }
        }

    }


});

function recaptchaCallback(){
    SELECTED_RECAPTCHA = true;
}