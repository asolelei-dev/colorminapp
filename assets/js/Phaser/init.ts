/// <reference path='./phaser.d.ts'/>

module MyGame {
    export class InitPhaser {
        static gameRef:Phaser.Game;

        public static initGame(width:number, height:number, scale:number) {

            let config = {
                type: Phaser.AUTO,
                width: width,
                height: height,
                backgroundColor: '#dddddd',
                scene: [PreloadScene, LaunchScene, SaveSloteScene, MainScene, ModelingScene ],
                banner: true,
                title: 'Color Minis',
                url: 'https://updatestage.littlegames.app',
                version: '1.0.0',
                parent:"game-canvas",
                fps: {
                    min: 1,
                    target: 60,
                    forceSetTimeOut: false,
                    deltaHistory: 1
                },
                input: {
                    activePointers: 3
                }
            }

            this.gameRef = new Phaser.Game(config);
            this.gameRef.global = {
                colorsRanges:[],
            }

            LayoutContants.getInstance().setScreenBounds(width, height);
            LayoutContants.getInstance().setRootScale(scale);
        }
    }
}


window.onload = () => {
    setTimeout(() => {
        var scale = 1;
        if(window.innerWidth < 900) scale = 768 / window.innerWidth;
        var canvas = document.getElementById("game-canvas");
        canvas.style.transform = `scale(${1/scale})`;
        canvas.style.transformOrigin = `top left`;
        MyGame.InitPhaser.initGame(window.innerWidth * scale, window.innerHeight * scale, scale);    
    }, 1000);
};