class BottomSwitchBar{
	offImageTexture:string;
	onImageTexture:string;
	thumImageTexuture:string;
	sliderPathImageTexture:string;
	indicator:Phaser.GameObjects.Image;
	thumbButton:Phaser.GameObjects.Image;
	sliderPathImage:Phaser.GameObjects.Image;
	container:Phaser.GameObjects.Container;
	callback:(switchStatus:SwitchStatus)=>void;
	scene:ModelingScene;
	switchStatus:SwitchStatus;
	model:Model;
	isPingPong:boolean = true;
	isPing:boolean = true;
	timer:any;
	tween:Phaser.Tweens.Tween;
	stepDuration:number;
	minFrame: number;
	maxFrame: number;
	currentFrame:number;

	constructor(scene:ModelingScene, onImageTexture:string, offImageTexture:string, thumImageTexuture:string, sliderPathImageTexture:string, model: Model,  callback:(switchStatus:SwitchStatus)=>void ){
		this.offImageTexture = offImageTexture;
		this.onImageTexture = onImageTexture;
		this.sliderPathImageTexture = sliderPathImageTexture;
		this.thumImageTexuture = thumImageTexuture;
		this.callback = callback;
		this.scene = scene;
		this.model = model;
		this.switchStatus= SwitchStatus.RIGHT;
		this.container = this.scene.add.container(this.scene.uiGenerator.backgroundBoxTopper.displayWidth/2, +this.scene.game.config.height - CST.LAYOUT.BUTTONS_SIZE_BASE/LayoutContants.getInstance().SCREEN_HEIGHT_COEF/2);
		this.container.setDepth(2);
		this.createElement();
		this.bindSwitchEvent();
		

		this.minFrame = 0;
		this.maxFrame = parseInt(this.model.frameSetup.split(",")[1]);
		this.currentFrame = parseInt(this.model.frameSetup.split(",")[2]);
		this.stepDuration = 1000/(this.maxFrame - this.minFrame);
		this.initTimer();
	}

	createElement(){
		
		this.sliderPathImage = this.scene.add.image(0, 0, this.sliderPathImageTexture);
		this.sliderPathImage.displayWidth = (+this.scene.game.config.width - 3 * CST.LAYOUT.BUTTONS_SIZE) * 0.5
		this.sliderPathImage.setScale(LayoutContants.getInstance().SCALE);
		this.thumbButton = this.scene.add.image(0, 0, this.thumImageTexuture);
		this.thumbButton.setScale(LayoutContants.getInstance().SCALE);
		// this.thumbButton.x = this.sliderPathImage.displayWidth/2  - this.thumbButton.displayWidth/4;

		if(this.switchStatus != SwitchStatus.STOP){
			this.indicator = this.scene.add.image(-this.sliderPathImage.displayWidth/2 - 50, 0, this.onImageTexture);
		}else{
			this.indicator = this.scene.add.image(-this.sliderPathImage.displayWidth/2 - 50, 0, this.offImageTexture);	

		}
		this.indicator.setScale(LayoutContants.getInstance().SCALE);
		this.container.add(this.indicator);
		this.container.add(this.sliderPathImage);
		this.container.add(this.thumbButton);
		

	}

	bindSwitchEvent(){

		if(this.model.isOrbit == "1"){
			var isDown = false;
			var lastPosX = 0;
			this.thumbButton.setInteractive({
				hitAreaCallback:()=>{}
			}).on('pointerdown', (pointer, localX, localY, event)=>{
				isDown = true;
			}).on('pointermove', (pointer, localX, localY, event)=>{
				// console.log(pointer.x);
				if(isDown){
					if(lastPosX == 0){
						lastPosX = pointer.x;
					}
					const dx = pointer.x - lastPosX;
					this.thumbButton.x += dx;
					lastPosX = pointer.x;

					const delta = 30;
				 
					if(this.thumbButton.x - this.sliderPathImage.x > delta){
						if(this.switchStatus != SwitchStatus.RIGHT){
							// console.log("swich right!");
							this.switchStatus = SwitchStatus.RIGHT;
							this.callback(SwitchStatus.RIGHT);
							this.setSwtichOn(SwitchStatus.RIGHT, false);
							this.scene.playSound(CST.SOUND.COLOR_GENERAL_SOUND);
						}
					}
	
					if(this.sliderPathImage.x - this.thumbButton.x > delta){
							
							if(this.switchStatus != SwitchStatus.LEFT){
								   // console.log("swich left!");
								this.switchStatus = SwitchStatus.LEFT;
								this.callback(SwitchStatus.LEFT);
								this.setSwtichOn(SwitchStatus.LEFT, false);
								this.scene.playSound(CST.SOUND.COLOR_GENERAL_SOUND);
							}
					}
	
					if(Math.abs(this.sliderPathImage.x - this.thumbButton.x) < delta){
						if(this.switchStatus != SwitchStatus.STOP){
							// console.log("swich off!");
							this.switchStatus = SwitchStatus.STOP;
							this.callback(SwitchStatus.STOP);
							this.setSwtichOn(SwitchStatus.STOP, false);
							this.scene.playSound(CST.SOUND.COLOR_GENERAL_SOUND);
						}
					}
	
					// detect bound and fix position
					if(this.thumbButton.x <= -(this.sliderPathImage.displayWidth/2 - this.thumbButton.displayWidth/2)){
						this.thumbButton.x = -(this.sliderPathImage.displayWidth/2 - this.thumbButton.displayWidth/2);
					}
	
					if(this.thumbButton.x >= +(this.sliderPathImage.displayWidth/2 - this.thumbButton.displayWidth/2)){
						this.thumbButton.x = this.sliderPathImage.displayWidth/2 - this.thumbButton.displayWidth/2;
					}
	
				}
			}).on('pointerup', (pointer, localX, localY, event)=>{
				// console.log('pointerup');
				isDown = false;
				if(this.switchStatus == SwitchStatus.STOP){
					this.thumbButton.x = 0;
				}
				if(this.switchStatus == SwitchStatus.RIGHT){
					this.thumbButton.x = (this.sliderPathImage.displayWidth - this.thumbButton.displayWidth)/2;
				}
				if(this.switchStatus == SwitchStatus.LEFT){
					this.thumbButton.x = -(this.sliderPathImage.displayWidth - this.thumbButton.displayWidth)/2;
				}
				lastPosX = 0;
			}).on('pointerout', ()=>{
				isDown = false;
			});


			this.indicator.setInteractive()
			.on('ponterdown', (pointer, localX, localY, event)=>{
			})
			.on('pointerup', (pointer, localX, localY, event)=>{
				if(this.switchStatus == SwitchStatus.STOP){
					this.setSwtichOn(SwitchStatus.RIGHT, true);
					this.callback(SwitchStatus.RIGHT);
				}else{
					this.setSwtichOn(SwitchStatus.STOP, true);
					this.callback(SwitchStatus.STOP);
				}
				
			});


		}else{
			let lastX = 0;
			this.sliderPathImage.setInteractive()
			.on('pointerdown', (pointer, localX, localY, event)=>{
				
			}).on('pointermove', (pointer, localX, localY, event)=>{
				if(pointer.isDown){
					if(lastX == 0) lastX = pointer.x;
					var dx = pointer.x - lastX;
					if(this.thumbButton.x + dx  >= this.thumbButton.displayWidth/2 -this.sliderPathImage.displayWidth/2){
	
					} else {
						this.thumbButton.x = this.thumbButton.displayWidth/2 - this.sliderPathImage.displayWidth/2;
						dx = 0;
					}
					if(this.thumbButton.x + dx <= -this.thumbButton.displayWidth/2 +this.sliderPathImage.displayWidth/2){
	
					} else {
						this.thumbButton.x = -this.thumbButton.displayWidth/2 +this.sliderPathImage.displayWidth/2;
						dx = 0;
					}
					this.thumbButton.x +=  dx;
					lastX = pointer.x;

					if(!this.isPingPong){
						this.updateBodyPartFrame();
					}
				}
				
			}).on('pointerup', (pointer, localX, localY, event)=>{
				lastX = 0;
			}).on('pointerout', ()=>{
				lastX = 0;
			});

			this.indicator.setInteractive()
			.on('ponterdown', (pointer, localX, localY, event)=>{
				
			})
			.on('pointerup', (pointer, localX, localY, event)=>{
				this.togglePingPongIndicator();
			});

		}
	}

	updateBodyPartFrame(){
		const frame = this.getPingPongFrame();
		this.currentFrame = frame;
		this.scene.bodyPartsManager.setFrame(frame);
	}

	getPingPongFrame(){
		const distance = this.sliderPathImage.displayWidth - this.thumbButton.displayWidth;
		const bx = this.thumbButton.x + (this.sliderPathImage.displayWidth - this.thumbButton.displayWidth)/2;
		const ud = distance / (this.maxFrame - this.minFrame);
		const frame = this.maxFrame - ((this.isPing)?Math.ceil(bx/ud):Math.floor(bx/ud));
		if(frame>this.maxFrame) return this.maxFrame;
		if(frame<this.minFrame) return this.minFrame;
		return frame;
	}

	updateThumbButtonPositionByFrame(frame:integer){
		this.currentFrame = frame;
		const stdFrame = this.maxFrame/2;
		console.log("stdFrame =====> " , stdFrame);
		const distance = this.sliderPathImage.displayWidth - this.thumbButton.displayWidth;
		const ud = distance / (this.maxFrame - this.minFrame - 1);

		this.thumbButton.x = ud * (- frame + stdFrame);

		// if(this.tween) this.tween.remove();
		// this.tween = this.scene.tweens.add({
        //     targets: this.thumbButton,
        //     x: ud * (- frame + stdFrame),
        //     ease: 'Linear',
		// 	duration: this.stepDuration,
		// 	repeat:0,
		// 	onComplete:()=>{
		// 		console.log(this.timer);
		// 	}
        // });
	}

	setSwtichOn(status:SwitchStatus, movingButton:boolean){

		if(this.model.isOrbit == "1"){
			this.switchStatus = status;
			if(status == SwitchStatus.RIGHT){
				this.indicator.setTexture(this.onImageTexture);

				if(movingButton) this.thumbButton.x = this.sliderPathImage.displayWidth/2 - this.thumbButton.displayWidth/2;
			}else if(status == SwitchStatus.LEFT){
				this.indicator.setTexture(this.onImageTexture);
				if(movingButton) this.thumbButton.x = -(this.sliderPathImage.displayWidth/2 - this.thumbButton.displayWidth/2);
			}else{
				this.indicator.setTexture(this.offImageTexture);
				if(movingButton) this.thumbButton.x = 0;
			}
		}

		if(status == SwitchStatus.STOP) {
			this.setPingPongFlag(false);
			this.indicator.setTexture(this.offImageTexture);
		}
	}

	togglePingPongIndicator(){
		if(!this.isPingPong){
			this.indicator.setTexture(this.onImageTexture);
			this.isPingPong = true;
		}else{
			this.indicator.setTexture(this.offImageTexture);
			this.isPingPong = false;
		}
	}


	updatePingPong(){
		if(!this.scene.isFileLoaded) return;
		const minFrame = 0;
		const maxFrame = parseInt(this.model.frameSetup.split(",")[1]);
		if(this.isPingPong){
			var newFrame = 0
			if(this.isPing){
				newFrame = this.currentFrame- 1;
				if(newFrame < minFrame) {
					newFrame = minFrame;
					setTimeout(() => {
						this.isPing = false;	
					}, 1000);
					
				}
			}else{
				newFrame = this.currentFrame + 1;	
				console.log("newFrame for pong ==> ", newFrame);
				if(newFrame > maxFrame) {
					newFrame = maxFrame;
					setTimeout(() => {
						this.isPing = true;
					}, 1000);
				}
			}

			console.log("newFrame ==> ", newFrame);
			this.updateThumbButtonPositionByFrame(newFrame);
			this.scene.bodyPartsManager.setFrame(newFrame);
			this.scene.bodyPartsManager.updateModel();

		}
	}

	initTimer(){
		if(this.scene.model.isOrbit == "0"){
			this.timer = this.scene.time.addEvent({
				delay: this.stepDuration,                // ms
				callback: ()=>{

					this.updatePingPong();
				},
				//args: [],
				loop: true
			});
		}
	
	}

	setPingPongFlag(flag:boolean){
		this.isPingPong = flag;
	}

	setFrame(frame:integer){
		if(this.scene.model.isOrbit == "0"){
			this.updateThumbButtonPositionByFrame(frame);
		}
		
	}

	setVisible(visible:boolean){
		this.indicator.setVisible(visible);
		this.thumbButton.setVisible(visible);
		this.sliderPathImage.setVisible(visible);

	}

}