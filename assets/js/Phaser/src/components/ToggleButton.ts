class ToggleButton {
    scene:Phaser.Scene;
    defaultTexture:string;
    hightlightTexture:string;
    stack:Phaser.GameObjects.Container;
    backgroundImage:Phaser.GameObjects.Image;
    callback:(boolean)=>void;
    isON:boolean;
    constructor(scene:Phaser.Scene, x:number, y:number, defaultTexture:string, highlightTexture:string, callback){
        this.scene = scene;
        this.defaultTexture = defaultTexture;
        this.hightlightTexture = highlightTexture;
        this.callback = callback;
        this.backgroundImage = this.scene.add.image(0, 0, defaultTexture);
        this.stack = this.scene.add.container(x, y);
        this.stack.add(this.backgroundImage);
        this.stack.setScale(LayoutContants.getInstance().HEIGHT_SCALE);
        this.isON = false;
        this.initElements();
        this.initEvent();
    }

    initElements(){
        if(this.isON){
            this.backgroundImage.setTexture(this.hightlightTexture);
        }else{
            this.backgroundImage.setTexture(this.defaultTexture);
        }
    }

    initEvent(){
        this.backgroundImage.setInteractive().on('pointerdown', (pointer, localX, localY, event)=>{
        }).on('pointermove', (pointer, localX, localY, event)=>{
        }).on('pointerup', (pointer, localX, localY, event)=>{
            this.setToggle();
            this.callback(this.isON);
        });
    }

    setToggle(){
        const isOn = !this.isON;
        if(isOn){
            this.backgroundImage.setTexture(this.hightlightTexture);
        }else{
            this.backgroundImage.setTexture(this.defaultTexture);
        }
        this.isON = isOn;
    }

    setActive(flag:boolean){        
        this.isON = flag;
        if(this.isON){
            this.backgroundImage.setTexture(this.hightlightTexture);
        }else{
            this.backgroundImage.setTexture(this.defaultTexture);
        }
    }

}