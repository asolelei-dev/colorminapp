class ModelItem {

    modelButton:Phaser.GameObjects.Image;
    backgroundImage:Phaser.GameObjects.Image;
    model:Model;
    fileNames:FileNames;
    scrollMode:integer;
    scene: Phaser.Scene;
    stack:Phaser.GameObjects.Container;
    tabIndex:integer;

    constructor(scene: Phaser.Scene, scrollMode:integer, model?:Model){
        this.scene = scene;
        this.model = model;
        this.scrollMode = scrollMode;
        this.fileNames = new FileNames().generateFileNames(model, true);
        this.stack = new Phaser.GameObjects.Container(scene);
        this.initGroup(model);
    };

    initGroup(model:Model){

        if(this.getSelectedModelArray().includes(this.model.iconName)){
            this.backgroundImage = new Phaser.GameObjects.Image(this.scene,0,0,'IAPicon_BGBlue');
        }else{
            this.backgroundImage = new Phaser.GameObjects.Image(this.scene,0,0,'IAPicon_BGGrey');
        }
        this.backgroundImage.setOrigin(0,0);
        this.modelButton = new Phaser.GameObjects.Image(this.scene,0,0,model.iconName);
        this.modelButton.setOrigin(0,0);
        this.stack.add(this.backgroundImage);
        this.stack.add(this.modelButton);

    }

    updateBackground(flag:boolean){

        if(flag){
            this.backgroundImage.setTexture("IAPicon_BGBlue");
        }else{
            this.backgroundImage.setTexture("IAPicon_BGGrey");
        }

        if(!this.getSelectedModelArray().includes(this.model.iconName)){
            const new_selected_models_str = `${localStorage.getItem("selected_models")},${this.model.iconName}`;
            localStorage.setItem("selected_models", new_selected_models_str);
        }
    }

    getSelectedModelArray():string[]{
        const selected_models_str = localStorage.getItem("selected_models");
        if(selected_models_str){
            return selected_models_str.split(",");
        }else{
            return [];
        }
    }

    getDisplayWidth(){
        return this.backgroundImage.width * this.stack.scale;
    }
    getDisplayHeight(){
        return this.backgroundImage.height * this.stack.scale;
    }

    setDisplayWidth(displayWidth:number){
        const scale = displayWidth/this.backgroundImage.displayWidth;
        this.stack.setScale(scale);
    }

}
