class ColorPalletePanel {
    palleteNumber:number = 10
    callback:(pn:integer)=>void
    scene: Phaser.Scene
    stack:Phaser.GameObjects.Container
    buttonNext:Phaser.GameObjects.Image
    buttonPrevious:Phaser.GameObjects.Image
    backgroundBg:Phaser.GameObjects.Image
    text: Phaser.GameObjects.Text
    timerEvent: Phaser.Time.TimerEvent;
    stackOriginX:number;
    constructor(scene:Phaser.Scene, callback:(pn:integer)=>void){
        this.scene = scene;
        this.stack = this.scene.add.container(0, 0).setDepth(25);
        this.callback = callback
        this.initElement()
        this.bindEvent()
    }

    initElement(){

        const layout = LayoutContants.getInstance()
        this.backgroundBg = this.scene.add.image(0, 0, "BG_PaletteNew")
        this.buttonNext = this.scene.add.image(170, 30, "PaletteArrowR");
        this.buttonPrevious = this.scene.add.image(-130, 30, "PaletteArrowL");

        this.text = this.scene.add.text(0,-25,"1");
        this.text.setColor("#807f7d");
        this.text.setFontSize(100);
        this.text.setFontFamily("Nevis");
        
        this.stack.add(this.backgroundBg)
        this.stack.add(this.buttonNext)
        this.stack.add(this.buttonPrevious)
        this.stack.add(this.text)

        const scale = layout.BUTTONS_SIZE * 3/this.backgroundBg.displayWidth
        this.stackOriginX = layout.SCREEN_WIDTH - this.backgroundBg.displayWidth/2 * scale;
        this.stack.setPosition(this.stackOriginX, layout.SCREEN_HEIGHT - this.backgroundBg.displayHeight/2 * scale)
        this.stack.setScale(scale)
        //this.stack.setDepth(99);
        
    }

    correctTextCenter(){
        this.text.x = 20 - this.text.displayWidth * 0.5
    }

    setVisible(flag:boolean){
        this.stack.setVisible(flag)
    }

    bindEvent(){

        this.backgroundBg.setInteractive().on('poniterdown', ()=>{});

        this.buttonNext.setInteractive()
        .on('pointerdown', (pointer, localX, localY, event) => {
          
            this.timerEvent = this.scene.time.addEvent({
                delay: 1000,
                callback: ()=>{
                    this.autoChangePalleteNumber(true)
                },
                loop: false
            })
        })
        .on('pointerup', (pointer, localX, localY, event)=>{
            this.increasePalleteNumber();
            this.timerEvent.destroy();
            this.callback(this.palleteNumber);
        })
        this.buttonPrevious.setInteractive()
        
        .on('pointerdown', (pointer, localX, localY, event) => {
            this.timerEvent = this.scene.time.addEvent({
                delay: 1000,
                callback: ()=>{
                    this.autoChangePalleteNumber(false)
                },
                loop: false
            })
        })
        .on('pointerup', (pointer, localX, localY, event)=>{
            this.decreasePalleteNumber();
            this.timerEvent.destroy();
            this.callback(this.palleteNumber);
        })
    }

    getPalleteNumber(){
        return this.palleteNumber
    }

    setPalleteNumber(pallete:integer){
        this.palleteNumber = pallete
        this.text.setText(`${this.palleteNumber - 9}`)
        this.correctTextCenter()
    }

    increasePalleteNumber(){
        if(this.palleteNumber < 80){
            this.palleteNumber++
        }else{
            this.palleteNumber = 10
        }
        this.text.setText(`${this.palleteNumber - 9}`)
        this.correctTextCenter()
        
    }

    decreasePalleteNumber(){
        if(this.palleteNumber > 10){
            this.palleteNumber--
        }else{
            this.palleteNumber = 80
        }
        this.text.setText(`${this.palleteNumber - 9}`)
        this.correctTextCenter()
    }

    autoChangePalleteNumber(increase:boolean){
        this.timerEvent = this.scene.time.addEvent({
            delay:200,
            callback: ()=>{
                if(increase) this.increasePalleteNumber()
                else this.decreasePalleteNumber()
                this.callback(this.palleteNumber);
            },
            loop: true
        })
    }

}