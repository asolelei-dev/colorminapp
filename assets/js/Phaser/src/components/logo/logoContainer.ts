/// <reference path='../../../phaser.d.ts'/>
class LogoContainer extends Phaser.GameObjects.Container {
    scene:Phaser.Scene;
    logo:LogoSprite;
    logoFrame:Phaser.GameObjects.Image;
    constructor(scene: Phaser.Scene){
        const yOrigin = (Number)(scene.game.config.height) / 2;
        const xOrigin = (Number)(scene.game.config.width) / 2;
        super(scene, xOrigin, yOrigin);
        this.scene = scene;
        scene.add.existing(this);
        this.addLogo();

        this.scene.events.on(CST.EVENTS.LOADED, ()=>{
            this.logoFrame.setTexture('logoFrame2');
        });
    };

    addLogo(){
        this.logo = new LogoSprite(this.scene, 'logo');
        this.logoFrame = new Phaser.GameObjects.Image(this.scene,0,0,'logoFrame1');
        this.add(this.logo);
        this.add(this.logoFrame);
    }
};
