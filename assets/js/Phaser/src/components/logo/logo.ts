/// <reference path='../../../phaser.d.ts'/>

class LogoSprite extends Phaser.GameObjects.Sprite {
    scene:Phaser.Scene;

    constructor(scene: Phaser.Scene, texture:string, frame?: number|string){
        super(scene, 0, 0, texture, frame);
        scene.sys.updateList.add(this);
        scene.sys.displayList.add(this);
        this.scene = scene;
        this.animate(scene);
    };

    animate(scene: Phaser.Scene){

        const loading = scene.anims.create({
            key:'loading',
            repeat:0,
            frameRate:1,
            frames: scene.anims.generateFrameNames('logo', {
                prefix:'AnimFill_',
                suffix:'.png',
                start:2,
                end:7,
                zeroPad:1
            }),
        });
        this.play("loading");
        this.on('animationcomplete', (animation, frame)=>{

        }, this);
        this.on('animationupdate', (animation, frame)=>{
            if(frame.index == 6){
                this.scene.events.emit(CST.EVENTS.LOADED, true);
            }
        }, this);
    }
};
