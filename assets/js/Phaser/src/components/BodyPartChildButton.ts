// const _LEFTMOST_SLIDER_POSITION = 0;
// const _RIGHTMOST_SLIDER_POSITION = 8;

class ChildBodyPartButton {

    // _LEFTMOST_SLIDER_POSITION = 0;
    // _RIGHTMOST_SLIDER_POSITION = 8;

    scene:ModelingScene;
    bodyPartsManager:BodyPartsManager;
    atlasFront:string;
    region:string;
    bodyPart:BodyPart;
    stack:Phaser.GameObjects.Container;
    color:Phaser.Display.Color;
    uiGenerator:UIGenerator;
    interpolatingCoef:integer;
    interpolatingCoefIncrement:integer;
    lightBlinkingColor:Phaser.Display.Color;
    darkBlinkingColor:Phaser.Display.Color;
    image:Phaser.GameObjects.Image;
    index:integer;

    // blink features Edited by SY
    blinkProgress:integer;
    blinkCurColor:Phaser.Display.Color = new Phaser.Display.Color();
    blinkStartColor:Phaser.Display.Color;
    blinkreqColor:Phaser.Display.Color;
    blinkIncrement:integer;
    blinkRepeatNum:number;
    blinkRepeatLimit:number;

    constructor(bodyPart:BodyPart){
        this.bodyPart = bodyPart;
        this.scene = this.bodyPart.scene;
        this.bodyPartsManager = bodyPart.bodyPartsManager;
        this.stack = new Phaser.GameObjects.Container(this.scene);
        this.lightBlinkingColor = new Phaser.Display.Color(255, 255, 255);
        this.darkBlinkingColor = new Phaser.Display.Color(255, 255, 255);
        this.uiGenerator = this.bodyPart.scene.uiGenerator;
        this.createImage();
        this.interpolatingCoef = 0;
        this.interpolatingCoefIncrement = 1;
        // blink features Edied by SY
        this.blinkProgress = 0;
        this.blinkRepeatNum = 0;
        this.blinkRepeatLimit = 3;
        this.blinkIncrement = 14;
      
    }

    createImage(){
        this.atlasFront = this.scene.getHDAtalasFront();//model_name-front
        const tabRegions = this.bodyPart.modelPart.getTabRegions();
        
        

        const tabRegion = tabRegions[0];
        


        this.image = new Phaser.GameObjects.Image(this.scene, 0, 0, this.atlasFront, `${tabRegion}.png`);
        this.image.setScale(LayoutContants.getInstance().BUTTONS_SIZE/this.image.displayHeight);
   
        this.stack.add(this.image);

        


        // const buttonName = this.bodyPart.modelPart.feature.partName;
        // this.image = new Phaser.GameObjects.Image(this.scene, 0, 0, `${this.atlasFront}`, `${buttonName}.png`);
        // this.image.displayHeight = LayoutContants.getInstance().BUTTONS_SIZE*1.3;
        // this.image.displayWidth = LayoutContants.getInstance().BODY_PART_BUTTON_WIDTH/LayoutContants.getInstance().SCREEN_SIZE_COEF;

        // this.stack.add(this.image);
        // this.stack.setPosition(this.image.displayWidth/2, this.image.displayHeight/2);
    }

    onTapButton(){

        this.performSelection();
        // setting blink start

        this.resetColorForBlinking();

        this.scene.setBlinkingEnabled(true);

        this.scene.playSound(CST.SOUND.COLOR_GENERAL_SOUND);
        
        this.uiGenerator.setColorModelFromSelectedBodyPart();

    

        const colorButton = this.uiGenerator.getSelectedBodyPart().colorButton;

        var buttonIndex = 0;
        if(colorButton) buttonIndex = colorButton.buttonIndex; // set bodypart's color button

        this.uiGenerator.setColorSelection(buttonIndex); // set color for current selected bodypart 
        this.uiGenerator.resetMarkInColorButtons();
        this.uiGenerator.createUserAction();

    }
    
    performSelection(){
        
        this.bodyPart.depend = false;
        this.bodyPartsManager.setSelectedBodyPart(this.bodyPart); // set focused bodypart
        this.uiGenerator.setSelectedChildBodyPartButton(this);  // set selected ui button

        // set marker 
        const colorButton = this.uiGenerator.getSelectedColorButton();
        if (!colorButton.isMarked) {
            colorButton.setMarked(true);
        }


        this.uiGenerator.hideFloatColorSlider();
        
        //this.bodyPartsManager.setLinkedByColorSprites();

        //this.uiGenerator.setMaterialButtons(this.atlasFront);
        this.uiGenerator.setColorsToButtons();
        
        this.bodyPartsManager.setMaterial(this.bodyPartsManager.selectedBodyPart.getParnetBodyPart().bodyPartIndex);
        
        // if (checkIfContainsBodypart(this.bodyPartsManager.hiddenBodyParts, this.bodyPart)) {
        //     // this.bodyPartsManager.showBodyParts();
        // }
    }

    resetColorForBlinking() {
        //set initilal values Edited by SY
        this.blinkProgress = this.blinkIncrement;
        this.blinkRepeatNum = 0;
        this.blinkCurColor = this.color;

        this.blinkInitialColors();

        this.bodyPart.initColor();

        //setTimeout(()=>{this.stopBlinkWorking()}, 3000);
    }

    // Created funtion by SY
    // blinkInitialColors(){
    //     let s_v = 0.5, r_v= 0.5;
    //     let hsv = Phaser.Display.Color.RGBToHSV(this.blinkCurColor.red, this.blinkCurColor.green, this.blinkCurColor.blue);

    //     //V
    //     if(hsv.v + 0.25 > 1)
    //     {
    //         s_v = 1;
    //         r_v = 0.5;
    //     }
    //     else if(hsv.v - 0.25< 0)
    //     {
    //         s_v= 0;
    //         r_v = 0.5;
    //     }
    //     else
    //     {
    //         s_v = hsv.v + 0.25;
    //         r_v = hsv.v - 0.25;
    //     }




    //     this.blinkStartColor = new Phaser.Display.Color();
    //     this.blinkreqColor = new Phaser.Display.Color();

    //     Phaser.Display.Color.HSVToRGB(hsv.h, hsv.s, s_v, this.blinkStartColor);
    //     Phaser.Display.Color.HSVToRGB(hsv.h, hsv.s, r_v, this.blinkreqColor);

    // }
    blinkInitialColors(){
        if(this.blinkCurColor == undefined) this.blinkCurColor = new Phaser.Display.Color();
        let r_v= 0.5;
        let hsv = Phaser.Display.Color.RGBToHSV(this.blinkCurColor.red, this.blinkCurColor.green, this.blinkCurColor.blue);

        //V
        if(hsv.v + 0.5 > 1)
        {          
            r_v = 0;
        }
        else if(hsv.v - 0.5< 0)
        { 
            r_v = 1;
        }
        else
        {
            r_v = 1;
        }

        this.blinkStartColor = new Phaser.Display.Color();
        this.blinkreqColor = new Phaser.Display.Color();

        Phaser.Display.Color.HSVToRGB(hsv.h, hsv.s, hsv.v, this.blinkStartColor);
        Phaser.Display.Color.HSVToRGB(hsv.h, hsv.s, r_v, this.blinkreqColor);

    }


    setColor(color:Phaser.Display.Color) {
   
        this.color = color;
        this.image.tint = Phaser.Display.Color.GetColor(color.red, color.green, color.blue);
        this.resetColorForBlinking();
    }


    blink(callback:()=>void) {

        if(this.blinkRepeatNum > this.blinkRepeatLimit)
        {            
            this.bodyPart.setBodyPartColor(this.blinkCurColor);
            callback();
            return;
        }else{

            if(this.blinkProgress < 100){
                var colorObject = Phaser.Display.Color.Interpolate.ColorWithColor(this.blinkStartColor, this.blinkreqColor, 100, this.blinkProgress);
                this.bodyPart.setBodyPartColor(Phaser.Display.Color.ObjectToColor(colorObject));
                this.blinkProgress += this.blinkIncrement;
            }else{
                var temp = this.blinkStartColor;
                this.blinkStartColor = this.blinkreqColor;
                this.blinkreqColor = temp;
                this.blinkProgress = this.blinkIncrement;
                this.blinkRepeatNum += 1;
            }

        }
    }



    getDisplayHeight(){
        return this.image.displayHeight;
    }

    getDisplayWidth(){
        return this.image.displayWidth;
    }

    
}
