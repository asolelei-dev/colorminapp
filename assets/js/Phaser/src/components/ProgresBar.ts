class ProgressBar{
    backgroundSlider:Phaser.GameObjects.Image
    color:number
    percent:number
    scene:ModelingScene
    progress: Phaser.GameObjects.Graphics;
    stack:Phaser.GameObjects.Container;
    height:number;
    width:number;
    text:Phaser.GameObjects.Text;
    constructor(scene:ModelingScene, backgroundTexture:string, color:number){
        this.scene = scene;
        this.backgroundSlider = this.scene.add.image(0, 0, backgroundTexture);
        this.backgroundSlider.setScale(LayoutContants.getInstance().SCALE);
        const width = this.backgroundSlider.displayWidth - 5;
        const height = this.backgroundSlider.displayHeight - 5;
        this.width = width;
        this.height = height;
        const x = this.scene.uiGenerator.backgroundBoxTopper.displayWidth/2;
        const y =  +this.scene.game.config.height - CST.LAYOUT.BUTTONS_SIZE_BASE/LayoutContants.getInstance().SCREEN_HEIGHT_COEF/2;
        this.stack = this.scene.add.container(x, y);
        this.color = color;
        this.percent = 0;
        this.progress = this.scene.add.graphics();
        this.progress.fillStyle(color);
        var indicator = this.scene.add.image(-this.backgroundSlider.displayWidth/2 - 50, 0, "AutoOff");
        indicator.setScale(LayoutContants.getInstance().SCALE);
        
        this.text = this.scene.add.text(0,0,"loading 3D");
        this.text.setColor("0x000000");
        this.text.setOrigin(0.5, 0.5);
        this.text.setFontFamily("Nevis");

        this.stack.add(indicator);
        this.stack.add(this.backgroundSlider)
        this.stack.add(this.progress)
        this.stack.add(this.text);
        this.stack.setDepth(2);
        this.setPercent(0)
    }

    setPercent(percent:number){
        this.percent = percent;
        var newwidth = this.width * this.percent;
        if(newwidth < this.height) newwidth = this.height;
        this.progress.fillRoundedRect(-this.width/2, -this.height/2, newwidth, this.height, this.height/2)
    }

    increasePercent(){
        const dp = (1 - this.percent) * 0.1;
        const percent = this.percent + dp;
        this.setPercent(percent);
    }

    setVisible(flag:boolean){
        this.stack.setVisible(flag);
    }

}