const BACKGROUND = "Background";
const FADE = "Fade";
const SHADOW = "Shadow";
const NOTHING = "tmp_Nada";

class BodyPart {
    scene: ModelingScene;
    bodyPartsManager: BodyPartsManager;
    modelPart: ModelPart;
    bodyPartIndex: number;
    material:Material;
    childBodyPart:BodyPart; // child object
    claySprites:BodySprite[]; 
    glossSprites:BodySprite[];
    solidGreySprite:Phaser.GameObjects.Sprite;
    tMenuButton: TMenuButton;
    selectedTabIndex:integer = 0;
    childBodyPartButton: ChildBodyPartButton; // childBodyPartButton
    selectedChildBodyPartIndex:integer = -1;
    depend:boolean = true;

    isHidden:boolean;
    color:Phaser.Display.Color = new Phaser.Display.Color(255, 255, 255);
    colorButton:ColorButton;
    palleteNumber = 4;
    backgroundTexture:string;
    fadeTexture:string;
    dx:number = 0;
    dy:number = 0;
    colorModeNumber = 0;
    

    constructor(scene:ModelingScene, bodyPartsManager:BodyPartsManager, modelPart:ModelPart, bodyPartIndex:integer){
        this.scene = scene;
        this.bodyPartsManager = bodyPartsManager;
        this.modelPart = modelPart;
        this.bodyPartIndex = bodyPartIndex;
        this.material = modelPart.materials[0];
        this.claySprites = [];
        this.glossSprites = [];
        this.solidGreySprite = scene.uiGenerator.solidGreySprite;
       
    }
    getChildLink(){
        return this.modelPart.feature.colorLink;
    }
    setChildLink(colorLink){
        this.modelPart.feature.colorLink = colorLink;
    }
    getParnetBodyPart():BodyPart{
        let parentId = this.modelPart.feature.parent_id;
        var result:BodyPart[] = this.bodyPartsManager.bodyParts.filter(it => it.modelPart.feature.part_id == parentId);
        if(result.length > 0)
        {
            return result[0];
        }
    }

    setTMenuButton(){
        const buttonName = this.modelPart.feature.tMenuButton;
        if(buttonName != null && buttonName != ""){
            this.tMenuButton = new TMenuButton(this);
  
        }

    }

    setChildBodyPartButton(){
        const buttonName = this.modelPart.feature.tMenuButton;
        if(buttonName != null && buttonName != ""){
            this.childBodyPartButton = new ChildBodyPartButton(this);
        }
    }

    setMaterial(tabIndex:integer){

        this.material = this.modelPart.materials[tabIndex];
        this.selectedTabIndex = tabIndex;

        // if (game.isSavesSlotsScreen()) {
        //     setSpriteFromMaterial(game.getFirstFrame());
        //     setColor();
        //     return;
        // }

        for (var frame = this.scene.minFrame; frame < this.scene.maxFrame + 1; frame++) {
            this.setSpriteFromMaterial(frame);
            this.initColor();
        }
    }

    setChildBodyPart(childBodyPartIndex:integer){

        this.selectedChildBodyPartIndex = childBodyPartIndex;
        var results = this.bodyPartsManager.bodyParts.filter(it => it.modelPart.feature.part_id == childBodyPartIndex);
        if(results.length > 0){
            this.childBodyPart = results[0];
        }       
    }

    isBackground(){
        return this.modelPart.partName.includes("Background");
    }


    setSpriteFromMaterial(frame:integer) {
        if (this.material == null) {
            return;
        }

        if (this.material.clayRegion != null && this.material.clayRegion != "") {
            this.setClaySpriteFromMaterial(frame);
        }

        if (this.material.glossRegion != null && this.material.glossRegion != "") {
            this.setGlossSpriteFromMaterial(frame);
        }

        if (this.material.glossAlpha != null) {
            const glossAlpha = this.material.glossAlpha;
            if (this.glossSprites[frame] != null) {
                this.glossSprites[frame].setAlpha(glossAlpha);
            }
        }
    }

    updateBodyPart(frame:integer){

        const clayRegion = `${this.material.clayRegion}${frame}`;
        this.bodyPartsManager.updateSprite(clayRegion, this.modelPart, this.claySprites[frame]);

        const glossRegion = `${this.material.glossRegion}${frame}`;
        this.bodyPartsManager.updateSprite(glossRegion, this.modelPart, this.glossSprites[frame]);
        
    }

    setClaySpriteFromMaterial(frame:integer){

        // if (this.material.clayRegion.includes(NOTHING)) {
        //     if(this.claySprites[frame]){
        //         this.claySprites[frame].destroy();
        //         this.claySprites[frame] = null;
        //     }
        //     return;
        // }

        
        const clayRegion = `${this.material.clayRegion}${frame}`;
        this.claySprites[frame] = this.bodyPartsManager.setSprite(clayRegion, this.modelPart, this.claySprites[frame], frame);
        if(this.claySprites[frame]){
            if(this.modelPart.feature.isChildren)
            {
                // 
                this.claySprites[frame].linkChildBodyPartButtonIndex(this.childBodyPartButton.bodyPart.modelPart.feature.part_id);                 
            }else{
                this.claySprites[frame].linkTmenuButtonIndex(this.tMenuButton.bodyPart.modelPart.tMenuOrder);
            }
            
            this.claySprites[frame].setFeature(this.modelPart.feature);
            this.claySprites[frame].makePixcelPerfectTouchEvent();
        }
    }

    setGlossSpriteFromMaterial(frame:integer){
        
        // if (this.material.glossRegion.includes(NOTHING)) {
        //     if(this.glossSprites[frame]){
        //         this.glossSprites[frame].destroy();
        //         this.glossSprites[frame] = null;
        //     }
        //     return;
        // }
        
        if (this.material.glossRegion.includes(FADE)) {
            const newFadeImage = `${this.material.glossRegion}`;
          
            this.scene.uiGenerator.setBackground(this.bodyPartsManager.backgroundTexture, newFadeImage);
            return;
        }
        
        const glossRegion = `${this.material.glossRegion}${frame}`;
        
        this.glossSprites[frame] = this.bodyPartsManager.setSprite(glossRegion, this.modelPart, this.glossSprites[frame], frame);
        
    }

    showSprites() {
        this.claySprites.forEach(element => {
            if(element != null){
                element.visible = true;
            }
        });
        this.isHidden = false;
    }

    hideSprites(){

        this.claySprites.forEach(element => {
            if(element != null){
                element.setVisible(false);
            }
            
        });

        this.glossSprites.forEach(element => {
            if(element != null){
                element.setVisible(false);
            }
        });
        this.isHidden = true;
    }

    setColor(colorButton:ColorButton) {
        this.colorButton = colorButton;
        this.color = colorButton.color;
        this.setSelectorColor(this.color);
    }

    setSelectorColor(color:Phaser.Display.Color){
       
        this.color = color;
        if(this.isChild())
        {
            if(this.childBodyPartButton != null){
                this.childBodyPartButton.setColor(color);
            }

        }
        else{
            if(this.tMenuButton != null){
                this.tMenuButton.setColor(color);
            }
        }

        this.initColor();
    }

    initColor() {
        this.claySprites.forEach(element => {
            
            if(element != undefined){
                element.tint = Phaser.Display.Color.GetColor(this.color.red, this.color.green, this.color.blue);
            }
        });
    }

    setSprites(frame:integer) {
        if (this.material != null) {
            this.setSpriteFromMaterial(frame);
            // this.setSpriteFromMaterial(frame, false);
        }

        if (this.modelPart.partName.includes(BACKGROUND)) {
            this.backgroundTexture = this.bodyPartsManager.getBackgroundTexture();
            this.fadeTexture = this.bodyPartsManager.getFadeTexture();
            this.scene.uiGenerator.setBackground(this.backgroundTexture, this.fadeTexture);
            // (ScreenManager.getNewScreen()).setBackgroundCoordinates(backgroundSprite, BACKGROUND_SPRITE);
            // (ScreenManager.getNewScreen()).setBackgroundCoordinates(fadeSprite, FADE_SPRITE);
            return;
        }

        if (this.modelPart.partName.includes(SHADOW)) {
            // const glossRegionName = `${this.modelPart.partGlossImage}${frame}`;
            // this.glossSprites[frame] = this.bodyPartsManager.setSprite(glossRegionName, this.modelPart, this.glossSprites[frame]);
            return;
        }

        if (this.modelPart.partBaseImage != null) {
            this.setRegularSprites(frame);
            this.checkPartToggle(frame);

        }
    }

    setRegularSprites(frame:integer) {
        if (!this.modelPart.partBaseImage.includes(NOTHING)) {
            if(this.modelPart.partBaseImage != ""){
                const clayRegionName = `${this.modelPart.partBaseImage}${frame}`;
                this.claySprites[frame] = this.bodyPartsManager.setSprite(clayRegionName, this.modelPart, this.claySprites[frame], frame);    
            }
        }

        if (this.modelPart.partBaseImage != null) {
            if (!this.modelPart.partGlossImage.includes(NOTHING) && !this.modelPart.partGlossImage.includes(BACKGROUND)) {
                if(this.modelPart.partGlossImage != ""){
                    const glossRegionName = `${this.modelPart.partGlossImage}${frame}`;
                    this.glossSprites[frame] = this.bodyPartsManager.setSprite(glossRegionName, this.modelPart, this.glossSprites[frame], frame);
                }
            }
        }
        
    }

    checkPartToggle(frame:integer) {
        const partToggle = this.modelPart.feature.partToggle;
        if (partToggle == null) {
            return;
        }

        if (frame == this.scene.firstFrame) {
            if (!partToggle.includes("1")) {
                // this.hideSprites();
            }else{
                this.showSprites();
            }
            return;
        }

        if (this.isHidden) {
            this.hideSprites();
            this.bodyPartsManager.hiddenBodyParts.push(this);
        }else{
            this.showSprites();
            this.bodyPartsManager.shownBodyParts.push(this);
        }
    }

    setPalleteNumber(palleteNumber:number){
        
        if(palleteNumber < 9){
            this.palleteNumber = palleteNumber;
        }else{
            this.colorModeNumber = palleteNumber;

        }
        
    }

    setInitialPalletNumber(palleteNumber:number){
        if(palleteNumber < 9){
            this.palleteNumber = palleteNumber;
            this.colorModeNumber = 0;
        }else{
            this.palleteNumber = 4;
            this.colorModeNumber = palleteNumber;

        }
    }

    render(frame:integer){
        
        // console.log(frame)
        // if(this.scene.isFileLoaded){
        //     this.hideSprites();
        // }

        this.hideSprites();

        // if (this.fadeTexture != null) {
        //     if(!this.scene.blinkingEnabled){
        //         this.bodyPartsManager.uiGenerator.backgroundSprite.tint = Phaser.Display.Color.GetColor(this.color.red, this.color.green, this.color.blue);
        //         this.bodyPartsManager.uiGenerator.solidGreySprite.tint = Phaser.Display.Color.GetColor(this.color.red, this.color.green, this.color.blue);
        //     }
        // }

        if (this.glossSprites[frame] != null) {
            const element = this.glossSprites[frame];
            
            element.setDepth(21);
            element.setVisible(true);
        }

        if (this.claySprites[frame] != null) {
            const element = this.claySprites[frame];
            //console.log("bodypart.render", element);
            element.setDepth(20);
            element.setVisible(true);
            if(!this.scene.blinkingEnabled){
                element.tint = Phaser.Display.Color.GetColor(this.color.red, this.color.green, this.color.blue);
            }
        }

    }

    // Edited by SY
    setBodyPartColor(color:Phaser.Display.Color) {//called when blinking
        
        if (this.backgroundTexture != null) {
            
            this.bodyPartsManager.uiGenerator.backgroundSprite.tint = Phaser.Display.Color.GetColor(color.red, color.green, color.blue);
            this.bodyPartsManager.uiGenerator.solidGreySprite.tint = Phaser.Display.Color.GetColor(color.red, color.green, color.blue);
            // this.bodyPartsManager.uiGenerator.fadeSprite.tint = Phaser.Display.Color.GetColor(color.red, color.green, color.blue);
            
            return;
        }
    
        this.claySprites.forEach(element => {
            if(element != undefined){
                
                element.tint = Phaser.Display.Color.GetColor(color.red, color.green, color.blue);
            }
        });
    }

    setScale(scale:number, frame:integer){

        if (this.claySprites[frame] != null) {
            const element = this.claySprites[frame];
            // element.setScale(2);
        }

        if (this.glossSprites[frame] != null) {
            const element = this.glossSprites[frame];
        }

    }

    setColorMode(colormode:integer){
        this.colorModeNumber = colormode;

    }

    removeMarkInColorButton(){
        if(this.colorButton){
            this.colorButton.removeMark();
        }
    }

    makeMarkInColorButton(){
        if(this.colorButton){
            this.colorButton.makeMark();
        }
    }

    checkPixelPerfectPos(x, y, scale, frame):BodySprite{
        if(this.claySprites[frame]){
            return this.claySprites[frame].checkPixelPerfectPos(x, y, scale);
        }
    }

    hasParent(parent:BodyPart):boolean{
        if(parent.modelPart.feature.part_id == this.modelPart.feature.parent_id)
        {
            return true;
        }
        return false;
    }

    hasSibling(sibling:BodyPart):boolean{
        if(sibling.modelPart.feature.parent_id == this.modelPart.feature.parent_id)
        {
            return true;
        }
        return false;
    }

    isParent(){
        return this.modelPart.feature.parent_id == 0?true:false;
    }

    isChild(){
        return this.modelPart.feature.parent_id != 0?true:false;
    }

    getPartId(){
        return this.modelPart.feature.part_id;
    }

    getParentId(){
        return this.modelPart.feature.parent_id;
    }

    getChildBodyParts():BodyPart[]{
        let childBPs = [];
        let parentId = this.getPartId();
        if(this.isParent()){
         
            childBPs = this.scene.bodyPartsManager.bodyParts.filter(it => it.getParentId() == parentId);

        }
        return childBPs;
    }


}