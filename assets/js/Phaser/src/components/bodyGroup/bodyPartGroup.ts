class BodyPartGroup extends Phaser.GameObjects.Container {
    clayPartsArr:MyBodyPart[] = [];
    glossPartsArr:MyBodyPart[] = [];
    currentIndex:integer;
    bodyPartIndex:integer;
    constructor(scene:Phaser.Scene, parts_arr:ModelPart[], modelName:string ){
        super(scene, 0, 0);
        scene.add.existing(this);
        this.scene = scene;
        this.currentIndex = 1;
        this.initBodyPart(parts_arr, modelName);

    }

    initBodyPart(parts_arr:ModelPart[], modelName:string){
        var average_X = 0;
        var average_Y = 0;
        var total_X = 0;
        var total_Y = 0;
        var totals = 0;
        var self = this;
        
        parts_arr.forEach(element => {
            if(!element.partName.includes("Shadow")){
                totals += 1;
                total_X += +element.partPosX;
                total_Y += +element.partPosY;
            }
        });

        average_X = total_X / totals;
        average_Y = total_Y / totals;


        parts_arr.forEach(element => {
            if(!element.partName.includes("Shadow") && !element.partName.includes("Background")){
                if(!element.partBaseImage.includes("tmp_Nada")){
                    const clay_bodyPart = new MyBodyPart(this.scene, element, modelName, average_X, average_Y);
                    const gloss_bodyPart = new MyBodyPart(this.scene, element, modelName, average_X, average_Y);
                    this.add(clay_bodyPart);
                    this.add(gloss_bodyPart);
                    this.clayPartsArr.push(clay_bodyPart);
                    this.glossPartsArr.push(gloss_bodyPart);    
                }
            }
        });

        this.setFrameIndex(this.currentIndex);

    }

    setFrameIndex(frameindex:number){
        this.clayPartsArr.forEach(element => {
            element.setFramIndex(frameindex);
        });
        this.glossPartsArr.forEach(element => {
            element.setFramIndex(frameindex);
        });
    }

    moveLeft(){
        const newIndex = this.currentIndex + 1;
        this.currentIndex = Math.abs(newIndex % 29);
        if(this.currentIndex == 0){
            this.currentIndex = 1;
        }
        this.setFrameIndex(this.currentIndex);
    }

    moveRight(){
        const newIndex = this.currentIndex - 1;
        this.currentIndex = Math.abs(newIndex % 29);

        if(this.currentIndex == 0){
            this.currentIndex = 28;
        }
        this.setFrameIndex(this.currentIndex);
    }

}