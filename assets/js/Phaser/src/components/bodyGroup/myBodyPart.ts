
class MyBodyPart extends Phaser.GameObjects.Sprite {
    modelPart:ModelPart;
    modelName:String;
    startFrame:integer;
    rendorMode:integer;
    currentFrameIndex:integer;
    bodyPartIndex:integer;
    constructor(scene: Phaser.Scene, modelPart:ModelPart, modelName:string, averageX:number, averageY:number){
        const SCREEN_SIZE_COEF = 1;
        const xSize =  +modelPart.partSizeX/SCREEN_SIZE_COEF;
        const ySize =  +modelPart.partSizeY/SCREEN_SIZE_COEF
        const xPos = +scene.game.config.width - (averageX - +modelPart.partPosX/SCREEN_SIZE_COEF + +scene.game.config.width/2);
        const yPos = averageY - +modelPart.partPosY/SCREEN_SIZE_COEF + +scene.game.config.height/2;
        const x = xPos;
        const y = yPos;
        super(scene, x, y, modelName, `${modelPart.partBaseImage}${1}.png`);
        this.tint = 0x4287f5;
        this.currentFrameIndex = 1;
        this.scene = scene;
        this.modelPart = modelPart;
        this.modelName = modelName;
        this.displayHeight = ySize;
        this.displayWidth = xSize;
        this.startFrame = 0;

        scene.sys.updateList.add(this);
        scene.sys.displayList.add(this);


        //this.animate(scene);

        var self = this;

        scene.events.on("stop-animation", ()=>{
            this.startFrame = 10;
            this.anims.pause();
        });

        scene.events.on("start-animation", ()=>{
            
            this.anims.resume();
        });
    };

    animate(scene: Phaser.Scene){
        
        // const hair = this.add.sprite(100, 100, "model", "08Hair_Clay1.1.png");
        // hair.tint = 0x000000;

        // this.anims.create({
        //     key:'rotate-hair',
        //     repeat:-1,
        //     frameRate:10,
        //     frames: this.anims.generateFrameNames('model', {
        //         prefix:'08Hair_Clay1.',
        //         suffix:'.png',
        //         start:1,
        //         end:29,
        //         zeroPad:1
        //     }),
        // });
        // hair.play("rotate-hair");
        

        if(this.modelPart.partName.includes("Background")){
            return;
        }

        
        scene.anims.create({
            key:`${this.modelPart.partName}`,
            repeat:-1,
            frameRate:2,
            frames: scene.anims.generateFrameNames(`${this.modelName}`, {
                prefix:`${this.modelPart.partBaseImage}`,
                suffix:'.png',
                start:this.startFrame,
                end:29,
                zeroPad:1
            }),
        });

        

        this.play(`${this.modelPart.partName}`);
        

        this.on('animationcomplete', (animation, frame)=>{

        }, this);
        this.on('animationupdate', (animation, frame)=>{
            this.startFrame = 0;
        }, this);
    }

    setFramIndex(index:integer){
        this.currentFrameIndex = 1;
        if(index == CST.FRONT_FRAME){
            this.setTexture(`${this.modelName}-front`,`${this.modelPart.partBaseImage}${index}.png`);
        }else{
            this.setTexture(`${this.modelName}`,`${this.modelPart.partBaseImage}${index}.png`);
        }
        
    }

}