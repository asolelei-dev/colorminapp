
class ColorButton {
    SECOND:integer = 1000;
    uiGenerator:UIGenerator;
    stack:Phaser.GameObjects.Container;
    colorButtonClayTexture:string;
    colorButtonGlossTexture:string;
    color:Phaser.Display.Color = new Phaser.Display.Color(255, 255, 255);
    mainSelectorColor:Phaser.Display.Color = new Phaser.Display.Color(255, 255, 255);
    isSelected:boolean;
    isMarked:boolean;
    buttonIndex:integer;
    colorSelectorTexture:string;
    clayImage:Phaser.GameObjects.Image;
    glossImage:Phaser.GameObjects.Image;
    colorSelector:Phaser.GameObjects.Image;
    bodyPartsManager:BodyPartsManager;
    // timer:TimerHandler;
    coloredBodyPartsCount:integer[];
    scene:ModelingScene;
    isPressed:boolean;
    globalX: number;
    globalY: number;
    colorModeNumber: number; //default 0
    depth: number;
    material: Material;

    constructor(scene:ModelingScene, buttonIndex:integer){
        this.scene = scene;
        this.uiGenerator = scene.uiGenerator;
        this.buttonIndex = buttonIndex;
        this.bodyPartsManager = scene.bodyPartsManager;
        this.stack = new Phaser.GameObjects.Container(scene);
        this.colorSelector = new Phaser.GameObjects.Image(scene, 0,0, "SelectHoop");
        this.coloredBodyPartsCount = [];
        this.depth = 30;
    }

    setImages(colorButtonMaterial:Material){

        this.material = colorButtonMaterial;

        this.setImage(colorButtonMaterial);
        this.setGlossImage(colorButtonMaterial);
       
        this.resetStack();
    }
    
    setImage(colorbuttonMaterial:Material){
        const atlasFront = this.scene.getHDAtalasFront();
        this.clayImage = new Phaser.GameObjects.Image(this.scene, 0,0,`${atlasFront}`, `${colorbuttonMaterial.clayButtonRegion}.png`);
        // this.clayImage.texture.setFilter(Phaser.Textures.FilterMode.LINEAR);
        //this.clayImage.setDepth(21);
        this.clayImage.setScale(LayoutContants.getInstance().BUTTONS_SIZE/this.clayImage.displayWidth);
        this.setColor(this.color, this.colorModeNumber);
    }

    setGlossImage(colorbuttonMaterial:Material){
        const atlasFront = this.scene.getHDAtalasFront();
        this.glossImage = new Phaser.GameObjects.Image(this.scene, 0,0,`${atlasFront}`, `${colorbuttonMaterial.glossButtonRegion}.png`);
        //this.glossImage.setDepth(20);
        // this.glossImage.texture.setFilter(Phaser.Textures.FilterMode.LINEAR);
        this.glossImage.setScale(LayoutContants.getInstance().BUTTONS_SIZE/this.glossImage.displayWidth);
        
        this.glossImage.setAlpha(+colorbuttonMaterial.glossButtonAlpha);
    }

    resetStack(){
        this.stack.removeAll();
        this.stack.add(this.clayImage);
        this.stack.add(this.glossImage);
        if (this.isMarked) {
            this.addMainSelector();
        }
        
    }

    setImagesStack(){        
        if(this.isMarked){
            this.addMainSelector();
        }
    }

    addMainSelector(){

        this.setMainSelectorColor();

        let _r = Math.min(255, this.color.red + 150);
        let _g = Math.min(255, this.color.green + 150);
        let _b = Math.min(255, this.color.blue + 150);

        

        this.colorSelector.setTint(Phaser.Display.Color.GetColor(_r, _g, _b));
        this.colorSelector.setScale(LayoutContants.getInstance().MAIN_COLOR_SELECTION_SCALE);
        this.stack.add(this.colorSelector);
    }

    removeMark(){
        this.colorSelector.setVisible(false)
    } 

    makeMark(){


        let _r = Math.min(255, this.color.red + 150);
        let _g = Math.min(255, this.color.green + 150);
        let _b = Math.min(255, this.color.blue + 150);

        this.colorSelector.setTint(Phaser.Display.Color.GetColor(_r, _g, _b));

        this.colorSelector.setVisible(true)        
    }

    performSelection(isUndo:Boolean = false){
        this.scene.stopBlinking(); // stop blinking effect of bodypart

        this.deselectCurrentlySelectedColorButton(); //setting selected status
        this.setSelected(true); // set selected flag var as true

        let sprite = this.bodyPartsManager.selectedBodyPart.claySprites[this.scene.firstFrame];

        if(!this.bodyPartsManager.selectedBodyPart.isBackground()){

            if(sprite == undefined){
                this.bodyPartsManager.setMaterial(1);
            }else if(sprite != undefined){
                if(sprite.name == "empty") {
                    if(!isUndo){
                        this.bodyPartsManager.setMaterial(1);
                    }
                    else {
                        this.bodyPartsManager.setMaterial(0);
                    }
                }
            }
        }
        

        this.uiGenerator.setColorSelection(this.buttonIndex)

        if(this.bodyPartsManager.selectedBodyPart.isParent()) // if has childs, change them together
            this.uiGenerator.setChildLinkedColor();
    }




    
    setMarked(isMarked:boolean) {

        this.isMarked = isMarked;

        if (isMarked) {
            this.addMainSelector();
            return;
        }

        this.removeMainSelector();
    }

    deselectCurrentlySelectedColorButton(){
        const selectedColorButton = this.uiGenerator.getSelectedColorButton();
        selectedColorButton.removeFromColoredBodyPartsCount();
    }
    addToColoredBodyPartsCount(){
        this.coloredBodyPartsCount.push(1);
        this.setMarked(true);
    }

    removeFromColoredBodyPartsCount(){
        if (this.coloredBodyPartsCount.length != 0) {
            this.coloredBodyPartsCount.pop();
        }
        if (this.coloredBodyPartsCount.length == 0) {
            this.setMarked(false);
        }
    }

    removeMainSelector() {
        this.stack.remove(this.colorSelector);
    }

    setMainSelectorColor(){
        this.mainSelectorColor = new Phaser.Display.Color(this.color.red, this.color.green, this.color.blue, this.color.alpha)

    }

    setColor(color:Phaser.Display.Color, colorModeNumber:integer){
        this.colorModeNumber = colorModeNumber;
        this.color = color;
        this.clayImage.tint = Phaser.Display.Color.GetColor32(color.red, color.green, color.blue, color.alpha);
    }
    

    setSelected(selected:boolean) {
        this.isSelected = selected;
    }

    onTapButton(){
        
        this.performSelection();
        
        this.scene.callColorPoofer();
        this.uiGenerator.createUserAction();
        this.scene.playSound(CST.SOUND.COLOR_CHANGE_SOUND);
      
    }

    onPressButton(start:boolean){
        if(start){
            this.isPressed = true
            this.scene.time.addEvent({
                delay: 500,
                callback: ()=>{
                    // spawn a new apple
                    if(this.isPressed){
                        // console.log("press start"+start);        //this.startMainScene();
                    }
                },
                loop: false
            })
        }else{
            // if (!uiGenerator.getFloatColorSlider().isVisible() && x < 0 && y < 0) {
            //     isPressed = false;
            //     return;
            // }

            this.isPressed = false;
            this.performSelection();
            this.scene.callColorPoofer();
            this.uiGenerator.createUserAction();
            this.scene.stopBlinking();
        }

    }

    setPosition(x:number, y:number){
        this.globalX = x;
        this.globalY = y;
    }


}