class DiceScreenTopBar {
    scene:ModelingScene;
    backButton:CustomImageButton;
    shopingCartButton:CustomImageButton;
    text:Phaser.GameObjects.Text;
    priceText:Phaser.GameObjects.Text;
    backgroundImage:Phaser.GameObjects.Image;
    stack:Phaser.GameObjects.Container;
    blueBG:Phaser.GameObjects.Graphics;
    grayBG:Phaser.GameObjects.Graphics;
    shopImage: Phaser.GameObjects.Image;
    
    constructor(scene:ModelingScene, x:number, y:number, text:string, price:string){
        this.scene = scene;
        this.stack = this.scene.add.container(x, y);
        this.backButton = new CustomImageButton(this.scene, 0, 10*LayoutContants.getInstance().HEIGHT_SCALE,  "back_button", ()=>{
            this.scene.goSloteScene();
            this.scene.playSound(CST.SOUND.COLOR_GENERAL_SOUND);
        });
        this.backButton.setTopLeftOrigin();

        this.shopingCartButton = new CustomImageButton(this.scene, LayoutContants.getInstance().SCREEN_WIDTH, 0,  "StoreButton", ()=>{
            this.scene.playSound(CST.SOUND.COLOR_GENERAL_SOUND);
            if(this.scene.model.shopID == ""){
                location.href = this.scene.model.shopLink;
            }else{
                const jsonData = Global.getInstance().getSavedModel().getJSONData();
                var event = new CustomEvent('web.start.shoping-cart', { detail: {modelCharacteristics:this.scene.getModelCharacteristics(), savedModel:jsonData}});
                document.dispatchEvent(event);
            }
        });
        this.shopingCartButton.setTopRightOrigin();
        this.shopingCartButton.setScale(1.3);

        this.backgroundImage = this.scene.add.image(0, 0, "TMenu_Back");
        this.backgroundImage.setOrigin(0,0);
        this.backgroundImage.setScale(LayoutContants.getInstance().HEIGHT_SCALE);
        this.backgroundImage.displayWidth = LayoutContants.getInstance().SCREEN_WIDTH;
        this.blueBG = this.scene.add.graphics();
        this.blueBG.fillStyle(0x007bff);
        const blueBGX = this.backButton.getDisplayWidth() + 5;
        const blueBGY = this.backgroundImage.displayHeight * 0.05;
        const blueBGW = LayoutContants.getInstance().SCREEN_WIDTH - 2 * blueBGX;
        const blueBGH = this.backgroundImage.displayHeight * 0.9;

        this.blueBG.fillRect(blueBGX, blueBGY, blueBGW, blueBGH);
        this.text = this.scene.add.text(0, 0, text);
        this.priceText = this.scene.add.text(0, 0, price);
        this.setTitle(text, price);

        this.initShopImage();

        this.stack.add(this.backgroundImage);
        this.stack.add(this.blueBG);
        this.stack.add(this.backButton.stack);
        this.stack.add(this.shopingCartButton.stack);
        this.stack.add(this.text);
        this.stack.add(this.priceText);
        this.stack.depth = 100;
    }

    initShopImage(){
        if(!this.scene.model.shopSkip && this.scene.model.shopID == ""){
            this.grayBG = this.scene.add.graphics();
            this.grayBG.fillStyle(0xdddddd);
            this.grayBG.fillRect(0, 0,  LayoutContants.getInstance().SCREEN_WIDTH,  LayoutContants.getInstance().SCREEN_HEIGHT);
            this.shopImage = this.scene.add.image(0, this.backgroundImage.displayHeight, this.scene.model.shopImage);
            if(!LayoutContants.getInstance().isLandScape){
                this.shopImage.scale = LayoutContants.getInstance().SCREEN_WIDTH/this.shopImage.displayWidth;
            }else{
                this.shopImage.scale = (LayoutContants.getInstance().SCREEN_HEIGHT - this.backgroundImage.displayHeight)/this.shopImage.displayHeight;
                this.shopImage.x = LayoutContants.getInstance().SCREEN_WIDTH/2 - this.shopImage.displayWidth/2;
            }
            

            this.shopImage.setOrigin(0,0);
            this.stack.add(this.grayBG);
            this.stack.add(this.shopImage);
        }
    }

    setTitle(title:string, price:string){
        this.text.text = title;
        this.priceText.text = price;
        this.text.setColor("#ffffff");
        this.priceText.setColor("#ffffff");
        this.text.setFontSize(60 * LayoutContants.getInstance().SCALE);
        this.priceText.setFontSize(70* LayoutContants.getInstance().SCALE);
        this.text.setOrigin(1, 0.5);
        this.priceText.setOrigin(1, 0.5);
        this.text.setFontFamily("Nevis");
        this.priceText.setFontFamily("Nevis");
        this.priceText.x = this.backgroundImage.displayWidth - 1.3 * this.shopingCartButton.getDisplayWidth();
        this.text.x = this.backgroundImage.displayWidth - 1.5 * this.shopingCartButton.getDisplayWidth() - this.priceText.displayWidth - 10;
        this.text.y = this.backgroundImage.displayHeight/2;
        this.priceText.y = this.backgroundImage.displayHeight/2;
        this.text.setAlign("right");
        this.priceText.setAlign("right");
    }

    setVisible(flag:boolean){
        this.stack.setVisible(flag);
    }


}