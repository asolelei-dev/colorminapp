class ColorProoferSprite extends Phaser.GameObjects.Sprite {
    scene:Phaser.Scene;

    constructor(scene: Phaser.Scene, texture:string, frame?: number|string){
        super(scene, 500, 500, texture, frame);
        scene.add.existing(this);
        this.scene = scene;
        // this.animate(scene);
        this.depth = 100;
        this.scale = 3;
        this.setVisible(false);
        this.scene = this.scene;
        this.scene.anims.create({
            key:'color-proof',
            repeat:0,
            frameRate:23,
            frames: this.scene.anims.generateFrameNames('proof', {
                prefix:'Poof.',
                suffix:'.png',
                start:1,
                end:23,
                zeroPad:1
            }),
        });

        this.on('animationcomplete', (animation, frame)=>{
            this.setVisible(false);
        }, this);
    };

    startColorProof(color:Phaser.Display.Color, posX:number, posY:number){
        this.tint = Phaser.Display.Color.GetColor(color.red, color.green, color.blue);
        this.setVisible(true);
        this.x = posX;
        this.y = posY;
        this.play("color-proof");
        
    }
};
