class CustomImageButton {
	callback:()=>void;
    scene: Phaser.Scene;
    stack:Phaser.GameObjects.Container;
    backGroundImage:Phaser.GameObjects.Image;
    text: Phaser.GameObjects.Text;
    backgroundTexture:string;
    highlightTexture:string;
	constructor(scene:Phaser.Scene, x:number, y:number, texture:string, callback:()=>void){
        this.scene = scene;
        this.stack = this.scene.add.container(x, y);
        
        this.backGroundImage = this.scene.add.image(0, 0, texture);
        this.text = this.scene.add.text(0,0,"");
        this.stack.add(this.backGroundImage);
        this.stack.add(this.text);
        this.stack.setScale(LayoutContants.getInstance().HEIGHT_SCALE);
        this.stack.depth = 500;
        this.callback = callback;
        this.backgroundTexture = texture;
        this.highlightTexture = texture;
        this.bindEvent();
    }

    getDisplayWidth(){
        return this.backGroundImage.displayWidth * LayoutContants.getInstance().HEIGHT_SCALE;
    }



    setTitle(title:string){
        this.text.text = title;
        this.text.setColor("0x000000");
        this.text.setFontSize(50);
        this.text.setOrigin(0.5, 0.5);
        this.text.setFontFamily("Nevis");
        if(title == "") this.stack.removeAll();
    }

    setHighLightTexture(texture:string){
        this.highlightTexture = texture;
    }

    bindEvent(){
        var isDown = false;

        this.backGroundImage.setInteractive().on('pointerdown', (pointer, localX, localY, event)=>{
            isDown = true;
            
        }).on('pointermove', (pointer, localX, localY, event)=>{

        }).on('pointerup', (pointer, localX, localY, event)=>{
            if(isDown){
                this.setStatus(true);
                this.callback();
                isDown = false;
            }
        }).on('pointerout', ()=>{
            if(isDown){
                isDown = false;
            }
        })
    }

    setStatus(selected:boolean){
        if(selected){
            this.backGroundImage.setTexture(this.highlightTexture);
        }else{
            this.backGroundImage.setTexture(this.backgroundTexture);
        }
    }

    setVisible(visible:boolean){
        this.stack.setVisible(visible);
    }

    setTopLeftOrigin(){
        this.backGroundImage.setOrigin(0,0);
        const dx = this.backGroundImage.displayWidth/2;
        const dy = this.backGroundImage.displayHeight/2;
        // this.stack.y += dy;
        // this.stack.x += dx;
    }

    setTopRightOrigin(){
        this.backGroundImage.setOrigin(1,0);
    }

    setRightOrigin(){
        this.backGroundImage.setOrigin(1,0.5);
    }

    //  * added by asset *

    setLeftOrigin(){
        this.backGroundImage.setOrigin(0,0.5);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    setScale(scale){
        const os = this.stack.scale;
        this.stack.setScale(os*scale);
    }

}