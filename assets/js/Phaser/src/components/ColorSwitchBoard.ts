class ColorSwitchBoard {

    scene:ModelingScene;
    stack:Phaser.GameObjects.Container;
    colorMode:integer;
    colorPalleteModeBG: Phaser.GameObjects.Image;
    DefaultColorModeBtn: ColorModeButton;
    CustomColorModeBtn: ColorModeButton;
    madeOff:boolean;
    callback:(integer)=>void;

    constructor(scene:ModelingScene, x:number, y:number, colormode:integer, madeOff:boolean, callback:(integer)=>void){
        this.scene = scene;
        this.colorMode = colormode;
        this.stack = this.scene.add.container(x, y);
        this.callback = callback;
        this.madeOff = madeOff;
        if(!madeOff){
            this.initElement();
        }
    }

    initElement(){
        //background
        this.colorPalleteModeBG = this.scene.add.image(300, 300, "ModeBG")
        this.colorPalleteModeBG.displayHeight = LayoutContants.getInstance().BUTTONS_SIZE * 1.2
        this.colorPalleteModeBG.displayWidth = LayoutContants.getInstance().BUTTONS_SIZE * 2.1
        this.colorPalleteModeBG.setPosition(LayoutContants.getInstance().SCREEN_WIDTH - LayoutContants.getInstance().BUTTONS_SIZE * 2.85, LayoutContants.getInstance().BUTTONS_SIZE * 1.2)
        this.colorPalleteModeBG.setOrigin(0,0)
        this.stack.add(this.colorPalleteModeBG)

        //default color button
        this.DefaultColorModeBtn = new ColorModeButton(this.scene, this.colorPalleteModeBG.x + this.colorPalleteModeBG.displayWidth * 0.29, this.colorPalleteModeBG.y + this.colorPalleteModeBG.displayHeight/2, "ModeRainbow","ModeGlow",()=>{
            this.callback(CST.COLOR_MODE.DEFAULT);
            this.CustomColorModeBtn.setStatus(false)
            this.DefaultColorModeBtn.setStatus(true)
        });
        this.DefaultColorModeBtn.setStatus(true)
        this.stack.add(this.DefaultColorModeBtn.stack)

        //custom color button
        this.CustomColorModeBtn = new ColorModeButton(this.scene, this.colorPalleteModeBG.x + this.colorPalleteModeBG.displayWidth * 0.78, this.colorPalleteModeBG.y + this.colorPalleteModeBG.displayHeight/2, "ModePalette","ModeGlow",()=>{
            this.callback(CST.COLOR_MODE.CUSTOME);
            this.DefaultColorModeBtn.setStatus(false)
            this.CustomColorModeBtn.setStatus(true)
        });
        this.stack.add(this.CustomColorModeBtn.stack)
    }

    getElementHeight(){
        if(!this.madeOff){
            return LayoutContants.getInstance().BUTTONS_SIZE * 1.2;
        }else{
            return 0;
        }
        
    }

}