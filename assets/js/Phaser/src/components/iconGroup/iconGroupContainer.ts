class IconGroupContainer extends Phaser.GameObjects.Container {

    modelButton:Phaser.GameObjects.Image;
    backgroundImage:Phaser.GameObjects.Image;
    model:Model;
    fileNames:FileNames;
    scrollMode:integer;

    constructor(scene: Phaser.Scene, width: number, height:number, scrollMode:integer, model?:Model){
        super(scene, 0, 0);
        scene.add.existing(this);
        this.scene = scene;
        this.model = model;
        this.scrollMode = scrollMode;
        this.fileNames = new FileNames().generateFileNames(model, true);
        this.initGroup(width, height, model);
    };

    initGroup(width:number, height:number,  model:Model){

        if(this.getSelectedModelArray().includes(this.model.iconName)){
            this.backgroundImage = new Phaser.GameObjects.Image(this.scene,0,0,'IAPicon_BGBlue');
        }else{
            this.backgroundImage = new Phaser.GameObjects.Image(this.scene,0,0,'IAPicon_BGGrey');
        }
        
        this.modelButton = new Phaser.GameObjects.Image(this.scene,0,0,model.iconName);
        if(this.scrollMode == 1){
            this.setScale(width/this.modelButton.width);
        }else{
            this.setScale(height/this.modelButton.height);
        }
        
        this.add(this.backgroundImage);
        this.add(this.modelButton);
        // this.modelButton.setInteractive().on('pointerdown',()=>{
        //     alert("model"+model.iconName);
        // })
    }

    updateBackground(flag:boolean){
        if(flag){
            this.backgroundImage.setTexture("IAPicon_BGBlue");
        }else{
            this.backgroundImage.setTexture("IAPicon_BGGrey");
        }
        //localStorage.setItem();
        if(!this.getSelectedModelArray().includes(this.model.iconName)){
            const new_selected_models_str = `${localStorage.getItem("selected_models")},${this.model.iconName}`;
            localStorage.setItem("selected_models", new_selected_models_str);
        }
    }

    getSelectedModelArray():string[]{
        const selected_models_str = localStorage.getItem("selected_models");
        if(selected_models_str){
            return selected_models_str.split(",");
        }else{
            return [];
        }
    }

};
