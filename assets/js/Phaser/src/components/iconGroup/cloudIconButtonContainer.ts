
// created by asset
class CloudIconButtonContainer {

    scene:Phaser.Scene;
    stack: Phaser.GameObjects.Container;
    board: Phaser.GameObjects.Container;
    origin: { x: number, y: number }
    size: { width: number, height: number }
    scale: { x: number, y: number };

    backgroundImage:Phaser.GameObjects.Image;
   
    downloadButton: CustomImageTextureButton;
    likeButton: CustomImageTextureButton;
    shareButton: CustomImageTextureButton;
    
    buttonSize: number;
    counter: { like:number, download: number};
    
    likeButtonTexture: string = "Button_CloudLike"
    LAYOUT_CONSTANT:LayoutContants = LayoutContants.getInstance();

    visibility: boolean;
    is_shared: boolean;
    setShareFlag: Function;
    likeIncrement: Function;
    downloadIncrement: Function;

    constructor(scene: Phaser.Scene, config: any){

        this.scene = scene;
        this.counter = {
            download: config.counter.download, 
            like: config.counter.like, 
        }
        this.origin = { x: config.x, y: 0 }
        this.scale = { x: 1, y: 1 }
        this.size = { width: config.width, height: config.height };
        this.buttonSize = 5;
        this.visibility = config.visibility;

        this.stack = scene.add.container(config.x, config.y);
        this.board = scene.add.container(config.x, config.y);
       

        // this.board.setPosition(config.x, config.y);
        // this.board.setDisplaySize(config.width, config.height);
        this.stack.depth = 499; // under the cloud button
        this.board.depth = 500; // under the cloud button
        // this.board.setScale( this.scale.x, this.scale.y);

        this.setShareFlag = config.setShareFlag
        this.likeIncrement = config.likeIncrement
        this.downloadIncrement = config.downloadIncrement
        this.is_shared = config.is_shared
        this.init()
    };

    init(){
        
        this.backgroundImage = this.scene.add.image(this.origin.x, this.origin.y, "solid-new");
        this.backgroundImage.setOrigin(0, 0)
        this.backgroundImage.setDisplaySize(this.size.width, this.size.height);
        // this.backgroundImage.setScale( this.scale.x, this.scale.y);
        this.stack.add(this.backgroundImage);

        var px = 50
       
        this.likeButton = new CustomImageTextureButton(
            this.scene, 
            this.board,
            this.size.width / 2, 
            this.size.height * 1 / 5, 
            185,
            200,
            this.likeButtonTexture, 
            this.counter.like, 
            ()=>{
                this.likeIncrement()
                if ( this.counter.like ) {
                    this.likeButton.setStatus(true)
                } else {
                    this.likeButton.setStatus(false)
                }
               
                console.log('click the like button', this.counter.like)
                this.likeButton.updateState(this.counter.like)
                this.setDataFromAPI();
            }
        );
        this.likeButton.setHighLightTexture("Button_CloudLike_Red")
        this.likeButton.setTopLeftOrigin(); 
        
               
           
        this.downloadButton = new CustomImageTextureButton(
            this.scene, 
            this.board,
            this.size.width / 2, 
            this.size.height * 3 / 5, 
            185,
            200,
            "Button_CloudDownload", 
            this.counter.download,
            ()=>{
                this.downloadIncrement()
                console.log('click the download button', this.counter.download)
                this.downloadButton.updateState(this.counter.download)
                this.setDataFromAPI();
            }
        );

        this.downloadButton.setTopLeftOrigin(); 
       

        this.shareButton = new CustomImageTextureButton(
            this.scene, 
            this.board,
            this.size.width / 2, 
            this.size.height * 3 / 7, 
            185,
            185,
            "Button_CloudShare", 
            "Share",
            ()=>{
                const res = this.setShareFlag();
                if (res) {
                    this.is_shared = true;
                    this.updateButton();
                }
            }
        );

        

        this.shareButton.setTopLeftOrigin(); 

        this.stack.add(this.board)
        this.board.setVisible(this.visibility)
    }

    updateButton(){
        console.log("this.is_shared", this.is_shared)
        this.shareButton.setVisible(!this.is_shared);
        this.downloadButton.setVisible(this.is_shared)
        this.likeButton.setVisible(this.is_shared)
        console.log('!!this.counter.like', !!this.counter.like)
        this.likeButton.setStatus(!!this.counter.like)
        this.likeButton.updateState(this.counter.like)
        this.downloadButton.updateState(this.counter.download)

    }
   
    setDataFromAPI(){
        console.log('call set-data-from-api function')
        // var event = new CustomEvent('setModelData', { detail: {model_name: this.model.modelName, counter: this.counter, callback:(res)=>{
            
        //     // we need to refresh the counter 
        //     // using call back data 

        //     this.apiData = res;
        //     this.update()
        // }}});
        // document.dispatchEvent(event);

    }
};
