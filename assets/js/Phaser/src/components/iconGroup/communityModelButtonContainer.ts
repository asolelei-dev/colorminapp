
// created by asset
class communityModelButtonContainer {

    scene:Phaser.Scene;
    stack: Phaser.GameObjects.Container;
    origin: { x: number, y: number }
    size: { width: number, height: number }
    scale: { x: number, y: number };
    backgroundImage:Phaser.GameObjects.Image;
    downloadButton: CustomImageTextureButton;
    likeButton: CustomImageTextureButton;
    buttonSize: number;
    counter: { like:number, download: number};
    likeButtonTexture: string = "Button_CloudLike"
    
    likeIncrement: Function;
    downloadIncrement: Function;

    constructor(scene: Phaser.Scene, config: any){

        this.scene = scene;
        this.counter = {
            download: config.counter.download, 
            like: config.counter.like, 
        }
        this.origin = { x: 0, y: config.y }
        this.scale = { x: 1, y: 1 }
        this.size = { width: config.width, height: config.height };
        this.buttonSize = 5;

        this.stack = scene.add.container(config.x, config.y);

        this.likeIncrement = config.likeIncrement
        this.downloadIncrement = config.downloadIncrement
    
        this.init()
    };

    init(){
        
        this.backgroundImage = this.scene.add.image(this.origin.x, this.origin.y, "solid-new");
        this.backgroundImage.setOrigin(0, 0)
        // this.backgroundImage.setDepth(1002)
        this.backgroundImage.setDisplaySize(this.size.width, this.size.height);
        // this.backgroundImage.setScale( this.scale.x, this.scale.y);
        
        this.stack.add(this.backgroundImage);
    

        const buttons = { width: 185, height: 200 };
        this.likeButton = new CustomImageTextureButton(
            this.scene, 
            this.stack,
            ( this.size.width - buttons.width ) / 2 , 
            this.size.height * 1 / 5, 
            buttons.width,
            buttons.height,
            this.likeButtonTexture, 
            this.counter.like, 
            ()=>{
                this.likeIncrement()
            }
        );
        this.likeButton.setHighLightTexture("Button_CloudLike_Red")
        this.likeButton.setTopLeftOrigin(); 
        this.likeButton.stack.setDepth(1002);
               
           
        this.downloadButton = new CustomImageTextureButton(
            this.scene, 
            this.stack,
            ( this.size.width - buttons.width ) / 2 , 
            this.size.height * 3 / 5, 
            buttons.width,
            buttons.height,
            "Button_CloudDownload", 
            this.counter.download,
            ()=>{
               
            }
        );
        this.downloadButton.stack.setDepth(1002);
        this.downloadButton.setTopLeftOrigin(); 
  
    }

    updateButton(){
      
        this.likeButton.setStatus(!!this.counter.like)
        this.likeButton.updateState(this.counter.like)
        this.downloadButton.updateState(this.counter.download)

    }
   
    setDataFromAPI(){
        console.log('call set-data-from-api function')

    }
};
