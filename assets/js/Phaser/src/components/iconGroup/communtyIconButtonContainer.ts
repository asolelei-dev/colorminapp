
// created by asset
class CommuntyIconButtonContainer {

    scene:Phaser.Scene;
    stack: Phaser.GameObjects.Container;
    board: Phaser.GameObjects.Container;
    origin: { x: number, y: number }
    size: { width: number, height: number }
    scale: { x: number, y: number };

    backgroundImage:Phaser.GameObjects.Image;

    downloadButton: CustomImageTextureButton;
    likeButton: CustomImageTextureButton;
    dateButton: CustomImageTextureButton;
    randomButton: CustomImageTextureButton;
    featureButton: CustomImageTextureButton;

    buttonSize: number;
    counter: { like:number, download: number, flag: boolean};
    likeButtonTexture: string = "Button_CloudLike"
    LAYOUT_CONSTANT:LayoutContants = LayoutContants.getInstance();

    visibility: boolean;
    selected: string;
    sortFlag: string;
    setSortFlag: Function;

    constructor(scene: Phaser.Scene, config: any){

        this.scene = scene;
        this.counter = {
            download: 0, 
            like: 0,
            flag: false
        }
        this.origin = { x: config.x, y: config.y - 20 }
        this.scale = { x: 0.6, y: 0.75 }
        this.size = { width: config.width, height: config.height };
        this.buttonSize = 5;
        this.visibility = config.visibility;
        this.selected = "";

        this.stack = scene.add.container(config.x, config.y);
        
        this.board = scene.add.container(config.x, config.y);
       
        
        // this.board.setPosition(config.x, config.y);
        // this.board.setDisplaySize(config.width, config.height);
        this.stack.depth = 499; // under the cloud button
        this.board.depth = 499; // under the cloud button
        
        this.sortFlag = config.sortFlag
        this.setSortFlag = config.setSortFlag;
        // this.board.setScale( this.scale.x, this.scale.y);
        this.init()
    };

    init(){
        
        this.backgroundImage = this.scene.add.image(this.origin.x, this.origin.y, "solid-new");
        this.backgroundImage.setOrigin(0, 0)
        this.backgroundImage.setDisplaySize(this.size.width, this.size.height);
        // this.backgroundImage.setScale( this.scale.x, this.scale.y);
        this.stack.add(this.backgroundImage);
        var px = 50
      
        // community button
        this.featureButton = new CustomImageTextureButton(
            this.scene, 
            this.board,
            this.size.width / 2 + px, 
            this.size.height * 2 / 30, 
            160,
            180,
            "Button_CloudFeatured", 
            this.sortFlag ==="feature"?"Sort by feature":"",
            ()=>{
                if (this.sortFlag !== "feature") { 
                   
                    this.featureButton.setStatus(true)
                    this.likeButton.setStatus(false)
                    this.downloadButton.setStatus(false)
                    this.dateButton.setStatus(false)
                    this.randomButton.setStatus(false)

                    this.featureButton.updateState("Sort by feature")
                    this.likeButton.updateState("")
                    this.downloadButton.updateState("")
                    this.randomButton.updateState("")
                    this.dateButton.updateState("")

                    this.setSortFlag("feature");
                }
            }
        );

        this.featureButton.setTopLeftOrigin(); 
        this.featureButton.setHighLightTexture("Button_CloudFeatured_Blue");  
        this.featureButton.setStatus(this.sortFlag === "feature");
        this.featureButton.updateState(this.sortFlag ==="feature"?"Sort by feature":"");

        this.likeButton = new CustomImageTextureButton(
            this.scene, 
            this.board,
            this.size.width / 2 + px, 
            this.size.height * 10 / 30, 
            160,
            180,
            "Button_CloudLike", 
            this.sortFlag ==="like"?"Sort by like":"",
            ()=>{
                if (this.sortFlag !== "like") { 
                    this.featureButton.setStatus(false)
                    this.likeButton.setStatus(true)
                    this.downloadButton.setStatus(false)
                    this.dateButton.setStatus(false)
                    this.randomButton.setStatus(false)
    
                    this.likeButton.updateState("Sort by like")
                    this.dateButton.updateState("")
                    this.downloadButton.updateState("")
                    this.randomButton.updateState("")
                    this.featureButton.updateState("") 
                    
                    this.setSortFlag("like");
                }
            }
        );

        this.likeButton.setTopLeftOrigin(); 
        this.likeButton.setHighLightTexture("Button_CloudLike_Blue"); 
        this.likeButton.setStatus(this.sortFlag === "like");
        this.likeButton.updateState(this.sortFlag ==="like"?"Sort by like":"");

        this.downloadButton = new CustomImageTextureButton(
            this.scene, 
            this.board,
            this.size.width / 2 + px, 
            this.size.height * 18 / 30, 
            160,
            180,
            "Button_CloudDownload", 
            this.sortFlag ==="download"?"Sort by download":"",
            ()=>{
                if (this.sortFlag !== "download") {
                    this.featureButton.setStatus(false)
                    this.likeButton.setStatus(false)
                    this.downloadButton.setStatus(true)
                    this.dateButton.setStatus(false)
                    this.randomButton.setStatus(false)
                    
                    this.downloadButton.updateState("Sort by download")
                    this.likeButton.updateState("")
                    this.dateButton.updateState("")
                    this.randomButton.updateState("")
                    this.featureButton.updateState("")

                    
                    this.setSortFlag("download");
                }
            }
        );
        this.downloadButton.setHighLightTexture("Button_CloudDownload_Blue"); 

        this.downloadButton.setTopLeftOrigin(); 
        this.downloadButton.setStatus(this.sortFlag === "download");
        this.downloadButton.updateState(this.sortFlag ==="download"?"Sort by download":"");

        this.dateButton = new CustomImageTextureButton(
            this.scene, 
            this.board,
            this.size.width / 2 + px, 
            this.size.height * 26 / 30, 
            160,
            180,
            "Button_CloudDate", 
            this.sortFlag ==="date"?"Sort by date":"",
            ()=>{
                if (this.sortFlag !== "date") {
                    this.featureButton.setStatus(false)
                    this.likeButton.setStatus(false)
                    this.downloadButton.setStatus(false)
                    this.dateButton.setStatus(true)
                    this.randomButton.setStatus(false)
                    
                    this.dateButton.updateState("Sort by date")
                    this.likeButton.updateState("")
                    this.downloadButton.updateState("")
                    this.randomButton.updateState("")
                    this.featureButton.updateState("")
                    
                    this.setSortFlag("date");
                }
            }
        );

        this.dateButton.setTopLeftOrigin(); 
        this.dateButton.setHighLightTexture("Button_CloudDate_Blue"); 
        this.dateButton.setStatus(this.sortFlag === "date");
        this.dateButton.updateState(this.sortFlag ==="date"?"Sort by date":"");

        this.randomButton = new CustomImageTextureButton(
            this.scene, 
            this.board,
            this.size.width / 2 + px, 
            this.size.height * 34 / 30, 
            160,
            180,
            "Button_CloudRandom", 
            this.sortFlag ==="random"?"Sort by random":"",
            ()=>{
                if (this.sortFlag !== "random") {
                    this.featureButton.setStatus(false)
                    this.likeButton.setStatus(false)
                    this.downloadButton.setStatus(false)
                    this.dateButton.setStatus(false)
                    this.randomButton.setStatus(true)
                    
                    this.randomButton.updateState("Sort by random")
                    this.likeButton.updateState("")
                    this.downloadButton.updateState("")
                    this.dateButton.updateState("")
                    this.featureButton.updateState("")
                    
                    this.setSortFlag("random");
                }
            }
        );

        this.randomButton.setTopLeftOrigin(); 
        this.randomButton.setHighLightTexture("Button_CloudRandom_Blue"); 
        this.randomButton.setStatus(this.sortFlag === "random");
        this.randomButton.updateState(this.sortFlag ==="random"?"Sort by random":"");

        this.stack.add(this.board)
        this.board.setVisible(this.visibility)
        this.board.setScale(0.3)
    }
   
    setDataFromAPI(){
        console.log('call set-data-from-api function')
        // var event = new CustomEvent('setModelData', { detail: {model_name: this.model.modelName, counter: this.counter, callback:(res)=>{
            
        //     // we need to refresh the counter 
        //     // using call back data 

        //     this.apiData = res;
        //     this.update()
        // }}});
        // document.dispatchEvent(event);

    }
};
