class CustomButton extends Phaser.GameObjects.Sprite {
    callback:()=>void;
    scene:ModelingScene;
    soundID: number;
    timer: Phaser.Time.TimerEvent;
	constructor(scene:ModelingScene, x:number, y:number, texture?:string, callback?:()=>void, repeativeInLongPress?:boolean){
        super(scene, x, y,texture);
        this.callback = callback;
        var isDown = false;
        let isPress = false;
        let repeative_task = null; // handler to perform interval task
        this.scene = scene;

  

		this.setInteractive().on('pointerdown', (pointer, localX, localY, event)=>{
            isDown = true;
            //check long press event if repeativeInLongPress = true
            if(repeativeInLongPress){

                if(this.timer) this.timer.remove();

                this.timer = this.scene.time.addEvent({                
                    delay: 600,
                    callback: ()=>{
                        if(isDown)
                             repeative_task = setInterval(callback, 400);
                    },
                    loop: false
                });

            }


        }).on('pointermove', (pointer, localX, localY, event)=>{
            
        }).on('pointerup', (pointer, localX, localY, event)=>{

            if(isDown){

                if(!repeative_task) // check if it's long press event 
                    callback(); // trigger callback if it's one clicking

                isDown = false; // set pointer down flag as false

                // remove repeative task
                clearInterval(repeative_task); 
                repeative_task = null;

                // play effect sound
                if(!this.soundID){
                    this.scene.playSound(CST.SOUND.COLOR_GENERAL_SOUND);
                }else{
                    this.scene.playSound(this.soundID);
                }

            }

        }).on('pointerout', ()=>{
            if(isDown){
                isDown = false;
            }
        })
        this.scene.add.existing(this);
    }
    

    setSoundType(soundID:integer){
        this.soundID = soundID;
    }

    setTextre(texture:string){
        this.setTexture(texture)
        // this.update()
    }

}