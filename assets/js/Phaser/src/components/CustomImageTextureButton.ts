class CustomImageTextureButton {
	callback:()=>void;
    scene: Phaser.Scene;
    stack: Phaser.GameObjects.Container;
    backGroundImage:Phaser.GameObjects.Image;
    text: Phaser.GameObjects.Text;
    backgroundTexture:string;
    highlightTexture:string;
    originX: number;
    originY: number;
    width:  number;
    height:  number;
    textHeight:  number;
    fontSize: number;
    wrapWidth: number;

	constructor(scene:Phaser.Scene, stack:Phaser.GameObjects.Container, x:number, y:number, width: number, height: number,  texture:string, counter:any, callback:()=>void){
        this.scene = scene;
        this.stack = stack;
        this.originX = x; 
        this.originY = y;
        this.width = width; 
        this.height = height;
        this.textHeight = 35;
        this.fontSize = 30;
        this.wrapWidth = 220;
        
        this.backGroundImage = this.scene.add.image(this.originX, this.originY, texture);
        this.backGroundImage.setDisplaySize(this.width, this.height - this.textHeight)
        this.stack.add(this.backGroundImage);

        const textWidth = this.fontSize * `${counter}`.length
        const textDx = this.originX + ( this.width - textWidth / 2 ) / 2;
        
        const textDy = this.originY + this.height - this.fontSize;
        
        this.text = this.scene.add.text(textDx, textDy, "", { font: `bold ${this.fontSize}pt Arial` });
        this.text.setText(this.text.getWrappedText(String(counter)))
        // this.text.setFontSize(this.fontSize)
        this.text.setPosition(textDx, textDy)
        this.text.depth = 600
        // this.text.lineSpacing = 1
        this.text.setWordWrapWidth(this.width);
        this.text.setAlign("center");
        
       
        
        this.stack.add(this.text);
        
        this.stack.setScale(LayoutContants.getInstance().HEIGHT_SCALE);
        // this.stack.depth = 500;
        this.callback = callback;
        this.backgroundTexture = texture;
        this.highlightTexture = texture;
        this.bindEvent();
    }

    getDisplayWidth(){
        return this.backGroundImage.displayWidth * LayoutContants.getInstance().HEIGHT_SCALE;
    }

    updateState(title:any){
        this.text.text = String(title);
       
        if (String(title).length) {
            const textWidth = this.fontSize * `${title}`.length
            var textDx, textDy;

            if (textWidth > this.wrapWidth) {
                
                textDx = this.originX;
                textDy = this.originY + this.height - this.fontSize;
                this.text.setWordWrapWidth(this.wrapWidth);
                this.text.setText(this.text.getWrappedText(String(title)))
            } else {
                textDx = this.originX + ( this.width - textWidth ) / 2;
                textDy = this.originY + this.height - this.fontSize;
                this.text.setText(String(title))
            }
            
            this.text.setPosition(textDx, textDy)
            this.text.depth = 600
            this.text.setAlign("center");
        }
    }


    setTitle(title:string, x:number, y:number){
        this.text.text = title;
        this.text.setColor("0x000000");
        this.text.setFontSize(50);
        this.text.setFontFamily("Nevis");
        if(title == "") this.stack.removeAll();
    }

    setHighLightTexture(texture:string){
        this.highlightTexture = texture;
    }

    bindEvent(){
        var isDown = false;

        this.backGroundImage.setInteractive().on('pointerdown', (pointer, localX, localY, event)=>{
            isDown = true;
            
        }).on('pointermove', (pointer, localX, localY, event)=>{

        }).on('pointerup', (pointer, localX, localY, event)=>{
            if(isDown){
                this.setStatus(true);
                this.callback();
                isDown = false;
            }
        }).on('pointerout', ()=>{
            if(isDown){
                isDown = false;
            }
        })
    }

    setStatus(selected:boolean){
        if(selected){
            this.backGroundImage.setTexture(this.highlightTexture);
        }else{
            this.backGroundImage.setTexture(this.backgroundTexture);
        }
    }

    setVisible(visible:boolean){
        // console.log('visible', visible)
        this.backGroundImage.setVisible(visible)
        this.text.setVisible(visible)
    }

    setTopLeftOrigin(){
        this.backGroundImage.setOrigin(0,0);
        const dx = this.backGroundImage.displayWidth/2;
        const dy = this.backGroundImage.displayHeight/2;
        // this.stack.y += dy;
        // this.stack.x += dx;
    }

    setTopRightOrigin(){
        this.backGroundImage.setOrigin(1,0);
    }

    setRightOrigin(){
        this.backGroundImage.setOrigin(1,0.5);
    }

    setLeftOrigin(){
        this.backGroundImage.setOrigin(0,0.5);
    }

    setScale(scale){
        const os = this.stack.scale;
        this.stack.setScale(os*scale);
    }

}