class HelperContainer {
    scene:Phaser.Scene;
    stack:Phaser.GameObjects.Container;
    helperImage:Phaser.GameObjects.Image;
    helpers:string[] = [];
    timer:any;
    currentHelperIndex:integer = 0;
    isFinishedHelp:boolean = false;
    grayBG:Phaser.GameObjects.Graphics;
    helperIndex:integer;
    pendingAction:()=>void;
    
    constructor(scene:Phaser.Scene, helperIndex:integer){
        this.scene = scene;
        this.helpers = Global.getInstance().getHelperImages();
        this.stack = this.scene.add.container(0,0);
        this.stack.depth = 1000;

        this.helperIndex = helperIndex;

        this.helperImage = this.scene.add.image(0, 0, this.helpers[helperIndex]);

        if(!LayoutContants.getInstance().isLandScape){
            this.helperImage.scale = LayoutContants.getInstance().SCREEN_WIDTH * 0.75/this.helperImage.displayWidth;
        }else{
            this.helperImage.scale = LayoutContants.getInstance().SCREEN_HEIGHT * 0.75/this.helperImage.displayHeight;
        }

        this.helperImage.x = LayoutContants.getInstance().SCREEN_WIDTH /2;
        this.helperImage.y = LayoutContants.getInstance().SCREEN_HEIGHT /2;
        this.stack.add(this.helperImage);

        this.addEventLisnter();
    }

    addEventLisnter(){
        if(localStorage.getItem(`helper${this.helperIndex}`) == "true"){
            this.stack.setVisible(false);
        }

        this.helperImage.setInteractive()
        .on("pointerup", ()=>{
            if(this.helperIndex == 2){
                this.fadeOut();
            }else{
                if(this.pendingAction){
                    this.pendingAction();
                }else{
                    this.stack.setVisible(false);
                } 
            }
            localStorage.setItem(`helper${this.helperIndex}`, "true");
        });
    }

    fadeOut(){
        this.scene.tweens.add({
            targets: this.stack,
            alpha: 0,
            ease: 'Cubic.easeOut',  
            duration: 1000,
        });
    }

    setHelperIndex(index:integer){
        if(index == 2 && localStorage.getItem(`helper1`) != "true"){
            this.pendingAction = ()=>{
                this.setIndex(index);
            }
        }else{
            this.setIndex(index);
        }
    }

    setIndex(index:integer){
        this.helperIndex = index;
        this.helperImage.setTexture(this.helpers[index]);
        this.stack.setVisible(true);
        this.pendingAction = null;
    }

}