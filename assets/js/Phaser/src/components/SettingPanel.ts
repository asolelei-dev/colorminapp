class SettingPanel{
    scene:Phaser.Scene;
    settingButton:ToggleButton;
    stack:Phaser.GameObjects.Container;
    backgroundImage: Phaser.GameObjects.Image;
    height: number;
    RateAppButton: CustomImageButton;
    PrivacyPolicyButton: CustomImageButton;
    musicButton: ToggleButton;
    soundButton: ToggleButton;
    
    debugButton: ToggleButton; // toggle button to show/hide Debug form

    availableDistance: number;
    deltaY:number;
    isOn: boolean;
    visibleBarHeight:number;
    fpsButton: ToggleButton;

    constructor(scene:Phaser.Scene, height:number,  backgroundTexture:string, callback){
        this.scene = scene;
        this.height = height;
        this.visibleBarHeight = 32 * LayoutContants.getInstance().root_scale;
        this.stack = this.scene.add.container(0, 0);
        this.backgroundImage = this.scene.add.image(0, 0, backgroundTexture);
        this.backgroundImage.displayWidth = LayoutContants.getInstance().SCREEN_WIDTH;
        this.backgroundImage.displayHeight = height;
        this.backgroundImage.setOrigin(0,0);
        this.deltaY = 202;
        this.settingButton = new ToggleButton(scene, LayoutContants.getInstance().SCREEN_WIDTH/2, height - (70 + this.deltaY) * LayoutContants.getInstance().HEIGHT_SCALE, "Button_GoToSettings", "Button_GoToSettingsON", callback);
        this.isOn = false;

        this.RateAppButton = new CustomImageButton(scene, LayoutContants.getInstance().SCREEN_WIDTH/2, height - (320 + this.deltaY) * LayoutContants.getInstance().HEIGHT_SCALE, "Button_RateMe02", ()=>{
            if(this.isOn){
                var event = new CustomEvent('app-link', { detail: "rate" });
                document.dispatchEvent(event);
            }
        });

        this.PrivacyPolicyButton = new CustomImageButton(scene, LayoutContants.getInstance().SCREEN_WIDTH - 200 * LayoutContants.getInstance().SCALE, height - (350 + this.deltaY) * LayoutContants.getInstance().HEIGHT_SCALE, "GDRP", ()=>{
            if(this.isOn){
                var event = new CustomEvent('app-link', { detail: "privacy" });
                document.dispatchEvent(event);
            }
        });
        this.PrivacyPolicyButton.setScale(0.8);

        this.musicButton = new ToggleButton(scene, 300 * LayoutContants.getInstance().SCALE,  (400 - this.deltaY) * LayoutContants.getInstance().HEIGHT_SCALE, "ButtonSwitch_OFF", "ButtonSwitch_ON", (isOn)=>{
            if(isOn) localStorage.setItem("music", 'on');
            else localStorage.setItem("music", 'off');
        });
        
        // initialize toggle button for show or hide debug form on theyes page.
        const url = window.location.href;
        if(url.includes("color_debug")){
            this.debugButton = new ToggleButton(scene, 300 * LayoutContants.getInstance().SCALE,  (600 - 202) * LayoutContants.getInstance().HEIGHT_SCALE, "ButtonSwitch_OFF", "ButtonSwitch_ON", (isOn)=>{
                if(isOn) localStorage.setItem("debug", 'on');
                else localStorage.setItem("debug", 'off');
            });
        
            this.fpsButton = new ToggleButton(scene,  LayoutContants.getInstance().SCREEN_WIDTH/2,  (400 - this.deltaY) * LayoutContants.getInstance().HEIGHT_SCALE, "ButtonSwitch_OFF", "ButtonSwitch_ON", (isOn)=>{
                if(isOn) localStorage.setItem("fps", 'on');
                else localStorage.setItem("fps", 'off');
            });
        }



        this.soundButton = new ToggleButton(scene, LayoutContants.getInstance().SCREEN_WIDTH - 300 * LayoutContants.getInstance().SCALE,  (400 - this.deltaY) * LayoutContants.getInstance().HEIGHT_SCALE, "ButtonSwitch_OFF", "ButtonSwitch_ON", (isOn)=>{
            if(isOn) localStorage.setItem("sound", 'on');
            else localStorage.setItem("sound", 'off');
        });

        this.musicButton.setActive(((localStorage.getItem("music") || 'on') === 'on'));
        this.soundButton.setActive(((localStorage.getItem("sound") || 'on') === 'on'));
        if(this.debugButton) this.debugButton.setActive(((localStorage.getItem("debug") || 'off') === 'on')); // intialize value at startup for debug form
        if(this.fpsButton) this.fpsButton.setActive(((localStorage.getItem("fps") || 'off') === 'on')); // intialize value at startup for debug form

        this.stack.add(this.backgroundImage);
        this.stack.add(this.settingButton.stack);
        this.stack.add(this.PrivacyPolicyButton.stack);
        this.stack.add(this.RateAppButton.stack);
        this.stack.add(this.musicButton.stack);
        this.stack.add(this.soundButton.stack);
        if(this.debugButton)  this.stack.add(this.debugButton.stack); // add debug toggle button in view
        if(this.fpsButton)  this.stack.add(this.fpsButton.stack); // add fps toggle button in view

        this.initText();
        this.initSetting();
    }

    initText(){

        var textRateApp = this.scene.add.text(0, -200, "Rate App");
            textRateApp.setColor("#807f7d");
            textRateApp.setFontSize(100);
            textRateApp.setFontFamily("Nevis");
            textRateApp.x = -textRateApp.displayWidth /2;
            this.RateAppButton.stack.add(textRateApp);


        var textPrivacy = this.scene.add.text(0, -180, "Privacy");
            textPrivacy.setColor("#807f7d");
            textPrivacy.setFontSize(50);
            textPrivacy.setFontFamily("Nevis");
            textPrivacy.x = -this.PrivacyPolicyButton.backGroundImage.displayWidth/2;
            this.PrivacyPolicyButton.stack.add(textPrivacy);


        var textMusic = this.scene.add.text(0, -150, "Music");
            textMusic.setColor("#807f7d");
            textMusic.setFontSize(100);
            textMusic.setFontFamily("Nevis");
            textMusic.x = -textMusic.displayWidth /2;
            this.musicButton.stack.add(textMusic);


        if(this.debugButton) {
            var textDebug = this.scene.add.text(0, -150, "Debug");
            textDebug.setColor("#807f7d");
            textDebug.setFontSize(100);
            textDebug.setFontFamily("Nevis");
            textDebug.x = -textDebug.displayWidth /2;
            this.debugButton.stack.add(textDebug);

        }

        if(this.fpsButton) {
            var textFPS = this.scene.add.text(0, -150, "FPS");
            textFPS.setColor("#807f7d");
            textFPS.setFontSize(100);
            textFPS.setFontFamily("Nevis");
            textFPS.x = -textFPS.displayWidth /2;
            this.fpsButton.stack.add(textFPS);
        }
       


        var textSound = this.scene.add.text(0, -150, "Sound FX");
            textSound.setColor("#807f7d");
            textSound.setFontSize(100);
            textSound.setFontFamily("Nevis");
            textSound.x = -textSound.displayWidth /2;
        this.soundButton.stack.add(textSound);
        this.availableDistance = (-850 + this.deltaY) * LayoutContants.getInstance().HEIGHT_SCALE;
        this.stack.y = this.availableDistance + this.visibleBarHeight;
        
    }

    getHeight(){
        return this.height;
    }

    setDepth(depth:integer){
        this.stack.depth = depth;
    }

    initSetting(){
        const isMusicOn = (localStorage.getItem("music") || 'on') === 'on';
        const isSoundOn = (localStorage.getItem("music") || 'on') === 'on';
    }
    
    setOn(isOn:boolean){
        this.isOn = isOn;
      
    }
}