class ColorModeButton {
	callback:()=>void;
    scene: Phaser.Scene;
    stack:Phaser.GameObjects.Container;
    backGroundImage:Phaser.GameObjects.Image;
    highlightImage:Phaser.GameObjects.Image;
	constructor(scene:Phaser.Scene, x:number, y:number, defaultTexture:string, hightLightTexture:string, callback:()=>void){
        this.scene = scene;
        this.stack = this.scene.add.container(x, y);
        this.stack.setScale(LayoutContants.getInstance().SCALE);
        this.backGroundImage = this.scene.add.image(0, 0, defaultTexture);
        this.stack.depth = 500;
        this.callback = callback;
        this.highlightImage = this.scene.add.image(0, 0, hightLightTexture).setVisible(false);
        this.stack.add(this.backGroundImage);
        this.stack.add(this.highlightImage);
        this.stack.setScale(LayoutContants.getInstance().BUTTONS_SIZE/this.backGroundImage.displayWidth)
        this.bindEvent();
    }

    bindEvent(){
        var isDown = false;
        this.backGroundImage.setInteractive().on('pointerdown', (pointer, localX, localY, event)=>{
            isDown = true;
        }).on('pointermove', (pointer, localX, localY, event)=>{

        }).on('pointerup', (pointer, localX, localY, event)=>{
            if(isDown){
                this.setStatus(true);
                this.callback();
                isDown = false;
            }
        }).on('pointerout', ()=>{
            if(isDown){
                isDown = false;
            }
        })
    }

    setStatus(selected:boolean){
        if(selected){
            this.highlightImage.setVisible(true);
        }else{
            this.highlightImage.setVisible(false);
        }
    }

    setVisible(visible:boolean){
        this.stack.setVisible(visible);
    }
}