function checkIfContainsBodypart(source:BodyPart[], taget:BodyPart):boolean{
        var result = false;
        source.forEach(element => {
            if(element.modelPart.partName == taget.modelPart.partName) result = true;
        });
        return result;
}