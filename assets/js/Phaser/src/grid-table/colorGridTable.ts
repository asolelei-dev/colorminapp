
class ColorGridTable {
    scene:ModelingScene;
    items:ColorButton[];
    innerStack:Phaser.GameObjects.Container;
    stack:Phaser.GameObjects.Container;
    table: Phaser.GameObjects.Image;
    config:any;
    limitTopY:number;
    limitBottomY:number;
    timer: Phaser.Time.TimerEvent;
    tableMovement:boolean;
    graphics: Phaser.GameObjects.Graphics;
    currentItemIndex: integer;
    smoothMovTimer: Phaser.Time.TimerEvent;
    removetimer: Phaser.Time.TimerEvent;
    PrevTableMovement: Boolean;
    lastPosY: number = 0;
    constructor(scene:ModelingScene, config:any){
        this.table = config.background;

        this.table.setPosition(config.x, config.y);
        this.table.displayHeight = config.height - 100;
        this.table.displayWidth = config.width;

        this.table.setOrigin(0,0);
        this.table.tint = 0xffffff;
        this.table.alpha = 0.01;
        this.stack = scene.add.container(config.x, config.y);
        this.stack.setDepth(1);

        this.bindEvent(scene);
        this.scene = scene;
        this.config = config;
        this.tableMovement = false;
        this.PrevTableMovement = false;

    }

    bindEvent(scene:Phaser.Scene){
        var isDown = false;
        var lastPosY = 0;
        var travelDistance = 0;
        var velocityY = 0;
        var startPosX;
        var startPosY;

        this.table.setInteractive().on('pointerdown', (pointer, localX, localY, event)=>{
            if(this.smoothMovTimer != undefined) this.smoothMovTimer.remove();

            
            isDown = true;
       
            if(this.tableMovement) {
                this.PrevTableMovement = true;
            }
            else {
                this.PrevTableMovement = false;
            }

            this.tableMovement = false;
     
            startPosX = pointer.x;
            startPosY = pointer.y;
            if(this.scene.uiGenerator.colorMode != CST.COLOR_MODE.CUSTOME)
            this.detectPressDown(pointer, event);
            
            

        }).on('pointermove', (pointer, localX, localY, event)=>{
            if(isDown){
                if(lastPosY == 0){
                    lastPosY = pointer.y;
                }
                this.stack.y += pointer.y - lastPosY;
                travelDistance += Math.abs(pointer.y - lastPosY);
                lastPosY = pointer.y;
                if(travelDistance > 5){
                    this.tableMovement = true;
                    if(this.timer) this.timer.remove();
                
                }
            }
        }).on('pointerup', (pointer, localX, localY, event)=>{
            lastPosY = 0;
            
            if(isDown && !this.tableMovement){
                isDown = false;
                this.correctPosition();
                this.detectEvent(startPosX, startPosY, event);
            }
            travelDistance = 0;
            if(this.tableMovement){
                isDown = false;
                this.doInertiaMovement(velocityY);
            }
            this.emitPressUpEvent();
        }).on('pointerout', (pointer, localX, localY, event)=>{
            lastPosY = 0;
            if(isDown){
                isDown = false;
                this.correctPosition();
                // this.detectEvent(startPosX, startPosY, event);
            }
            travelDistance = 0;
        })

        scene.events.on("velocity", (vx, vy)=>{
            velocityY = vy;
        });
    }

    setItems(items:any[]){
       
        this.items = items;
        var index = 0;
        

        var sizeVal = 0;
        items.forEach(element => {
            const col = index % this.config.table.columns;
            sizeVal = (this.config.scrollMode == 0) ? this.config.table.cellHeight:this.config.table.cellWidth;
            const x = col * sizeVal + 0.5 * sizeVal;
            const y = Math.floor(index / this.config.table.columns) * sizeVal + 0.5 * sizeVal;
            var cellContainer = this.scene.add.container(x,y);
            element.setPosition(x, y);
            cellContainer.add(element.stack);
            this.stack.add(cellContainer);
            index ++;
        });

        if(this.config.scrollMode == 0){
            this.table.displayHeight = Math.ceil(sizeVal * items.length / this.config.table.columns);
            this.limitTopY = this.stack.y;
            this.limitBottomY = - Math.max(this.table.displayHeight - this.config.height, 0) + this.config.y;
        }
    }

    emitPressUpEvent(){
        setTimeout(() => {
            this.table.emit("cell.pressup");    
        }, 10);
    }

    detectPressDown(pointer:Phaser.Input.Pointer, event:any){
        if(this.tableMovement){
            if(this.timer) this.timer.remove();
            return;
        }

        if(this.timer) this.timer.remove();

        this.items.forEach(element => {
            if(element.clayImage.getBounds().contains(pointer.x, pointer.y)){
                this.currentItemIndex = element.buttonIndex;
                this.timer = this.scene.time.addEvent({
                    delay: 600,
                    callback: ()=>{                       
                        if(this.currentItemIndex == element.buttonIndex && !this.tableMovement && !this.PrevTableMovement){                            
                            this.table.emit("cell.press", element.buttonIndex);
                        }
                    },
                    loop: false
                });
            }
        });
    }

    detectEvent(x:number, y:number, event:any){
        if(!this.tableMovement && !this.PrevTableMovement){
            this.items.forEach(element => {
                if(element.clayImage.getBounds().contains(x, y)){
                    this.table.emit("cell.click", element.buttonIndex);
                    this.currentItemIndex = -1;
                }
            });
        }
    }

    emitMoveEvent(x:number, y:number, event:any){
        this.table.emit("cell.move", x, y);
    }

    correctPosition(){
        if(this.config.scrollMode == 0){
            if(this.stack.y > this.limitTopY){
                this.scrollTo(this.limitTopY);
            }
            if(this.stack.y < this.limitBottomY){
                this.scrollTo(this.limitBottomY);
            }
        }
    }

    scrollTo(targetPosY:number){
        var frames = 15;
        const unit = (targetPosY - this.stack.y)/frames;
        if(unit == 0){
            return;
        }
        var duration = 100;
        var delay = duration/frames;
        if(this.timer) this.timer.destroy();
        this.timer = this.scene.time.addEvent({
            delay: delay,
            callback: ()=>{
                if(duration > 0){
                    this.stack.y += unit;
                    duration -= delay;
                }
            },
            repeat: frames
        });
        
    }

    scrollToTop(flag:boolean){
        if(flag){
            this.scrollTo(this.limitTopY);
        }
    }

    getX(){
        return this.stack.x;
    }

    moveByX(dx:integer){
        this.stack.x += dx;
        this.table.x += dx;
    }

    setPosition(pos:any){
        
        if(pos.x != undefined){
            this.stack.x = pos.x;
            this.table.x = pos.x;
        }
        if(pos.y != undefined){
            var dy = 0;
            if(this.lastPosY == 0) this.lastPosY = pos.y;
            dy = pos.y - this.lastPosY;
            this.lastPosY = pos.y;
            // var dy = pos.y - this.stack.y;
            this.stack.y += dy;
            this.table.y += dy;
            this.limitTopY = pos.y;
        }
    }

    setVisible(visible:boolean){
        this.stack.setVisible(visible);
        this.table.setVisible(visible);
    }

    doInertiaMovement(v:number){
        if(this.smoothMovTimer != undefined) this.smoothMovTimer.remove();
        var velocity = v;
        this.smoothMovTimer = this.scene.time.addEvent({
            delay: 8,
            callback: ()=>{
                velocity -=  velocity / 9;
                if(Math.abs(velocity) < 1){
                    this.tableMovement = false;
                }

                if(Math.abs(velocity) < 0.01){
                    //this.tableMovement = false;
                    this.smoothMovTimer.remove();
                    this.correctPosition();
                }
                this.stack.y += velocity;
                if(this.stack.y > this.limitTopY){
                    this.tableMovement = false;
                    this.smoothMovTimer.remove();
                    this.correctPosition();

                }
                if(this.stack.y < this.limitBottomY){
                    this.tableMovement = false;
                    this.smoothMovTimer.remove();
                    this.correctPosition();

                }
            },
            loop: true
        });
    }

}