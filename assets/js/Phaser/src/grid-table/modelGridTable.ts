
class ModelGridTable {
    scene:Phaser.Scene;
    items:ModelItem[];
    stack:Phaser.GameObjects.Container;
    table: Phaser.GameObjects.Image;
    config:any;
    limitTopY:number;
    limitBottomY:number;
    timer: Phaser.Time.TimerEvent;
    tableMovement:boolean;
    graphics: Phaser.GameObjects.Graphics;
    smoothMovTimer: any;
    tween:any;
    constructor(scene:Phaser.Scene, config:any){
        this.table = config.background;

        this.table.setPosition(config.x, config.y);
        this.table.displayHeight = config.height;
        this.table.displayWidth = config.width;
        this.table.setOrigin(0,0);
        this.table.tint = 0xCCCCCC;
        this.table.alpha = 0.01
        this.stack = scene.add.container(config.x, config.y);
        this.stack.add(this.table);

        this.bindEvent(scene);
        this.scene = scene;
        this.config = config;
        this.tableMovement = false;

    }

    bindEvent(scene:Phaser.Scene){
        var isDown = false;
        var lastPosY = 0;
        var travelDistance = 0;
        var velocityY = 0;

        let closeSettingPannel = () => {
                // check if setting pannel is opened
                if(this.config.settingPannel.isOn){
                    // clear smooth timer 
                    if(this.smoothMovTimer != undefined) this.smoothMovTimer.remove();
    
                    // setting pannel is closed
                    this.config.toggleSettingPannel(false);
                    this.config.settingPannel.settingButton.setActive(false); // make active mark as false
                }
        }

        let movingTableByPy = (py) => {
            // init last pos
            if(lastPosY == 0){
                lastPosY = py;
            }

            // moving table by pointer
            this.stack.y += py - lastPosY;
            // get moving distance
            travelDistance += Math.abs(py - lastPosY);                
            lastPosY = py; // backup last position

            // check if pointer is moved by 10
            if(travelDistance > 10){
                this.tableMovement = true; // make tableMovement as true
            }
        }

        let _t = null;

        let movingTableByDy = (dy) => {

            // moving table by pointer
            this.stack.y = this.stack.y - dy;
            // get moving distance
            travelDistance += Math.abs(dy);             
            
            this.correctPosition();
            
        }

        // mouse scroll event
        this.table.on('wheel', function(pointer, dx, dy, dz, event){ 
            closeSettingPannel(); // close setting pannel if it's opened already
            //console.log('dy', pointer);
              
            movingTableByDy(dy); 
        });

        this.table.setInteractive().on('pointerdown', (pointer, localX, localY, event)=>{            
            closeSettingPannel(); // close setting pannel if it's opened already
            isDown = true; // mark down flag as true
        }).on('pointermove', (pointer, localX, localY, event)=>{
            if(isDown){
                //console.log(pointer);
                movingTableByPy(pointer.y); // moving table                
            }
        }).on('pointerup', (pointer, localX, localY, event)=>{

            lastPosY = 0; // remove last position 

            if(isDown){
                isDown = false;
                this.correctPosition();
                this.detectEvent(pointer, event);
            }

            travelDistance = 0;

            if(this.tableMovement){
                this.doInertiaMovement(velocityY);
            }

            this.tableMovement = false;

            
        }).on('pointerout', (pointer, localX, localY, event)=>{
            lastPosY = 0;
            if(isDown){
                isDown = false;
                this.correctPosition();
                this.detectEvent(pointer, event);
            }
            travelDistance = 0;
            this.tableMovement = false;
        })

        scene.events.on("velocity", (vx, vy)=>{
            velocityY = vy;
        });
    }

    setItems(items:ModelItem[]){

        this.stack.removeAll();

        this.items = items;
        const columns = this.config.table.columns;
        const width = this.config.width;
        const height = this.config.height;

        const cellWidth = Math.max(this.config.table.cellWidth, this.config.width/columns);

        items.forEach(element => {
            element.setDisplayWidth(cellWidth);
        });

 
        const itemH = items[0].getDisplayHeight();
        const itemW = items[0].getDisplayWidth();
        

        // var paddingTop = Math.max((height - itemH * Math.ceil(items.length / columns))/2, 0);
        // var marginLeft = (width - columns * itemW) /(columns+1);
        var paddingTop = 0;
        var marginLeft = 0;

        if(this.config.scrollMode == 0){
            this.table.displayHeight = itemH * Math.ceil(items.length / columns) + 2*paddingTop;
            this.limitTopY = this.config.y;
            this.limitBottomY = - Math.max(this.table.displayHeight - this.config.height, 0) + this.config.y;
        }

        var index = 0;
        items.forEach(element => {
            element.tabIndex = index;
            const col = index % columns;
            const x = col * itemW + (col+1)*marginLeft;
            const y = Math.floor(index / columns) * itemH + paddingTop;
            var cellContainer = this.scene.add.container(x,y);
            cellContainer.add(element.stack);
            this.stack.add(cellContainer);
            index ++;
        });

        this.stack.y = this.config.y;

    }

    detectEvent(pointer:Phaser.Input.Pointer, event:any){
        if(!this.tableMovement){

            this.items.forEach(element => {
                if(element.backgroundImage.getBounds().contains(pointer.x, pointer.y)){
                    if(LayoutContants.getInstance().SCREEN_HEIGHT - (parseFloat(localStorage.getItem("bottom-height")) || 0) >= pointer.y ){
                        this.table.emit("cell.click", element.tabIndex);
                    }
                }
            });
        }
    }

    correctPosition(){

        if(this.config.scrollMode == 0){
            if(this.stack.y > this.limitTopY){
                this.scrollTo(this.limitTopY);
            }
            if(this.stack.y < this.limitBottomY){
                this.scrollTo(this.limitBottomY);
            }
        }
    }







    scrollTo(targetPosY:number){
        if(this.tween) this.tween.remove();
        this.tween = this.scene.tweens.add({
            targets: this.stack,
            y: targetPosY,
            // alpha: { start: 0, to: 1 },
            // alpha: 1,
            // alpha: '+=1',
            ease: 'Back.easeIn',       // 'Cubic', 'Elastic', 'Bounce', 'Back'
            duration: 300,
            repeat: 0,            // -1: infinity
            yoyo: false
        });


        // var frames = 15;
        // const unit = (targetPosY - this.stack.y)/frames;
        // if(unit == 0){
        //     return;
        // }
        // var duration = 100;
        // var delay = duration/frames;
        // if(this.timer) this.timer.destroy();
        
        // this.timer = this.scene.time.addEvent({
        //     delay: delay,
        //     callback: ()=>{
        //         if(duration > 0){
        //             this.stack.y += unit;
        //             duration -= delay;
        //         }
        //     },
        //     repeat: frames
        // });
        
    }

    scrollToTop(flag:boolean){
        if(flag){
            this.scrollTo(this.limitTopY);
        }
    }

    moveByX(dx:integer){
        this.stack.x += dx;
        this.table.x += dx;
    }

    setVisible(visible:boolean){
        this.stack.setVisible(visible);
        this.table.setVisible(visible);
    }


    doInertiaMovement(v:number){
        if(this.smoothMovTimer != undefined) this.smoothMovTimer.remove();
        var velocity = v;
        this.smoothMovTimer = this.scene.time.addEvent({
            delay: 8,
            callback: ()=>{
                velocity -=  velocity / 9
                this.stack.y += velocity;
                if(this.stack.y > this.limitTopY){
                    this.smoothMovTimer.remove();
                    this.correctPosition();
                }
                if(this.stack.y < this.limitBottomY){
                    this.smoothMovTimer.remove();
                    this.correctPosition();
                }
            },
            loop: true
        });
    }

    increaseDY(y:number){
        this.stack.y += y;
        //this.table.y += y;
        this.limitTopY += y;
        this.limitBottomY += y;
    }


    increaseTableDY(y:number){
        
        this.table.y += y;
        
    }
    

}