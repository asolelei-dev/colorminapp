
class CommunityModelBoard {
    scene:Phaser.Scene;
    item: CommunityModel;
    stack:Phaser.GameObjects.Container;
    table: Phaser.GameObjects.Image;
    config:any;
    itemW:number;
    itemH:number;
    modelName: string;
    cloudBoard: communityModelButtonContainer; //added by asset
    cloudBoardConfig: { x: number, y: number, width: number, height: number}
    exitButton: CustomImageButton;
    setShowFlag: Function;
    editModel: Function;

    constructor(scene:Phaser.Scene, config:any){

        this.scene = scene;
        this.config = config;
        this.modelName = config.modelName;

        this.stack = scene.add.container(config.x, config.y);
        this.stack.setDepth(1000);

        this.setShowFlag = config.changeCommunityModelBoardFlag;
        this.editModel = config.editCommunityModel;

        this.table = config.background;
        this.table.displayHeight = config.height;
        this.table.displayWidth = config.width;
        this.table.tint = 0xCCCCCC;
        this.table.alpha = 0.01;
        this.table.setPosition(config.x, config.y);
        this.table.setOrigin(0,0);

        this.exitButton = new CustomImageButton(this.scene, 0, 0, "back_button", ()=>{
            this.goMainScene();
        });

        this.exitButton.stack.setDepth(1001)
        this.exitButton.stack.setAlpha(.5)
        this.exitButton.setTopLeftOrigin();

        this.cloudBoardConfig = { x: this.config.width - 120, y: 0, width: 350, height: config.height }
        const cloudBoardConfig = { 
            ...this.cloudBoardConfig,
            visibility: true,
            counter: {
                like: 1, 
                download: 1,
            },
            setShareFlag: () => { 
            },
            likeIncrement: () => {
                console.log("like increment")
                config.editCommunityModel()
            },
            downloadIncrement: () => {
            }
        }
        this.cloudBoard = new communityModelButtonContainer(this.scene, cloudBoardConfig);
        // this.stack.add(this.cloudBoard.stack);
        this.cloudBoard.stack.setDepth(1001);
        
    }

    setItems(item:CommunityModel){
        this.item = item;
        const cellWidth = this.config.width;
        const cellHeight = this.config.height;
        item.setDisplaySize(cellWidth, cellHeight);
        this.itemH = item.getDisplayHeight();
        this.itemW = item.getDisplayWidth();

        this.table.displayWidth = this.itemW;
        // console.log("this.itemH", this.itemH, this.config.height)
        // this.cloudBoard.stack.displayHeight = this.config.height;
        const x = 0;
        const y = 0;
        var cellContainer = this.scene.add.container(x,y);
        item.initBodyPart();
        cellContainer.add(item.stack);
        this.stack.add(cellContainer);
        this.stack.x = this.config.x;

    }
    goMainScene(){
        this.setShowFlag(true);
        this.setVisible(false);
    }

    setVisible(visibility:boolean){
        this.stack.setVisible(visibility);
        this.exitButton.setVisible(visibility)
        this.cloudBoard.stack.setVisible(visibility)
    }
}