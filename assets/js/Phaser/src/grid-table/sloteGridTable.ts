
class SlotGridTable {
    scene:Phaser.Scene;
    items:any[];
    stack:Phaser.GameObjects.Container[];
    table: Phaser.GameObjects.Image[];
    
    config:any;
    limitLeftX:number[];
    limitRightX:number[];
    itemW:number[];
    itemH:number[];

    timer: Phaser.Time.TimerEvent;
    tableMovement:boolean[];
    smoothMovTimer: any;
    
    headerStack: Phaser.GameObjects.Container;// added by asset
    UImargin: Phaser.GameObjects.Image; // added by asset
    titleImage: Phaser.GameObjects.Image; // added by asset

    communityHeaderStack: Phaser.GameObjects.Container;
    communitytitleImage: Phaser.GameObjects.Image; // added by asset

    cloudBoard: CloudIconButtonContainer; //added by asset
    cloudBoardConfig: { x: number, y: number, width: number, height: number}

    communityBoard: CommuntyIconButtonContainer; //added by asset
    communityBoardConfig: { x: number, y: number, width: number, height: number }

    modelName: string;
    overlapWidth: number;
    sloteItemsData: any[];
    sloteItemsInfo: { is_shared: boolean, like:number, download: number, shared_date: string }[];

    currentSloteIndex: number;

    containerAry: Phaser.GameObjects.Container[];
    sortFlag: string;
    isLoggedIn: boolean;
    user_id: string;


    constructor(scene:Phaser.Scene, config:any){

        this.stack = []
        this.table = []
        this.tableMovement = [false, false]
        this.items = []
        this.itemW = []
        this.itemH = []
       
        this.limitLeftX = []
        this.limitRightX = []
        // this.limitLeftX = []
        // this.limitLeftX = []
        for (var i=0; i<2; i++) {
            
            this.table[i] = config.background[i];
           
            this.table[i].displayHeight = config.height / 2;
            this.table[i].displayWidth = config.width;
           
            this.table[i].tint = 0xCCCCCC;
            this.table[i].alpha = 0.01
            
            this.tableMovement[i] = false;
        }
        
        this.table[0].setPosition(config.x, config.y);
        this.table[0].setOrigin(0,0);
        this.table[1].setPosition(config.x, config.y + config.height / 2);
        this.table[1].setOrigin(0,0);
        const bindConfig = { isDown: [false, false], lastPosX: [0,0], travelDistance: [0,0], velocityX: [0,0] }
        
        this.bindEvent(scene, bindConfig, 0);
       
       
        this.scene = scene;
        this.config = config;
        this.modelName = config.modelName;
        console.log('modelName', this.modelName)

        // added by asset


        this.overlapWidth = 30 
        this.cloudBoardConfig = { x: 0, y: config.y, width: config.x + this.overlapWidth, height: config.height }
        this.communityBoardConfig = { x: 0, y: config.y + 220, width: config.x + this.overlapWidth, height: config.height }
        
        this.stack = []
        this.stack[0] = scene.add.container(config.x, config.y)
        this.stack[1] = scene.add.container(config.x, config.y + config.height / 2)
     
        this.headerStack = scene.add.container(config.x, config.y);
        this.headerStack.depth = 510

        this.communityHeaderStack = scene.add.container(0, config.height / 2 + 30);
        this.communityHeaderStack.depth = 510

        // this.headerStack.setDisplaySize(300, 100)

        this.sloteItemsData = config.sloteItemsData;
        this.sloteItemsInfo = [];
        this.currentSloteIndex = 0;

        this.containerAry = [];
        this.sortFlag = "feature";
        this.isLoggedIn = false;
        this.user_id = "";

        this.checkUserLoggedIn();
    }

    checkUserLoggedIn() {
        var event = new CustomEvent('checkUserLoggedIn', { detail: { callback:(res)=>{
            console.log("checkUserLoggedIn", res);
            const loginFlag = res[0]; 
            this.isLoggedIn = loginFlag;
            this.user_id = this.isLoggedIn?res[1]: this.user_id;
        }}});
        document.dispatchEvent(event);
    }

    updateSloteItemData (){
        console.log('updateSloteItemData', this.items, this.currentSloteIndex)
        const { id: mid, ...rest } = this.items[0][this.currentSloteIndex].sloteItemInfo;
        const { is_shared, download, like, shared_date } = this.sloteItemsInfo[this.currentSloteIndex]
        const model_data = { ...rest, is_shared, download_count: download, like_count: like, shared_date }
       
        this.config.updateSloteItemData(mid, model_data)
    }

    bindEvent(scene:Phaser.Scene, config: any, tableIndex: integer){

        var { isDown, lastPosX, travelDistance, velocityX } = config
        // const i = tableIndex
       
        // isDown[i] = false;
        // lastPosX[i] = 0;
        // travelDistance[i] = 0;
        // velocityX[i] = 0;

        this.table[0].setInteractive().on('pointerdown', (pointer, localX, localY, event)=>{
            
            if (pointer.y < ( this.config.height / 2 + this.config.y )) {
                isDown[0] = true;
                console.log('pointdown', 0)
            } 
            // isDown[0] = true;
            console.log('pointdown', 0)
        }).on('pointermove', (pointer, localX, localY, event)=>{
            // console.log('pointermove', 0)
            if(isDown[0]){
                // console.log('pointermove', isDown, 0)
                if(lastPosX[0] == 0){
                    lastPosX[0] = pointer.x;
                }
                this.stack[0].x += pointer.x - lastPosX[0];
                travelDistance[0] += Math.abs(pointer.x - lastPosX[0]);
                lastPosX[0] = pointer.x;
                if(travelDistance[0] > 10){
                    this.tableMovement[0] = true;

                    // added by asset
                    if ( this.cloudBoard ) {
                        this.cloudBoard.board.setVisible(!this.tableMovement[0])
                    }
                }
            }
        }).on('pointerup', (pointer, localX, localY, event)=>{
            // console.log('pointerup', 0)
            lastPosX[0] = 0;
            if(isDown[0]){
                isDown[0] = false;
                
                this.correctPosition(0);
                this.detectEvent(pointer, event, 0);
            }
            travelDistance[0] = 0;
            if(this.tableMovement[0]){
                this.doInertiaMovement(velocityX[0], 0);
            }
           
        }).on('pointerout', (pointer, localX, localY, event)=>{
            lastPosX[0] = 0;
            if(isDown[0]){
                isDown[0] = false;
                // console.log('pointerout', 0)
                this.correctPosition(0);
                this.detectEvent(pointer, event, 0);
            }
            travelDistance[0] = 0;
            this.tableMovement[0] = false;
        })
        

        scene.events.on("velocity", (vx, vy)=>{
            velocityX[0] = vx;
        });

        this.table[1].setInteractive().on('pointerdown', (pointer, localX, localY, event)=>{
            if (pointer.y > ( this.config.height / 2 + this.config.y )) {
                isDown[1] = true;
                // console.log('pointdown', 1, pointer.y, this.config.height / 2)
            }
            // isDown[1] = true;
            // console.log('pointdown', 1)
        }).on('pointermove', (pointer, localX, localY, event)=>{
            // console.log('pointermove', 1)
            if(isDown[1]){
                // console.log('pointermove', isDown, 1)
                if(lastPosX[1] == 0){
                    lastPosX[1] = pointer.x;
                }
                this.stack[1].x += pointer.x - lastPosX[1];
                travelDistance[1] += Math.abs(pointer.x - lastPosX[1]);
                lastPosX[1] = pointer.x;
                if(travelDistance[1] > 10){
                    this.tableMovement[1] = true;

                    // added by asset
                    
                }
            }
        }).on('pointerup', (pointer, localX, localY, event)=>{
            // console.log('pointerup', 1)
            lastPosX[1] = 0;
            if(isDown[1]){
                isDown[1] = false;
                
                this.correctPosition(1);
                this.detectEvent(pointer, event, 1);
            }
            travelDistance[1] = 0;
            if(this.tableMovement[1]){
                this.doInertiaMovement(velocityX[1], 1);
            }
            this.tableMovement[1] = false;

            // added by asset
           
        }).on('pointerout', (pointer, localX, localY, event)=>{
            lastPosX[1] = 0;
            if(isDown[1]){
                isDown[1] = false;
                // console.log('pointerout', 1)
                this.correctPosition(1);
                this.detectEvent(pointer, event, 1);
            }
            travelDistance[1] = 0;
            this.tableMovement[1] = false;
        })
        

        scene.events.on("velocity", (vx, vy)=>{
            velocityX[1] = vx;
        });
    }

    // added by asset

    setItems(items:SlotItem[], tableIndex: integer){
        this.items[tableIndex] = items;
        const columns = this.config.table.columns;
        // console.log("setItems", items)
        items.forEach((element, index) => {
            const cellWidth = this.config.table.cellWidth;
            const cellHeight = this.config.table.cellHeight;
            // console.log('foreach', index, element)
            // added by asset
            element.setDisplaySize(cellWidth, cellHeight, tableIndex === 1);
        });

        const itemH = items[0].getDisplayHeight();
        const itemW = items[0].getDisplayWidth();

        
        this.itemW[tableIndex] = itemW;
        this.itemH[tableIndex] = itemH;

        this.table[tableIndex].displayWidth = itemW * Math.ceil(items.length / columns);
        

        this.limitLeftX[tableIndex] = this.config.x;
        this.limitRightX[tableIndex] = - Math.max(this.table[tableIndex].displayWidth - this.config.width, 0) + this.config.x;
    
        if (tableIndex === 0) {
            items.forEach((element, index) => {
                const { is_shared, like_count, download_count, shared_date } = element.sloteItemInfo
                this.sloteItemsInfo[index] = { is_shared, like: like_count, download: download_count, shared_date }
            })
        } 

        var index = 0;
        items.forEach(element => {
            const col = index % columns;
            const x =  Math.floor(index / columns) * itemW;
            const y = col * itemH;
            var cellContainer = this.scene.add.container(x,y);
            if (tableIndex) {
                this.containerAry.push(cellContainer);
            }
            element.initBodyPart();
            let model_visibility = false;
            if(element.shareFlag){
                if (element.sloteItemInfo.is_shared){
                    model_visibility = true
                } else {
                    model_visibility = false
                }
                
            } else {
                model_visibility = true
            }
            cellContainer.add(element.stack);
            this.stack[tableIndex].add(cellContainer);
            element.bodyPartManager.bodyPartStack.setVisible(model_visibility);
            element.cloudBoard.setVisible(model_visibility);
            index ++;
        });
        
        this.stack[tableIndex].x = this.config.x;
       
        if ( tableIndex === 0 ) {
            // added by asset

            const { download, like, is_shared } = this.sloteItemsInfo[this.currentSloteIndex]
            const cloudBoardConfig = { 
                ...this.cloudBoardConfig,
                visibility: !this.tableMovement[0],
                counter: {
                    like, 
                    download,
                },
                is_shared,
                setShareFlag: () => { 
                    this.checkUserLoggedIn();
                    if (this.isLoggedIn) {
                        console.log("shared slote")
                        // const currentSlote = this.items[1][this.currentSloteIndex]
                        this.sloteItemsInfo[this.currentSloteIndex].is_shared = true; 
                        this.sloteItemsInfo[this.currentSloteIndex].like = 1; 
                        this.cloudBoard.counter.like = this.sloteItemsInfo[this.currentSloteIndex].like
                        // this.items[1][this.currentSloteIndex].counter.like = this.sloteItemsInfo[this.currentSloteIndex].like;
                        // this.items[1][this.currentSloteIndex].updateCloudButtons();
                        
                        this.sloteItemsInfo[this.currentSloteIndex].shared_date = new Date().toISOString();
                        // currentSlote.bodyPartManager.bodyPartStack.setVisible(true);
                        // currentSlote.like_button.setVisible(true);
                        // currentSlote.download_button.setVisible(true);
                        const sortedSharedSloteIndexs = this.getSortedSharedSloteIndexs(this.sortFlag);
                        this.sortSharedSlotes(sortedSharedSloteIndexs);

                        this.updateSloteItemData();
                        return true;
                    } else {
                        document.getElementById("btn-login").click();
                        return false;
                    }
                    
                },
                likeIncrement: () => {
                    // this.checkUserLoggedIn();
                    // if (this.isLoggedIn) {
                    //     const { like } = this.sloteItemsInfo[this.currentSloteIndex]
                    //     this.sloteItemsInfo[this.currentSloteIndex].like = like?like - 1:like + 1; 
                    //     this.cloudBoard.counter.like = this.sloteItemsInfo[this.currentSloteIndex].like
                    //     this.items[1][this.currentSloteIndex].counter.like = this.sloteItemsInfo[this.currentSloteIndex].like;
                    //     this.items[1][this.currentSloteIndex].updateCloudButtons();

                    //     if(this.sortFlag === 'like') {
                    //         const sortedSharedSloteIndexs = this.getSortedSharedSloteIndexs(this.sortFlag);
                    //         this.sortSharedSlotes(sortedSharedSloteIndexs);
                    //     }
                    //     this.updateSloteItemData();
                    // } else {
                    //     document.getElementById("btn-login").click();
                    // }
                    
                },
                downloadIncrement: () => {
                    this.checkUserLoggedIn();
                    if (this.isLoggedIn) {
                        const { download } = this.sloteItemsInfo[this.currentSloteIndex]
                        this.sloteItemsInfo[this.currentSloteIndex].download = download + 1; 
                        this.cloudBoard.counter.download = this.sloteItemsInfo[this.currentSloteIndex].download;
                        this.items[1][this.currentSloteIndex].counter.download = this.sloteItemsInfo[this.currentSloteIndex].download;
                        this.items[1][this.currentSloteIndex].updateCloudButtons()
                        if(this.sortFlag === 'download') {
                            const sortedSharedSloteIndexs = this.getSortedSharedSloteIndexs(this.sortFlag);
                            this.sortSharedSlotes(sortedSharedSloteIndexs);
                        }
                        this.updateSloteItemData();
                    } else {
                        document.getElementById("btn-login").click();
                    }
                    
                }
            }
            this.cloudBoard = new CloudIconButtonContainer(this.scene, cloudBoardConfig)

            const titleImageID = `${this.modelName}` // gonna use this variable as modal title image identifier
            this.titleImage = this.scene.add.image(0, 0, `${this.modelName}_Title`)
            this.titleImage.setOrigin(0, 0);
            // this.titleImage.setScale(.6)
            this.titleImage.setDisplaySize(itemW * 3 / 5, 70)
            this.titleImage.setPosition(itemW / 5, -20);
    
            this.headerStack.add(this.titleImage);

            this.cloudBoard.updateButton();

            
        } else {
            // added by asset
            const communityBoardConfig = { 
                ...this.communityBoardConfig,
                sortFlag: this.sortFlag,
                visibility: !this.tableMovement[1],
                setSortFlag: (flag) => {
                    this.sortFlag = flag;
                    this.communityBoard.sortFlag = this.sortFlag;
                    const sortedSharedSloteIndexs = this.getSortedSharedSloteIndexs(this.sortFlag);
                    console.log("communityBoardConfigsortedSharedSloteIndexs", sortedSharedSloteIndexs)
                    this.sortSharedSlotes(sortedSharedSloteIndexs);
                }
                
            }
            this.communityBoard = new CommuntyIconButtonContainer(this.scene, communityBoardConfig)

            // added by asset

            this.communitytitleImage = this.scene.add.image(0, 0, "UISaveslotCommunity")
            this.communitytitleImage.setOrigin(0, 0);
            // this.communitytitleImage.setScale(.4)
            this.communitytitleImage.setDisplaySize(this.config.x + this.overlapWidth, 70)
            this.communitytitleImage.setPosition(0, 10);
    
            this.communityHeaderStack.add(this.communitytitleImage);

            this.UImargin = this.scene.add.image(0, 0, "UISaveslotMiddle")
            this.UImargin.setOrigin(0, 0);
            this.UImargin.setScale(.6);
            this.UImargin.setPosition(0, 0);
            
            this.UImargin.setDisplaySize(this.config.width + this.config.x, 10)

            this.communityHeaderStack.add(this.UImargin);

           
            this.items[tableIndex].forEach((element, index) => {
                // console.log('updatecloudbutton', element)
                if (element.shareFlag && element.sloteItemInfo.is_shared) {
                    const { download, like } = this.sloteItemsInfo[index]
                    element.counter = { download, like }
                    element.updateCloudButtons();
                }
                
            })

            const sortedSharedSloteIndexs = this.getSortedSharedSloteIndexs(this.sortFlag);
            this.sortSharedSlotes(sortedSharedSloteIndexs);
            
        }

    }

    addItems(items:SlotItem[], tableIndex:integer){

        const columns = this.config.table.columns;
       
        items.forEach((element, index) => {
            const cellWidth = this.config.table.cellWidth;
            const cellHeight = this.config.table.cellHeight;

            // added by asset
            element.setDisplaySize(cellWidth, cellHeight, index % 2 === 1);
        });

        this.table[tableIndex].displayWidth += this.itemW[tableIndex] * Math.ceil(items.length / columns);

        this.limitLeftX[tableIndex] = this.config.x;
        this.limitRightX[tableIndex] = - Math.max(this.table[tableIndex].displayWidth - this.config.width, 0) + this.config.x;
        

        var index = this.items[tableIndex].length;
        items.forEach(element => {
            const col = index % columns;
            const x =  Math.floor(index / columns) * this.itemW[tableIndex];
            const y = col * this.itemH[tableIndex];
            var cellContainer = this.scene.add.container(x,y);
            element.initBodyPart();
            cellContainer.add(element.stack);
            this.stack[tableIndex].add(cellContainer);
            
            index ++;
        });

        this.items[tableIndex] = this.items[tableIndex].concat(items);
    }

    detectEvent(pointer:Phaser.Input.Pointer, event:any, tableIndex: integer){
        // console.log('detectEvent', tableIndex, this.tableMovement)
        if(!this.tableMovement[tableIndex]){
            this.items[tableIndex].forEach((element, cellIndex) => {
                if(element.shelfSprite.getBounds().contains(pointer.x, pointer.y)){
                    this.checkUserLoggedIn();
                    if (tableIndex) {
                        if (this.sloteItemsInfo[cellIndex].is_shared) {
                            this.table[tableIndex].emit("cell.click", element.tabIndex);
                        }
                    } else {
                        if (this.isLoggedIn) {
                            this.table[tableIndex].emit("cell.click", element.tabIndex);
                        } else {
                            document.getElementById("btn-login").click();
                        }
                    }
                }
            });
        }
    }

    correctPosition(tableIndex: integer){

        // console.log('correctPosition', tableIndex)
        if(this.config.scrollMode == 1){
            if(this.stack[tableIndex].x > this.limitLeftX[tableIndex]){
                this.scrollTo(this.limitLeftX[tableIndex], tableIndex);
            } else if(this.stack[tableIndex].x < this.limitRightX[tableIndex]){
                this.scrollTo(this.limitRightX[tableIndex], tableIndex);
            } else{
                if( this.itemW && this.config.width ) {
                   
                    const gapIndex = Math.round((this.stack[tableIndex].x  - (this.config.x + this.overlapWidth)) / this.itemW[tableIndex]);
                   
                    const posX = this.itemW[tableIndex] * gapIndex + this.config.x;
                    this.scrollTo(posX, tableIndex);
                }
            }
        }
    }
    

    // Original fn: modified by asset

    // correctPosition(){

    //     if(this.config.scrollMode == 1){
    //         if(this.stack.x > this.limitLeftX){
    //             this.scrollTo(this.limitLeftX);
    //         }
    //         if(this.stack.x < this.limitRightX){
    //             this.scrollTo(this.limitRightX);
    //         }
    //     }
    // }

    scrollTo(targetPosX:number, tableIndex: integer){
        // console.log('scrollTo', tableIndex)
        var frames = 15;
        const unit = (targetPosX - this.stack[tableIndex].x)/frames;
        // console.log('unit', unit, targetPosX, tableIndex)
        if(unit == 0){
            return;
        }
        var duration = 100;
        var delay = duration/frames;
        if(this.timer) this.timer.destroy();
        this.timer = this.scene.time.addEvent({
            delay: delay,
            callback: ()=>{
                if(duration > 0){
                    // console.log('scrollTo', duration)
                    this.stack[tableIndex].x += unit;
                    duration -= delay;
                } else {
                    if (tableIndex === 0) {
                        this.tableMovement[0] = false;

                        // added by asset
                        if ( this.cloudBoard ) {
                            this.cloudBoard.board.setVisible(!this.tableMovement[0])
                        }
                    }
                }
            },
            repeat: frames
        });
        
    }

    scrollToLeft(flag:boolean, tableIndex:integer){
        if(flag){
            this.scrollTo(this.limitLeftX[tableIndex], tableIndex);
        }
    }

    moveByX(dx:integer, tableIndex:integer){
        this.stack[tableIndex].x += dx;
        this.table[tableIndex].x += dx;
    }

    setVisible(visible:boolean, tableIndex:integer){
        this.stack[tableIndex].setVisible(visible);
        this.table[tableIndex].setVisible(visible);
    }

    dataChanged(tableIndex:integer){
        this.scrollTo(this.limitRightX[tableIndex], tableIndex);
    }


    doInertiaMovement(v:number, tableIndex:integer){
        // console.log('doInertiaMovement', tableIndex)
        var steps = 0;
        if(this.smoothMovTimer != undefined) this.smoothMovTimer.remove();
        var velocity = v;
        this.smoothMovTimer = this.scene.time.addEvent({
            delay: 10,
            callback: ()=>{
                velocity -=  velocity / 10
                this.stack[tableIndex].x += velocity;
                if(velocity < 1 && velocity > -1) this.smoothMovTimer.remove();
                if(this.stack[tableIndex].x > this.limitLeftX[tableIndex]){
                    this.smoothMovTimer.remove();
                    this.correctPosition(tableIndex);
                } else if(this.stack[tableIndex].x < this.limitRightX[tableIndex]){
                    this.smoothMovTimer.remove();
                    this.correctPosition(tableIndex);
                } else{
                    if( this.itemW && this.config.width ) {
                   
                        const gapIndex = Math.round((this.stack[tableIndex].x  - (this.config.x + this.overlapWidth)) / this.itemW[tableIndex]);
                        if( this.currentSloteIndex !== gapIndex && tableIndex === 0 ) {
                            this.currentSloteIndex = Math.abs(gapIndex);
                            this.cloudBoard.is_shared = this.sloteItemsInfo[this.currentSloteIndex].is_shared
                            const { download, like } = this.sloteItemsInfo[this.currentSloteIndex]
                            console.log('this.currentSloteIndex', this.currentSloteIndex, download, like)
                            this.cloudBoard.counter = { download, like }
                            this.cloudBoard.updateButton()
                        }
                        const posX = this.itemW[tableIndex] * gapIndex + this.config.x;
                        this.scrollTo(posX, tableIndex); 
                    }
                }
                steps ++;
            },
            loop: true 
        });
    }  

    sortSharedSlotes(sort_indexs: any[]){
        sort_indexs.forEach((e, i) => {
            this.containerAry[e.index].x = Math.floor(i / this.config.table.columns)*this.itemW[1] 
        })
    }

    getSortedSharedSloteIndexs(sort_flag: string){
        
        const sort_indexs = this.sloteItemsInfo.map((e, index) => {
            return { index, ...e }
        })
        
        
        switch(sort_flag) {
            case "random": 
                return sort_indexs.sort((a, b) => { return !a.is_shared && b.is_shared?1 : -1});
            case "like": 
                return sort_indexs.sort((a, b) => { 
                    if (b.is_shared) {
                        if (a.is_shared) {
                            return a.like < b.like?1:-1;
                        } else {
                            return 1;
                        }
                    } else {
                        return -1;
                    }
                })
            case "download": 
                return sort_indexs.sort((a, b) => { 
                    if (b.is_shared) {
                        if (a.is_shared) {
                            return a.download < b.download?1:-1;
                        } else {
                            return 1;
                        }
                    } else {
                        return -1;
                    }
                })
            case "date": 
                return sort_indexs.sort((a, b) => { 
                    if (b.is_shared) {
                        if (a.is_shared) {
                            return a.shared_date < b.shared_date?1:-1;
                        } else {
                            return 1;
                        }
                    } else {
                        return -1;
                    }
                })
            default:
                return sort_indexs.sort((a, b) => { return !a.is_shared && b.is_shared?1 : -1});
        }
    }

}