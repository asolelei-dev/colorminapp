
class MaterialGridTable {
    scene:Phaser.Scene;
    items:MaterialButton[];
    childBodyParts:ChildBodyPartButton[];
    stack:Phaser.GameObjects.Container;
    table: Phaser.GameObjects.Image;
    config:any;
    limitTopY:number;
    limitBottomY:number;
    timer: Phaser.Time.TimerEvent;
    tableMovement:boolean;
    graphics: Phaser.GameObjects.Graphics;
    smoothMovTimer: any;
    tween:any;
    constructor(scene:Phaser.Scene, config:any){
        this.table = config.background;

        this.table.setPosition(config.x, config.y);
        this.table.displayHeight = config.height;
        this.table.displayWidth = config.width;
        this.table.setOrigin(0,0);
        this.table.tint = 0x000000;
        this.table.alpha = 0.01
        this.stack = scene.add.container(config.x, config.y);
        this.stack.depth = 11;

        this.bindEvent(scene);
        this.scene = scene;
        this.config = config;
        this.tableMovement = false;

    }

    bindEvent(scene:Phaser.Scene){
        var isDown = false;
        var lastPosY = 0;
        var travelDistance = 0;
        var velocityY = 0;

        this.table.setInteractive().on('pointerdown', (pointer, localX, localY, event)=>{
            isDown = true;
        }).on('pointermove', (pointer, localX, localY, event)=>{
            if(isDown){
                if(lastPosY == 0){
                    lastPosY = pointer.y;
                }
                this.stack.y += pointer.y - lastPosY;
                travelDistance += Math.abs(pointer.y - lastPosY);
                lastPosY = pointer.y;
                if(travelDistance > 10){
                    this.tableMovement = true;
                }
            }
        }).on('pointerup', (pointer, localX, localY, event)=>{
            lastPosY = 0;
            if(isDown){
                isDown = false;
                this.correctPosition();
                this.detectEvent(pointer, event);
            }
            travelDistance = 0;
            if(this.tableMovement){
                this.doInertiaMovement(velocityY);
            }
            this.tableMovement = false;
        }).on('pointerout', (pointer, localX, localY, event)=>{
            lastPosY = 0;
            if(isDown){
                isDown = false;
                this.correctPosition();
                this.detectEvent(pointer, event);
            }
            travelDistance = 0;
            this.tableMovement = false;
        })

        scene.events.on("velocity", (vx, vy)=>{
            velocityY = vy;
        });
    }

    setItems(items:any[], childBodyParts:any[] = []){


        this.items = items;
        this.childBodyParts = childBodyParts;

        var index = 0;
        
        this.stack.removeAll();

        var sizeVal = 0;
        items.forEach(element => {
            const col = index % this.config.table.columns;
            sizeVal = (this.config.scrollMode == 0) ? this.config.table.cellHeight:this.config.table.cellWidth;
            const x = col * sizeVal + 0.5 * this.config.width;
            const y = Math.floor(index / this.config.table.columns) * sizeVal + 0.5 * sizeVal;
            var cellContainer = this.scene.add.container(x,y);
            cellContainer.add(element.stack);
            element.stack.setInteractive
            this.stack.add(cellContainer);
            index ++;
        });
        
        // var graphic = this.scene.add.graphics();
        // graphic.lineStyle(5, 0xFF00FF, 1.0);
        // graphic.fillStyle(0xFFFFFF, 1.0);
        // graphic.fillRect(50, 50, 400, 200);        
        //graphics.strokeRect(50, 50, 400, 200);
        if(items.length > 0 && childBodyParts.length > 0){
                    //make spacer
                    const col = index % this.config.table.columns;
                    sizeVal = (this.config.scrollMode == 0) ? this.config.table.cellHeight:this.config.table.cellWidth;
                    const x = col * sizeVal + 0.5 * this.config.width;
                    const y = Math.floor(index / this.config.table.columns) * sizeVal + 0.1 * sizeVal;
                    var cellContainer = this.scene.add.container(x,y);
                    let spliter_img = this.scene.add.image(0,0,"tab-splitter");
                    spliter_img.displayWidth = items[0].tabImage.displayWidth;
                    spliter_img.displayHeight = 10;

                    //var graphics = this.scene.add.graphics();
                    // graphics.fillRect(0,0,items[0].tabImage.displayWidth,5);
                    // graphics.fillGradientStyle(0,0,0,0);
                    // graphics.fillStyle(0x000000);
                    //graphics.lineGradientStyle(items[0].tabImage.displayWidth, 0xff0000, 0xff0000, 0x0000ff, 0x0000ff, 1);
                    
                    //graphics.lineBetween(0, 0, 0 , 5);
                    cellContainer.add(spliter_img);
                    this.stack.add(cellContainer);
        }


        // set child bodyparts continuously
        childBodyParts.forEach(element => {
            const col = index % this.config.table.columns;
            sizeVal = (this.config.scrollMode == 0) ? this.config.table.cellHeight:this.config.table.cellWidth;
            const x = col * sizeVal + 0.5 * this.config.width;
            const y = Math.floor(index / this.config.table.columns) * sizeVal + 0.7 * sizeVal;
            var cellContainer = this.scene.add.container(x,y);
            cellContainer.add(element.stack);
            element.stack.setInteractive
            this.stack.add(cellContainer);
            index ++;
        });


        if(this.config.scrollMode == 0){
            this.table.displayHeight = Math.ceil(sizeVal * (items.length + childBodyParts.length) / this.config.table.columns);
            this.limitTopY = this.config.y;
            this.limitBottomY = - Math.max(this.table.displayHeight - this.config.height, 0) + this.config.y;
        }

        this.stack.y = this.config.y;

    }

    detectEvent(pointer:Phaser.Input.Pointer, event:any){
        if(!this.tableMovement){

                this.items.forEach(element => {
                    if(element.tabImage.getBounds().contains(pointer.x, pointer.y)){
                   
                        this.table.emit("cell.click", element.tabIndex);
                    }
                });

                this.childBodyParts.forEach(element => {
                    if(element.image.getBounds().contains(pointer.x, pointer.y)){
                        this.table.emit("cell.child.click", element.bodyPart.modelPart.feature.part_id);                    
                    }
                });
            
        }
    }

    correctPosition(){
        if(this.config.scrollMode == 0){
            if(this.stack.y > this.limitTopY){
                this.scrollTo(this.limitTopY);
            }
            if(this.stack.y < this.limitBottomY){
                this.scrollTo(this.limitBottomY);
            }
        }
    }

    
    scrollTo(targetPosY:number){

        if(this.tween) this.tween.remove();
        this.tween = this.scene.tweens.add({
            targets: this.stack,
            y: targetPosY,
            // alpha: { start: 0, to: 1 },
            // alpha: 1,
            // alpha: '+=1',
            ease: 'Back.easeIn',       // 'Cubic', 'Elastic', 'Bounce', 'Back'
            duration: 300,
            repeat: 0,            // -1: infinity
            yoyo: false
        });
        // var frames = 15;
        // const unit = (targetPosY - this.stack.y)/frames;
        // if(unit == 0){
        //     return;
        // }
        // var duration = 100;
        // var delay = duration/frames;
        // if(this.timer) this.timer.destroy();
        // this.timer = this.scene.time.addEvent({
        //     delay: delay,
        //     callback: ()=>{
        //         if(duration > 0){
        //             this.stack.y += unit;
        //             duration -= delay;
        //         }
        //     },
        //     repeat: frames
        // });
        
    }

    scrollToTop(flag:boolean){
        if(flag){
            this.scrollTo(this.limitTopY);
        }
    }

    moveByX(dx:integer){
        this.stack.x += dx;
        this.table.x += dx;
    }

    setPosition(pos:any){
        if(pos.x != undefined){
            this.stack.x = pos.x;
            this.table.x = pos.x;
            this.config.x = pos.x;
        }
        if(pos.y != undefined){
            this.stack.y = pos.y;
            this.table.y = pos.y;
            this.config.y = pos.y;
            this.limitTopY = this.stack.y;
            this.limitBottomY = - Math.max(this.table.displayHeight - this.config.height, 0) + this.stack.y;
        }
    }

    setVisible(visible:boolean){
        this.stack.setVisible(visible);
        this.table.setVisible(visible);
    }


    doInertiaMovement(v:number){
        if(this.smoothMovTimer != undefined) this.smoothMovTimer.remove();
        var velocity = v;
        this.smoothMovTimer = this.scene.time.addEvent({
            delay: 10,
            callback: ()=>{
                velocity -=  velocity / 10
                this.stack.y += velocity;
                if(this.stack.y > this.limitTopY){
                    this.smoothMovTimer.remove();
                    this.correctPosition();
                }
                if(this.stack.y < this.limitBottomY){
                    this.smoothMovTimer.remove();
                    this.correctPosition();
                }
            },
            loop: true
        });
    }

}