class CategoryGridTable{
    scene:Phaser.Scene;
    items:CategoryItem[];
    stack:Phaser.GameObjects.Container;
    table: Phaser.GameObjects.Image;
    config:any;
    timer: Phaser.Time.TimerEvent;
    tableMovement:boolean;
    graphics: Phaser.GameObjects.Graphics;
    limitLeftX: any;
    limitRightX: any;
    smoothMovTimer: any;
    constructor(scene:Phaser.Scene, config:any){
        this.table = config.background;

        this.table.setPosition(0, 0);
        this.table.displayHeight = config.height;
        this.table.displayWidth = config.width;
        this.table.setOrigin(0,0).setDepth(22).setAlpha(0.01);
        this.stack = scene.add.container(config.x, config.y).setDepth(22);
        this.stack.add(this.table)
        this.bindEvent(scene);
        this.scene = scene;
        this.config = config;
        this.tableMovement = false;

    }

    bindEvent(scene:Phaser.Scene){
        var isDown = false;
        var lastPosX = 0;
        var travelDistance = 0;
        var velocityX = 0;

        this.table.setInteractive().on('pointerdown', (pointer, localX, localY, event)=>{
            isDown = true;
        }).on('pointermove', (pointer, localX, localY, event)=>{
            if(isDown){
                if(lastPosX == 0){
                    lastPosX = pointer.x;
                }
                this.stack.x += pointer.x - lastPosX;
                travelDistance += Math.abs(pointer.x - lastPosX);
                lastPosX = pointer.x;
                if(travelDistance > 10){
                    this.tableMovement = true;
                }
            }
        }).on('pointerup', (pointer, localX, localY, event)=>{
            lastPosX = 0;
            if(isDown){
                isDown = false;
                this.correctPosition();
                this.detectEvent(pointer, event);
            }
            travelDistance = 0;
            if(this.tableMovement){
                this.doInertiaMovement(velocityX);
            }
            this.tableMovement = false;
        }).on('pointerout', (pointer, localX, localY, event)=>{
            lastPosX = 0;
            if(isDown){
                isDown = false;
                this.correctPosition();
                this.detectEvent(pointer, event);
            }
            travelDistance = 0;
            this.tableMovement = false;
        })

        scene.events.on("velocity", (vx, vy)=>{
            velocityX = vx;
        });
    }

    setItems(items:CategoryItem[]){
        this.items = items;
        var index = 0;
        var twidth = 0;
        items.forEach(element => {
            element.index = index;
            const itemH = items[index].getDisplayHeight();
            const itemW = items[index].getDisplayWidth();
            const x = twidth + itemW/2 + 20;
            const y = itemH / 2;
            var cellContainer = this.scene.add.container(x,y);
            cellContainer.add(element.stack);
            this.stack.add(cellContainer);
            index ++;
            twidth += itemW + 20;
        });
        
        if(this.config.scrollMode == 1){
            this.limitLeftX = this.config.x;
            this.table.displayWidth = twidth;
            this.limitRightX = - Math.max(twidth + 20 - this.config.width, 0) + this.config.x;

            // this.table.displayWidth = LayoutContants.getInstance().SCREEN_WIDTH;
        }
    }

    detectEvent(pointer:Phaser.Input.Pointer, event:any){
        if(!this.tableMovement){
            this.items.forEach(element => {
                if(element.itemImage.getBounds().contains(pointer.x, pointer.y)){
                    this.table.emit("cell.click", element.index);
                }
            });
        }
    }

    correctPosition(){

        if(this.config.scrollMode == 1){
            if(this.stack.x > this.limitLeftX){
                this.scrollTo(this.limitLeftX);
            }
            if(this.stack.x < this.limitRightX){
                this.scrollTo(this.limitRightX);
            }
        }
    }

    scrollTo(targetPosX:number){
        var frames = 15;
        const unit = (targetPosX - this.stack.x)/frames;
        if(unit == 0){
            return;
        }
        var duration = 100;
        var delay = duration/frames;
        if(this.timer) this.timer.destroy();
        this.timer = this.scene.time.addEvent({
            delay: delay,
            callback: ()=>{
                if(duration > 0){
                    this.stack.x += unit;
                    duration -= delay;
                }
            },
            repeat: frames
        });
        
    }

    moveByX(dx:integer){
        this.stack.x += dx;
        this.table.x += dx;
    }

    setVisible(visible:boolean){
        this.stack.setVisible(visible);
        this.table.setVisible(visible);
    }

    setMoveByY(dy:integer){
        this.stack.y += dy;
        this.table.y += dy;
    }


    doInertiaMovement(v:number){
        var steps = 0;
        if(this.smoothMovTimer != undefined) this.smoothMovTimer.remove();
        var velocity = v;
        this.smoothMovTimer = this.scene.time.addEvent({
            delay: 10,
            callback: ()=>{
                velocity -=  velocity / 10
                this.stack.x += velocity;
                if(velocity < 1 && velocity > -1) this.smoothMovTimer.remove();
                if(this.stack.x > this.limitLeftX){
                    this.smoothMovTimer.remove();
                    this.correctPosition();
                }

                if(this.stack.x < this.limitRightX){
                    this.smoothMovTimer.remove();
                    this.correctPosition();
                }
                steps ++;
            },
            loop: true
        });
    }  
}