
class TmenuGridTable {
    scene:Phaser.Scene;
    items:TMenuButton[];
    stack:Phaser.GameObjects.Container;
    table: Phaser.GameObjects.Image;
    config:any;
    timer: Phaser.Time.TimerEvent;
    tableMovement:boolean;
    graphics: Phaser.GameObjects.Graphics;
    limitLeftX: any;
    limitRightX: any;
    smoothMovTimer: any;
    tween:any;
    constructor(scene:Phaser.Scene, config:any){
        this.table = config.background;

        this.table.setPosition(config.x, config.y);
        this.table.displayHeight = config.height;
        this.table.displayWidth = config.width;
        this.table.setOrigin(0,0).setDepth(22);
        this.stack = scene.add.container(config.x, config.y).setDepth(22);

        this.bindEvent(scene);
        this.scene = scene;
        this.config = config;
        this.tableMovement = false;

    }

    bindEvent(scene:Phaser.Scene){
        var isDown = false;
        var lastPosX = 0;
        var travelDistance = 0;
        var velocityX = 0;

        this.table.setInteractive().on('pointerdown', (pointer, localX, localY, event)=>{
            isDown = true;
        }).on('pointermove', (pointer, localX, localY, event)=>{
            if(isDown){
                if(lastPosX == 0){
                    lastPosX = pointer.x;
                }
                this.stack.x += pointer.x - lastPosX;
                travelDistance += Math.abs(pointer.x - lastPosX);
                lastPosX = pointer.x;
                if(travelDistance > 10){
                    this.tableMovement = true;
                }
            }
        }).on('pointerup', (pointer, localX, localY, event)=>{
            lastPosX = 0;
            if(isDown){
                isDown = false;
                this.correctPosition();
                this.detectEvent(pointer, event);
            }
            
            if(this.tableMovement){
                this.doInertiaMovement(velocityX);
            }
            travelDistance = 0;
            this.tableMovement = false;
        }).on('pointerout', (pointer, localX, localY, event)=>{
            lastPosX = 0;
            if(isDown){
                isDown = false;
                this.correctPosition();
                this.detectEvent(pointer, event);
            }
            travelDistance = 0;
            this.tableMovement = false;
        })

        scene.events.on("velocity", (vx, vy)=>{
            velocityX = vx;
        });
    }

    setPosition(pos:any){
        if(pos.x != undefined){
            this.stack.x = pos.x;
            this.table.x = pos.x;
        }
        if(pos.y != undefined){
            this.stack.y = pos.y;
            this.table.y = pos.y;
        }
    }

    setItems(items:TMenuButton[]){
        this.items = items;
     
        const columns = this.config.table.columns;
        const width = this.config.width;
        const height = this.config.height;

      

        // get first element of array        
        let firstelement = null;

      
        items.forEach(element => {
            
            firstelement = element;
            return;
        });

        const itemH = firstelement.getDisplayHeight();
        const itemW = firstelement.getDisplayWidth();
        var paddingTop = 0;
        var marginLeft = 0;

        var index = 0;
        items.forEach(element => {
            
            element.index = index;
            const col = index % columns;
            const x = Math.floor(index / columns) * itemW + itemW/2;
            const y = col * itemH + itemH/2;
            var cellContainer = this.scene.add.container(x,y);
            cellContainer.add(element.stack);
            this.stack.add(cellContainer);
            index ++;
        });
        
        if(this.config.scrollMode == 1){
            this.table.displayWidth = Math.ceil(itemW * index / this.config.table.columns);
            this.limitLeftX = this.config.x;
            this.limitRightX = - Math.max(this.table.displayWidth - this.config.width, 0) + this.config.x;
           
            this.table.displayWidth = LayoutContants.getInstance().SCREEN_WIDTH;
        }
    }

    detectEvent(pointer:Phaser.Input.Pointer, event:any){
        if(!this.tableMovement){
            this.items.forEach(element => {
                if(element.image.getBounds().contains(pointer.x, pointer.y)){
                    
                    this.table.emit("cell.click", element.bodyPart.modelPart.tMenuOrder);
                }
            });
        }
    }

    correctPosition(){

        if(this.config.scrollMode == 1){
            if(this.stack.x > this.limitLeftX){
                this.scrollTo(this.limitLeftX);
            }
            if(this.stack.x < this.limitRightX){
                this.scrollTo(this.limitRightX);
            }
        }
    }

    scrollTo(targetPosX:number){
        // var frames = 15;
        // const unit = (targetPosX - this.stack.x)/frames;
        // if(unit == 0){
        //     return;
        // }
        // var duration = 100;
        // var delay = duration/frames;
        // if(this.timer) this.timer.destroy();
        // this.timer = this.scene.time.addEvent({
        //     delay: delay,
        //     callback: ()=>{
        //         if(duration > 0){
        //             this.stack.x += unit;
        //             duration -= delay;
        //         }
        //     },
        //     repeat: frames
        // });

        if(this.tween) this.tween.remove();
        this.tween = this.scene.tweens.add({
            targets: this.stack,
            x: targetPosX,
            // alpha: { start: 0, to: 1 },
            // alpha: 1,
            // alpha: '+=1',
            ease: 'Back.easeIn',       // 'Cubic', 'Elastic', 'Bounce', 'Back'
            duration: 300,
            repeat: 0,            // -1: infinity
            yoyo: false
        });
        
    }

    moveByX(dx:integer){
        this.stack.x += dx;
        this.table.x += dx;
    }

    setVisible(visible:boolean){
        this.stack.setVisible(visible);
        this.table.setVisible(visible);
    }

    setMoveByY(dy:integer){
        this.stack.y += dy;
        this.table.y += dy;
    }
    getTableY():number{
        return this.stack.y;
    }


    doInertiaMovement(v:number){
        var steps = 0;
        if(this.smoothMovTimer != undefined) this.smoothMovTimer.remove();
        var velocity = v;

        // if(this.tween) this.tween.remove();
        // this.tween = this.scene.tweens.add({
        //     targets: this.stack,
        //     x: '+'+v,
        //     // alpha: { start: 0, to: 1 },
        //     // alpha: 1,
        //     // alpha: '+=1',
        //     ease: 'Back.easeOut',       // 'Cubic', 'Elastic', 'Bounce', 'Back'
        //     duration: 300,
        //     repeat: 0,            // -1: infinity
        //     yoyo: false
        // });
 
        this.smoothMovTimer = this.scene.time.addEvent({
            delay: 8,
            callback: ()=>{
                velocity -=  velocity / 9
                this.stack.x += velocity;
                if(velocity < 1 && velocity > -1) this.smoothMovTimer.remove();
                if(this.stack.x > this.limitLeftX){
                    this.smoothMovTimer.remove();
                    this.correctPosition();
                }

                if(this.stack.x < this.limitRightX){
                    this.smoothMovTimer.remove();
                    this.correctPosition();
                }
                steps ++;
            },
            loop: true
        });
    }  

}