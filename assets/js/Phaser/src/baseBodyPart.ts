
class BaseBodyPart {
    scene: SaveSloteScene;
    bodyPartsManager: BaseBodyPartManager;
    modelPart: ModelPart;
    bodyPartIndex: number;
    material:Material;
    claySprite:Phaser.GameObjects.Sprite;
    glossSprite:Phaser.GameObjects.Sprite;
    color:Phaser.Display.Color;
    frame:integer;

    constructor(scene:SaveSloteScene, bodyPartsManager:BaseBodyPartManager, modelPart:ModelPart, bodyPartIndex:integer, frame:integer){
        this.scene = scene;
        this.bodyPartsManager = bodyPartsManager;
        this.modelPart = modelPart;
        this.bodyPartIndex = bodyPartIndex;
        this.material = modelPart.materials[0];
        this.frame = frame;
        this.setRegularSprites();
    }

    

    setSpriteFromMaterial() {

        if (this.material == undefined) {
            return;
        }

        if (this.material.clayRegion != undefined && this.material.clayRegion != "") {
            this.setClaySpriteFromMaterial();
        }

        if (this.material.glossRegion != undefined && this.material.glossRegion != "") {
            this.setGlossSpriteFromMaterial();
        }

        if (this.material.glossAlpha != undefined) {
            const glossAlpha = this.material.glossAlpha;
            if (this.glossSprite != undefined) {
                this.glossSprite.setAlpha(glossAlpha);
            }
        }
    }

    setClaySpriteFromMaterial(){
        if (this.material.clayRegion.includes(NOTHING)) {
            if(this.claySprite != undefined ) this.claySprite.destroy();
            this.claySprite = undefined;
            return;
        }
        
        const clayRegion = `${this.material.clayRegion}${this.frame}`;
        this.claySprite = this.bodyPartsManager.setSprite(clayRegion, this.modelPart, this.claySprite);


    }

    setGlossSpriteFromMaterial(){

        if (this.material.glossRegion.includes(NOTHING)) {
            if(this.glossSprite != undefined) this.glossSprite.destroy();
            this.glossSprite = undefined;
            return;
        }

        if (this.material.glossRegion.includes(FADE)) {
            const newFadeImage = `${this.material.glossRegion}`;
            this.bodyPartsManager.setBackGround(newFadeImage);
            return;
        }
        
        const glossRegion = `${this.material.glossRegion}${this.frame}`;
        this.glossSprite = this.bodyPartsManager.setSprite(glossRegion, this.modelPart, this.glossSprite);

    }

    setColor(color:Phaser.Display.Color){
        if(this.claySprite){
            this.claySprite.tint = Phaser.Display.Color.GetColor(color.red, color.green, color.blue); 
        }
    }

    setColorVal(color:number){
        if(this.claySprite){
            this.claySprite.tint = color; 
        }
    }

    setRegularSprites() {
        if (!this.modelPart.partBaseImage.includes(NOTHING)) {
            if(this.modelPart.partBaseImage != ""){
                const clayRegionName = `${this.modelPart.partBaseImage}${this.frame}`;
                this.claySprite = this.bodyPartsManager.setSprite(clayRegionName, this.modelPart, this.claySprite);    
            }
        }

        if (!this.modelPart.partGlossImage.includes(NOTHING) && !this.modelPart.partGlossImage.includes(BACKGROUND)) {
            
            if(this.modelPart.partGlossImage != "" && !this.modelPart.partGlossImage.includes(FADE)){

                const glossRegionName = `${this.modelPart.partGlossImage}${this.frame}`;
                this.glossSprite = this.bodyPartsManager.setSprite(glossRegionName, this.modelPart, this.glossSprite);
            }
        }
    }

    setMaterial(tabIndex:integer){
        this.material = this.modelPart.materials[tabIndex];
        this.setSpriteFromMaterial();
        
    }

}