class SlotItem {

    callback:()=>void;

    shelfSprite:Phaser.GameObjects.Image;
    fadeBackgroundSprite:Phaser.GameObjects.Image;
    solidBackgroundSprite:Phaser.GameObjects.Image;
    bodyParts:BaseBodyPart[];
    scene: SaveSloteScene;
    stack:Phaser.GameObjects.Container;
    model:Model;
    scrollMode:integer;
    tabIndex:integer;
    bodyPartManager:BaseBodyPartManager;
    savedModel:SavedModel; 
    jsonData:JSON;
    sloteItemInfo: {
        user_id: string,
        model_name: string,
        like_count: integer,
        download_count: integer,
        slot_index: integer,
        is_shared: boolean,
        shared_date: string,
        feature: integer,
    };
    shareFlag: boolean;
    cloudBoard: Phaser.GameObjects.Container;//added by asset
    // cloudBoardConfig: { x: number, y: number, width: number, height: number } 

    downloadButton: CustomImageTextureButton;
    likeButton: CustomImageTextureButton;

    counter: { download: number, like: number }

    constructor(scene: SaveSloteScene, width: number, height:number, scrollMode:integer, model?:Model, jsonData?:JSON, sloteItemInfos?: any ){

        this.scene = scene;
        this.model = model;
        this.scrollMode = scrollMode;
        this.stack = new Phaser.GameObjects.Container(scene);
        this.jsonData = jsonData;
        
        const { sloteItemInfo, tabIndex, shareFlag } = sloteItemInfos;
        this.sloteItemInfo = sloteItemInfo;
        this.tabIndex = tabIndex;
        this.shareFlag = shareFlag;
        this.counter = { download: 0,  like: 0}

        this.initGroup(width, height, model);

        
    };

    initGroup(width:number, height:number,  model:Model){ 
        
        this.shelfSprite = this.scene.add.image(0, 0, "shelf_adjusted")
        this.shelfSprite.setOrigin(0,0);

       

        this.solidBackgroundSprite =  this.scene.add.image(0, 0, "solid")
        this.solidBackgroundSprite.setOrigin(0,0); 
        this.solidBackgroundSprite.displayHeight = this.shelfSprite.displayHeight;
        this.solidBackgroundSprite.displayWidth = this.shelfSprite.displayWidth;
        // this.solidBackgroundSprite.tint = 0x000000;

        this.fadeBackgroundSprite = this.scene.add.image(0, 0, "Fade0");
        this.fadeBackgroundSprite.displayHeight = this.shelfSprite.displayHeight;
        this.fadeBackgroundSprite.displayWidth = this.shelfSprite.displayWidth;
        this.fadeBackgroundSprite.setOrigin(0,0);
        this.fadeBackgroundSprite.setAlpha(0.5);

        // added by asset
        
        this.stack.add(this.solidBackgroundSprite);
        this.stack.add(this.fadeBackgroundSprite);
        this.stack.add(this.shelfSprite);

        this.cloudBoard = this.scene.add.container(0, 0);
        this.cloudBoard.depth = 499;
        
        this.likeButton = new CustomImageTextureButton(
            this.scene, 
            this.cloudBoard,
            0,
            0, 
            120,
            150,
            "Button_CloudLike", 
            this.counter.like, 
            ()=>{
            
            }
        );

        this.likeButton.setTopLeftOrigin(); 
        this.likeButton.setHighLightTexture("Button_CloudLike_Red")

        this.downloadButton = new CustomImageTextureButton(
            this.scene, 
            this.cloudBoard,
            0,  
            200, 
            120,
            150,
            "Button_CloudDownload", 
            this.counter.download, 
            ()=>{
            
            }
        );

        this.downloadButton.setTopLeftOrigin(); 
    
        this.stack.add(this.cloudBoard)
    }

    initBodyPart(){

        var width = this.shelfSprite.displayWidth;
        var height = this.shelfSprite.displayHeight;

        //init bodypart manager
        this.bodyPartManager = new BaseBodyPartManager(this.scene, this, this.scene.modelParts, width, height);

        this.bodyPartManager.setAtlasFront(`${this.model.modelName}-front`);
        this.bodyParts = this.bodyPartManager.createBodyParts(this.scene.firstFrame);
        this.bodyPartManager.setUtmostSprites();

        this.savedModel = new SavedModel(this.jsonData);
        this.bodyPartManager.setSavedProperties(this.savedModel); 
    }
    

    getDisplayWidth(){
        return this.shelfSprite.displayWidth * this.stack.scale;
    }
    getDisplayHeight(){
        return this.shelfSprite.displayHeight * this.stack.scale;
    }

    setDisplaySize(displayWidth:number, displayHeight:number, visibility:boolean = true){
        const scaleX = displayWidth/this.shelfSprite.displayWidth;
        const scaleY = displayHeight/this.shelfSprite.displayHeight;
        this.shelfSprite.setScale(scaleX, scaleY);

        this.solidBackgroundSprite.displayWidth = this.shelfSprite.displayWidth;
        this.solidBackgroundSprite.displayHeight = this.shelfSprite.displayHeight;

        this.fadeBackgroundSprite.displayWidth = this.shelfSprite.displayWidth;
        this.fadeBackgroundSprite.displayHeight = this.shelfSprite.displayHeight;

        // added by asset
        // this.cloudBoard.setVisible(false);
        this.likeButton.setVisible(visibility);
        this.downloadButton.setVisible(visibility);
        this.cloudBoard.setScale(0.3)
        this.cloudBoard.setPosition(displayWidth * 0.7, displayHeight * 0.2);
        this.cloudBoard.setAlpha(.5)
        
    }

    setModel(model:Model){
        this.model = model;
    }

    setFadeTexture(fadeTexture:string){
        this.fadeBackgroundSprite.setTexture(fadeTexture);
    }

    updateData(savedModel:SavedModel){
        this.savedModel =savedModel;
        this.bodyPartManager.setSavedProperties(this.savedModel);
    }

    updateCloudButtons(){
        this.likeButton.updateState(this.counter.like);
        this.likeButton.setStatus(!!this.counter.like);
        this.downloadButton.updateState(this.counter.download);
    }

}