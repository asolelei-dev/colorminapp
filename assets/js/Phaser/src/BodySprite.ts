class BodySprite extends Phaser.GameObjects.Sprite {
    systems:any;
    scene:ModelingScene;
    flashSprite:Phaser.GameObjects.Sprite;
    tIndex:integer;
    childIndex:integer;
    moving: boolean = false;
    feature: Feature;
    initedTouchEvent:boolean = false;
    originTint: any;
    timerout: NodeJS.Timeout;
    enabledFlash: boolean = true;
    constructor(scene: ModelingScene, x: number, y: number, texture: string, frame?: string | integer){
        super(scene, x, y, texture, frame);
        this.systems = scene.sys;
        this.scene  = scene;
        this.setName("bodypart sprite");
        
        // if(typeof frame === "string"){
        //     if(frame.includes("Clay")){
        //         // console.log(frame);
        //         // this.perfectPixelCallback = this.makePixelPerfect(1);
                
        //     }
        // }

    }

    linkTmenuButtonIndex(tIndex:integer){
        this.tIndex = tIndex;
    }
    linkChildBodyPartButtonIndex(childIndex:integer){
        this.childIndex = childIndex;
    }
    makePixcelPerfectTouchEvent(){
        if(parseInt(this.feature.paralax) != 1){
        
            if(this.initedTouchEvent) return;
            this.initedTouchEvent = true;

            

            // this.setInteractive({
            //     pixelPerfect: true,
            //     alphaTolerance :150
            // })
            // // .on("pointerdown", (pointer, localX, localY, event)=>{
            // //     this.scene.touchZonePointerDown(pointer);
            // //     LayoutContants.getInstance().setModelMoving(false);
            // // }).on("pointermove", (pointer, localX, localY, event)=>{
            // //     if(pointer.isDown){
            // //         this.scene.touchZonePointerMove(pointer, localX);
            // //         this.moving = true;
            // //     }
            // // })
            // .on("pointerup", (pointer, x, y, event)=>{

            //     console.log(x, y, this.texture.key, this.frame.name);
            //     console.log("______________________________________");

            //     // if(this.scene.uiGenerator.viewMode == CST.LAYOUT.SHOP_MODE || this.scene.uiGenerator.viewMode == CST.LAYOUT.SAVE_PREVIEW_MODE) return;
            //     // this.scene.touchZonePointerUp(pointer);
            //     // if(!LayoutContants.getInstance().modelMoving){
            //     //     this.flash();
            //     //     this.scene.uiGenerator.listenBodyPartTouchEvent(this.tIndex);
            //     // }
            //     // this.moving = false;
            // }).on("pointerout", ()=>{});


        }
    }

    checkPixelPerfectPos(x, y, scale){

        const scaleX = scale * this.scaleX;
        const scaleY = scale * this.scaleY;
        
        if(this.getBounds().contains(x,y)){
            const localX = (x - this.getBounds().x)/scaleX;
            const localy = (y - this.getBounds().y)/scaleY;
            var textureManager = this.systems.textures;
            var alpha = textureManager.getPixelAlpha(localX, localy, this.texture.key, this.frame.name);
            if(alpha > 150){
                if(this.scene.uiGenerator.viewMode == CST.LAYOUT.SHOP_MODE || this.scene.uiGenerator.viewMode == CST.LAYOUT.SAVE_PREVIEW_MODE) return;
                // this.scene.touchZonePointerUp(pointer);
                if(!LayoutContants.getInstance().modelMoving){
                    return this;
                }
            }
        }

        
        
        // const x = x1 - (this.x - this.displayWidth);
        // const y = y1 - (this.y - this.displayHeight);
        // console.log(x1, y1, x, y, this.texture.key, this.frame.name);

        

    }

    initflashSprite(){
        if(this.flashSprite != undefined){
            return;
        }
        const bound = this.getBounds();
        this.flashSprite = this.scene.add.sprite(bound.x, bound.y, this.texture.key, this.frame.name);
        this.flashSprite.displayWidth = bound.width;
        this.flashSprite.displayHeight = bound.height;
        this.flashSprite.setOrigin(0,0);
    }

    performSelection(){
        // this.initflashSprite();
        if(this.originTint == undefined) this.originTint = this.tint;
        this.flash();
        // this.scene.uiGenerator.listenBodyPartTouchEvent(this.tIndex);
        
    }

    CreatePixelPerfectHandler(textureManager, alphaTolerance){
        return function (x, y, gameObject)
        {
            var alpha = textureManager.getPixelAlpha(x, y, gameObject.texture.key, gameObject.frame.name);
            return (alpha && alpha >= alphaTolerance);
        };
    }

    makePixelPerfect(alphaTolerance:number)
    {
        if (alphaTolerance === undefined) { alphaTolerance = 1; }
        var textureManager = this.systems.textures;
        return this.CreatePixelPerfectHandler(textureManager, alphaTolerance);
    }

    flash(){

        // if(this.enabledFlash) this.tint = 0xffffff;
        this.timerout = setTimeout(() => {
            if(this.tIndex >= 0)
                this.scene.uiGenerator.listenBodyPartTouchEvent(this.tIndex);
            else{
                
                this.scene.uiGenerator.listenBodyPartTouchEvent(this.childIndex, true);
            }
        }, 100);
    }

    setFeature(feature:Feature){
        this.feature = feature;
    }
    setFlashEnable(flag:boolean){
        this.enabledFlash = flag;
    }

    resetColor(){
        this.tint = this.originTint;
    }

    setColor(color:number){
        this.originTint = color;
        this.tint = color;
    }
}