class BaseBodyPartManager {

    textures:string[];
    scene:SaveSloteScene;
    modelParts:ModelPart[];
    fadeTexture:string;
    backgroundTexture:string;
    bodyParts:BaseBodyPart[];
    atlasFront:string;
    averageX:number;
    averageY:number;
    upMostSprite:Phaser.GameObjects.Sprite;
    downMostSprite:Phaser.GameObjects.Sprite;
    leftMostSprite:Phaser.GameObjects.Sprite;
    rightMostSprite:Phaser.GameObjects.Sprite;
    modelWidth:number;
    modelHeight:number;
    savedModel:SavedModel;
    frame: number;
    VIEW_PORT_WIDTH:number;
    VIEW_PORT_HEIGHT:number;
    bodyPartStack:Phaser.GameObjects.Container;
    userScale: number = 1;
    yAdjust: number;
    xAdjust: number;
    sloteItem:any;

    constructor(scene:SaveSloteScene, sloteItem:any, modelParts:ModelPart[], width:number, height:number){

        this.textures = [];
        this.scene = scene;
        this.modelParts = modelParts;
        this.VIEW_PORT_WIDTH = width;
        this.VIEW_PORT_HEIGHT = height;
        this.setAverageXY();
        this.sloteItem = sloteItem;

        this.bodyPartStack = this.scene.add.container(width/2, height/2);
        this.sloteItem.stack.add(this.bodyPartStack);
        this.yAdjust = this.modelParts[1].yAdjust;
        this.xAdjust = this.modelParts[1].xAdjust;

        this.userScale = this.modelParts[1].scaleAdjust;
        if(this.userScale == 0) this.userScale = 1;

        this.sloteItem = sloteItem;
        
    }   

    setAtlasFront(atlasFront:string){
        this.atlasFront = atlasFront;
    }

    dispose(){

    }

    setAverageXY(){

        var total_X = 0;
        var total_Y = 0;
        var totals = 0;
        this.modelParts.forEach(element => {
            if(!element.partName.includes("Shadow")){
                totals += 1;
                total_X += +element.partPosX;
                total_Y += +element.partPosY;
            }
        });

        this.averageX = total_X / totals;
        this.averageY = total_Y / totals;
    }
    

    createBodyParts(frame:integer):BaseBodyPart[]{
        this.bodyParts = [];
        this.frame = frame;
        this.modelParts.forEach(modelPart => {
            const modelPartIndex = this.modelParts.indexOf(modelPart);
            var bodyPart = new BaseBodyPart(this.scene, this, modelPart, modelPartIndex, frame);
            this.bodyParts[modelPartIndex] = bodyPart;
        });

        return this.bodyParts;
    }

    setSprite(regionName:string, modelPart:ModelPart, target:Phaser.GameObjects.Sprite){
        var atlas = `${this.atlasFront}-SD`;
        const xSize =  +modelPart.partSizeX;
        const ySize =  +modelPart.partSizeY;
        const xPos = +modelPart.partPosX - this.averageX;
        const yPos = - +modelPart.partPosY + this.averageY;
        
        const x = xPos / LayoutContants.getInstance().SCREEN_HEIGHT_COEF;
        const y = yPos / LayoutContants.getInstance().SCREEN_HEIGHT_COEF;
        if(target == undefined){
            target = new Phaser.GameObjects.Sprite(this.scene, x, y, atlas, `${regionName}.png`);
            target.displayHeight = ySize / LayoutContants.getInstance().SCREEN_HEIGHT_COEF;
            target.displayWidth = xSize / LayoutContants.getInstance().SCREEN_HEIGHT_COEF;
            if(regionName.toLowerCase().includes("shadow")){
                this.bodyPartStack.addAt(target, 0);
            }else{

                if(!modelPart.feature.isChildren){
                    if(regionName.includes("Gloss")){
                        this.bodyPartStack.add(target);
                        target.setName(`${modelPart.feature.part_id}`);
                    }else{
                        this.bodyPartStack.add(target);
                    }
                }else{
                    const parentPart = this.bodyPartStack.getByName(`${modelPart.feature.parent_id}`);
                    const parentZindex = this.bodyPartStack.getIndex(parentPart);
                    this.bodyPartStack.addAt(target, parentZindex);
                }
            }
        }else{

            target.setTexture(atlas, `${regionName}.png`);
        }
        return target;
    }
    

    getBackgroundTexture():string{
        return this.backgroundTexture;
    }

    getFadeTexture():string{
        return this.fadeTexture;
    }

    setUtmostSprites(){

        var frame = this.frame;
        this.bodyPartStack.setPosition(this.VIEW_PORT_WIDTH/2,this.VIEW_PORT_HEIGHT/2);

        var minX = 0;
        var maxX = 0;
        var minY = 0;
        var maxY = 0;

        const bodyParts = this.bodyParts.slice(1, this.bodyParts.length - 1);
        
        bodyParts.forEach(element => {
            const sprite = element.claySprite;
            if(sprite != null){

                const tempLX =  sprite.x - sprite.displayWidth/2
                const tempRX =  sprite.x + sprite.displayWidth/2
    
                const tempBY =  sprite.y - sprite.displayHeight/2
                const tempTY =  sprite.y + sprite.displayHeight/2


                if(minX == 0){
                    minX = tempLX;
                    this.leftMostSprite = sprite;
                }else{
                    if(tempLX < minX){
                        minX = tempLX;
                        this.leftMostSprite = sprite;
                    }
                }

                if(maxX == 0){
                    maxX = tempRX;
                    this.rightMostSprite = sprite;
                }else{
                    if(tempRX > maxX){
                        maxX = tempRX;
                        this.rightMostSprite = sprite;
                    }
                }

                if(minY == 0){
                    minY = tempBY;
                    this.downMostSprite = sprite;
                }else{
                    if(tempBY < minY){
                        minY = tempBY;
                        this.downMostSprite = sprite;
                    }
                }

                if(maxY == 0){
                    maxY = tempTY;
                    this.upMostSprite = sprite;
                }else{
                    if(tempTY > maxY){
                        maxY = tempTY
                        this.upMostSprite = sprite;
                    }
                }

            }
        });
        
        this.modelHeight = maxY  - minY;
        this.modelWidth = maxX  - minX;

        var leftX = this.getLeftX();
        var rightX = this.getRightX();
        var bottomY = this.getBottomYByScreenMode();
        var topY = this.getTopY();

        var userScale = this.userScale * (bottomY - topY) / this.modelHeight ;
        // var userScale = this.userScale * Math.min((rightX - leftX) / this.modelWidth, (bottomY - topY) / this.modelHeight) * 0.8;
        // var userScale = this.userScale * (bottomY - topY) / this.modelHeight

        
        this.bodyPartStack.setScale(userScale);

        const deltaY = bottomY - (this.bodyPartStack.y + userScale * this.upMostSprite.y);
        this.bodyPartStack.y += deltaY;
        this.bodyPartStack.y += this.yAdjust/LayoutContants.getInstance().SCREEN_HEIGHT_COEF/2;
        this.bodyPartStack.x += this.xAdjust/LayoutContants.getInstance().SCREEN_SIZE_COEF/2;

    }

    setSavedProperties(savedModel:SavedModel){

        this.savedModel = savedModel;
        
        for (var i = 0; i < this.bodyParts.length - 1; i++) {
            this.setMaterialFromSavedModel(this.bodyParts[i]);
        }

        for (var i = 0; i < this.bodyParts.length - 1; i++) {
            this.setColorFromSavedModel(this.bodyParts[i]);
        }

    }


    setMaterialFromSavedModel(bodyPart:BaseBodyPart) {
        const modelPartName = bodyPart.modelPart.partName;
        if (this.savedModel.getActiveTabs().has(modelPartName)) {
            const tabIndex = this.savedModel.getActiveTabs().get(modelPartName);
            bodyPart.setMaterial(tabIndex);
            this.scene.getModelCharacteristics().setFinish(tabIndex);
        }
    }

    setColorFromSavedModel(bodyPart:BaseBodyPart) {
        const modelPartName = bodyPart.modelPart.partName;
        if ( this.savedModel.getPartColors().has(modelPartName)) {
            const customColorData = this.savedModel.getPartColors().get(modelPartName);

            var a = 1;
            var r = customColorData.r * 255;
            var g = customColorData.g * 255;
            var b = customColorData.b * 255;
            const customColor = Phaser.Display.Color.GetColor(r, g, b);
            
            if (bodyPart != null) {
                bodyPart.setColorVal(customColor);
            }

            if (bodyPart.bodyPartIndex == 0) {
                this.sloteItem.solidBackgroundSprite.tint = customColor;
            }
        }
    }

    setFrame(newframe:integer){
        this.frame = newframe;
    }

    getBottomYByScreenMode(){
        return (this.VIEW_PORT_HEIGHT - 80 / LayoutContants.getInstance().SCREEN_HEIGHT_COEF);
    }

    getTopY(){
        return LayoutContants.getInstance().BUTTONS_SIZE_BASE/2/LayoutContants.getInstance().SCREEN_HEIGHT_COEF;
    }

    getRightX(){
        return (this.VIEW_PORT_WIDTH - 10 / LayoutContants.getInstance().SCREEN_SIZE_COEF);
    }

    getLeftX(){
        return 10 / LayoutContants.getInstance().SCREEN_SIZE_COEF;
    }

    resetbodyPartStackScale(screenMode:integer){
        if(screenMode == CST.LAYOUT.SAVE_PREVIEW_MODE){
            var bottomY = this.getBottomYByScreenMode();
            this.userScale = this.modelParts[1].scaleAdjust;
            if(this.userScale == 0) this.userScale = 1;
            var userScale = this.userScale * (bottomY - LayoutContants.getInstance().BUTTONS_SIZE) / this.modelHeight ;
        }
    }

    setBackGround(fadeTexture:string){
        this.fadeTexture = fadeTexture;
        this.sloteItem.setFadeTexture(fadeTexture);
    }    
}