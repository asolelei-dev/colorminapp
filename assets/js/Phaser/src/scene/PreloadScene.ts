/// <reference path='../../phaser.d.ts'/>

class PreloadScene extends Phaser.Scene{
    key_A: Phaser.Input.Keyboard.Key;
    image: any;
    models_Arr: Array<Model> = new Array<Model>();
    soundFX: Phaser.Sound.BaseSound;
    LAYOUT_CONSTANT:LayoutContants = LayoutContants.getInstance();
    loadImage: Phaser.GameObjects.Image;
    
    constructor(){
        super({key: CST.SCENES.PRELOAD});
    }
    preload(){
        //if(Global.getInstance().checkLogoView())
        this.loadLogoImages();
    }
    create(){
        this.scene.start(CST.SCENES.LAUNCH);
    }

    loadLogoImages(){
        this.load.image("logoFrame1", "assets/img/bootscreen/logo/Anim_Top1.png");
        this.load.image("logoFrame2", "assets/img/bootscreen/logo/Anim_Top2.png");
        this.load.image("tab-splitter", "assets/img/TabSplitter.png");
        this.load.multiatlas('logo','assets/img/bootscreen/logo/logo.json', 'assets/img/bootscreen/logo');
        this.load.audio('button-2', ['assets/sounds/button-2.mp3']);
    }
}