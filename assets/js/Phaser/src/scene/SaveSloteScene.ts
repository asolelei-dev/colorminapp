

class SaveSloteScene extends Phaser.Scene {
    model:Model;
    fileNames:FileNames;
    atlasFront:string;
    atlasFrames:string;
    modelParts:ModelPart[];
    isFileLoaded:boolean;
    firstFrame:number;
    minFrame:number;
    maxFrame:number;
    modelCharacteristics:ModelCharacteristics;
    slotsItems:SlotItem[];
    LAYOUT_CONSTANT:LayoutContants = LayoutContants.getInstance();
    
    loadImage: Phaser.GameObjects.Image;
    sloteGridTable:SlotGridTable;
    initialData: JSON[];
    apiData: {};
    exitButton: CustomImageButton;
    addButton: CustomImageButton;

    sloteItems:any[];
    sloteItemsData:any[];
    sloteNum: number;
    initSound: Boolean = true;
    lastPosY: any;
    lastPosX: any;
    selectedIndex: integer;
    isDebug: boolean;
    name: any;
    resolution:string = "SD";
    titleImage:string;
    user_id: string;
    isLoggedIn: boolean;
    communityModelBoard:CommunityModelBoard;
    communityModel: CommunityModel;
    communityModelIndex: number;
    communityItemData: any[];
    isCommunityModelBoardShown: boolean;

    modelDataType: string;

    constructor(){
        super({key: CST.SCENES.SAVE_SLOTE});
        this.name = CST.SCENES.SAVE_SLOTE;
    }

    init(data:any = {initSound:true}){
        
        Global.getInstance().setCurrentScene(this.scene); 
        Global.getInstance().setNavigation(this.scene);
        var selectedModel = Global.getInstance().getModel();

        this.isFileLoaded = false;
        this.model = selectedModel;        
        this.fileNames = new FileNames().generateFileNames(selectedModel,false);
        this.atlasFront = `${this.model.modelName}-front`;
        this.atlasFrames = `${this.model.modelName}`;
        this.frameSetup();
        this.modelCharacteristics = new ModelCharacteristics();
        this.initSound = data.initSound;
        this.apiData = { 'savedData': [], 'sharedData': [] }; 
        this.sloteNum = 60; 
        this.sloteItemsData = [];
        this.isLoggedIn = false;
        this.user_id = "";
        this.communityItemData = [];
        this.sloteItems = [];
        this.communityModelIndex = 0;
        this.isDebug = false;

        this.checkUserLoggedIn();
    }

    preload(){
        this.load.multiatlas(`${this.model.modelName}-front-SD`,`assets/img/models/${this.model.modelName}/SD/${this.fileNames.atlasSDFront}`, `assets/img/models/${this.model.modelName}/SD`);
       
        this.load.json(`${this.model.modelName}-save`, `assets/img/models/${this.model.modelName}/${this.fileNames.saveFile}`);
        this.load.audio(this.model.modelName, [`assets/img/models/${this.model.modelName}/${this.fileNames.song}`]);
        
        if(!this.model.shopSkip && this.model.shopID == ""){
            this.load.image(this.model.shopImage, `assets/img/models/${this.model.modelName}/${this.model.shopImage}`)//shop image
        }   

        this.load.image(`${this.model.modelName}_Title`, `assets/img/models/${this.model.modelName}/${this.model.modelName}_Title.png`)//title image
        this.loadModels();
    }


    async create(){

        this.playModelBackgroundSong();

        CsvParser.parsePartsCsv( 
            `${CST.HOST_ADDRESS}/assets/img/models/${this.model.modelName}/${this.fileNames.partsCsv}`, 
            `${CST.HOST_ADDRESS}/assets/img/models/${this.model.modelName}/${this.fileNames.featuresCsv}`, 
            (result)=>{
                this.modelParts = result;
                Global.getInstance().setSeletectedModelParts(result);
                this.isFileLoaded = true;
        });

        // We need to show the initial model when user didn't login...
        this.initialData = this.cache.json.get(`${this.model.modelName}-save`) || [];
        
        //document 
        var event = new CustomEvent('web.getToken', { detail: "event test" });
        document.dispatchEvent(event);

        var event = new CustomEvent('onScreenChange', { detail: CST.SCENES.SAVE_SLOTE });
        document.dispatchEvent(event);
        
        this.bindEventLisner(0);
        this.bindEventLisner(1);

        if (this.isLoggedIn) {
            this.getSavedDataFromAPI();
            this.getSharedDataFromAPI();
            console.log('savedData', this.apiData['savedData'])
        }

        // let indexAry = [];
        // for( var i=0; i<this.sloteNum; i++ ) {
        //     indexAry.push({flag: false, index: i});
        // }
        
        // const fn = async () => {
        //     console.log('async function call')
        //     this.sloteItemsData.forEach(item => {
        //         this.deleteSloteItemData(item.id);
        //     })
            

        //     // if (this.sloteItemsData) {
        //     //     this.sloteItemsData.forEach((item, index) => {
        //     //         if (item) {
        //     //             indexAry[item.slot_index].flag = true;
        //     //         }
        //     //     })
        
        //     //     var addSloteItemsAry = indexAry.filter(item => item.flag === false);
        //     //     var addSloteItemsNum = addSloteItemsAry?addSloteItemsAry.length:0;
        //     //     console.log('addSloteItemsAry',addSloteItemsAry)
        //     //     if (addSloteItemsNum) {
        //     //         // console.log('needed to add slotdata', addSloteItemsNum, addSloteItemsAry)
        //     //         var addResultNum = 0;
        //     //         addSloteItemsAry.forEach((item, i) => {
        //     //             const model_data = {
        //     //                 "model_name": this.model.modelName,
        //     //                 "like_count": 0,
        //     //                 "download_count": 0,
        //     //                 "slot_index": item.index,
        //     //                 "is_shared": false,
        //     //                 "shared_date": "1999-01-01T00:00:00.350993Z",
        //     //                 "feature": 1
        //     //             }
        
        //     //             this.addSloteItemData(model_data).then(res => {
                           
        //     //                 if (res === "success") {
        //     //                     addResultNum++;
        //     //                     if (addSloteItemsNum == addResultNum) {
        //     //                         this.getSloteItemData().then(res => {
        //     //                             fn();
        //     //                         }).catch(err => {
                                       
        //     //                         })
        //     //                     }
        //     //                 }
        //     //             })
        //     //         })
        //     //     } else {
        //     //         // this.renderReady = true;
        //     //     }
        //     // }
        // }
        // this.getSloteItemData().then(res => {
        //     fn();
        // }).catch(err => {
        //     console.log("getSloteItemData error occured")
        // })
    }

    loadModels(){                     
         
    }

    checkUserLoggedIn() {
        var event = new CustomEvent('checkUserLoggedIn', { detail: { callback:(res)=>{
            console.log("checkUserLoggedIn", res);
            const loginFlag = res[0]; 
            this.isLoggedIn = loginFlag;
            this.user_id = this.isLoggedIn?res[1]: this.user_id;
        }}});
        document.dispatchEvent(event);
    }

    
    removeTextures(){              
        
        this.textures.remove(`${this.model.modelName}-front-SD`);
        this.textures.remove(`${this.model.modelName}-save`);
        this.textures.remove(this.model.modelName);            
    }
    update(){

        if(this.isDebug){
            if(this.isFileLoaded){
                this.initElements();
                this.isFileLoaded = false;
            }
        } else{
            if(this.isFileLoaded && this.apiData['savedData'] && this.apiData['sharedData']){
                if ( this.isLoggedIn ){
                    if (this.apiData['savedData'].length){
                        this.initElements(); 
                        this.isFileLoaded = false;
                    }
                } else {
                    this.initElements(); 
                    this.isFileLoaded = false;
                }
                
                // console.log('update render')
            }
        }

        const pointer = this.input.activePointer;
        if(pointer.isDown){
            if(!this.lastPosY) this.lastPosY = pointer.y;3000
            if(!this.lastPosX) this.lastPosX = pointer.x;
            const time = 1/this.game.loop.actualFps;
            const vy = (pointer.y - this.lastPosY);
            const vx = (pointer.x - this.lastPosX);
            this.lastPosY = pointer.y;
            this.lastPosX = pointer.x;
            this.events.emit("velocity", vx, vy)

        }else{
            this.lastPosY = undefined;
            this.lastPosX = undefined;
        }
    }

    initElements(){

        var scrollMode = 1;
        var marginLeft = 100;
        var marginTop = 50; 
        var cellHeight = (this.LAYOUT_CONSTANT.SCREEN_HEIGHT - marginTop)/2;
        var cellWidth = 400; 
        var columns = Math.round((this.LAYOUT_CONSTANT.SCREEN_HEIGHT  - 100)/( cellHeight * 2));
        
        this.sloteItems = [];
        this.sloteItems[0] = this.getItems(cellWidth, cellHeight, scrollMode, 0, Math.max(this.sloteNum, Global.getInstance().getSloteItemsCount(this.model.modelName)));
        this.sloteItems[1] = this.getItems(cellWidth, cellHeight, scrollMode, 0, Math.max(this.sloteNum, Global.getInstance().getSloteItemsCount(this.model.modelName)), true);

        console.log('slotitemsinfo', this.sloteItems[0], this.sloteItems[1])
        this.sloteGridTable = new SlotGridTable(this, {
            x: marginLeft , 
            y: marginTop, 
            width: this.LAYOUT_CONSTANT.SCREEN_WIDTH - marginLeft, 
            height: this.LAYOUT_CONSTANT.SCREEN_HEIGHT - marginTop, 
            background: [this.add.image(0, 0, "solid"), this.add.image(0, 0, "solid")],
            scrollMode: scrollMode,
            modelName: this.model.modelName,
            sloteItemsData: this.sloteItemsData,
            table: {
                cellWidth: cellWidth,
                cellHeight:cellHeight,
                columns: columns,
            },
            updateSloteItemData: this.updateSloteItemData,
        })

        var tableIndex = 0
        for (var i=0; i<2; i++) {
            tableIndex = i;
            if (this.sloteItems[tableIndex] && this.sloteItems[tableIndex].length) {
                this.sloteGridTable.setItems(this.sloteItems[tableIndex], tableIndex);
            }
        }

        if (this.sloteItems[1].length) {
            // listner when clicking model in slote pannel
            this.sloteGridTable.table[1].on("cell.click", (cellIndex)=>{
                this.communityModelBoard.setVisible(true);
                this.changeCommunityModelBoardFlag(true);
                this.communityModelIndex = cellIndex;
                console.log('community slot cell click', this.communityModelIndex);
            });
        }

        // listner when clicking model in slote pannel
        this.sloteGridTable.table[0].on("cell.click", (cellIndex)=>{   
            this.editCommunityModel(cellIndex);
        });

        this.exitButton = new CustomImageButton(this, 0, 0, "back_button", ()=>{
            this.goMainScene();
        });

        this.exitButton.setTopLeftOrigin();
        console.log("sloteItems", this.sloteItems[0])
        this.communityModel = new CommunityModel(
            this, 
            this.LAYOUT_CONSTANT.SCREEN_WIDTH, 
            this.LAYOUT_CONSTANT.SCREEN_HEIGHT, 
            this.model, 
            this.sloteItems[0][0].savedModel,
        );

        
        this.communityModelBoard = new CommunityModelBoard(this, {
            x: 0,
            y: 0,
            width: this.LAYOUT_CONSTANT.SCREEN_WIDTH, 
            height: this.LAYOUT_CONSTANT.SCREEN_HEIGHT, 
            background: this.add.image(0, 0, "Fade8"),
            modelName: this.model.modelName,
            changeCommunityModelBoardFlag: this.changeCommunityModelBoardFlag,
            editCommunityModel: () => {
                this.editCommunityModel()
            }
        })

        this.communityModelBoard.setItems(this.communityModel);
        this.communityModelBoard.setVisible(false);
        this.changeCommunityModelBoardFlag(false);
        this.communityModelIndex = 0;
    }

    changeCommunityModelBoardFlag (flag) {
        this.isCommunityModelBoardShown = flag;
    }

    editCommunityModel(cellIndex = undefined){
        if (cellIndex !== undefined) {
            if (!this.isCommunityModelBoardShown) {
                this.sloteItems[0][cellIndex].savedModel.setIndex(cellIndex);
                this.selectedIndex = cellIndex;
                this.startModelingScene(this.sloteItems[0][cellIndex].savedModel);
            }
        } else {
            if (this.sloteItems[1].length) {
                this.sloteItems[1][this.communityModelIndex].savedModel.setIndex(this.communityModelIndex);
                this.selectedIndex = this.communityModelIndex;
                this.startModelingScene(this.sloteItems[1][this.communityModelIndex].savedModel);
            }
        }
    }

    // starting ModelingScene
    startModelingScene(savedModel:SavedModel){
        Global.getInstance().setCurrentModel(savedModel);
        if (this.isCommunityModelBoardShown) {
            window.location.hash = CST.SCENES.MODELING + '/' + Global.getInstance().getModel().modelName + '?' + 'sharemodelview';
        } else {
            window.location.hash = CST.SCENES.MODELING + '/' + Global.getInstance().getModel().modelName;
        }
        
        this.scene.start(CST.SCENES.MODELING);
    }

    
    frameSetup(){
        const frameSetup = this.model.frameSetup.split(",");
        this.firstFrame = +frameSetup[2];
        this.minFrame = +frameSetup[0];
        this.maxFrame = +frameSetup[1];
    }
    


    getModelCharacteristics():ModelCharacteristics {
        return this.modelCharacteristics;
    }

    getItems(width:number, height:number, scrollMode:integer, offset:integer, count:integer, shareFlag:boolean = false):SlotItem[]{
        
        var result = [];
        
        if(offset+count > this.initialData.length) return result;

        for(var i = offset; i < offset + count; i++){
            let jsonData = this.initialData[i];
            var itemData = jsonData;
            result.push(itemData);
        }
        console.log('initial_item_datas', result)
        var itemArr = [];
        result.forEach((element, i) => {
            var itemData = element;

            if (this.isDebug) {
                if (localStorage.getItem(`${this.model.modelName}${i+offset}`)){
                    var jsonStr = localStorage.getItem(`${this.model.modelName}${i+offset}`);
                    var localJSONOBJp = JSON.parse(jsonStr);
                    itemData = localJSONOBJp;
                }
                
            } else {
                var apiJSON = this.findSavedModel(i+offset, shareFlag);
                if(apiJSON){
                    console.log("exist slote info", apiJSON, i)
                    itemData = apiJSON;
                }
            }
            
            var sloteItem;
            if (itemData.hasOwnProperty('slote_info')) {
                const { id, user_id, model_name, index: slot_index, slote_info } = itemData || {}
                const sloteItemInfo = { id, user_id, model_name, slot_index, ...slote_info }
               
                const sloteItemInfos = {
                    sloteItemInfo,
                    tabIndex: i+offset,
                    shareFlag
                }
                sloteItem = new SlotItem(this, width, height, scrollMode, this.model, itemData.saved_data, sloteItemInfos);
            } else {
                const slote_info = {
                    like_count: 0,
                    download_count: 0,
                    slot_index: 0,
                    is_shared: false,
                    shared_date: "",
                    feature: 0 
                }
                const sloteItemInfo = { id: 0, user_id: this.user_id, model_name: this.model.modelName, slot_index: i, ...slote_info }
                const sloteItemInfos = {
                    sloteItemInfo,
                    tabIndex: i+offset,
                    shareFlag
                }
                sloteItem = new SlotItem(this, width, height, scrollMode, this.model, itemData, sloteItemInfos);
            }
             
            itemArr.push(sloteItem);
            // if (shareFlag) {
            //     this.communityItemData.push(itemData)
            // }
            
            
        });
        
        return itemArr;
    }

    goMainScene(){
        //this.removeTextures();
        // window.location.hash = CST.SCENES.MAIN;
        
        window.location.href = `${CST.HOST_ADDRESS}#MainScene`;
        window.location.reload();

        //window.location.href = `${CST.HOST_ADDRESS}?selected_model=${0}`;
        // this.scene.start(CST.SCENES.MAIN);
    }

    playModelBackgroundSong(){

        if((localStorage.getItem("music") || 'on') === 'on'){
            if(this.initSound){                
                this.sound.stopAll();
                this.sound.play(this.model.modelName, {volume:0.2, loop:true});
            }
        }else{
            this.initSound = true;
            this.sound.stopAll();
        }        
    }

    bindEventLisner(tableIndex: number){
        
        document.addEventListener('update_slote', e => {
                const index = this.selectedIndex;
                this.sloteItems[tableIndex][index].updateData(Global.getInstance().getSavedModel());
            }
        );

        document.addEventListener('upload_data', (e:any) => {
            this.copyStringToClipboard(e.detail.saved_data);
        });
    }

    copyStringToClipboard (str) {
        // Create new element
        var el = document.createElement('textarea');
        // Set value (string to be copied)
        el.value = str;
        // Set non-editable to avoid focus and move outside of view
        el.setAttribute('readonly', '');
        el.style.position = "absolute";
        el.style.left = '-9999px';
        document.body.appendChild(el);
        // Select text inside element
        el.select();
        // Copy text to clipboard
        document.execCommand('copy');
        // Remove temporary element
        document.body.removeChild(el);
    }

    getSavedDataFromAPI(){
        var event = new CustomEvent('getSavedData', { detail: {model_name: this.model.modelName, shareFlag: false, callback:(res)=>{
            this.apiData['savedData'] = res;
            console.log('getSavedDataFromAPI',res)
        }}});
        document.dispatchEvent(event);
    }

    getSharedDataFromAPI(){
        var event = new CustomEvent('getSavedData', { detail: {model_name: this.model.modelName, shareFlag: true, callback:(res)=>{
            this.apiData['sharedData'] = res;
            console.log('getSharedDataFromAPI',res)
        }}});
        document.dispatchEvent(event);
    }

    findSavedModel(index:integer, shareFlag: boolean):any {
        var result;
        if(shareFlag){
            this.apiData['sharedData'].forEach(element => {
                if(parseInt(element["index"]) == index){
                    result = element;
                }
            });
        } else {
            this.apiData['savedData'].forEach(element => {
                if(parseInt(element["index"]) == index){
                    result = element;
                }
            });
        }
       
        return result;
    }

    getSloteItemData(slote_index = undefined){

        var promise = new Promise((resolve, reject) => {
            var event = new CustomEvent('getSloteData', { detail: {model_name: this.model.modelName, slote_index, callback:(res)=>{
                this.sloteItemsData = res;
                resolve(res)
                console.log('this.sloteItemsData', this.sloteItemsData)
            }}});
            document.dispatchEvent(event);
        })
        return promise
    }

    addSloteItemData(model_data) {
        var promise = new Promise((resolve, reject) => {
            var event = new CustomEvent('addSloteData', { detail: {model_data, callback:(res)=>{
                resolve(res)
            }}});
            document.dispatchEvent(event);
        })
        return promise
        
    }

    updateSloteItemData(mid, model_data) {
        var event = new CustomEvent('setSloteData', { detail: {mid, model_data, callback:(res)=>{
            console.log("update success", res);
        }}});
        document.dispatchEvent(event);
    }

    deleteSloteItemData(mid) {
        var event = new CustomEvent('delSloteData', { detail: {mid, callback:(res)=>{
            
        }}});
        document.dispatchEvent(event);
    }

    delay(ms: number) {
        return new Promise( resolve => setTimeout(resolve, ms) );
    }

}