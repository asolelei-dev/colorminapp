/// <reference path='../../phaser.d.ts'/>

class ModelingScene extends Phaser.Scene{

    SOLID_GREY_SPRITE = "solidGreySprite";
    FADE_SPRITE = "fadeSprite";
    BACKGROUND_SPRITE = "backgroundSprite";
    NUMBER_OF_POOFS = 24;
    COLOR_SELECTOR_ALPHA_CHANGE = 0.02;

    soundFX: Phaser.Sound.BaseSound;
    model:Model;
    fileNames:FileNames;
    bodyPart:BodyPartGroup;
    modelParts:ModelPart[];
    currentPointX:number;
    animationDirection:SwitchStatus;
    animationRate:integer;
    blinkingEnabled:boolean;
    
    rexUI: any;
    uiGenerator: UIGenerator;
    firstFrame:integer;
    minFrame:integer;
    maxFrame:integer;
    smoothMovTimer: any;

    loadingStarted:boolean;
    isSavesSlotsScreen: boolean;
    bodyPartsManager: BodyPartsManager;
    
    bodyParts: BodyPart[];
    atlasFront: string;
    frame: number;
    frameSliderMoveRight: boolean;
    atlas: string;
    atlasFrames: string;
    colorSelectorsAlfa: number;
    modelCharacteristics:ModelCharacteristics;
    savedModels:SavedModel[];
    selectedModelNumber:number;
    isEditBoxScreen: boolean;
    colorProofSprite:ColorProoferSprite;
    bodyGroup: BodyPartGroup;
    isUndoRedoCooldowned: boolean;
    screenMode:integer;
    moveDistance: number = 0;
    isFloatingColorVisible: boolean;
    testsong: Phaser.Loader.LoaderPlugin;

    isFileLoaded: boolean;
    panStart: boolean;
    pinchStart: boolean;

    startPosX:number = 0;
    startPosY:number = 0;

    lastPosX: number;
    lastPosY: number;

    lastPinchVector: Phaser.Math.Vector2;

    //pinch
    pinchP1X:number;
    pinchP1Y:number;
    pinchP2X:number;
    pinchP2Y:number;
    //isMobilePanStart:boolean;

    touchCenterX: number;
    touchCenterY: number;
    selectedCenterY: number;
    selectedCenterX: number;
    savedModel: SavedModel;
    LAYOUT_CONSTANT:LayoutContants = LayoutContants.getInstance();
    loadImage: Phaser.GameObjects.Image;
    backgrondSong: Phaser.Sound.BaseSound;
    swoosh: Phaser.Sound.BaseSound;
    generalsound: Phaser.Sound.BaseSound;
    backsound: Phaser.Sound.BaseSound;
    resolution:string = "SD";
    timer:any;
    HDFile:Phaser.Loader.File;
    isPrevBtnDown: boolean;
    fpsText:Phaser.GameObjects.Text;
    config:any = {
        showingFPS:false
    }
    name:any;
    secondClick:boolean; // double click flag
    lastRenderTime:any;
    rexGestures:any;
    magnetStatus: number;
    helperContainer: HelperContainer;

    constructor(){
        super({key:CST.SCENES.MODELING});
        this.name = CST.SCENES.MODELING;
        this.animationDirection = SwitchStatus.RIGHT;
        this.animationRate = 30;
        this.loadingStarted = true;
        
        this.screenMode = CST.LAYOUT.EDIT_MODE;
    }
    
    init(){        

        this.resolution = "SD";
        this.isFileLoaded = false;

        // Global.getInstance().setCurrentScene(this.scene);
        // Global.getInstance().setNavigation(this.scene);
        
        this.initButtonSounds();
        var selectedModel = Global.getInstance().getModel();

        console.log("selectedModel -------------> ", selectedModel);
       
        this.model = selectedModel;

        this.fileNames = new FileNames().generateFileNames(selectedModel, true);
        this.atlasFront = `${this.model.modelName}-front`;
        this.atlasFrames = `${this.model.modelName}`;
        
        this.frameSetup();
        this.modelCharacteristics = new ModelCharacteristics();
        this.modelCharacteristics.setName(this.model.modelName);
        this.modelCharacteristics.setShopId(this.model.shopID);

       

        if ( this.checkPreviewMode() ) {
            console.log('previewmode')
            this.screenMode = CST.LAYOUT.PREVIEW_MODE;
            
        } else {
            this.screenMode = CST.LAYOUT.EDIT_MODE;
        }
        this.animationDirection = CST.ANIMATION.DIRECTION.RIGHT;
        this.modelParts = Global.getInstance().getSeletectedModelParts();
        
        //this.cache.destroy();
    }

    preload(){                
        this.load.multiatlas(`${this.atlasFront}-HD`,`${CST.HOST_ADDRESS}/assets/img/models/${this.model.modelName}/HD/${this.fileNames.atlasFront}`, `${CST.HOST_ADDRESS}/assets/img/models/${this.model.modelName}/HD`);
        // this.load.multiatlas(`${this.atlasFront}-SD`,`${CST.HOST_ADDRESS}/assets/img/models/${this.model.modelName}/SD/${this.fileNames.atlasSDFront}`, `${CST.HOST_ADDRESS}/assets/img/models/${this.model.modelName}/SD`);        
    }


    resetScreen(){
        this.savedModel = Global.getInstance().getSavedModel();
        this.uiGenerator.resetElementPositionByScreenMode(this.screenMode)
        if(this.atlasFrames != null){
            this.startOnFramesLoaded(this.atlasFrames);
            this.bodyPartsManager.setSavedProperties(this.savedModel);
            this.uiGenerator.setInitialUserAction();
        }
        Global.getInstance().setResetModelFlag(false);
    }

    create(){

        this.helperContainer = new HelperContainer(this, 1);
                
        this.buildStage();
        
        this.startTimer();

        this.resetScreen();
        this.bindLoadEvent();
        this.bodyPartsManager.resetSprite();
        
        this.loadModels("SD");
        // this.uiGenerator.materialButtons[this.bodyPartsManager.selectedBodyPart.selectedTabIndex].onTapButton();
        // debugger
        this.showFPS();
        
    
            
    }

    checkPreviewMode() {
        const url = window.location.href;
        console.log('modelingscenepreview', url)
        if (url.includes('sharemodelview')){
            return true
        } else {
            return false
        }
       
    }

    loadModels(resol:string){

        if(resol == "SD"){
            this.uiGenerator.bottomSwitchBar.setVisible(false)
            this.load.on("progress", (value)=>{
                this.uiGenerator.progressBar.increasePercent();
            }, this);
        }

        this.load.on("complete", ()=>{
                
            this.uiGenerator.progressBar.setVisible(false);
            
            if(this.screenMode == CST.LAYOUT.EDIT_MODE){
                this.uiGenerator.bottomSwitchBar.setVisible(true);
            }
            
            if(resol == "SD"){
                this.events.emit("loadedSD");
            }

            if(resol == "HD"){                
                this.events.emit("loadedHD");
            }

        }, this)
        .on("customfileloaded", (file)=>{
            console.log(file);
        })


        if(resol == "HD"){
            this.load.customLoad = true;
            this.load.multiatlas(`${this.model.modelName}-${resol}`,`${CST.HOST_ADDRESS}/assets/img/models/${this.model.modelName}/${resol}/${this.fileNames.atlasFrames}`, `${CST.HOST_ADDRESS}/assets/img/models/${this.model.modelName}/${resol}`);
        }else{
            this.load.multiatlas(`${this.model.modelName}-${resol}`,`${CST.HOST_ADDRESS}/assets/img/models/${this.model.modelName}/${resol}/${this.fileNames.atlasSDFrames}`, `${CST.HOST_ADDRESS}/assets/img/models/${this.model.modelName}/${resol}`);
        }
        this.load.start();
    }

    bindLoadEvent(){
        this.events.once("loadedSD", ()=>{
            
            this.resolution = "SD";
            this.isFileLoaded = true;
            
            this.bodyPartsManager.resetSprite();            

            this.loadModels("HD");
            
        });

        this.events.once("loadedHD", ()=>{
            // this.resolution = "HD";
            // if(this.timer) clearInterval(this.timer)
            // //this.textures.remove(`${this.model.modelName}-SD`);
            // this.bodyPartsManager.resetSprite();
            // this.startTimer();
            
     
        });

        document.addEventListener('customfileloaded', (e:any) => { 
                this.HDFile = e.detail;
            }
        );
        /**
         * Keyboard Bind functions...
         */
        this.input.keyboard.on("keyup_CTRL", ()=>{
            this.panStart = false;
            this.startPosX = 0;
            this.startPosY = 0;
        });

        this.input.keyboard.on("keydown_CTRL", ()=>{
            if(this.screenMode == CST.LAYOUT.EDIT_MODE || this.screenMode == CST.LAYOUT.PREVIEW_MODE){
                this.panStart = true;
                this.startPosX = 0;
                this.startPosY = 0;
            }
            
        });

        this.input.keyboard.on("keyup_SHIFT", ()=>{

            this.pinchStart = false;
            this.lastPinchVector = undefined;
            //this.resetCenterPointer();

        });

        this.input.keyboard.on("keydown_SHIFT", ()=>{
            if(this.screenMode == CST.LAYOUT.EDIT_MODE || this.screenMode == CST.LAYOUT.PREVIEW_MODE){
                
                this.lastPinchVector = undefined;
                if(this.input.activePointer.isDown){
                    this.bodyPartsManager.tCenterX = this.input.activePointer.x;
                    this.bodyPartsManager.tCenterY = this.input.activePointer.y;    
                }
                if(!this.pinchStart){
                    this.selectedCenterY = this.input.activePointer.x;
                    this.selectedCenterX = this.input.activePointer.y;
                    this.pinchStart = true;
                }
            }
            
        });

    }

    startTimer(){
       
        if(this.timer) clearInterval(this.timer);
        this.timer = setInterval(()=>{
            this.updateFrame();
        }, 2000/29)

    }
    

    updateFrame(){
        
        
        // debugger

        if(this.resolution == "SD"){
            if(this.HDFile && this.bodyPartsManager.frame == this.firstFrame){
                this.helperContainer.setHelperIndex(2);
                if(this.HDFile.multiFile)this.HDFile.multiFile.addToCache();
                this.resolution = "HD";
                this.bodyPartsManager.resetSprite();
            }
        }

        if(this.bodyPartsManager && this.isFileLoaded){
            

            if(this.model.isOrbit == "0"){
                // this.uiGenerator.bottomSwitchBar.updatePingPong();
                // const frame = this.uiGenerator.bottomSwitchBar.getPingPongFrame();
                // if(this.uiGenerator.bottomSwitchBar.isPingPong){
                //     this.bodyPartsManager.setFrame(frame);
                // }else{
                //     // this.uiGenerator.bottomSwitchBar.updateThumbButtonPositionByFrame(this.bodyPartsManager.frame);
                // }

            }else{

                if(this.animationDirection == SwitchStatus.RIGHT){
                    this.bodyPartsManager.rotateRight();
                }
    
                if(this.animationDirection == SwitchStatus.LEFT){
                    this.bodyPartsManager.rotateLeft();
                }

            }

            this.bodyPartsManager.updateModel();
            
            // this.uiGenerator.bottomSwitchBar.setFrame(this.bodyPartsManager.frame);

            
        }

        if(this.bodyPartsManager){
            if (this.blinkingEnabled) {
                if(this.bodyPartsManager.selectedBodyPart.modelPart.feature.isChildren){
                    this.uiGenerator.selectedChildBodyPartButton.blink(()=>{
                        this.stopBlinking();
                    });
                }else{

                    this.uiGenerator.selectedTMenuButton.blink(()=>{
                        this.stopBlinking();
                    });
                }
                // this.setBlinkingTimerStarted(false);
            }
        }
    }

    /**
     * Model Scene Update 
     * This method is called once per game step while the scene is running
     */
    update(){
        // console.log(this.frame,"===================================")
        // console.log('colorMode', this.uiGenerator.colorMode);
        // console.log('palleteNumber', this.uiGenerator.palleteNumber);
        // console.log('bodycolorMode', this.bodyPartsManager.selectedBodyPart.colorModeNumber);
        // console.log('bodypalleteNumber', this.bodyPartsManager.selectedBodyPart.palleteNumber);
        // console.log("===================================",this.frame)

        if(this.config.showingFPS){
            this.fpsText.setText(`FPS:${this.game.loop.actualFps}`);
        }
        
        if(this.isFileLoaded&&this.bodyPartsManager){
            if(this.screenMode == CST.LAYOUT.EDIT_MODE || this.screenMode == CST.LAYOUT.PREVIEW_MODE){

                // && Math.abs(deltaPointer2Dis)>1
                if(this.pinchP1X == undefined && this.input.pointer1.isDown && this.input.pointer2.isDown ){
                    if(this.time.now - this.lastRenderTime > 20){
                        this.touchCenterX = (this.input.pointer1.x +this.input.pointer2.x)/2;
                        this.touchCenterY = (this.input.pointer1.y +this.input.pointer2.y)/2;
                        this.pinchP1X = this.input.pointer1.x;
                        this.pinchP2X = this.input.pointer2.x;
                        this.pinchP1Y = this.input.pointer1.y;
                        this.pinchP2Y = this.input.pointer2.y;
                    }

                    this.secondClick = false;
                }else if(this.pinchP1X && this.input.pointer1.isDown && this.input.pointer2.isDown){

                    // moving center pos while performing pinch
                    var dx = (this.input.pointer1.x + this.input.pointer2.x)/2 - this.touchCenterX;
                    var dy = (this.input.pointer1.y + this.input.pointer2.y )/2 - this.touchCenterY;
                    // Moving
                    this.bodyPartsManager.moveByX(dx);
                    this.bodyPartsManager.moveByY(dy);                                        
                    this.LAYOUT_CONSTANT.setModelMoving(true);


                    let currentPinchDis = Phaser.Math.Distance.Between(this.input.pointer1.x, this.input.pointer1.y, this.input.pointer2.x, this.input.pointer2.y);
                    let lastPinchDis = Phaser.Math.Distance.Between(this.pinchP1X, this.pinchP1Y, this.pinchP2X, this.pinchP2Y);
                    // Scaling
                    //let distance = Math.abs(currentPinchDis - lastPinchDis);

                    let scale = currentPinchDis/lastPinchDis;
         
                  
                    this.bodyPartsManager.setScaleMulti(scale, this.touchCenterX, this.touchCenterY);
                    this.uiGenerator.setScaleDiceImageByDScale();   
                
                    
                    this.LAYOUT_CONSTANT.setModelMoving(true);

                    this.pinchP1X = this.input.pointer1.x;
                    this.pinchP2X = this.input.pointer2.x;
                    this.pinchP1Y = this.input.pointer1.y;
                    this.pinchP2Y = this.input.pointer2.y;

                    this.touchCenterX = (this.input.pointer1.x +this.input.pointer2.x)/2;
                    this.touchCenterY = (this.input.pointer1.y +this.input.pointer2.y)/2;

                }else{
                    //console.log('undefined')
                    this.pinchP1X = undefined;
                    this.pinchP2X = undefined;
                    this.pinchP1Y = undefined;
                    this.pinchP2Y = undefined;

                    this.touchCenterX = undefined;
                    this.touchCenterY = undefined;
                    this.secondClick = true;
                    this.lastRenderTime = this.time.now;
                }


                
            }
        }

        const pointer = this.input.activePointer;
        if(pointer.isDown){
            if(!this.lastPosY) this.lastPosY = pointer.y;
            if(!this.lastPosX) this.lastPosX = pointer.x;
            const time = 1/this.game.loop.actualFps;
            const vy = (pointer.y - this.lastPosY);
            const vx = (pointer.x - this.lastPosX);
            this.lastPosY = pointer.y;
            this.lastPosX = pointer.x;
            this.events.emit("velocity", vx, vy)

        }else{
            this.lastPosY = undefined;
            this.lastPosX = undefined;
        }


    }
    frameSetup(){
        const frameSetup = this.model.frameSetup.split(",");
        this.minFrame = +frameSetup[0];
        this.maxFrame = +frameSetup[1];
        this.firstFrame = +frameSetup[2];
    }

    getColorFromRange(i:integer, colorSliderPosition:integer):Phaser.Display.Color{
        const colorRange = this.game.global.colorsRanges[i];
        var tempColor = new Phaser.Display.Color(255, 255, 255);
        tempColor.red = colorRange.red[colorSliderPosition]*255;
        tempColor.green = colorRange.green[colorSliderPosition]*255;
        tempColor.blue = colorRange.blue[colorSliderPosition]*255;
        tempColor.alpha = 0;
        return tempColor;
    }

    callColorPoofer(){
        const selectedbodyPart = this.bodyPartsManager.selectedBodyPart;
        if(!selectedbodyPart.modelPart.partName.includes("Background")){
            const pos = this.bodyPartsManager.getCurrentSelectedPartPos();
            if(pos){
                this.colorProofSprite.startColorProof(selectedbodyPart.color, pos.x, pos.y);
            }
        }
    }

    stopBlinking(){
        if (this.blinkingEnabled) {
            this.setBlinkingEnabled(false);
        }
    }

    setBlinkingEnabled(blinkingEnabled:boolean) {
        this.blinkingEnabled = blinkingEnabled;
    }

    buildStage(){
        this.loadingStarted = false;
        this.isSavesSlotsScreen = false;
        this.uiGenerator = new UIGenerator(this);
        this.uiGenerator.createImagesFromAssets();
        this.uiGenerator.setImagesVisibility();

        this.bodyPartsManager = new BodyPartsManager(this, this.modelParts);
        this.bodyPartsManager.setAtlasFront(this.getAtlasfront());

        this.setFirstFrame();

        this.uiGenerator.createTables();
        this.uiGenerator.setActorsLocation();

        this.uiGenerator.populateColorsTable();
        
        this.bodyParts = this.bodyPartsManager.createBodyParts(this.getAtlasfront(), this.frame);

        this.bodyPartsManager.setInitialSelectedBodyPart();
        const material = this.bodyPartsManager.selectedBodyPart.material;
        this.bodyPartsManager.setColorButtonImages(material);

        this.uiGenerator.setColorsToButtons();
        this.uiGenerator.setColorSelection(0);
        this.uiGenerator.getSelectedColorButton().addToColoredBodyPartsCount();
        this.uiGenerator.setBackground(this.bodyPartsManager.backgroundTexture, this.bodyPartsManager.fadeTexture);
        this.uiGenerator.populateBodyPartTable();
        this.uiGenerator.setTMenuSelector();
        this.uiGenerator.populateMaterialTable();
        this.uiGenerator.setMaterialButtons(`${this.getHDAtalasFront()}`);
        this.bodyPartsManager.setUtmostSprites(this.screenMode);

        this.isEditBoxScreen = true;


        this.uiGenerator.populateStage();

        this.setUpEventHandler();

        this.bodyPartsManager.renderBodyPart();
        this.colorProofSprite = new ColorProoferSprite(this, "proof", "Poof.01.png");
        
    }

    setFirstFrame(){
        this.frame = this.firstFrame;
        this.checkFrame();
        this.frameSliderMoveRight = true;
    }

    checkFrame(){
        if (this.frame == this.firstFrame){
            this.atlas = this.atlasFront;
        } else {
            this.atlas = this.atlasFrames;
        }
    }

    setColorSelectorsAlfa(colorSelectorsAlfa:number) {
        this.colorSelectorsAlfa = colorSelectorsAlfa;
    }

    getModelCharacteristics():ModelCharacteristics {
        return this.modelCharacteristics;
    }

    startOnFramesLoaded(atlasFrames:string) {
        this.setAtlasFrames(atlasFrames);
        this.bodyPartsManager.setLinkedBodyParts();
    }

    setAtlasFrames(atlasFrames:string) {
        this.bodyPartsManager.setAtlasFrames(atlasFrames);
        this.atlasFrames = atlasFrames;
        for (var i = this.minFrame; i < this.maxFrame + 1; i++) {
            if (i != this.firstFrame) {
                this.bodyPartsManager.createFramesSprites(i);
            }
        }
    }


    setUpEventHandler(){

        //color adjust frame:
        var colorFrame = this.uiGenerator.colorAdjustFrame;
        var colorButton = this.uiGenerator.colorAdjustButton;
        var dot  = this.uiGenerator.colorAdjustDot;
        var self = this;
        colorFrame.isDrag = false;
        colorFrame.setName("colorAdjust");
        var LastPosX = 0;
        var modelScene = this;
        colorFrame.setInteractive({hitAreaCallback:()=>{
        }}).on('pointerdown', function(pointer, localX, localY, event){
            modelScene.stopBlinking();
            colorFrame.isDrag = true;
        }).on('pointerup', function(pointer, localX, localY, event){
            colorFrame.isDrag = false;
            LastPosX = 0;
            self.uiGenerator.correctColorAdjustbutton(colorFrame);
        });

        this.input.on("pointerdown", (event, target)=>{
                this.lastPinchVector = undefined;                
                LayoutContants.getInstance().setModelMoving(false);
        }, this);

        this.input.on("pointermove", (event, targets)=>{
            if(targets.length > 0){
                var target = targets[0];
                if(target.name == "colorAdjust"){
                    if(target.isDrag){
                        if(LastPosX == 0){
                            LastPosX = event.x;
                        }
                        if(event.x > this.uiGenerator.colorAdjustFrame.x - this.uiGenerator.colorAdjustFrame.displayWidth+38/LayoutContants.getInstance().SCREEN_SIZE_COEF && event.x < this.uiGenerator.colorAdjustFrame.x - 20/LayoutContants.getInstance().SCREEN_SIZE_COEF){
                            colorButton.x = event.x;
                            self.uiGenerator.onChangeColorAdjustment(colorButton);
                        }
                        dot.x = colorButton.x;
                        LastPosX = event.x;
                    }
                }
            }else{
                if(colorFrame.isDrag){
                    colorFrame.isDrag = false;
                    LastPosX = 0;
                    self.uiGenerator.correctColorAdjustbutton(colorButton);
                }
            }
        }, this);



        //bodypart drag
        let lastTime = 0;
        var isDown = false;

        this.uiGenerator.touchZone.setInteractive()
        .on('pointerdown', (pointer, localX, localY, event)=>{
            if(pointer.button == 1){
                if(this.screenMode == CST.LAYOUT.EDIT_MODE || this.screenMode == CST.LAYOUT.PREVIEW_MODE){
                    this.panStart = true;
                    this.startPosX = 0;
                    this.startPosY = 0;
                }
            }
            isDown = true;
            this.touchZonePointerDown(pointer);

        })
        .on('pointermove', (pointer, localX, localY, event)=>{
            if(pointer.isDown){
                this.touchZonePointerMove(pointer, localX);
            }
        })
        .on('pointerup', (pointer, localX, localY, event)=>{

            if(isDown){

                if(pointer.button == 1){
                    this.panStart = false;
                    this.startPosX = 0;
                    this.startPosY = 0;
                }
    
                this.touchZonePointerUp(pointer);
                if(this.secondClick){
                    let clickDelay = this.time.now - lastTime;
                    lastTime = this.time.now;
                    if(clickDelay < 300) {
                        this.uiGenerator.fadeInBackgroundSprite();
                    }
                }

            }else{
                isDown = false;
            }

            
        })
        .on('pointerout', ()=>{

        });

        this.uiGenerator.touchZone.on('wheel', function(pointer, dx, dy, dz, event){
            self.bodyPartsManager.setScaleBy(2 * dy/1000);
        });


        //drag buttonPreview
        this.isPrevBtnDown = false;
        var btnPrevLX = 0;
        var btnPrevLY = 0;
        var buttonPreview = this.uiGenerator.buttonPreview;
        var originX = buttonPreview.x;
        var originY = buttonPreview.y;
        this.magnetStatus = 0;
        var isTopTrend = false;

        // it causes this event when press down preview button - Modified by SY
        buttonPreview.setInteractive().on("pointerdown", (pointer, localX, localY, event)=>{
            this.isPrevBtnDown = true;
            btnPrevLX = 0;
            btnPrevLY = 0;
            
        })

        // it causes this event when moving preview button under pressed down - Modified by SY
        this.input.on("pointermove", (event, target)=>{
            if(this.isPrevBtnDown && event.x >  originX - buttonPreview.displayWidth && buttonPreview.x <= LayoutContants.getInstance().SCREEN_WIDTH){
                if(btnPrevLX == 0) btnPrevLX = event.x;
                if(btnPrevLY == 0) btnPrevLY = event.y;
                var deltaX = event.x - btnPrevLX;
                var deltaY = event.y - btnPrevLY;

                buttonPreview.x += deltaX;
                buttonPreview.y += deltaY;
                if(buttonPreview.x > LayoutContants.getInstance().SCREEN_WIDTH) {
                    buttonPreview.x = LayoutContants.getInstance().SCREEN_WIDTH;
                    deltaX = 0;
                }
                if(buttonPreview.x < originX) {
                    buttonPreview.x = originX
                    deltaX = 0;
                };

                if(buttonPreview.y < 0) {
                    buttonPreview.y = 0
                    deltaY = 0;
                };

                if(buttonPreview.y > this.LAYOUT_CONSTANT.BUTTONS_SIZE * 1.2) {
                    buttonPreview.y = this.LAYOUT_CONSTANT.BUTTONS_SIZE * 1.2
                    deltaY = 0;
                };
                if(buttonPreview.x < originX + 0.5*LayoutContants.getInstance().BUTTONS_SIZE){
                    this.magnetStatus = 0;
                }else if(buttonPreview.x  < originX + 1 * LayoutContants.getInstance().BUTTONS_SIZE){
                    this.magnetStatus = 1;
                }else if(buttonPreview.x  < originX + (1.8+0.5) * LayoutContants.getInstance().BUTTONS_SIZE){
                    this.magnetStatus = 2;
                }else{
                    this.magnetStatus = 3;
                }
                if(buttonPreview.y < originY - 0.6 * LayoutContants.getInstance().BUTTONS_SIZE){
                    isTopTrend = true;
                }else{
                    isTopTrend = false;
                }
                
                // this.events.emit("prevbutton.pontermove", deltaX);
                btnPrevLX = event.x;
                btnPrevLY = event.y;
                this.uiGenerator.updateGridTablesBasedOnPreviewBtn(deltaX);
            }
            
        }, this);

        // it causes this event when pressing up preview button - Modified by SY
        this.input.on("pointerup", (event, targets)=>{
            
            var deltaX = 0;
            var deltaY = 0;
            
            if(this.isPrevBtnDown){
                this.isPrevBtnDown = false;
                if(this.magnetStatus == 0){
                    this.screenMode = CST.LAYOUT.EDIT_MODE;
                    deltaX = originX - buttonPreview.x;
                    
                }else if(this.magnetStatus == 1){
                    deltaX = originX+0.8*LayoutContants.getInstance().BUTTONS_SIZE - buttonPreview.x;
                }else if(this.magnetStatus == 2){
                    deltaX = originX+1.8*LayoutContants.getInstance().BUTTONS_SIZE - buttonPreview.x;
                }else{
                    deltaX = LayoutContants.getInstance().SCREEN_WIDTH - buttonPreview.x;
                    this.screenMode = CST.LAYOUT.PREVIEW_MODE;
                }

         
                if(isTopTrend){
                    deltaY = - buttonPreview.y;
                }else{
                    deltaY = originY - buttonPreview.y;
                }

                if(this.magnetStatus == 3){
                    this.uiGenerator.colorAdjustContainer.setVisible(false);
                }else{
                    this.uiGenerator.colorAdjustContainer.setVisible(true);
                }

                buttonPreview.x += deltaX;
                buttonPreview.y += deltaY;
                // this.events.emit("prevbutton.pontermove", deltaX, isLeftTrend);
                this.uiGenerator.updateGridTablesBasedOnPreviewBtn(deltaX);
            }

        }, this);



        //color adjust frame:
        var isSelected = false;
        this.input.on("pointermove", (event, target)=>{
            isSelected = true;
            if(this.uiGenerator.floatColorSliderBG.visible){
                this.uiGenerator.updateFloatingPosition(event.x);
                this.uiGenerator.setFloatingColorAdjustPosition();
            }
        }, this);

        this.input.on("pointerup", (event, target)=>{
            if(isSelected && this.uiGenerator.floatColorSliderBG.visible){
                this.uiGenerator.selectedColorButton.onTapButton();
                isSelected = false;
            }
            setTimeout(() => {
                this.uiGenerator.hideFloatColorSlider();
            }, 500);
        }, this);

    }

    setFrame(frame:integer) {
        // this.frame = frame;
        // this.checkFrame();
        // this.bodyParts = this.bodyPartsManager.setFrame(frame);
        // this.bodyPartsManager.setUtmostSprites(frame);
    }
    setUndoRedoCooldowned(undoRedoCooldowned:boolean) {
        this.isUndoRedoCooldowned = undoRedoCooldowned;
    }

    playSound(soundId:integer){
        if(soundId == CST.SOUND.BACKGROUND_SOUND){
            this.backgrondSong.play();
        }

        if((localStorage.getItem("sound") || 'on') === 'on'){
            if(soundId == CST.SOUND.COLOR_CHANGE_SOUND){
                this.swoosh.play();
            }
    
            if(soundId == CST.SOUND.COLOR_GENERAL_SOUND){
                this.generalsound.play();
            }
    
            if(soundId == CST.SOUND.CANCEL_SOUND){
                this.backsound.play();
            }    
        }

    }

    goMainScene(){
        clearInterval(this.timer);
        
        //window.location.hash = CST.SCENES.MAIN ;
        window.location.href = CST.HOST_ADDRESS + '#' + CST.SCENES.MAIN ;
    }

    goSloteScene(){
        clearInterval(this.timer)
        // console.log('here')
        // window.location.reload();
        window.location.hash = CST.SCENES.SAVE_SLOTE +'/'+Global.getInstance().getModel().modelName;
        this.scene.start(CST.SCENES.SAVE_SLOTE, {initSound:false});

        // var event = new CustomEvent('update_slote', { detail: true });
        // document.dispatchEvent(event);

        // this.scene.switch(CST.SCENES.SAVE_SLOTE);
    }

    resetCenterPointer(){
        // this.bodyPartsManager.resetCenterOffY();
    }

    setScreeMode(screenMode:integer){
    
        this.screenMode = screenMode;
        this.bodyPartsManager.resetDXDY();
        this.bodyPartsManager.setUtmostSprites(screenMode);

    }

    addEventListner(){
        document.addEventListener('web.exit.shoping-cart', e => {
            }
        );
    }

    initButtonSounds(){
        this.backgrondSong = this.sound.add("backgroundSong", {volume:0.2});
        this.swoosh = this.sound.add("swoosh");
        this.generalsound = this.sound.add("generalsound");
        this.backsound = this.sound.add("backsound");
    }

    getResolution(){
        return this.resolution || "SD";
    }

    getAtlasfront(){
        return `${this.atlasFront}-${this.getResolution()}`;
    }

    getHDAtalasFront(){
        return `${this.atlasFront}-HD`;
    }

    touchZonePointerDown(pointer:Phaser.Input.Pointer){
        if(this.isFileLoaded){
            this.startPosX = pointer.x;
            this.startPosY = pointer.y;
            this.animationDirection = SwitchStatus.STOP;
            this.uiGenerator.bottomSwitchBar.setSwtichOn(SwitchStatus.STOP, true);
        }
    }

    touchZonePointerMove(pointer:Phaser.Input.Pointer, localX:number){
        if(this.isPrevBtnDown) return;
        this.currentPointX= pointer.x;
                
        if(this.startPosX == 0){
            this.startPosX = pointer.x;
        }

        if(this.startPosY == 0){
            this.startPosY = pointer.y;
        }
        

        if(this.panStart){

            const moveX = pointer.x - this.startPosX;
            const moveY = pointer.y - this.startPosY;

            this.bodyPartsManager.moveByX(moveX);
            this.bodyPartsManager.moveByY(moveY);

            this.startPosX = pointer.x;
            this.startPosY = pointer.y;

            this.LAYOUT_CONSTANT.setModelMoving(true);

        }else if(this.pinchStart){
            //console.log('pinchStart', this.bodyPartsManager.tCenterX, this.bodyPartsManager.tCenterY)
            // if(this.bodyPartsManager.tCenterX){

            // }
            const currentVetor = new Phaser.Math.Vector2(pointer.x, pointer.y);
            let ox =  this.bodyPartsManager.bodyPartStack.x + this.bodyPartsManager.bodyPartInnerStack.x;
            let oy = this.bodyPartsManager.bodyPartStack.y + this.bodyPartsManager.bodyPartInnerStack.y;
            
            const originVector = new Phaser.Math.Vector2(ox,oy);

            
            if(this.lastPinchVector == undefined){
                this.lastPinchVector = currentVetor;
            }

            // let currentPinchDis = Phaser.Math.Distance.Between(pointer.x, pointer.y, this.bodyPartsManager.tCenterX, this.bodyPartsManager.tCenterY);
            // let lastPinchDis = Phaser.Math.Distance.Between(this.pinchP1X, this.pinchP1Y, this.pinchP2X, this.pinchP2Y);
            // Scaling
            //let distance = Math.abs(currentPinchDis - lastPinchDis);

            //let scale = currentPinchDis/lastPinchDis;


            let scale = currentVetor.distanceSq(originVector)/this.lastPinchVector.distanceSq(originVector);
            
            if(scale > 0.1){
                this.bodyPartsManager.setScaleMulti(
                    scale,
                 this.selectedCenterX, this.selectedCenterY );
    
                this.uiGenerator.setScaleDiceImageByDScale();
            }

            this.lastPinchVector = currentVetor;
            this.LAYOUT_CONSTANT.setModelMoving(true);
        }else{
            if(!this.input.pointer2.isDown && this.isFileLoaded){
                
                if(pointer.x > this.startPosX + 0.01*this.uiGenerator.touchZone.scaleX){
                    this.LAYOUT_CONSTANT.setModelMoving(true);
                    this.bodyPartsManager.rotateRight();
                    this.startPosX = pointer.x;
                }

                if(pointer.x < this.startPosX - 0.01*this.uiGenerator.touchZone.scaleX){
                    this.bodyPartsManager.rotateLeft();
                    this.LAYOUT_CONSTANT.setModelMoving(true);
                    this.startPosX = pointer.x;
                }

                this.uiGenerator.bottomSwitchBar.setFrame(this.bodyPartsManager.frame);

            }

        }
    }

    touchZonePointerUp(pointer:Phaser.Input.Pointer){
        this.bodyPartsManager.checkPixelPerfectPos(pointer.x, pointer.y);
    }

    showFPS(){
        if(localStorage.getItem("fps") == 'on'){

            this.fpsText = this.add.text(100,500,"FPS:");
            this.fpsText.setColor("0x000000");
            this.fpsText.setFontFamily("Nevis");
            this.fpsText.depth = 1000;
            this.config.showingFPS = true;

        }
    }
}