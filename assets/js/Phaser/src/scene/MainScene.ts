/// <reference path='../../phaser.d.ts'/>

const Random = Phaser.Math.Between;

const COLOR_PRIMARY = 0xCCCCCC;
const COLOR_LIGHT = 0xCCCCCC;
const COLOR_DARK = 0xe2e2e2;

class MainScene extends Phaser.Scene{

    name:any;
    models_Arr: Array<Model> = new Array<Model>();
    print: Phaser.GameObjects.Text;
    LAYOUT_CONSTANT:LayoutContants = LayoutContants.getInstance();
    modelItemTable:ModelGridTable;
    isMusicPlay:boolean;
    loadImage: Phaser.GameObjects.Image;
    lastPosY:number;
    lastPosX: number;
    goNextScene: boolean;
    selectedModelIndex: integer;
    settingPanel: SettingPanel;
    timer: Phaser.Time.TimerEvent;
    category_Arr: Array<string> = new Array<string>();
    categoryGridTable: CategoryGridTable;
    categoryItems: CategoryItem[];
    selectedCategoryItem: CategoryItem;
    categorlSelector:Phaser.GameObjects.Graphics;
    categorySelectorTween: Phaser.Tweens.Tween;
    modelItems: ModelItem[];
    filteredModelSourc:any;
    selectorTimer: Phaser.Time.TimerEvent;
    defaultHeaderBar: Phaser.GameObjects.Graphics;
    loaded: boolean;
    helperContainer:HelperContainer;

    constructor(){
        super({key:CST.SCENES.MAIN});      
       
        this.name = CST.SCENES.MAIN;
        this.loaded = false;
        
    }

    preload(){
        this.loadModels();
    }

    removeTextures(){
        return;
        this.models_Arr.forEach(model => {
            this.textures.remove(model.iconName);
        });

       this.textures.remove("IAPicon_BGBlue");
        
       this.textures.remove("IAPicon_BGGrey");
        
       this.textures.remove("IAP-Button-Category");
        this.category_Arr.forEach(element => {
            this.textures.remove(element);
        })
        this.category_Arr = []; 

    }
    loadModels(){
        
    

        this.models_Arr.forEach(model => {
            this.load.image(model.iconName, `${CST.HOST_ADDRESS}/assets/img/icons/${model.iconName}.png`);
        });

        this.load.image("IAPicon_BGBlue", `${CST.HOST_ADDRESS}/assets/img/icons/IAPicon_BGBlue.png`);
        
        this.load.image("IAPicon_BGGrey", `${CST.HOST_ADDRESS}/assets/img/icons/IAPicon_BGGrey.png`);
        
        this.load.image("IAP-Button-Category", `${CST.HOST_ADDRESS}/assets/img/category/IAP-Button-Category.png`);

        this.models_Arr.forEach(element => {
            if(this.category_Arr.indexOf(element.category) == -1){
                this.category_Arr.push(element.category);
                this.load.image(element.category, `${CST.HOST_ADDRESS}/assets/img/category/${element.category}.png`);
            }
        });


        


    }

    init(){        
        Global.getInstance().setCurrentScene(this.scene);
        this.models_Arr = Global.getInstance().getModelArr();

        // Global.getInstance().setNativgation(this.scene);
    }



    create(){
        
        this.load.start();
        this.initElements();
        this.addEventListner();
        var event = new CustomEvent('onScreenChange', { detail: CST.SCENES.MAIN });
        document.dispatchEvent(event); 
    }

    update(){
        this.playBackgroundSong();
        const pointer = this.input.activePointer;
        if(pointer.isDown){
            if(!this.lastPosY) this.lastPosY = pointer.y;
            if(!this.lastPosX) this.lastPosX = pointer.x;
            //const time = 1/this.game.loop.actualFps;
            const vy = (pointer.y - this.lastPosY);
            const vx = (pointer.x - this.lastPosX);
            this.lastPosY = pointer.y;
            this.lastPosX = pointer.x;
            this.events.emit("velocity", vx, vy)

        }else{
            this.lastPosY = undefined;
            this.lastPosX = undefined;
        }
    }

    initElements(){        
        this.helperContainer = new HelperContainer(this, 0);
        this.initSetting();
        this.initModelTable();
        this.initialSelectCategory();

        this.defaultHeaderBar = this.add.graphics();
        this.defaultHeaderBar.fillStyle(0xdddddd);
        this.defaultHeaderBar.fillRect(0, 0, this.LAYOUT_CONSTANT.SCREEN_WIDTH, this.settingPanel.visibleBarHeight);
        this.defaultHeaderBar.setDepth(1000);
    }

    initialSelectCategory(){
  
        this.categorlSelector = this.add.graphics();
        this.selectedCategoryItem = this.categoryItems[0];
        this.markSelectedCategory();
    }

    initSetting(){

        this.settingPanel = new SettingPanel(this, 1000*this.LAYOUT_CONSTANT.HEIGHT_SCALE, "NewSettings_BG2", (isOn)=>{
            this.toggleSettingPanel(isOn);
        });
        this.settingPanel.setDepth(100);
    }

    initModelTable(){
        var scrollMode = 1; // 0:vertical, 1:horizontal
        var scrollMode = 0;
        var cellWidth = 300;
        var y = this.settingPanel.getHeight() + this.settingPanel.availableDistance + this.settingPanel.visibleBarHeight;
        var columns = Math.floor(this.LAYOUT_CONSTANT.SCREEN_WIDTH/cellWidth);

        
        this.modelItemTable = new ModelGridTable(this, {
            x: 0,
            y: y,
            width: this.LAYOUT_CONSTANT.SCREEN_WIDTH,
            height: this.LAYOUT_CONSTANT.SCREEN_HEIGHT - y,
            background: this.add.image(0, 0, ""),
            scrollMode: scrollMode,
            table: {
                cellWidth: cellWidth,
                cellHeight:400,
                columns: columns,
            },
            toggleSettingPannel :  (isOn)=>{
                this.toggleSettingPanel(isOn);
            },
            settingPannel: this.settingPanel
        })

        // listner when clicking model
        this.modelItemTable.table.on("cell.click", (cellIndex)=>{
            
            this.modelItems[cellIndex].updateBackground(true);
            Global.getInstance().setModel(this.modelItems[cellIndex].model);
            this.startSloteScene(this.modelItems[cellIndex].model.modelName);
        });

        var self = this;
        var scrollMode = 1; // 0:vertical, 1:horizontal
        this.categoryGridTable = new CategoryGridTable(this, {
            x: 0,
            y: this.settingPanel.getHeight() - 140 * LayoutContants.getInstance().HEIGHT_SCALE,
            width: LayoutContants.getInstance().SCREEN_WIDTH,
            height: 100 * LayoutContants.getInstance().HEIGHT_SCALE,
            background: this.add.image(0, 0, "TMenu_Back"), 
            scrollMode: scrollMode
        });

        var categoryItems = this.getCategoryItems();
        this.categoryGridTable.setItems(categoryItems);
        this.settingPanel.stack.add(this.categoryGridTable.stack);

        this.categoryGridTable.table.on("cell.click", (cellIndex)=>{
            let tempItem = this.categoryItems[cellIndex];

            if(this.selectedCategoryItem.index != tempItem.index){
                this.selectedCategoryItem = tempItem;
                this.markSelectedCategory();
            }
            
        });
    }

    markSelectedCategory(){
      
        this.toggleSettingPanel(false, true);
        this.settingPanel.settingButton.setActive(false);

        this.categorlSelector.clear();
        this.categorlSelector.fillStyle(0x57aafb);
        this.selectedCategoryItem.addSelectector(this.categorlSelector);
        const height = this.selectedCategoryItem.getOriginDisplayHeight();
        const width = this.selectedCategoryItem.getOriginDisplaywidth() + 30;
        const frames = 10;
        const totalSec = 200;
        if(this.selectorTimer) this.selectorTimer.destroy();
        var index = 0;
        this.selectorTimer = this.time.addEvent({
            delay: totalSec/frames,
            callback: ()=>{
                const newWidth = index * (width - height)/frames + height;
                this.categorlSelector.fillRoundedRect(-newWidth/2, -height/2, newWidth, height, height/2);
                index ++;
            },
            repeat: frames
        });

        this.modelItems = this.getItems(1);
        this.modelItemTable.setItems(this.modelItems);

    }

    getItems(scrollMode:integer):ModelItem[]{
        var result = [];
        this.models_Arr.forEach(element => {
            if(element.category == this.selectedCategoryItem.category){
                result.push(new ModelItem(this, scrollMode, element));
            }
        });
        return result;
    }

    getCategoryItems():CategoryItem[]{
        var result = [];
        this.category_Arr.forEach(element => {
            result.push(new CategoryItem(this, element));
        });
        this.categoryItems = result;
        return result;

    }

    startSloteScene(modelName:any){
        //this.removeTextures();
        //window.location.hash =  CST.SCENES.SAVE_SLOTE + '/' + modelName; 
        window.location.href = CST.HOST_ADDRESS + '#' + CST.SCENES.SAVE_SLOTE + '/' + modelName; 
        this.scene.start(CST.SCENES.SAVE_SLOTE, { initSound: true });

    }

    startModelingScene(index:integer){
        //this.removeTextures();
        //window.location.href = CST.SCENES.MODELING + '/' + this.models_Arr[index].modelName;
        window.location.href = CST.HOST_ADDRESS + '#' + CST.SCENES.SAVE_SLOTE + '/' + this.models_Arr[index].modelName;
    }

    playBackgroundSong(){

        if((localStorage.getItem("music") || 'on') === 'on'){
            if(!this.isMusicPlay){
                this.sound.stopAll();
                this.sound.play("backgroundSong", {volume:0.2, loop:true});
                this.isMusicPlay = true;
            }
        }else{
            this.isMusicPlay = false;
            this.sound.stopAll();
        }

    }

    addEventListner(){
        document.addEventListener('web.exit.shoping-cart', e => {
            }
        );
    }

    toggleSettingPanel(isOn:boolean, onlySettingPannel:boolean = false){
        const availableDistance =  this.settingPanel.availableDistance;
        //const settingPanelHeight = this.settingPanel.getHeight();

        this.settingPanel.setOn(isOn);
        //const bottomY = availableDistance + this.settingPanel.visibleBarHeight;
        
        const topY = this.settingPanel.visibleBarHeight;
        const bottomY = this.settingPanel.availableDistance + this.settingPanel.visibleBarHeight;
        if(this.timer) this.timer.destroy();
        this.timer = this.time.addEvent({
            delay: 10,
            callback: ()=>{
                if(isOn){

                    this.settingPanel.stack.y += 10;
                    if(!onlySettingPannel) this.modelItemTable.increaseDY(10);
                    this.modelItemTable.increaseTableDY(10);

                    if(this.settingPanel.stack.y > topY) {
                        this.settingPanel.stack.y = topY;
                        this.timer.destroy();
                    }
                    
                }else{

                    this.settingPanel.stack.y -= 10;
                    if(!onlySettingPannel) this.modelItemTable.increaseDY(-10);
                    this.modelItemTable.increaseTableDY(-10);
                    if(this.settingPanel.stack.y < bottomY){
                        this.settingPanel.stack.y = bottomY;
                        this.timer.destroy();
                    } 
                    
                }
            },
            loop: true
        });

    }
    
}