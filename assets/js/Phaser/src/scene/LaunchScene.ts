
class LaunchScene extends Phaser.Scene{
    key_A: Phaser.Input.Keyboard.Key;
    image: any;
    models_Arr: Array<Model> = new Array<Model>();
    soundFX: Phaser.Sound.BaseSound;
    LAYOUT_CONSTANT:LayoutContants = LayoutContants.getInstance();
    loadImage: Phaser.GameObjects.Image;
    selectedModelIndex: string;
    fullloaded:boolean;
    timer: any;
    name:any;
    constructor(){
        super({key: CST.SCENES.LAUNCH});
        this.name = CST.SCENES.LAUNCH;
        console.log('start lanuchscene')
    }
    preload(){

        this.createImagesFromAssets();
        this.loadSounds();

        Global.getInstance().setNavigation(this.scene);
        this.loadModels();
    }
    
    init(){
        Global.getInstance().setCurrentScene(this.scene);
  
        
        
    }

    create(){

        // this.helperContainer = new HelperContainer(this, this.helpersArr);
        // this.timer = this.scene.time.addEvent({
        //     delay: 1000,                // ms
        //     callback: ()=>{

        //         this.updatePingPong();
        //     },
        //     //args: [],
        //     loop: true
        // });


    }
    
    update(){

        if(this.fullloaded){

            this.sound.play("button-2");
            this.fullloaded = false;

            this.time.addEvent({
                delay: 2000,
                callback: ()=>{
                    this.startMainScene();
                    // this.startModelingScene();
                },
                loop: false
            })
            
        }
    }

    startMainScene(){
       

        if(this.models_Arr.length > 1){

            Global.getInstance().setScreenModelArr(this.models_Arr);
            // this.scene.start(CST.SCENES.MAIN);
            Global.getInstance().routeNavigation();

        }else{
            this.time.addEvent({
                delay: 1000,
                callback: ()=>{
   
                    this.startMainScene();
                },
                loop: false
            })
        }
    }

    // startModelingScene(){
    //     if(this.models_Arr.length > 1){
    //         Global.getInstance().setModel(this.models_Arr[0]);
    //         this.removeTextures();
    //         window.location.hash = CST.SCENES.MODELING + '/' + this.models_Arr[0].modelName;
    //         // CsvParser.parsePartsCsv(
    //         //     `${CST.HOST_ADDRESS}/assets/img/models/23-004_SM-HD/23-004_SM_Parts.csv`, 
    //         //     `${CST.HOST_ADDRESS}/assets/img/models/23-004_SM-HD/23-004_SM_Features.csv`, 
    //         //     (result)=>{
    //         //         Global.getInstance().setSeletectedModelParts(result);

                   
                    
    //         //         //this.scene.start(CST.SCENES.MODELING, this.models_Arr[0]);
    //         // });
    //     }else{
    //         this.time.addEvent({
    //             delay: 1000,
    //             callback: ()=>{
    //                 this.startModelingScene();
    //             },
    //             loop: false
    //         })
    //     }
    // }

    createImagesFromAssets(){
        this.load.image("EditBox_Model_1", `assets/img/EditBox_Model_1.png`);
        this.load.image("BoxBGFade", `assets/img/BoxBGFade.png`);
        this.load.image("BoxFull", `assets/img/BoxFull.png`);

        // this.load.multiatlas('individual_image','assets/img/individual_image.json', 'assets/img');
        this.load.image("shelf_adjusted", 'assets/img/shelf_adjusted.png');
        this.load.image("back_button", 'assets/img/back_button.png');
        this.load.image("StoreButton", 'assets/img/StoreButton.png');
        this.load.image("Button_AddPage", 'assets/img/Button_AddPage.png');

        //modeling scene asset
        this.load.image("EditBox_ModelMask", "assets/img/EditBox_ModelMask.png");
        this.load.image("close_rate", "assets/img/close_rate.png");
        this.load.image("rate", "assets/img/rate.png");
        this.load.image("Menu_SelectsBG", "assets/img/Menu_SelectsBG.png");
        this.load.image("Menu_SelectsBG", "assets/img/Menu_SelectsBG.png");
        this.load.image("SelectHoop", "assets/img/SelectHoop.png");
        this.load.image("FiguromoBox_BG_extension", "assets/img/FiguromoBox_BG_extension.png");
        this.load.image("BG_ValueNew_Gradient", "assets/img/BG_ValueNew_Gradient.png");
        this.load.image("BG_ValueNew", "assets/img/BG_ValueNew.png");
        this.load.image("Button_ValueNew", "assets/img/Button_ValueNew.png");
        this.load.image("ColorAdjust_Button_Dot", "assets/img/ColorAdjust_Button_Dot.png");
        this.load.image("Button_NewPreviewTab", "assets/img/Button_NewPreviewTab.png");
        this.load.image("FloatSlider_BGTrans", "assets/img/FloatSlider-BGTrans.png");
        this.load.image("FloatSlider_Strip2", "assets/img/FloatSlider-Strip2.png");
        this.load.image("ColorAdjust_Button", "assets/img/ColorAdjust_Button.png");
        this.load.image("TMenu_Selection", "assets/img/TMenu_Selection.png");
        this.load.image("TMenu_SaveON", "assets/img/TMenu_SaveON.png");
        this.load.image("Button_BackNEW", "assets/img/Button_BackNEW.png");
        this.load.image("Button_SaveNEW", "assets/img/Button_SaveNEW.png");
        this.load.image("NewSettings_BG1", "assets/img/NewSettings_BG1.png");
        this.load.image("dice", "assets/img/dice.png");
        this.load.image("back_button", "assets/img/back_button.png");
        this.load.image("solid_grey_8", "assets/img/solid_grey_8.png");
        this.load.image("TMenu_Back", "assets/img/TMenu_Back.png");
        this.load.image("Button_Undo", "assets/img/Button_Undo.png");
        this.load.image("Button_redo", "assets/img/Button_redo.png");
        this.load.image("Button_SlideGroove", "assets/img/Button_SlideGroove.png");
        this.load.image("Button_FrameSlideNew", "assets/img/Button_FrameSlideNew.png");
        this.load.image("AutoOn", "assets/img/AutoOn.png");

        //  Cloud Community Buttons
        // 
        //  Date, Like, Random, Sahre, Download
        // 
        //  * added by asset *
        
        this.load.image("Button_CloudFeatured", "assets/img/Button_CloudFeatured.png");
        this.load.image("Button_CloudRandom", "assets/img/Button_CloudRandom.png");
        this.load.image("Button_CloudDownload", "assets/img/Button_CloudDownload.png");
        this.load.image("Button_CloudLike", "assets/img/Button_CloudLike.png");
        this.load.image("Button_CloudLike_Red", "assets/img/Button_CloudLike_Red.png");
        this.load.image("Button_CloudDate", "assets/img/Button_CloudDate.png");
        this.load.image("Button_CloudShare", "assets/img/Button_CloudShare.png");

        this.load.image("Button_CloudFeatured_Blue", "assets/img/Button_CloudFeatured_Blue.png");
        this.load.image("Button_CloudRandom_Blue", "assets/img/Button_CloudRandom_Blue.png");
        this.load.image("Button_CloudDownload_Blue", "assets/img/Button_CloudDownload_Blue.png");
        this.load.image("Button_CloudLike_Blue", "assets/img/Button_CloudLike_Blue.png");
        this.load.image("Button_CloudDate_Blue", "assets/img/Button_CloudDate_Blue.png");
        this.load.image("Button_CloudShare_Blue", "assets/img/Button_CloudShare_Blue.png");
        // this.load.image("Button_Download", "assets/img/Button_Download.png");
        this.load.image("Model_Title", "assets/img/SM_Title.png");

        // margin saveslots
        this.load.image("UISaveslotBottom", "assets/img/UISaveslotBottom.png");
        this.load.image("UISaveslotMiddle", "assets/img/UISaveslotMiddle.png");
        this.load.image("UISaveslotTop", "assets/img/UISaveslotTop.png");
        this.load.image("UISaveslotCommunity", "assets/img/UISaveslotCommunity.png");
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        this.load.image("Button_Size1", "assets/img/Button-Size1.png");
        this.load.image("Button_Size1SEL", "assets/img/Button-Size1SEL.png");
        this.load.image("Button_ShopClose", "assets/img/Button_ShopClose.png");
        this.load.image("Button_ShopBuy", "assets/img/Button_ShopBuy.png");
        this.load.image("AutoOff", "assets/img/AutoOff.png");
        this.load.image("AutoOn", "assets/img/AutoOn.png");
        
        this.load.image('Fade0','assets/img/Fade0.png'); 
        this.load.image('Fade1','assets/img/Fade1.png');
        this.load.image('Fade2','assets/img/Fade2.png');
        this.load.image('Fade3','assets/img/Fade3.png');
        this.load.image('Fade4','assets/img/Fade4.png');
        this.load.image('Fade5','assets/img/Fade5.png');
        this.load.image('Fade6','assets/img/Fade6.png');
        this.load.image('Fade7','assets/img/Fade7.png');
        this.load.image('Fade8','assets/img/Fade8.png');
        this.load.image('solid','assets/img/solid.jpg');
        // this.load.image('solid-2','assets/img/solid-2.jpg');
        this.load.image('solid-new','assets/img/solid-new.png');

        this.load.multiatlas('new_tab_bg','assets/img/new_tab_bg.json', 'assets/img');
        this.load.multiatlas('proof','assets/img/proof.json', 'assets/img');

        this.load.image('ModeRainbow', 'assets/img/ModeRainbow.png')
        this.load.image('ModePalette', 'assets/img/ModePalette.png')
        this.load.image('ModeGlow', 'assets/img/ModeGlow.png')
        this.load.image('ModeBG', 'assets/img/ModeBG.png')
        this.load.image('PaletteArrowL', 'assets/img/PaletteArrowL.png')
        this.load.image('PaletteArrowR', 'assets/img/PaletteArrowR.png')
        this.load.image('BG_PaletteNew', 'assets/img/BG_PaletteNew.png')
        //setttings
        this.load.image('Button_GoToSettings', 'assets/img/Button_GoToSettings.png') //setting off
        this.load.image('Button_GoToSettingsON', 'assets/img/Button_GoToSettingsON.png')//setting on
        this.load.image('Button_RateMe02', 'assets/img/Button_RateMe02.png')//rate app icon
        this.load.image('GDRP', 'assets/img/GDRP.png')//privacypolicy

        this.load.image('ButtonSwitch_OFF', 'assets/img/ButtonSwitch_OFF.png')//switch off
        this.load.image('ButtonSwitch_ON', 'assets/img/ButtonSwitch_ON.png')//switch on
        this.load.image('NewSettings_BG2', 'assets/img/NewSettings_BG2.png')//setting background

        this.load.json(`categoryData`, `assets/img/category/Categories.json`);
        this.load.image(`IAP-Button-Category`, `assets/img/category/IAP-Button-Category.png`);

        this.load.image(`empty`, `assets/img/empty.png`);


        //load image helper images.
        var arr = [];
        for(var i = 1; i <= 3; i++ ){
            this.load.image(`helper_${i}`, `assets/img/helpers/helper_${i}.png`)//setting background
            arr.push(`helper_${i}`);
        }

        Global.getInstance().setHelpersImages(arr);

    }

    loadModels(){
        const self = this;
        self.models_Arr = [];

        Papa.parse(`${CST.HOST_ADDRESS}/assets/cvs/Real3d_V2.csv`, {
            download: true,
            complete: function(results) {
                var i = 0;
                results.data.forEach(function(entry){
                    if(i < results.data.length-1 && i > 0){
                        const model = new Model(
                            entry[CST.CSVPARSER.MODEL_NAME_COL],
                            entry[CST.CSVPARSER.ICON_NAME_COL],
                            entry[CST.CSVPARSER.IS_ORBIT_COL],
                            entry[CST.CSVPARSER.DATA_8_HD_COL],
                            entry[CST.CSVPARSER.DATA_HD_COL],
                            entry[CST.CSVPARSER.DATA_8_SD_COL],
                            entry[CST.CSVPARSER.DATA_SD_COL],
                            entry[CST.CSVPARSER.EXTRA_SMALL_SIZE_COL],
                            entry[CST.CSVPARSER.SMALL_SIZE_COL],
                            entry[CST.CSVPARSER.MEDIUM_SIZE_COL],
                            entry[CST.CSVPARSER.LARGE_SIZE_COL],
                            entry[CST.CSVPARSER.SHOP_ID_COL],
                            entry[CST.CSVPARSER.FRAME_SETUP_COL],
                            entry[CST.CSVPARSER.CATEGORY_COL],
                            entry[CST.CSVPARSER.SHOP_TEXT_COL],
                            entry[CST.CSVPARSER.SHOP_PRICE_COL],
                            entry[CST.CSVPARSER.SHOP_IMAGE_COL],
                            entry[CST.CSVPARSER.SHOP_lINK_COL],
                            entry[CST.CSVPARSER.SHOP_SKIP_COL],
                            entry[CST.CSVPARSER.ZOOM_ADJUST_COL],
                            entry[CST.CSVPARSER.MADE_OFF_COL]
                        );
                        self.models_Arr.push(model);
                    }
                    i++;
                })
                Global.getInstance().setScreenModelArr(self.models_Arr);
                self.loadColors(self);
               
            }
        });
    }

    startSaveSlote(){
        if(this.models_Arr.length > 1){
          
            window.location.hash = CST.SCENES.SAVE_SLOTE +'/'+ this.models_Arr[0].modelName;
        }else{
            this.time.addEvent({
                delay: 1000,
                callback: ()=>{
                    this.startSaveSlote();
                },
                loop: false
            })
        }
        
    }

    loadColors(scene:Phaser.Scene){
        CsvParser.parseColorCsv(`${CST.HOST_ADDRESS}/assets/cvs/Table_Pallets-ALL.csv`, (result:ColorsRange[])=>{
            scene.game.global.colorsRanges = result;

            if(Global.getInstance().checkLogoView()){
                var loadImage;
                var loadProcessImage:Phaser.GameObjects.Image;
                var pgoregress =  0;
                var virProgress = 0;
    
    
                loadProcessImage = this.add.image(this.LAYOUT_CONSTANT.SCREEN_WIDTH/2, this.LAYOUT_CONSTANT.SCREEN_HEIGHT/2, "logo", "AnimFill_2.png");
                loadProcessImage.setOrigin(0.5,0.5);
                loadImage = this.add.image(this.LAYOUT_CONSTANT.SCREEN_WIDTH/2, this.LAYOUT_CONSTANT.SCREEN_HEIGHT/2,"logoFrame1");
    
                
    
                this.load.on('progress', function (value) {
                    if(loadProcessImage){
                        if(value == 1){                    
                            pgoregress = 5;
                        }else if(value > 0.8){                    
                            pgoregress = 4;
                        }else if(value > 0.6){                   
                            pgoregress = 3;
                        }else if(value > 0.4){                    
                            pgoregress = 2;
                        }else if(value > 0.2){                    
                            pgoregress = 1;
                        }
                    }
                }, this);
        
                 this.timer = setInterval(()=>{
        
                    if(pgoregress == 5){
                        if(virProgress == 4){
                            virProgress ++;
                            loadProcessImage.setTexture("logo", "AnimFill_7.png");
                            loadImage.setTexture("logoFrame2")
                            this.fullloaded = true;
                            clearInterval(this.timer);
                        }
                    }
        
                    if(pgoregress >= 4){
                        if(virProgress == 3){
                            virProgress ++;
                            loadProcessImage.setTexture("logo", "AnimFill_6.png");
                        }
                    }
        
        
                    if(pgoregress >= 3){
                        if(virProgress == 2){
                            virProgress ++;
                            loadProcessImage.setTexture("logo", "AnimFill_5.png");
                        }
                    }
        
                    if(pgoregress >= 2){
                        if(virProgress == 1){
                            virProgress ++;
                            loadProcessImage.setTexture("logo", "AnimFill_4.png");
                        }
                    }
        
                    
                    if(pgoregress >= 1){
                        if(virProgress == 0){
                            virProgress ++;
                            loadProcessImage.setTexture("logo", "AnimFill_3.png");
                        }
                    }
                }, 500);
        
                // this.load.on('fileprogress', function (file) {
        
                // });
            }
            else{
                this.load.on('complete', ()=>{
                    this.startMainScene();
                })
            }
        });
    }

    loadSounds(){
        this.load.audio('backgroundSong', ['assets/sounds/00-000_UI_Song.mp3']);
        this.load.audio('backsound', ['assets/sounds/button-6.mp3']);
        this.load.audio('colorchangesound', ['assets/sounds/button-2.mp3']);
        this.load.audio('generalsound', ['assets/sounds/button-1.mp3']);
        this.load.audio('swoosh', ['assets/sounds/swoosh.mp3']);
        this.load.audio('slideOpen', ['assets/sounds/slideOpen.mp3']);
    }
}