
class TestScene extends Phaser.Scene{

    outContainer:Phaser.GameObjects.Container;
    innerContainer:Phaser.GameObjects.Container;
    testImage:Phaser.GameObjects.Image;
    testImage1:Phaser.GameObjects.Image;
    modelScale:number = 0.2;
    pinchStart:boolean = false;
    lastPinchVector:Phaser.Math.Vector2;
    tCenterX:number;
    tCenterY:number;
    userScale:number = 1;
    startTargetX:number;
    startTargetY:number;
    startOutX:number;
    startOutY:number;
    originX:number;
    originY:number;
    cam1: Phaser.Cameras.Scene2D.BaseCamera;

    constructor(){
        super({key:"test"});
    }
    preload(){
        this.load.image("BoxFull1", `assets/BoxFull.png`);
        this.load.image("BoxFull2", `assets/BoxFull.png`);
    }

    create(){
        

        this.cam1 = this.cameras.main.setSize(512, 384).setName('Camera 1');

        this.testImage1 = this.add.image(LayoutContants.getInstance().SCREEN_WIDTH/2, LayoutContants.getInstance().SCREEN_HEIGHT/2, "BoxFull1");
        this.testImage1.setScale(this.modelScale).setTint(0xdddddd).setAlpha(0.3);
        this.testImage1.displayWidth = 500;
        this.testImage1.displayHeight = 200;

        this.outContainer = this.add.container(LayoutContants.getInstance().SCREEN_WIDTH/2, LayoutContants.getInstance().SCREEN_HEIGHT/2);
        this.testImage = this.add.image(0,0, "BoxFull2").setScale(this.modelScale);
        this.outContainer.add(this.testImage);

        this.bindEvent();
        this.tCenterX = LayoutContants.getInstance().SCREEN_WIDTH/2;
        this.tCenterY = LayoutContants.getInstance().SCREEN_HEIGHT/2;
    }

    update(){
        var pointer = this.input.activePointer;
    }


    setScaleMulti(scale:number, posX:number, posY:number){
        
        const ox0 = this.testImage1.originX;
        const oy0 = this.testImage1.originY;

        if(this.testImage1.getBounds().contains(posX, posY)){
            
            const x = posX - this.testImage1.x + this.testImage1.displayWidth * ox0;
            const y = posY - this.testImage1.y + this.testImage1.displayWidth * oy0;

            const newOx = x/this.testImage1.displayWidth;
            const newOy = y/this.testImage1.displayHeight;

            var dx = this.testImage1.displayWidth*(newOx-ox0);
            var dy = this.testImage1.displayHeight*(newOy-oy0);

            this.testImage1.x += dx;
            this.testImage1.y += dy;

            this.testImage1.setOrigin(newOx, newOy);
            this.outContainer.setPosition(this.testImage1.x - dx, this.testImage1.y - dy);
        }

        this.testImage1.scale *= scale;
        
        var ux = this.testImage1.x - this.testImage1.displayWidth * this.testImage1.originX + this.testImage1.displayWidth/2;
        var uy = this.testImage1.y - this.testImage1.displayWidth * this.testImage1.originY +this.testImage1.displayHeight/2;
        this.outContainer.scale *= scale;
        this.outContainer.x = ux;
        this.outContainer.y = uy;

        
    }

    refreshScale(startPosX:number, startPosY:number, startTargetX:number, startTargetY:number){

        // var dx = startPosX - startTargetX;
        // var dy = startPosY - startTargetY;

        
        
        // this.outContainer.x = LayoutContants.getInstance().SCREEN_WIDTH/2;
        // this.outContainer.y =  LayoutContants.getInstance().SCREEN_HEIGHT/2;

        // console.log("scale end dex", this.innerContainer.x);

        // this.innerContainer.x += dx/this.userScale; 
        // this.innerContainer.y += dy/this.userScale;
    }

    bindEvent(){
        this.input.keyboard.on("keyup_SHIFT", ()=>{
            this.pinchStart = false;
        });

        this.input.keyboard.on("keydown_SHIFT", ()=>{
            
            this.lastPinchVector = undefined;
            if(this.input.activePointer.isDown && !this.pinchStart){
                this.pinchStart = true;
                this.tCenterX = this.input.activePointer.x;
                this.tCenterY = this.input.activePointer.y;
            }
        });


        this.input.on("pointermove", (events, targets)=>{

            if(this.pinchStart && events.isDown){

                const currentVetor = new Phaser.Math.Vector2(events.x, events.y);
                const originVector = new Phaser.Math.Vector2(this.outContainer.x, this.outContainer.y);
                
                if(this.lastPinchVector == undefined){
                    this.lastPinchVector = currentVetor;
                }

                this.setScaleMulti(currentVetor.distanceSq(originVector)/this.lastPinchVector.distanceSq(originVector),
                    this.tCenterX, 
                    this.tCenterY
                );

                this.lastPinchVector = currentVetor;
            }
        });

        this.input.on("pointerdown", (events, targets)=>{
            this.tCenterX = events.x;
            this.tCenterY = events.y;
        });

        this.input.on("pointerup", (events, targets)=>{
            this.refreshScale(
                this.tCenterX, 
                this.tCenterY,
                this.startTargetX,
                this.startTargetY); 
        });
    }

}