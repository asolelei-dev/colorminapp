class CommunityModel {

    callback:()=>void;

    fadeBackgroundSprite:Phaser.GameObjects.Image;
    solidBackgroundSprite:Phaser.GameObjects.Image;
    bodyParts:BaseBodyPart[];
    scene: SaveSloteScene;
    stack:Phaser.GameObjects.Container;
    model:Model;
    tabIndex:integer;
    bodyPartManager:BaseBodyPartManager;
    savedModel:SavedModel; 
    jsonData:JSON;
    // cloudBoardConfig: { x: number, y: number, width: number, height: number } 

    downloadButton: CustomImageTextureButton;
    likeButton: CustomImageTextureButton;
    counter: { download: number, like: number }
    shelfSprite: { displayWidth: number, displayHeight: number }

    constructor(
            scene: SaveSloteScene, 
            width: number, 
            height:number, 
            model?:Model, 
            jsonData?:JSON,
        ){

        this.scene = scene;
        this.model = model;
        this.stack = new Phaser.GameObjects.Container(scene);
        this.jsonData = jsonData;
        
        this.counter = { download: 0,  like: 0}
        this.shelfSprite = { displayWidth: width, displayHeight:height }
        this.initGroup(width, height, model);

        
    };

    initGroup(width:number, height:number,  model:Model){ 

        this.solidBackgroundSprite =  this.scene.add.image(0, 0, "Fade8")
        this.solidBackgroundSprite.setOrigin(0,0); 
        this.solidBackgroundSprite.displayHeight = this.shelfSprite.displayHeight;
        this.solidBackgroundSprite.displayWidth = this.shelfSprite.displayWidth;
        // this.solidBackgroundSprite.tint = 0x000000;

        this.fadeBackgroundSprite = this.scene.add.image(0, 0, "Fade8");
        this.fadeBackgroundSprite.displayHeight = this.shelfSprite.displayHeight;
        this.fadeBackgroundSprite.displayWidth = this.shelfSprite.displayWidth;
        this.fadeBackgroundSprite.setOrigin(0,0);
        this.fadeBackgroundSprite.setAlpha(0.5);

        // added by asset
        
        this.stack.add(this.solidBackgroundSprite);
        this.stack.add(this.fadeBackgroundSprite);
    }

    initBodyPart(){

        var width = this.shelfSprite.displayWidth;
        var height = this.shelfSprite.displayHeight;

        //init bodypart manager
        this.bodyPartManager = new BaseBodyPartManager(this.scene, this, this.scene.modelParts, width, height);

        this.bodyPartManager.setAtlasFront(`${this.model.modelName}-front`);
        this.bodyParts = this.bodyPartManager.createBodyParts(this.scene.firstFrame);
        this.bodyPartManager.setUtmostSprites();

        this.savedModel = new SavedModel(this.jsonData);
        this.bodyPartManager.setSavedProperties(this.savedModel); 
    }
    

    getDisplayWidth(){
        return this.shelfSprite.displayWidth * this.stack.scale;
    }
    getDisplayHeight(){
        return this.shelfSprite.displayHeight * this.stack.scale;
    }

    setDisplaySize(displayWidth:number, displayHeight:number, visibility:boolean = true){
        const scaleX = displayWidth/this.shelfSprite.displayWidth;
        const scaleY = displayHeight/this.shelfSprite.displayHeight;

        this.solidBackgroundSprite.displayWidth = this.shelfSprite.displayWidth;
        this.solidBackgroundSprite.displayHeight = this.shelfSprite.displayHeight;

        this.fadeBackgroundSprite.displayWidth = this.shelfSprite.displayWidth;
        this.fadeBackgroundSprite.displayHeight = this.shelfSprite.displayHeight;

        // added by asset
    }

    setModel(model:Model){
        this.model = model;
    }

    setFadeTexture(fadeTexture:string){
        this.fadeBackgroundSprite.setTexture(fadeTexture);
    }

    updateData(savedModel:SavedModel){
        this.savedModel =savedModel;
        this.bodyPartManager.setSavedProperties(this.savedModel);
    }

    updateCloudButtons(){
        this.likeButton.updateState(this.counter.like);
        this.likeButton.setStatus(!!this.counter.like);
        this.downloadButton.updateState(this.counter.download);
    }

}