class BodyPartsManager {

    textures:string[];
    scene:ModelingScene;
    uiGenerator:UIGenerator;
    modelParts:ModelPart[];
    linkedByColorBodyParts:BodyPart[];
    linkedByTabBodyParts:BodyPart[];
    hiddenBodyParts:BodyPart[];
    shownBodyParts:BodyPart[];
    fadeTexture:string;
    backgroundTexture:string;
    selectedBodyPart:BodyPart;
    bodyParts:BodyPart[];
    atlasFront:string;
    atlasFrames:string;
    averageX:number;
    averageY:number;
    selectedBodyPartIndex:integer;
    colorButtons: any;
    upMostSprite:Phaser.GameObjects.Sprite;
    downMostSprite:Phaser.GameObjects.Sprite;
    leftMostSprite:Phaser.GameObjects.Sprite;
    rightMostSprite:Phaser.GameObjects.Sprite;
    modelWidth:number;
    modelHeight:number;
    savedModel:SavedModel;
    frame: number;
    VIEW_PORT_WIDTH:number;
    VIEW_PORT_HEIGHT:number;
    bodyPartStack:Phaser.GameObjects.Container;
    bodyPartInnerStack:Phaser.GameObjects.Container;
    tCenterX:number;
    tCenterY:number;
    testImage1: Phaser.GameObjects.Image;
    testImage2: Phaser.GameObjects.Image;
    deltaScale: number = 0;
    userScale: number = 1;
    dy: number = 0;
    dx: number = 0;
    yAdjust: number;
    xAdjust: number;
    scaleDy:number;
    scaleDx:number;
    initialScale:number;
    deltaY: number;
    maxScale:number;
    minScale: number;
    bodyIndex:integer;
    // initialBodyStackY:number;
    // initialBodyStackX:number;
    originBPX: any;
    originBPY: number;
    originScale: number;
    tween:any;
    isPing:boolean = false;
    zoomAdjust:number;


    dynamicCenterX:any;
    constructor(scene:ModelingScene, modelParts:ModelPart[]){
    
        this.textures = [];
        this.scene = scene;
        this.uiGenerator = scene.uiGenerator;
        this.modelParts = modelParts;
        this.linkedByColorBodyParts = [];
        this.linkedByTabBodyParts = [];
        this.hiddenBodyParts = [];
        this.shownBodyParts = [];
        this.fadeTexture = this.uiGenerator.fadeTextures[0];
        this.backgroundTexture = this.uiGenerator.backgroundTexture;
        this.setAverageXY();
        this.VIEW_PORT_HEIGHT = +scene.game.config.height
        this.VIEW_PORT_WIDTH = scene.uiGenerator.backgroundBoxTopper.displayWidth;
        this.bodyPartStack = this.scene.add.container(this.VIEW_PORT_WIDTH/2,this.VIEW_PORT_HEIGHT/2);

        this.bodyPartInnerStack = this.scene.add.container(0, 0);

        this.bodyPartStack.add(this.bodyPartInnerStack);

        // this.testImage1= this.scene.add.image(0, 0, "");
        // this.testImage2= this.scene.add.image(0, 0, "");

        this.dy = 0;
        this.dx = 0;
        this.yAdjust = this.modelParts[1].yAdjust;
        this.xAdjust = this.modelParts[1].xAdjust;
        this.scaleDy = 0;

        this.userScale = this.modelParts[1].scaleAdjust;
        if(this.userScale == 0) this.userScale = 1;
        const url = window.location.href;
        if(url.includes('color_debug'))
            this.addDebugFunction();
        this.originBPX = 0;
        this.zoomAdjust = this.scene.model.zoomAdjust;
    }   

    setAtlasFront(atlasFront:string){
        this.atlasFront = atlasFront;
    }

    dispose(){

    }

    setAverageXY(){

        this.VIEW_PORT_HEIGHT = LayoutContants.getInstance().SCREEN_HEIGHT
        this.VIEW_PORT_WIDTH = LayoutContants.getInstance().SCREEN_WIDTH

        var total_X = 0;
        var total_Y = 0;
        var totals = 0;
        this.modelParts.forEach(element => {
            if(!element.partName.includes("Shadow")){
                totals += 1;
                total_X += +element.partPosX;
                total_Y += +element.partPosY;
            }
        });

        this.averageX = total_X / totals;
        this.averageY = total_Y / totals;
        // this.averageY = this.VIEW_PORT_HEIGHT/2;

    }

    setLinkedBodyParts(){
        this.setLinkedByTabBodyParts();
        this.setLinkedByColorSprites();
    }

    setLinkedByTabBodyParts(){
        this.linkedByTabBodyParts = [];
        const currentTabLink = +this.getSelectedBodyPartTabLink();
        if(currentTabLink == 0) return;

        for(var i = 0; i < this.bodyParts.length; i++){

            if(this.bodyParts[i].modelPart.partName == this.selectedBodyPart.modelPart.partName){
                continue;
            }

            const tablLink = +this.bodyParts[i].modelPart.feature.tabLink;

            if(tablLink == null || tablLink != currentTabLink){
                continue;
            }

            this.linkedByTabBodyParts.push(this.bodyParts[i]);
        }
    }

    getSelectedBodyPartTabLink(){
        const chosenModelPart = this.modelParts[this.selectedBodyPart.bodyPartIndex];
        return chosenModelPart.feature.tabLink;
    }

    setLinkedByColorSprites(){
        
        this.linkedByColorBodyParts = [];
        const currentColorLink = +this.getSelectedBodyPartColorLink();

        if(currentColorLink == 0) return;

        for(var i = 0; i < this.bodyParts.length; i++){

            if(this.bodyParts[i].modelPart.partName == this.selectedBodyPart.modelPart.partName){
                continue;
            }

            const colorLink = +this.bodyParts[i].modelPart.feature.colorLink;

            if(colorLink == null || colorLink != currentColorLink){
                continue;
            }

            this.linkedByColorBodyParts.push(this.bodyParts[i]);
        }

        
     

    }

    getSelectedBodyPartColorLink(){

        return this.modelParts[this.selectedBodyPart.bodyPartIndex].feature.colorLink;
    }

    /**
     * create bodyparts, child-bodypart/top-menu buttons and load sprites 
     * @param atlas string
     * @param frame integer
     */
    createBodyParts(atlas:string, frame:integer):BodyPart[]{

        this.bodyParts = [];
        this.frame = frame;
        this.modelParts.forEach(modelPart => {
            const modelPartIndex = this.modelParts.indexOf(modelPart);
            var bodyPart = new BodyPart(this.scene, this, modelPart, modelPartIndex);
            this.bodyParts[modelPartIndex] = bodyPart;
            
            if(atlas == this.atlasFront){
                
                if(bodyPart.modelPart.feature.isChildren){
                    // if child bodypart
                    bodyPart.setChildBodyPartButton();
                }else{
                    // if parent bodypart
                    bodyPart.setTMenuButton();
                }
            }
            bodyPart.setSprites(frame);
        });
        
        return this.bodyParts;
    }



    setMaterial(tabIndex:integer){
       
        // this.scene.stopBlinking();
        if(this.selectedBodyPart.isParent()){
            // let parentBodyPart = this.selectedBodyPart.getParnetBodyPart();
            // parentBodyPart.setMaterial(tabIndex);
            // this.setlinkedSpritesMaterial(tabIndex);
            // this.setColorButtonImages(parentBodyPart.material);
        // }else{
            this.selectedBodyPart.setMaterial(tabIndex);
            this.setlinkedSpritesMaterial(tabIndex);
            this.setColorButtonImages(this.selectedBodyPart.material);
            this.uiGenerator.setTabSelector();
        }
        
        
    }



    setlinkedSpritesMaterial(tabindex:integer) {

        if (this.linkedByTabBodyParts.length == 0) {
            return;
        }
        this.linkedByTabBodyParts.forEach(bodyPart => {
            const linkedMaterials = bodyPart.modelPart.materials[tabindex];
            if (linkedMaterials.clayRegion != null) {
                bodyPart.setMaterial(tabindex);
            }
        });
        
    }


    // set sprites into bodypartinnerstack
    setSprite(regionName:string, modelPart:ModelPart, target:BodySprite, frame:integer){
        
        var atlas;
        const resolution = this.scene.getResolution();
        if(regionName.endsWith(`${this.scene.firstFrame}`)){
            atlas = this.scene.getAtlasfront();
        }else{
            atlas = `${this.atlasFrames}-${resolution}`;
        }

        const xSize =  +modelPart.partSizeX;
        const ySize =  +modelPart.partSizeY;
        const screenWidth = this.VIEW_PORT_WIDTH;
        const screenHeight = this.VIEW_PORT_HEIGHT;
        const xPos = +modelPart.partPosX - this.averageX;
        const yPos = - +modelPart.partPosY + this.averageY;
        const x = xPos / LayoutContants.getInstance().SCREEN_HEIGHT_COEF;
        const y = yPos / LayoutContants.getInstance().SCREEN_HEIGHT_COEF;
        const orderIndex  = +modelPart.sortOrder;
        
        if(target == undefined){

            if(this.scene.isFileLoaded){

                if(regionName.includes(NOTHING)){
                    target = new BodySprite(this.scene, x, y, "empty");
                    target.setName("empty");
                }else{
                    target = new BodySprite(this.scene, x, y, atlas, `${regionName}.png`);
                    target.setName('noempty');
                }

                target.displayHeight = ySize / LayoutContants.getInstance().SCREEN_HEIGHT_COEF;
                target.displayWidth = xSize / LayoutContants.getInstance().SCREEN_HEIGHT_COEF;
                target.setVisible(false);
                if(regionName.toLowerCase().includes("shadow")){
                    this.bodyPartInnerStack.addAt(target, 0);
                }else{
                    this.insertBodyPart(regionName, modelPart, target, frame);
                }
            }else{
                if(atlas == this.atlasFront){
                    if(regionName.includes(NOTHING)){
                        
                        target = new BodySprite(this.scene, x, y, "empty");
                        target.setName("empty");
                    }else{
                        target = new BodySprite(this.scene, x, y, atlas, `${regionName}.png`);
                        target.setName('noempty');
                    }
                    target.displayHeight = ySize / LayoutContants.getInstance().SCREEN_HEIGHT_COEF;
                    target.displayWidth = xSize / LayoutContants.getInstance().SCREEN_HEIGHT_COEF;
                    if(regionName.toLowerCase().includes("shadow")){
                        
                        this.bodyPartInnerStack.addAt(target, 0);
                    }else{
                        this.insertBodyPart(regionName, modelPart, target, frame);
                    }
                    
                    target.setVisible(true);
                }
            }

        }else{
            const displayWidth = target.displayWidth;
            const displayHeight = target.displayHeight;
            if(regionName.includes(NOTHING)){
                target.setTexture('empty');
                target.setName("empty");
            }else{
                target.setName('noempty');
                target.setTexture(atlas, `${regionName}.png`);
            }
            target.displayHeight = displayHeight;
            target.displayWidth = displayWidth;
            if(!this.scene.isFileLoaded){
                target.setVisible(true);
            }
        }
        
        return target;
    }

    insertBodyPart(regionName:string, modelPart:ModelPart, target:BodySprite, frame:integer){
        if(!modelPart.feature.isChildren){
            if(regionName.includes("Gloss")){
                this.bodyPartInnerStack.add(target);
                target.setName(`${modelPart.feature.part_id}-${frame}`);
            }else{
                this.bodyPartInnerStack.add(target);
            }
        }else{
            const parentPart = this.bodyPartInnerStack.getByName(`${modelPart.feature.parent_id}-${frame}`);
            const parentZindex = this.bodyPartInnerStack.getIndex(parentPart);
            this.bodyPartInnerStack.addAt(target, parentZindex);
        }
    }

    updateSprite(regionName:string, modelPart:ModelPart, target:Phaser.GameObjects.Sprite){
        var atlas;
        if(regionName.endsWith(`${this.scene.firstFrame}`)){
            atlas = this.atlasFront;
        }else{
            const resolution = this.scene.getResolution();
            atlas = `${this.atlasFrames}-${resolution}`;
        }
        if(target){

            const displayWidth = target.displayWidth;
            const displayHeight = target.displayHeight;
            if(regionName.includes(NOTHING)){
                target.setTexture('empty');
                target.setName("empty");
            }else{
                target.setName('noempty');
                target.setTexture(atlas, `${regionName}.png`);
            }
            target.displayHeight = displayHeight;
            target.displayWidth = displayWidth;
        }
    }

    showBodyParts(){
        var markedAsShown = [];
        this.hiddenBodyParts.forEach(bodyPart => {
            bodyPart.showSprites();
            markedAsShown.push(bodyPart);
            bodyPart.isHidden = false;
        });

        this.hiddenBodyParts = [];

        this.shownBodyParts.forEach(bodyPart => {
            bodyPart.hideSprites();
            this.hiddenBodyParts.push(bodyPart);
            bodyPart.isHidden = true;
        });

        this.shownBodyParts = [];
        this.shownBodyParts = markedAsShown;
    }

    // set current selected bodypart object and bodypart index
    setSelectedBodyPart(selectedBodyPart:BodyPart) {
        this.selectedBodyPart = selectedBodyPart;
        this.selectedBodyPartIndex = selectedBodyPart.bodyPartIndex;
    }

    setCurrentBodyPartColor(colorMode:integer, colorButton:ColorButton){


        var color = colorButton.color;

        if(this.selectedBodyPart != null){
            this.selectedBodyPart.setColor(colorButton);
            this.selectedBodyPart.setColorMode(colorMode);
        }

        if (this.linkedByColorBodyParts.length != 0) {
            this.linkedByColorBodyParts.forEach(bodyPart => {
                bodyPart.setColor(colorButton);
                this.selectedBodyPart.setColorMode(colorMode);
                //if(checkIfContainsBodypart(self.hiddenBodyParts, bodyPart)){
                    // self.hideBodyParts();
                //}
            });
        }

        if (this.selectedBodyPart.backgroundTexture != null) {
            this.uiGenerator.backgroundSprite.setColor(Phaser.Display.Color.GetColor(color.red, color.green, color.blue));
            this.uiGenerator.solidGreySprite.tint = Phaser.Display.Color.GetColor(color.red, color.green, color.blue);
            this.uiGenerator.bottomPlaceHolder.tint = Phaser.Display.Color.GetColor(color.red, color.green, color.blue);
        }

        this.uiGenerator.colorAdjustGradient.tint = Phaser.Display.Color.GetColor(color.red, color.green, color.blue);


    }

 

    

    hideBodyParts() {
        this.hiddenBodyParts.forEach(element => {
            element.hideSprites();
        });
    }

    setInitialSelectedBodyPart(){
        if (this.selectedBodyPart == null) {
            

            this.selectedBodyPartIndex = this.getFirstBodyPart().bodyPartIndex;
            this.selectedBodyPart = this.bodyParts[this.selectedBodyPartIndex];
            this.uiGenerator.setSelectedTMenuButton(this.selectedBodyPart.tMenuButton);
      
        }
    }

    setColorButtonImages(colorButtonMaterial:Material) {
        this.colorButtons = this.uiGenerator.colorButtons;
        this.colorButtons.forEach(element => {
            element.setImages(colorButtonMaterial);
        });
    }

    getBackgroundTexture():string{
        return this.backgroundTexture;
    }

    getFadeTexture():string{
        return this.fadeTexture;
    }

    setUtmostSprites(screenMode:integer){

       
        var frame = this.frame;
        this.VIEW_PORT_WIDTH = this.scene.uiGenerator.backgroundBoxTopper.displayWidth;
        this.bodyPartStack.setPosition(this.VIEW_PORT_WIDTH/2,this.VIEW_PORT_HEIGHT/2);
        this.bodyPartInnerStack.setPosition(0,0);

        var minX = 0;
        var maxX = 0;
        var minY = 0;
        var maxY = 0;

        const bodyParts = this.bodyParts.slice(1, this.bodyParts.length - 1);
        
        bodyParts.forEach(element => {
            const sprite = element.claySprites[frame];
            
            if(sprite != null){
                if(sprite.name != "empty"){
                   
                    const tempLX =  sprite.x - sprite.displayWidth/2
                    const tempRX =  sprite.x + sprite.displayWidth/2
        
                    const tempBY =  sprite.y - sprite.displayHeight/2
                    const tempTY =  sprite.y + sprite.displayHeight/2


                    if(minX == 0){
                        minX = tempLX;
                        this.leftMostSprite = sprite;
                    }else{
                        if(tempLX < minX){
                            minX = tempLX;
                            this.leftMostSprite = sprite;
                        }
                    }

                    if(maxX == 0){
                        maxX = tempRX;
                        this.rightMostSprite = sprite;
                    }else{
                        if(tempRX > maxX){
                            maxX = tempRX;
                            this.rightMostSprite = sprite;
                        }
                    }

                    if(minY == 0){
                        minY = tempBY;
                        this.downMostSprite = sprite;
                    }else{
                        if(tempBY < minY){
                            minY = tempBY;
                            this.downMostSprite = sprite;
                        }
                    }

                    if(maxY == 0){
                        maxY = tempTY;
                        this.upMostSprite = sprite;
                    }else{
                        if(tempTY > maxY){
                            maxY = tempTY
                            this.upMostSprite = sprite;
                        }
                    }

                }

            }
            
        });
        
        this.modelHeight = maxY  - minY;
        this.modelWidth = maxX  - minX;

        var bottomY = this.getBottomYByScreenMode(screenMode);
        var topY = this.getTopY();
        
        var userScale = this.userScale * (bottomY - topY) / this.modelHeight;

        if(!this.initialScale) this.initialScale = this.userScale;
        this.bodyPartStack.setScale(userScale);
        this.deltaY = bottomY - (this.bodyPartStack.y + userScale * this.upMostSprite.y);

        this.bodyPartStack.y += this.deltaY;
        this.bodyPartStack.y += this.dy + this.yAdjust/LayoutContants.getInstance().SCREEN_HEIGHT_COEF;
        this.bodyPartStack.x += this.dx + this.xAdjust/LayoutContants.getInstance().SCREEN_SIZE_COEF;

        // detect bround of model background and protect overflow model's width and height - Edited by SY
        //console.log('bodypartmanager.setUtmostSprites', this.bodyPartStack.x, this.bodyPartStack.y);

        this.maxScale = 2.5 * this.bodyPartStack.scale * this.zoomAdjust;
        this.minScale = 0.5 * this.bodyPartStack.scale;

        //console.log("this.bodyPartStack.y", this.bodyPartStack.y);
        // if(!this.initialBodyStackY)
        // this.initialBodyStackY = this.bodyPartStack.y;
        // if(!this.initialBodyStackX)
        // this.initialBodyStackX = this.bodyPartStack.x;
        this.initializeOriginalPositionAndScale();

    }

    initializeOriginalPositionAndScale(){
        console.log("initializeOriginalPositionAndScale");
        if(this.originBPX == 0){
            this.dynamicCenterX = this.bodyPartStack.x;
            this.originBPX = this.bodyPartStack.x;
            this.originBPY = this.bodyPartStack.y;
            this.originScale = this.bodyPartStack.scale;
        }
    }

    moveByY(dy:number){
        this.bodyPartStack.y += dy;
        this.uiGenerator.fadeOutBackgroundSprite();

        //restrict pan zone.
        this.restrictPanZone();
    }

    moveByX(dx:number){
        
        this.bodyPartStack.x += dx;

    }

    restrictPanZone(){
        
            const x = this.bodyPartStack.x;
        const y = this.bodyPartStack.y;
        const magnetStatus = this.scene.magnetStatus;
        const mdh = this.modelHeight * this.bodyPartStack.scale;
        const mdw = this.modelWidth * this.bodyPartStack.scale;
        const bndLX = mdw/8;
        const bndRX = this.scene.uiGenerator.buttonPreview.x - mdw/4;
        const bndTY = this.scene.uiGenerator.buttonPreview.y + mdh/8;
        var bndBY = LayoutContants.getInstance().SCREEN_HEIGHT - mdh/8;
        if(this.scene.magnetStatus != 3){
            bndBY -= LayoutContants.getInstance().BUTTONS_SIZE;
        }

        if(x < bndLX - mdw/2){
            this.bodyPartStack.x = bndLX - mdw/2;
        }

        if(x  > bndRX + mdw/2){
            this.bodyPartStack.x = bndRX + mdw/2;
        }

        if(y  < bndTY - mdh/2){
            this.bodyPartStack.y = bndTY - mdh/2;
        }

        if(y > bndBY + mdh/2){
            this.bodyPartStack.y = bndBY + mdh/2;
        }

        
        
    }

    setStackPosX(x:number){
        this.bodyPartStack.x = x;
    }

    scaleBy(dscale:number){
        this.deltaScale += dscale;
        this.uiGenerator.fadeOutBackgroundSprite();
    }


    setScaleMulti(dscale:number, posX:number, posY:number){
        console.log(dscale);

        if(dscale == 0) return;
        var scale = this.bodyPartStack.scale * dscale;

        if(scale >= this.maxScale){
            scale = this.maxScale;
        }

        if(scale <= this.minScale){
            scale = this.minScale;
        }

        if(this.tween) this.tween.remove();

        this.bodyPartStack.scale = scale;
        
        this.uiGenerator.fadeOutBackgroundSprite();
        
    }

    setScaleBy(dscale:number){
        var newScale = this.bodyPartStack.scale + dscale;
        if(newScale == 0) return;
        if(newScale >= this.maxScale){
            newScale = this.maxScale;
        }

        if(newScale <= this.minScale){
            newScale = this.minScale;
        }
        if(this.tween) this.tween.remove();
        this.bodyPartStack.scale = newScale;
        this.uiGenerator.fadeOutBackgroundSprite();
        this.restrictPanZone();
    }

    zoom(target, targetKey, value, targetIndex, totalTargets, tween) { 

    }
    
    setAtlasFrames(atlasFrames:string){
        this.atlasFrames = atlasFrames;
    }

    createFramesSprites(frame:integer){
        
        this.bodyParts.forEach(element=>{
            if(element != null){
                element.setSprites(frame);
                element.initColor();
            }
        });
    }

    resetSprite(){
        //debugger
        
        this.bodyParts.forEach(element=>{
            if(element.modelPart.partName != BACKGROUND){
                for(var i = this.scene.minFrame; i <= this.scene.maxFrame; i++){
                    element.updateBodyPart(i);
                    element.setSpriteFromMaterial(i);
                }
                element.initColor();
            }else{
                element.initColor();
                this.uiGenerator.backgroundSprite.setColor(Phaser.Display.Color.GetColor(element.color.red, element.color.green, element.color.blue));
                this.uiGenerator.solidGreySprite.tint = Phaser.Display.Color.GetColor(element.color.red, element.color.green, element.color.blue);
            }
        });
        
    }

    setSavedProperties(savedModel:SavedModel){
        this.savedModel = savedModel;
        for (var i = 0; i < this.bodyParts.length - 1; i++) {
            this.setPalleteNumbersFromSavedModel(this.bodyParts[i]);
            this.setMaterialFromSavedModel(this.bodyParts[i]);
        }

        this.uiGenerator.palleteNumber  = this.bodyParts[1].palleteNumber;

        for (var i = 0; i < this.bodyParts.length - 1; i++) {
            this.setColorFromSavedModel(this.bodyParts[i]);
            this.setButtonIndexFromSavedModel(this.bodyParts[i]);
        }

        this.selectedBodyPart = this.getFirstBodyPart();
        this.selectedBodyPartIndex = this.selectedBodyPart.bodyPartIndex;
        
        if(this.selectedBodyPart.colorModeNumber > 9){
            this.uiGenerator.setColorMode(CST.COLOR_MODE.CUSTOME);
            this.uiGenerator.colorPalletePanel.setPalleteNumber(this.selectedBodyPart.colorModeNumber);
        }else{
            this.uiGenerator.setColorMode(CST.COLOR_MODE.DEFAULT);
        }
        this.uiGenerator.setColorSelection(this.bodyParts[this.selectedBodyPartIndex].colorButton.buttonIndex);
    }

    setPalleteNumbersFromSavedModel(bodyPart:BodyPart) {
        const modelPartName = bodyPart.modelPart.partName;
        if (this.savedModel.getSliderValues().has(modelPartName)) {
            const sliderPosition = +this.savedModel.getSliderValues().get(modelPartName);
            bodyPart.setInitialPalletNumber(sliderPosition);
        }
    }

    setMaterialFromSavedModel(bodyPart:BodyPart) {
        const modelPartName = bodyPart.modelPart.partName;
        if (this.savedModel.getActiveTabs().has(modelPartName)) {
            const tabIndex = this.savedModel.getActiveTabs().get(modelPartName);
            this.selectedBodyPart = bodyPart;
            this.selectedBodyPartIndex = bodyPart.bodyPartIndex;
            this.setMaterial(tabIndex);
            this.scene.getModelCharacteristics().setFinish(tabIndex);
        }
    }

    setColorFromSavedModel(bodyPart:BodyPart) {
 
        const modelPartName = bodyPart.modelPart.partName;
        if(bodyPart.colorButton){
            bodyPart.colorButton.isMarked = false;
            bodyPart.colorButton.resetStack();
        }
        if (this.savedModel.getPartColors().has(modelPartName)) {
            const customColorData = this.savedModel.getPartColors().get(modelPartName);
            var a = 1;
            const customColor = new Phaser.Display.Color(customColorData.r * 255, customColorData.g * 255, customColorData.b * 255, a*255);

            if (bodyPart != null) {
                bodyPart.setSelectorColor(customColor);
            }
            if (bodyPart.bodyPartIndex == 0) {
                this.uiGenerator.bottomPlaceHolder.tint = Phaser.Display.Color.GetColor(customColor.red, customColor.green, customColor.blue);
                this.uiGenerator.solidGreySprite.tint = Phaser.Display.Color.GetColor(customColor.red, customColor.green, customColor.blue);
            }
        }
    }

    setButtonIndexFromSavedModel(bodyPart:BodyPart) {
        const modelPartName = bodyPart.modelPart.partName;
        if (this.savedModel.getButtonsIndex().has(modelPartName)) {
            const buttonsIndex = this.savedModel.getButtonsIndex().get(modelPartName);
            const colorButton = this.uiGenerator.colorButtons[buttonsIndex];
            bodyPart.colorButton = colorButton;
            colorButton.addToColoredBodyPartsCount();
        }
    }

    renderBodyPart(){
        this.setBodyPartByFrame(this.frame);
    }
    setBodyPartByFrame(frame:integer){
        if(this.bodyParts){
            for (var i = 0; i < this.bodyParts.length; i++) {
                if (this.bodyParts[i] != null) {
                    this.bodyParts[i].render(frame);
                }
            }
        }
    }

    rotateLeft(){
        const newframe = this.frame + 1;
        this.frame = newframe;
        if(this.scene.model.isOrbit == "0"){
            if(this.frame > this.scene.maxFrame) this.frame = this.scene.maxFrame;
        }else{
            if(this.frame == this.scene.maxFrame+1){
                this.frame = 1;
            }
        }
    }

    rotateRight(){
        const newframe = this.frame - 1;
        this.frame = newframe;

        if(this.scene.model.isOrbit == "0"){

            if(this.frame < 0) this.frame = 0;

        }else{
            if(this.frame == 0){
                this.frame = this.scene.maxFrame;
            }
        }


        
    }
    
    updateModel(){
        this.setBodyPartByFrame(this.frame);
    }

    setFrame(newframe:integer){
        this.frame = newframe;
    }

    getCurrentSelectedPartPos():any{
        const selectedbodyPart = this.selectedBodyPart.claySprites[this.frame];
        if(selectedbodyPart){
            const defaultX = this.VIEW_PORT_HEIGHT/2;
            const defaultY = this.VIEW_PORT_WIDTH/2;
            if(selectedbodyPart != null){
                // const posX =  selectedbodyPart.x + this.averageX;
                // const posY =  selectedbodyPart.y - this.averageX;
                var scale = this.bodyPartStack.scale;

                return {x:selectedbodyPart.x * scale + this.bodyPartStack.x, y:selectedbodyPart.y*scale + this.bodyPartStack.y};    
            }else{
                return {x:defaultX, y:defaultY};
            }    
        }
    }

    saveModel(){

        var activeTabsMap = new Map<string, number>();
        var partColorsMap = new Map<string, any>();
        var sliderValuesMap = new Map<string, number>();
        var buttonsIndexMap = new Map<string, number>();

        for (var i = 0; i < this.bodyParts.length; i++) {
            var modelPartName = this.bodyParts[i].modelPart.partName;
            activeTabsMap.set(modelPartName, this.bodyParts[i].selectedTabIndex);

            var color = this.bodyParts[i].color;
            if (color != null) {
                var customColor = {
                    r:color.red/255,
                    g:color.green/255,
                    b:color.blue/255,
                    a:color.alpha/255
                };
                partColorsMap.set(modelPartName, customColor);
            }

            const bodyPart = this.bodyParts[i];
            var palleteNumber = this.bodyParts[i].palleteNumber;
            if (bodyPart.colorModeNumber > 9){//custom color mode
                palleteNumber = bodyPart.colorModeNumber;
            }

            sliderValuesMap.set(modelPartName, palleteNumber);

            var colorButton = this.bodyParts[i].colorButton;
            if (colorButton != null) {
                buttonsIndexMap.set(modelPartName, this.bodyParts[i].colorButton.buttonIndex);
            }
        }

        this.savedModel.setActiveTabs(activeTabsMap);
        this.savedModel.setPartColors(partColorsMap);
        this.savedModel.setSliderValues(sliderValuesMap);
        this.savedModel.setButtonsIndex(buttonsIndexMap);

        Global.getInstance().saveCurrentModel(this.savedModel);
    }

    getBottomYByScreenMode(screenMode:integer){

        if(screenMode == CST.LAYOUT.EDIT_MODE || screenMode == CST.LAYOUT.PREVIEW_MODE){
            return (this.VIEW_PORT_HEIGHT - 300 / LayoutContants.getInstance().SCREEN_HEIGHT_COEF);
        }

        if(screenMode == CST.LAYOUT.SAVE_PREVIEW_MODE || screenMode == CST.LAYOUT.SHOP_MODE){
            return (this.VIEW_PORT_HEIGHT - 220 / LayoutContants.getInstance().SCREEN_HEIGHT_COEF);
        }
    }

    getTopY(){
        return LayoutContants.getInstance().BUTTONS_SIZE_BASE/LayoutContants.getInstance().SCREEN_HEIGHT_COEF;
    }

    resetbodyPartStackScale(screenMode:integer){
        if(screenMode == CST.LAYOUT.SAVE_PREVIEW_MODE){
            var bottomY = this.getBottomYByScreenMode(screenMode);
            this.userScale = this.modelParts[1].scaleAdjust;
            if(this.userScale == 0) this.userScale = 1;
            var userScale = this.userScale * (bottomY - LayoutContants.getInstance().BUTTONS_SIZE) / this.modelHeight ;
        }
    }

    resetDXDY(){
        this.dx = 0;
        this.dy = 0;
        if(this.initialScale)this.userScale = this.initialScale;
        this.scaleDy = 0;
        this.scaleDx = 0;
    }

    getFirstBodyPart(){
        
        var firstBodyPart = this.bodyParts[1];
        
        this.bodyParts.forEach(element => {
            if(+element.modelPart.tMenuOrder > 0 && firstBodyPart.modelPart.tMenuOrder > element.modelPart.tMenuOrder) firstBodyPart = element
        });
        return firstBodyPart
    }

    addDebugFunction(){

        var data = {
            xAdjustment:this.xAdjust,
            yAdjustment:this.yAdjust,
            scaleAdjustment:this.userScale
        }

        var event = new CustomEvent('setDebugFrom', { detail: data });
        document.dispatchEvent(event);


        document.addEventListener('setCharacterAdjustment', (e:any) => {
            this.xAdjust = e.detail.xAdjustment;
            this.yAdjust = e.detail.yAdjustment;
            this.userScale = e.detail.scaleAdjustment;
            //console.log("bodyspartmanager.adddebugfunction 1", this.uiGenerator.backgroundSprite.displayWidth, this.uiGenerator.backgroundSprite.displayHeight)
            //console.log("bodyspartmanager.adddebugfunction 2", this.xAdjust, this.yAdjust)
            //if(this.xAdjust > this.uiGenerator.backgroundSprite)
            this.setUtmostSprites(this.scene.screenMode);
        }
    );
    }

    getBodyPartPos(){
        return {
            x:this.bodyPartInnerStack.x + this.bodyPartStack.x,
            y:this.bodyPartInnerStack.y + this.bodyPartStack.y,
            scale:this.bodyPartInnerStack.scale * this.bodyPartStack.scale
        }
    }

    checkPixelPerfectPos(x, y){
        var touchedElements = [];
        const scale = this.bodyPartInnerStack.scale * this.bodyPartStack.scale
        for(var element of this.bodyParts) {
            if(element.modelPart.feature.paralax != "1"){
                const touchedElement = element.checkPixelPerfectPos(x, y, scale, this.frame);
                if(touchedElement != undefined) {
                    
                    touchedElements.push(touchedElement);
                }
            }
        }

        if(touchedElements.length>0){
            touchedElements[touchedElements.length-1].performSelection();
        }
        
        if(touchedElements.length == 0){
            if(this.scene.uiGenerator.viewMode == CST.LAYOUT.SHOP_MODE || this.scene.uiGenerator.viewMode == CST.LAYOUT.SAVE_PREVIEW_MODE) return;
            if(LayoutContants.getInstance().modelMoving) return;

            if(this.uiGenerator.backgroundSprite.getBounds().contains(x, y)){
                this.uiGenerator.backgroundSprite.performSelection();
            }
        }

        setTimeout(() => {
            this.uiGenerator.backgroundSprite.resetColor();  
            this.uiGenerator.solidGreySprite.tint = this.uiGenerator.backgroundSprite.originTint;
        }, 100);
        
    }

    resetBodyPartStack(){

        // let x = 0;

        // switch(this.scene.screenMode){
        //     case CST.LAYOUT.EDIT_MODE:
        //             x = this.originBPX;
        //         break;
        //     case CST.LAYOUT.PREVIEW_MODE:
        //     default:
        //         x = LayoutContants.getInstance().SCREEN_WIDTH/2; 
        // }

        this.scene.tweens.add({
            targets: this.bodyPartStack,
            x: this.dynamicCenterX,
            y: this.originBPY,
            scale: this.originScale,
            ease: 'Cubic.easeIn',  
            duration: 500,
        });

    }
    
}