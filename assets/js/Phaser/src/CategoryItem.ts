class CategoryItem {

    scene: Phaser.Scene;
    stack: Phaser.GameObjects.Container;
    itemImage: Phaser.GameObjects.Image;
    category:string;
    index:integer;
    selectedImage: Phaser.GameObjects.Image;

    constructor(scene:Phaser.Scene, category:string){
        this.category = category;
        this.scene = scene;
        this.stack = this.scene.add.container(0, 0);
        this.itemImage = new Phaser.GameObjects.Image(scene, 0, 0, category);
        this.stack.add(this.itemImage);
        this.stack.setScale(LayoutContants.getInstance().HEIGHT_SCALE);
    }

    getDisplayHeight(){
        return this.itemImage.displayHeight * LayoutContants.getInstance().HEIGHT_SCALE;
    }
    getDisplayWidth(){
        return this.itemImage.displayWidth * LayoutContants.getInstance().HEIGHT_SCALE;
    }

    getOriginDisplaywidth(){
        return this.itemImage.displayWidth;
    }

    addSelectector(selector:Phaser.GameObjects.Graphics){
        this.stack.removeAll();
        this.stack.add(selector);
        this.stack.add(this.itemImage);
    }

    getOriginDisplayHeight(){
        return this.itemImage.displayHeight;
    }

}