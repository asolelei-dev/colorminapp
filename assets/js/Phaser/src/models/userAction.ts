class UserAction {
    uiGenerator:UIGenerator;
    bodyPartIndex:integer;
    colorPositionPair:ColorPositionPair;
    tabIndex:integer;
    

    constructor(uiGenerator:UIGenerator, bodyPartIndex:integer, colorPositionPair:ColorPositionPair, tabIndex:integer) {
        this.uiGenerator = uiGenerator;
        this.bodyPartIndex = bodyPartIndex;
        this.colorPositionPair = colorPositionPair;
        this.tabIndex = tabIndex;
        
    }

    performAction(){

        // this.uiGenerator.setColorSelection(this.uiGenerator.getSelectedColorButton().buttonIndex);
        this.uiGenerator.chooseTMenuButton(this.bodyPartIndex);
        
        this.uiGenerator.chooseTabButton(this.tabIndex);
        this.uiGenerator.chooseColorButton(this.colorPositionPair);

        // if(this.childColorLink){
        //     this.uiGenerator.chooseChildColorLink(this.childColorLink);
        // }
        
    }

}