class Global {

    private static instance: Global;
    modelArr: Model[];
    selectedModel: Model;
    modelParts: ModelPart[];
    savedModel: SavedModel;
    savedModelIndex:integer;
    sloteItemCountSet:Map<string, number> = new Map();
    modelCharacteristics = new ModelCharacteristics();
    resetModel: boolean;
    currentScene: any;
    helperImageArr:string[];

    public constructor() {
        // do something construct...
    }
    static getInstance() {
        if (!Global.instance) {
            Global.instance = new Global();
            // ... any one time initialization goes here ...
        }
        return Global.instance;
    }

    setScreenModelArr(arr:Array<Model>) { 
        this.modelArr = arr;
        
    }

    getModelArr():any{      
        return this.modelArr;
    }


    getModelByName(modelName:string){
        
        for(var i=0;i<this.modelArr.length;i++){
            if(this.modelArr[i].modelName === modelName)
            {
                return this.modelArr[i];
            }
        }

        return null;
    }
    setModel(model:Model){
        this.selectedModel = model;
    }

    getModel(){
        return this.selectedModel;
    }

    setCurrentModel(model:SavedModel){
        this.savedModel = model;
    }

    saveCurrentModel(model:SavedModel){
        this.savedModel.savetoJson(this.selectedModel.modelName);
    }

    getSavedModel():SavedModel {

        return this.savedModel;
    }

    setSeletectedModelParts(modelParts:ModelPart[]){
        this.modelParts = modelParts;
    }

    getSeletectedModelParts():ModelPart[]{
        return this.modelParts;
    }

    setSloteItemsCount(modelName:string, sloteItemsCount:integer){
        this.sloteItemCountSet.set(modelName, sloteItemsCount);
    }

    getSloteItemsCount(modelName:string){
        if(this.sloteItemCountSet.has(modelName)){
            return this.sloteItemCountSet.get(modelName);
        }else{
            return 0
        }
        
    }

    getModelCharacteristics():ModelCharacteristics{
        return this.modelCharacteristics;
    }

    setResetModelFlag(flag: boolean){
        this.resetModel = flag;

    }

    getResetModelFlag(){
        return this.resetModel;
    }

    setCurrentScene(curScene:any){
        this.currentScene = curScene;
    }

    getCurrentScene(){
        return this.currentScene;
    }

    setNavigation(_scene:any){
        // window.onhashchange = () =>{
        //     // this.routeNavigation();
        // }
    }
    checkLogoView(){
    
        //get current url
        const url = window.location.href;
        // convert url string to URL object
        var currentURL = new URL(url);
      
        // parse hash
        let urlHash = currentURL.hash;
        let urlSegments = [null, null];
        if(urlHash)
            urlSegments =  urlHash.replace('#','').split('/');
        
    
        
        /**
         * Route Navigations
         * [0] - Scene Name
         * [1] - Model Name
         */

        switch(urlSegments[0])
        {                
            case CST.SCENES.SAVE_SLOTE:
                if(urlSegments[1]){
                    //let curModel = Global.getInstance().getModelByName(urlSegments[1]);
                    //if(curModel){
                        return false
                    //}
                }                
            case CST.SCENES.MODELING:
                if(urlSegments[1]){
                    let curModel = Global.getInstance().getModelByName(urlSegments[1]);
                    //if(curModel){                        
                       return false
                    //}
                }
            case CST.SCENES.MAIN:
                return false;

            default:
                return true;
        }
    }

    setHelpersImages(arr:string[]){
        this.helperImageArr = arr;
    }

    getHelperImages(){
        return this.helperImageArr;
    }

    routeNavigation(){
        let _scene = this.currentScene;
        //get current url
        const url = window.location.href;
        // convert url string to URL object
        var currentURL = new URL(url);
      
        // parse hash
        let urlHash = currentURL.hash;
        let urlSegments = [null, null];
        if(urlHash)
            urlSegments =  urlHash.replace('#','').split('/');
        
    
        
        /**
         * Route Navigations
         * [0] - Scene Name
         * [1] - Model Name
         */
        
        switch(urlSegments[0])
        {                
            case CST.SCENES.SAVE_SLOTE:
                if(urlSegments[1]){
                    let curModel = Global.getInstance().getModelByName(urlSegments[1]);
                    if(curModel){

                     
                        Global.getInstance().setModel(curModel);
                       
                        _scene.start(CST.SCENES.SAVE_SLOTE, {initSound:true});
                        break;
                    }
                }                
            case CST.SCENES.MODELING:
                if(urlSegments[1]){
                    let curModel = Global.getInstance().getModelByName(urlSegments[1]);
                    if(curModel){
                        Global.getInstance().setModel(curModel);                        
                        // _scene.start(CST.SCENES.MODELING);
                        _scene.start(CST.SCENES.SAVE_SLOTE, {initSound:true});
                        // Global.getInstance().setModel(curModel);
                        // _scene.start(CST.SCENES.SAVE_SLOTE,{direct:true});
                        
                        break;
                    }
                }
            case CST.SCENES.MAIN:  
        
                if(Global.getInstance().getCurrentScene().name == CST.SCENES.MAIN)                             
                    break;
                 
                    _scene.start(CST.SCENES.MAIN);
                break;

            default:
                window.location.hash = CST.SCENES.MAIN;
                _scene.start(CST.SCENES.MAIN);
        }
    }

}