enum Finish {
    NORMAL = "Natural",
    GLOSS = "Gloss",
}

enum SwitchStatus {
    LEFT = -1,
    STOP = 0,
    RIGHT = 1
}