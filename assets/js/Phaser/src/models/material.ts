class Material {

    clayRegion:string;
    glossRegion:string;
    glossAlpha:number;
    clayButtonRegion:string;
    glossButtonRegion:string;
    glossButtonAlpha:number;

    constructor(clayRegion:string,
        glossRegion:string,
        glossAlpha:string,
        clayButtonRegion:string,
        glossButtonRegion:string,
        glossButtonAlpha:string){

            this.clayRegion = clayRegion;
            this.glossRegion = glossRegion;
            this.glossAlpha = +glossAlpha;
            this.clayButtonRegion = clayButtonRegion;
            this.glossButtonRegion = glossButtonRegion;
            this.glossButtonAlpha = +glossButtonAlpha;        
    }
}