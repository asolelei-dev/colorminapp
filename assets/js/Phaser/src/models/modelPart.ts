class ModelPart {

    partName:string;
    sortOrder:string;
    tMenuOrder:integer;
    partSizeX:number;
    partSizeY:number;
    partPosX:number;
    partPosY:number;
    scaleAdjust:number;
    xAdjust:number;
    yAdjust:number;
    partBaseImage:string;
    partGlossImage:string;
    glossAlpha:string;
    colorButtonBaseImage:string;
    colorButtonGlossImage:string;
    buttonGlossAlpha:string;
    tabRegions:string[];
    materials:Material[];
    feature:Feature;

    constructor(
        partName:string,
        sortOrder:string,
        tMenuOrder:string,
        partSizeX:string,
        partSizeY:string,
        partPosX:string,
        partPosY:string,
        scaleAdjust:string,
        xAdjust:string,
        yAdjust:string,
        partBaseImage:string,
        partGlossImage:string,
        glossAlpha:string,
        colorButtonBaseImage:string,
        colorButtonGlossImage:string,
        buttonGlossAlpha:string,
        tabRegions:string[],
        materials:Material[],
        feature:Feature
    ){
        this.partName = partName;
        this.sortOrder = sortOrder;
        this.tMenuOrder = +tMenuOrder;
        this.partSizeX = +partSizeX;
        this.partSizeY = +partSizeY;
        this.partPosX = +partPosX;
        this.partPosY = +partPosY;
        this.scaleAdjust = +scaleAdjust;
        this.xAdjust = +xAdjust;
        this.yAdjust = +yAdjust;
        this.partBaseImage = partBaseImage;
        this.partGlossImage = partGlossImage;
        this.glossAlpha = glossAlpha;
        this.colorButtonBaseImage = colorButtonBaseImage;
        this.colorButtonGlossImage = colorButtonGlossImage;
        this.buttonGlossAlpha = buttonGlossAlpha;
        this.tabRegions = tabRegions;
        this.materials = materials;
        this.feature = feature;
    }

    getTabRegions(){
        var result = [];
        this.tabRegions.forEach(element => {
            if(element != ""){
                result.push(element)
            }
        });
        return result;
    }
}