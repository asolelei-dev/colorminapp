class Feature {
    partName:string;
    palleteSection:string;
    partLink:string;
    partToggle:string;
    tMenuButton:string;
    tabLink:string;
    colorLink:string;
    paralax:string;
    part_id:integer;
    parent_id:integer;
    isChildren:boolean;
    colorLinkV2: string;
    colorModifier:integer;
    constructor(partName:string, 
        palleteSection:string,
        partLink:string,
        partToggle:string,
        tMenuButton:string,
        tabLink:string,
        colorLink:string,
        colorLinkV2:string,
        colorModifier: integer,
        paralax:string,
        part_id:integer,
        parent_id:integer
        ){
            this.partName = partName;
            this.palleteSection = palleteSection;
            this.partLink = partLink;
            this.partToggle = partToggle;
            this.tMenuButton = tMenuButton;
            this.tabLink = tabLink;
            this.colorLink = colorLink;
            this.colorLinkV2 = colorLinkV2;
            this.colorModifier = +colorModifier;
            this.paralax = paralax;
            this.part_id = part_id;
            this.parent_id = parent_id || 0;
            this.isChildren = this.parent_id > 0? true:false;

    }
}