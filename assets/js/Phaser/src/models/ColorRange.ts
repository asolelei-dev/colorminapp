class ColorsRange {

    xPos:string;
    yPos:string;
    red:number[];
    green:number[];
    blue:number[];

    constructor(
        xPos:string,
        yPos:string,
        red:number[],
        green:number[],
        blue:number[]
    ){
        this.xPos = xPos;
        this.yPos = yPos;
        this.red = red;
        this.green = green;
        this.blue = blue;
    }
}