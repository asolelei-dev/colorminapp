// interface Map {
//     [key: string]: "";
//  } 
class SavedModel {
    bloomData:Map<string, string> ;
    image:any;
    bgImg:any;
    bgImgOrientation:integer;
    slotName:any;
    filterName:any;
    filterIntensity:integer;
    backgroundIndex:integer ;
    palletMode:integer ;
    palletIndex:integer ;

    showMaster:boolean;
    activeTabs:Map<string, number>;
    partColors:Map<string, any>;
    sliderValues:Map<string, number>;
    buttonsIndex:Map<string, number>;
    index:integer;

    constructor(json:any){
        
        this.bloomData = this.objToStrMap(json.bloomData);
        this.activeTabs = this.objToStrMap(json.activeTabs);
        this.partColors = this.objToStrMap(json.partColors);
        this.sliderValues = this.objToStrMap(json.sliderValues);
        this.buttonsIndex = this.objToStrMap(json.buttonsIndex);

        this.backgroundIndex = json.backgroundIndex;
        this.bgImgOrientation = json.bgImgOrientation;
        this.filterIntensity = json.filterIntensity;
        this.filterIntensity = json.filterIntensity;
        this.palletIndex = json.palletIndex;
        this.showMaster = json.showMaster;

    }

    setIndex(index:integer){
        this.index = index;
    }

    getBloomData():Map<string, string> {
        return this.bloomData;
    }

    setBloomData(bloomData:Map<string, string>) {
        this.bloomData = bloomData;
    }

    getImage() {
        return this.image;
    }

    setImage(image:any) {
        this.image = image;
    }

    getBgImg() {
        return this.bgImg;
    }

    setBgImg(bgImg:any) {
        this.bgImg = bgImg;
    }

    getBgImgOrientation() {
        return this.bgImgOrientation;
    }

    setBgImgOrientation(bgImgOrientation:number) {
        this.bgImgOrientation = bgImgOrientation;
    }

    getSlotName() {
        return this.slotName;
    }

    setSlotName(slotName:any) {
        this.slotName = slotName;
    }

    getFilterName() {
        return this.filterName;
    }

    setFilterName(filterName:any) {
        this.filterName = filterName;
    }

    getFilterIntensity():number{
        return this.filterIntensity;
    }

    setFilterIntensity(filterIntensity:number) {
        this.filterIntensity = filterIntensity;
    }

    getBackgroundIndex() {
        return this.backgroundIndex;
    }

    setBackgroundIndex(backgroundIndex:number) {
        this.backgroundIndex = backgroundIndex;
    }

    getPalletMode():number {
        return this.palletMode;
    }

    setPalletMode(palletMode:number) {
        this.palletMode = palletMode;
    }

    getPalletIndex() {
        return this.palletIndex;
    }

    setPalletIndex(palletIndex:number) {
        this.palletIndex = palletIndex;
    }

    getShowMaster():boolean {
        return this.showMaster;
    }

    setShowMaster(showMaster:boolean) {
        this.showMaster = showMaster;
    }

    getActiveTabs():Map<string, number>  {
        return this.activeTabs;
    }

    setActiveTabs(activeTabs:Map<string, number>) {
        this.activeTabs = activeTabs;
    }

    getPartColors(): Map<string, any> {
        return this.partColors;
    }

    setPartColors(partColors:Map<string, any>) {
        this.partColors = partColors;
    }

    getSliderValues():Map<string, number> {
        return this.sliderValues;
    }

    setSliderValues(sliderValues:Map<string, number>) {
        this.sliderValues = sliderValues;
    }

    getButtonsIndex():Map<string, number> {
        return this.buttonsIndex;
    }

    setButtonsIndex(buttonsIndex:Map<string, number>) {
        this.buttonsIndex = buttonsIndex;
    }

    objToStrMap(obj) {
        let strMap = new Map();
        for (let k of Object.keys(obj)) {
            strMap.set(k, obj[k]);
        }
        return strMap;
    }

    savetoJson(modelName:string){
        var jsonData = this.getJSONData();
        console.log("call savetoJson")
        var event = new CustomEvent('upload_data', { detail: {model_name:modelName, index:this.index, saved_data:jsonData} });
        document.dispatchEvent(event);

        localStorage.setItem(`${modelName}${this.index}`, jsonData);
        
    }

    getJSONData(){
        var bloomData = this.strMapToObj(this.bloomData);
        var activeTabs = this.strMapToObj(this.activeTabs);
        var partColors = this.strMapToObj(this.partColors);
        var sliderValues = this.strMapToObj(this.sliderValues);
        var buttonsIndex = this.strMapToObj(this.buttonsIndex);

        var backgroundIndex = this.backgroundIndex;
        var bgImgOrientation = this.bgImgOrientation;
        var filterIntensity = this.filterIntensity;
        var palletIndex = this.palletIndex;
        var showMaster = this.showMaster;

        var jsonObj = {
            "bloomData":bloomData,
            "activeTabs":activeTabs,
            "partColors":partColors,
            "sliderValues":sliderValues,
            "buttonsIndex":buttonsIndex,
            "backgroundIndex":backgroundIndex,
            "bgImgOrientation":bgImgOrientation,
            "filterIntensity":filterIntensity,
            "palletIndex":palletIndex,
            "showMaster":showMaster
        }
        return JSON.stringify(jsonObj);
    }

    strMapToJson(strMap) {
        return JSON.stringify(this.strMapToObj(strMap));
    }

    strMapToObj(strMap) {
        var object = {};
        strMap.forEach((value, key) => {
            object[key] = value;
        });
        return object;
    }


}