class Model {

    modelName:string;
    iconName:string;
    data8HD:integer;
    dataHD:integer;
    data8SD:integer;
    dataSD:integer;
    extraSmallSize:string;
    smallSize:string;
    mediumSize:string;
    largeSize:string;
    shopID:string;
    frameSetup:string;
    isOrbit:string;
    category: string;
    shopText:string;
    shopPrice:string;
    shopImage:string;
    shopLink:string;
    shopSkip:boolean;
    zoomAdjust:number;
    modeOff:boolean;
    constructor(
        modelName:string,
        iconName:string,
        isOrbit:string,
        data8HD:string,
        dataHD:string,
        data8SD:string,
        dataSD:string,
        extraSmallSize:string,
        smallSize:string,
        mediumSize:string,
        largeSize:string,
        shopID:string,
        frameSetup:string,
        category:string,
        shopText:string,
        shopPrice:string,
        shopImage:string,
        shopLink:string,
        shopSkip:string,
        zoomAdjust:string,
        modeOff:string,
        ){
            this.modelName = modelName;
            this.iconName = iconName;
            this.data8HD = +data8HD;
            this.dataHD = +dataHD;
            this.data8SD = +data8SD;
            this.dataSD = +dataSD;
            this.extraSmallSize = extraSmallSize;
            this.smallSize = smallSize;
            this.mediumSize = mediumSize;
            this.largeSize = largeSize;
            this.shopID = shopID;
            this.frameSetup = frameSetup;
            this.isOrbit = isOrbit;
            this.category = category;
            this.shopText =shopText;
            this.shopPrice = shopPrice;
            this.shopImage = shopImage;
            this.shopLink = shopLink;
            this.shopSkip = (shopSkip == "1")? true: false;
            this.zoomAdjust = parseFloat(zoomAdjust);
            this.modeOff = (modeOff == "1")? true: false;
    }

    getExtraSmallSizeInCm():number{
        return +this.extraSmallSize.split("-")[0].replace("cm","");
    }

    getSmallSizeInCm():number{
        return +this.smallSize.split("-")[0].replace("cm","");
    }

    getMediumSizeInCm():number{
        return +this.mediumSize.split("-")[0].replace("cm","");
    }

    getLargeSizeInCm():number{
        return +this.largeSize.split("-")[0].replace("cm","");
    }

    getExtraSmallSizeInCmOfString():string{
        return this.extraSmallSize.split("-")[0];
    }

    getSmallSizeInCmOfString():string{
        return this.smallSize.split("-")[0];
    }

    getMediumSizeInCmOfString():string{
        return this.mediumSize.split("-")[0];
    }

    getLargeSizeInCmOfString():string{
        return this.largeSize.split("-")[0];
    }
}