class LayoutContants {

    private static instance: LayoutContants;
    public SCREEN_WIDTH = 720;
    public SCREEN_HEIGHT = 1280;
    public FLOAT_COLOR_BUTTON_POSITIONS  =  [210, 188, 166, 142, 120, 97, 73, 50, 28];
    public COLOR_ADJUST_DOT_POSITIONS  =  [210, 188, 166, 142, 120, 97, 73, 50, 28];
    public EDIT_BOX_BASE_WIDTH : number =  720;
    public EDIT_BOX_BASE_HEIGHT : number =  1280;

    public SCREEN_SIZE_COEF : number =  this.EDIT_BOX_BASE_WIDTH/this.SCREEN_WIDTH;
    public SCREEN_HEIGHT_COEF : number =  this.EDIT_BOX_BASE_HEIGHT/this.SCREEN_HEIGHT;
    public BASE_FPS : number =  60;
    public FADE_SPRITE_ALPHA : number =  0.5;

    public COLOR_ADJUST_BUTTON_SCALE : number =  0.40;
    public COLOR_ADJUST_DOT_SCALE : number =  0.80;
    public COLOR_ADJUST_SCALE : number =  0.455;
    public SCALE : number =  0.49;
    public BOOT_PROGRESSBAR_SCALE : number =  1.5;
    public SETTINGS_BUTTONS_SCALE : number =  0.7;
    public BUY_BUTTON_SCALE : number =  0.6;
    public SELECT_BUTTON_SCALE : number =  0.5;
    public SMALL_COLOR_SELECTION_SCALE : number =  0.09;
    public MEDIUM_COLOR_SELECTION_SCALE : number =  0.18;
    public MAIN_COLOR_SELECTION_SCALE : number =  0.27;
    public LARGE_COLOR_SELECTION_SCALE : number =  0.36;
    public LOADING_IMAGE_SCALE : number =  0.75;
    public NET_WARNING_SCALE : number =  0.75;
    public RATE_IMAGE_SCALE : number =  0.75;
    public PRIVACY_SCALE : number =  0.4;

    public lABEL_SCALE : number =  1.6;
    public lOADING_BITMAP_FONT_EXTRA_WIDTH : number =  100;
    public BUTTONS_SIZE_BASE : number =  85;
    public BODY_PART_BUTTON_WIDTH : number =  80;

    public BASIC_ASPECT_RATIO : number =  0.75;
    public BUTTONS_SIZE : number =  85;

    public COLOR_SLIDER_X_OFFSET : number =  22;
    public FLOAT_COLOR_SLIDER_X_OFFSET : number =  20;
    public X_OFFSET_BETWEEN_SLIDERS : number =  62;
    public FLOAT_SLIDER_BG_OFFSET_X : number =  49;
    public SLOTS_UPPER_OFFSET : number =  85 * 0.6;

    public SLIDER_BG_OFFSET_Y : number =  109;
    public SLIDER_Y_OFFSET : number =  27;
    public SLIDER_BUTTON_Y_OFFSET : number =  19;

    public SAVESLOT_OFFSET_Y_COEF : number =  0.0897;

    public BUY_NOW_OFFSET : number =  45;
    public SIZE_TEXT_OFFSET : number =  75;
    public ORDER_NOW_TEXT_SIZE : number =  40;
    public SIZE_TEXT_SIZE : number =  30;
    public PERCENTAGE_SIZE : number =  25;

    public MODEL_ICON_WIDTH_COEF : number =  1.99;
    public MODEL_ICON_HEIGHT_COEF : number =  1.5;

    public DICE_OFFSET_X : number =  0;
    public DICE_OFFSET_Y : number =  100;

    public SIZE_BUTTON_OFFSET : number =  110;
    public DICE_SIZE_IN_CM : number =  1.6;
    MODEL_OFFSET_Y: number;
    ACTUAL_ASPECT_RATIO: number;
    ASPECT_RATIO_COEF: number;
    BACKGROUND_OFFSET_COEF: number;
    BACKGROUND_OFFSET: number;
    NEW_COEF: number;
    HEIGHT_SCALE: number;
    modelMoving: boolean;
    isLandScape: boolean;
    root_scale:number = 1;
    ORIGIN_SCREEN_SIZE_COEF: number;
    
    public constructor() {
        // do something construct...
    }
    static getInstance() {
        if (!LayoutContants.instance) {
            LayoutContants.instance = new LayoutContants();
            // ... any one time initialization goes here ...
        }
        return LayoutContants.instance;
    }

    setRootScale(scale:number){
        this.root_scale = scale;
    }
    
    setScreenBounds(width:number, height:number) { 

        this.NEW_COEF = (this.SCREEN_HEIGHT - this.BUTTONS_SIZE_BASE) / (height - this.BUTTONS_SIZE_BASE);

        this.SCREEN_WIDTH = width;
        this.SCREEN_HEIGHT = height;

        this.SCREEN_SIZE_COEF  =  Math.max(this.EDIT_BOX_BASE_WIDTH/this.SCREEN_WIDTH, 1);
        this.ORIGIN_SCREEN_SIZE_COEF  =  this.EDIT_BOX_BASE_WIDTH/this.SCREEN_WIDTH;
        // this.SCREEN_HEIGHT_COEF =  Math.max(this.EDIT_BOX_BASE_HEIGHT/this.SCREEN_HEIGHT, 1);
        this.SCREEN_HEIGHT_COEF =  this.EDIT_BOX_BASE_HEIGHT/this.SCREEN_HEIGHT;
        this.COLOR_ADJUST_BUTTON_SCALE = 0.4 / this.SCREEN_SIZE_COEF;
        this.COLOR_ADJUST_DOT_SCALE = 0.80 / this.SCREEN_SIZE_COEF;

        this.COLOR_ADJUST_SCALE = 0.455 / this.SCREEN_SIZE_COEF;
        this.SCALE = 0.49 / this.SCREEN_SIZE_COEF;
        this.HEIGHT_SCALE = 0.49/ this.SCREEN_HEIGHT_COEF;
        this.BOOT_PROGRESSBAR_SCALE = 1.5 / this.SCREEN_SIZE_COEF;
        this.SETTINGS_BUTTONS_SCALE = 0.7 / this.SCREEN_SIZE_COEF;
        this.BUY_BUTTON_SCALE = 0.6 / this.SCREEN_SIZE_COEF;
        this.SELECT_BUTTON_SCALE = 0.5 / this.SCREEN_SIZE_COEF;
        this.SMALL_COLOR_SELECTION_SCALE = 0.09 / this.SCREEN_SIZE_COEF;
        this.MEDIUM_COLOR_SELECTION_SCALE = 0.18 / this.SCREEN_SIZE_COEF;
        this.MAIN_COLOR_SELECTION_SCALE = 0.27 / this.SCREEN_SIZE_COEF;
        this.LARGE_COLOR_SELECTION_SCALE = 0.36 / this.SCREEN_SIZE_COEF;
        this.LOADING_IMAGE_SCALE = 0.75 / this.SCREEN_SIZE_COEF;
        this.NET_WARNING_SCALE = 0.75 / this.SCREEN_SIZE_COEF;
        this.RATE_IMAGE_SCALE = 0.75 / this.SCREEN_SIZE_COEF;
        this.PRIVACY_SCALE = 0.4 / this.SCREEN_SIZE_COEF;

        this.lABEL_SCALE = 1.6 / this.SCREEN_SIZE_COEF;
        this.lOADING_BITMAP_FONT_EXTRA_WIDTH = 100 / this.SCREEN_SIZE_COEF;
        this.BUTTONS_SIZE_BASE = 85;
        this.BODY_PART_BUTTON_WIDTH = 80;

        this.BASIC_ASPECT_RATIO = 0.75;
        this.ACTUAL_ASPECT_RATIO = this.SCREEN_WIDTH / this.SCREEN_HEIGHT;
        this.ASPECT_RATIO_COEF = this.ACTUAL_ASPECT_RATIO / this.BASIC_ASPECT_RATIO;
        this.BACKGROUND_OFFSET_COEF = 1 - this.ASPECT_RATIO_COEF;
        this.BACKGROUND_OFFSET = this.SCREEN_HEIGHT * this.BACKGROUND_OFFSET_COEF * 0.5;

        this.MODEL_OFFSET_Y = 60 * (1 - this.SCREEN_HEIGHT_COEF);
        this.BUTTONS_SIZE = this.BUTTONS_SIZE_BASE / this.SCREEN_SIZE_COEF;

        this.COLOR_SLIDER_X_OFFSET = 22 / this.SCREEN_SIZE_COEF;
        this.FLOAT_COLOR_SLIDER_X_OFFSET = 20 / this.SCREEN_SIZE_COEF;
        this.X_OFFSET_BETWEEN_SLIDERS = 62 / this.SCREEN_SIZE_COEF;
        this.FLOAT_SLIDER_BG_OFFSET_X = 49 / this.SCREEN_SIZE_COEF;
        this.SLOTS_UPPER_OFFSET = this.BUTTONS_SIZE * 0.6;

        this.SLIDER_BG_OFFSET_Y = 109 / this.SCREEN_SIZE_COEF;
        this.SLIDER_Y_OFFSET = 27 / this.SCREEN_SIZE_COEF;
        this.SLIDER_BUTTON_Y_OFFSET = 19 / this.SCREEN_SIZE_COEF;

        this.SAVESLOT_OFFSET_Y_COEF = 0.0897;

        this.BUY_NOW_OFFSET = 45 / this.SCREEN_SIZE_COEF;
        this.SIZE_TEXT_OFFSET = 75 / this.SCREEN_SIZE_COEF;
        this.ORDER_NOW_TEXT_SIZE = 40 / this.SCREEN_SIZE_COEF;
        this.SIZE_TEXT_SIZE = 30 / this.SCREEN_SIZE_COEF;
        this.PERCENTAGE_SIZE = 25 / this.SCREEN_SIZE_COEF;

        this.MODEL_ICON_WIDTH_COEF = 1.99;
        this.MODEL_ICON_HEIGHT_COEF = 1.5;

        this.DICE_OFFSET_X = 0 / this.SCREEN_SIZE_COEF;
        this.DICE_OFFSET_Y = 100 / this.SCREEN_HEIGHT_COEF;

        this.SIZE_BUTTON_OFFSET = 110 / this.SCREEN_SIZE_COEF;
        this.DICE_SIZE_IN_CM = 1.6;

        this.isLandScape = (width > height)?true:false;

    }

    setModelMoving(flag:boolean){
        this.modelMoving=flag;
    }

    getScreenBounds():any{
        
        return {
            width:this.SCREEN_WIDTH,
            height:this.SCREEN_WIDTH
        }
    }

}