class ModelCharacteristics {
    
    name:string;
    size:string;
    finish:Finish;
    shopId:string;

    constructor(){

    }

    getSize() {
        return this.size;
    }

    setSize(size:string) {
        this.size = size;
    }

    getFinish():string {
        return this.finish;
    }

    setFinish(tabIndex:integer) {
        if (tabIndex > 0) {
            this.finish = Finish.GLOSS;
            return;
        }
        this.finish = Finish.NORMAL;
    }

    getName():string {
        return this.name;
    }

    setName(name:string) {
        this.name = name;
    }

    getShopId():string {
        return this.shopId;
    }

    setShopId(shopId:string) {
        this.shopId = shopId;
    }
}