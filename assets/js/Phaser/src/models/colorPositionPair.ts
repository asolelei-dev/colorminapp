class ColorPositionPair {
    palletNumber:integer;
    buttonIndex:integer;

    constructor(palletNumber:integer, buttonIndex:integer){
        this.palletNumber = palletNumber;
        this.buttonIndex = buttonIndex;
    }

}