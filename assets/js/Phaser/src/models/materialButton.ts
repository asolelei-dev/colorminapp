class MaterialButton {

    BACKGROUND_INDEX = 0;
    scene:ModelingScene;
    uiGenerator:UIGenerator;
    bodyPartsManager:BodyPartsManager;
    stack:Phaser.GameObjects.Container;
    tabTexture:string;
    tabImage:Phaser.GameObjects.Image;
    tabIndex:integer;

    constructor(scene:ModelingScene, uiGenerator:UIGenerator) {
        this.scene = scene;
        this.uiGenerator = uiGenerator;
        this.bodyPartsManager = scene.bodyPartsManager;
        this.stack = new Phaser.GameObjects.Container(scene);
    }

    setImage(tabRegion:String, atlas:string, tabIndex:integer) {

        var self = this;
        if (tabRegion.includes("tmp_Nada")) {
            this.stack.removeAll();
            return;
        }

        if(this.bodyPartsManager.selectedBodyPartIndex == this.BACKGROUND_INDEX && tabIndex > 0){
            atlas = "new_tab_bg";
        }

        this.tabImage = new Phaser.GameObjects.Image(this.scene, 0, 0, atlas, `${tabRegion}.png`);
        this.tabImage.setScale(LayoutContants.getInstance().BUTTONS_SIZE/this.tabImage.displayHeight);
        this.stack.removeAll();
        this.stack.add(this.tabImage);
        this.tabIndex = tabIndex;

    }

    setMaterial(tabIndex:integer) {

        // debugger;

        if(this.bodyPartsManager.selectedBodyPart.isChild()){
            // this.bodyPartsManager.setSelectedBodyPart();

            let parentBodyPart = this.bodyPartsManager.selectedBodyPart.getParnetBodyPart();
            
            this.uiGenerator.chooseTMenuButton(parentBodyPart.bodyPartIndex);
            this.uiGenerator.chooseTabButton(parentBodyPart.selectedTabIndex);
            this.uiGenerator.chooseColorButton(new ColorPositionPair(parentBodyPart.palleteNumber, parentBodyPart.colorButton.buttonIndex));
        }

        this.bodyPartsManager.setMaterial(tabIndex); // set materail
        this.uiGenerator.createUserAction(); // create action
        this.scene.getModelCharacteristics().setFinish(tabIndex); 
        this.uiGenerator.setColorSelectors();
    }

    dispose() {
        this.stack = null;
    }

    getContainer():Phaser.GameObjects.Container {
        return this.stack;
    }

    clearStack() {
        this.stack.removeAll();
    }

    onTapButton(){
    
        this.scene.playSound(CST.SOUND.COLOR_GENERAL_SOUND);
        this.setMaterial(this.tabIndex);
    }

}
