class FileNames {

    models:Model[];
    atlasFront:string;
    atlasFrames:string;
    song:string;
    partsCsv:string;
    featuresCsv:string;
    saveFile:string;
    atlasSDFrames: string;
    atlasSDFront: string;

    constructor(
        models?:Model[],
        atlasFront?:string,
        atlasFrames?:string,
        song?:string,
        partsCsv?:string,
        featuresCsv?:string,
        saveFile?:string){
            this.models = models;
            this.atlasFront = atlasFront;
            this.atlasFrames = atlasFrames;
            this.song = song;
            this.partsCsv = partsCsv;
            this.featuresCsv = featuresCsv;
            this.saveFile = saveFile;
    }

    generateFileNames(selectedModel:Model, isHd:boolean){

        this.atlasFront = selectedModel.modelName + "_Pack0.json";
        this.atlasFrames = selectedModel.modelName + "_Pack1.json";

        this.atlasSDFront = selectedModel.modelName + "-SD" + "_Pack0.json";
        this.atlasSDFrames = selectedModel.modelName + "-SD" + "_Pack1.json";

        // this.atlasFront = selectedModel.modelName + "_Pack0.json";
        // this.atlasFrames = selectedModel.modelName + "_Pack1.json";

        this.song = selectedModel.modelName + "_Song.mp3";
        this.partsCsv = selectedModel.modelName + "_Parts.csv";
        this.featuresCsv = selectedModel.modelName + "_Features.csv";
        this.saveFile = selectedModel.modelName + ".sav";

        return this;
    }

    
}