class UIGenerator {

    INITIAL_ALFA = 0.01;
    ALFA_INCREASE_VALUE = 0.030;
    ALFA_DECREASE_VALUE = 0.05;

    SECOND = 1000;
    ACTORS_MOVEMENT_VELOCITY = 15;
    COLOR_BUTTONS_NUMBER = 50;
    MATERIAL_BUTTONS_NUMBER = 20;
    NUMBER_OF_FRAME_SLIDER_POSITIONS = 4;

    FRAME_BUTTON_SLIDER_Y = 10;
    PREVIEW_BUTTON_WIDTH = 48;
    MOVE_RIGHT = "moveRight";
    MOVE_UP = "moveUp"; 
    TOPPER = "topper";
    BUY_IT_NOW = "Buy it Now";
    RATE_ME_COUNT = "RateMeCount";
    RATE_SHOWN = "RateShown";

    TRANSPARENT = 0;
    SEMITRANSPARENT = 0.5;
    OPAQUE = 1;
    NUMBER_OF_FADE_TEXTURES = 9;

    palleteNumber = 4;

    scene:ModelingScene;
    fadeTextures:string[];
    backgroundTexture:string;
    solidGreySprite:Phaser.GameObjects.Sprite;
    selectedColorButton:ColorButton;
    selectedTMenuButton:TMenuButton;
    selectedChildBodyPartButton:ChildBodyPartButton;
    currentlySelectedTMenuStack:Phaser.GameObjects.Container;
    tMenuSelector:Phaser.GameObjects.Image;
    bottomPlaceHolder:Phaser.GameObjects.Image;
    colorAdjustGradient:Phaser.GameObjects.Image;
    screenWidth:number;
    screenHeight:number;
    backgroundBoxTopper: Phaser.GameObjects.Image;
    currentlySelectedColorStack: Phaser.GameObjects.Container;
    smallColorSelector: Phaser.GameObjects.Image;
    mediumColorSelector: Phaser.GameObjects.Image;
    mainColorSelector: Phaser.GameObjects.Image;
    largeColorSelector: Phaser.GameObjects.Image;
    floatColorSliderBG: Phaser.GameObjects.Image;
    colorAdjustFrame: Phaser.GameObjects.Image;
    colorAdjustButton: Phaser.GameObjects.Image;
    colorAdjustDot: Phaser.GameObjects.Image;
    floatColorSliderButton: any;
    colorTableContainer:Phaser.GameObjects.Container;

    bodypartTable: TmenuGridTable;
    materialTable: MaterialGridTable;
    tMenubuttons: TMenuButton[] = [];
    bodyPartChildButtons: ChildBodyPartButton[] = []; // included into meterialGridTable container
    colorButtons:ColorButton[] = [];
    materialButtons: MaterialButton[] = [];
    childBodyPartButtons: ChildBodyPartButton[] = [];

    floatColorSlider: Phaser.GameObjects.Image;
    tabSelector: Phaser.GameObjects.Image;
    childBodyPartSelector: Phaser.GameObjects.Image;
    undoButton: CustomButton;
    redoButton: CustomButton;
    goToSaveScreen: Phaser.GameObjects.Image;
    framesAutoOff: Phaser.GameObjects.Image;
    framesAutoOn: Phaser.GameObjects.Image;
    frameSliderButton: Phaser.GameObjects.Image;
    framesSliderGroove: Phaser.GameObjects.Image;
    goToEditBox: CustomButton;
    saveButton: CustomButton;
    shelfSprite: Phaser.GameObjects.Sprite;
    shopBackgroundSprite: Phaser.GameObjects.Sprite;
    extraSmallSizeButton: CustomImageButton;
    smallSizeButton: CustomImageButton;
    mediumSizeButton: CustomImageButton;
    largeSizeButton: CustomImageButton;
    saveScreenUpperBackground: Phaser.GameObjects.Image;
    diceImage: Phaser.GameObjects.Image;
    colorBackgroundTexture: Phaser.GameObjects.Image;
    tMenuBackgroundTexture: Phaser.GameObjects.Image;
    isOrbit: number;
    framesSliderCoordinates: number[];
    userActionsSequence: UserAction[];
    texts: string[];
    loadingImage: Phaser.GameObjects.Image;
    closeRateAppBanner: Phaser.GameObjects.Image;
    rateAppBanner: Phaser.GameObjects.Image;
    backgroundSprite: BodySprite;
    fadeSprite: Phaser.GameObjects.Image;
    currentlySelectedMaterialStack:Phaser.GameObjects.Container;
    currentlySelectedChildBodyPartStack:Phaser.GameObjects.Container;
    
    userActionsIndex:number;
    backButton:CustomButton;
    topBarContainer:Phaser.GameObjects.Container;
    backButtonBG:Phaser.GameObjects.Image;
    bottomSwitchBar:BottomSwitchBar;
    LAYOUT_CONSTANT:LayoutContants = LayoutContants.getInstance();
    colorsTable: ColorGridTable;
    colorAdjustBG: Phaser.GameObjects.Image;
    preventEventImage: any;

    undoButtonAlpha:number = this.SEMITRANSPARENT;
    redoButtonAlpha:number =  this.SEMITRANSPARENT;
    touchZone: Phaser.GameObjects.Image;
    colorTableBackgroundImage: Phaser.GameObjects.Image;
    topBarAreaContainer: Phaser.GameObjects.Container;
    colorAdjustContainer: Phaser.GameObjects.Container;
    leftMovementContainer:Phaser.GameObjects.Container;
    colorIsAdjusted: boolean;
    stackWithFloatingSlider: Phaser.GameObjects.Container;
    shopBoxSprite: Phaser.GameObjects.Sprite;
    diceImageDeltaScale: number = 0;
    selectedSizeButtonIndex: number = 1;
    virtualMasicView: Phaser.GameObjects.Image;

    colorSwitchBoard: ColorSwitchBoard;

    colorPalletePanel: ColorPalletePanel;
    buttonPreview: Phaser.GameObjects.Image;
    colorMode: number;
    colorSelectorContainer: Phaser.GameObjects.Container;
    progressBar: ProgressBar;
    viewMode: number;
    isBackgroundFadeOuted: boolean;
    touchZoneCamera: any;
    diceScreenTopBar:DiceScreenTopBar;
    shopImage:Phaser.GameObjects.Image;


    constructor(scene:ModelingScene) {
        this.scene = scene;
        this.screenWidth = +this.scene.game.config.width;
        this.screenHeight = +this.scene.game.config.height;

        this.solidGreySprite = this.scene.add.sprite(0, this.screenHeight, "solid");
        this.solidGreySprite.displayWidth = this.screenWidth;
        this.solidGreySprite.displayHeight = this.screenHeight;
        this.solidGreySprite.setOrigin(0,1);

        this.leftMovementContainer = this.scene.add.container(0,0).setDepth(10);

        this.colorSelectorContainer = this.scene.add.container(0,0);

        this.colorMode = CST.COLOR_MODE.DEFAULT;

        this.initBackgroundTextures();
        // this.initLoadingImage();
        // this.initRateTextures();

        this.colorButtons = [];
        this.materialButtons = [];
        this.framesSliderCoordinates = new Array<number>();
        this.userActionsSequence = new Array<UserAction>();
        this.texts = new Array<string>();
        

    }

    initBackgroundTextures() {

        this.backgroundTexture = "EditBox_Model_1";

        this.backgroundSprite = new BodySprite(this.scene, 0, this.LAYOUT_CONSTANT.SCREEN_HEIGHT, "EditBox_Model_1")
        this.backgroundSprite.displayHeight = this.LAYOUT_CONSTANT.SCREEN_HEIGHT;
        this.backgroundSprite.displayWidth = this.screenWidth - 2.85 * this.LAYOUT_CONSTANT.BUTTONS_SIZE;
        this.backgroundSprite.setOrigin(0,1);
        this.backgroundSprite.setFlashEnable(false);
        this.scene.sys.displayList.add(this.backgroundSprite);
        this.scene.sys.updateList.add(this.backgroundSprite);


        this.fadeSprite = this.scene.add.image(0, 0, "Fade0");
        this.fadeSprite.displayHeight = this.screenHeight;
        this.fadeSprite.displayWidth = this.screenWidth;
        this.fadeSprite.setOrigin(0,0);
        this.fadeSprite.setAlpha(0.5);

        this.touchZone = this.scene.add.image(0, this.LAYOUT_CONSTANT.BUTTONS_SIZE, "Fade0");
        this.touchZone.displayHeight = this.screenHeight - 2 * this.LAYOUT_CONSTANT.BUTTONS_SIZE;
        this.touchZone.displayWidth = this.screenWidth;
        this.touchZone.setOrigin(0,0);
        
        this.backgroundBoxTopper = this.scene.add.image(0, this.screenHeight, "EditBox_ModelMask");
        this.backgroundBoxTopper.setDepth(1);
        this.backgroundBoxTopper.setOrigin(0, 1);
        this.backgroundBoxTopper.displayWidth = this.screenWidth - 2.85 * this.LAYOUT_CONSTANT.BUTTONS_SIZE;
        this.backgroundBoxTopper.displayHeight = this.LAYOUT_CONSTANT.SCREEN_HEIGHT;

        //this.touchZoneCamera = this.scene.cameras.add(0,  this.LAYOUT_CONSTANT.BUTTONS_SIZE, this.backgroundSprite.displayHeight, this.backgroundSprite.displayWidth );
        //this.touchZoneCamera.setViewport();
        //this.touchZoneCamera.setBounds(0,  this.LAYOUT_CONSTANT.BUTTONS_SIZE, this.backgroundSprite.displayWidth, this.backgroundSprite.displayHeight)
        //this.touchZoneCamera.setZoom(1.5)
        this.fadeTextures = [];
        for (var i = 0; i < this.NUMBER_OF_FADE_TEXTURES; i++) {
            const texture = `Fade${i}`;
            this.fadeTextures.push(texture);
        }
    }

    getSelectedColorButton():ColorButton {
        return this.selectedColorButton;
    }

    createUserAction(){
        var pnToS = this.palleteNumber
        if(this.colorMode == CST.COLOR_MODE.CUSTOME){
            pnToS = this.colorPalletePanel.getPalleteNumber()
        }

        const colorPositionPair = new ColorPositionPair(pnToS, this.selectedColorButton.buttonIndex);
        const selectedBodyPart = this.scene.bodyPartsManager.selectedBodyPart;
        // let childColorLinks = [];
        let userAction = null;
        if(selectedBodyPart.isParent()){            
            // selectedBodyPart.getChildBodyParts().forEach(element => {
            //     //if(element.depend == true)
            //         childColorLinks.push(element.getPartId());
            // });
            userAction = new UserAction(this, selectedBodyPart.bodyPartIndex, 
            colorPositionPair, selectedBodyPart.selectedTabIndex);
        }else{
            userAction = new UserAction(this, selectedBodyPart.bodyPartIndex, colorPositionPair, selectedBodyPart.getParnetBodyPart().selectedTabIndex);
        }
        
        this.addToUserActionsSequence(userAction);
 
    }

    addToUserActionsSequence(userAction:UserAction) {
        this.undoButton.alpha = this.OPAQUE;
        this.userActionsSequence = this.userActionsSequence.slice(0, this.userActionsIndex + 1);
        this.userActionsSequence.push(userAction);
        this.userActionsIndex++;
    }

    undo() {
        
        this.scene.setUndoRedoCooldowned(true);

        if (this.userActionsIndex > 0) {
            this.userActionsIndex--;
            const userAction = this.userActionsSequence[this.userActionsIndex];
            userAction.performAction();
        }

        if (this.userActionsIndex == 0){
            this.undoButton.alpha = this.SEMITRANSPARENT;
            this.undoButtonAlpha = this.SEMITRANSPARENT;
        }

        if (this.redoButton.alpha < 1 && this.userActionsSequence.length > 1) {
            this.redoButton.alpha = this.OPAQUE;
            this.redoButtonAlpha = this.OPAQUE;
        }
    }

    redo() {
        
        this.scene.setUndoRedoCooldowned(true);

        if (this.userActionsIndex < this.userActionsSequence.length - 1) {
            this.userActionsIndex++;
            var userAction = this.userActionsSequence[this.userActionsIndex];
            userAction.performAction();
        }

        if (this.userActionsIndex == this.userActionsSequence.length - 1) {
            this.redoButton.alpha = this.SEMITRANSPARENT;
            this.redoButtonAlpha = this.SEMITRANSPARENT;
        }

        if (this.undoButton.alpha < 1 && this.userActionsSequence.length > 1) {
            this.undoButton.alpha = this.OPAQUE;
            this.undoButtonAlpha = this.OPAQUE;
        }
    }

    setColorSelection(buttonNumber:integer){
        
        this.selectedColorButton = this.colorButtons[buttonNumber];
        this.setColorSlidersPosition();
        this.setColorFromSliderPosition(this.palleteNumber);
        this.setColorSelectors();
    }


    setColorSelectors() {
        this.scene.setColorSelectorsAlfa(this.INITIAL_ALFA);
        this.currentlySelectedColorStack = this.selectedColorButton.stack;
        this.currentlySelectedColorStack.add(this.colorSelectorContainer);
        this.colorSelectorContainer.setVisible(true)
    }

    setColorSlidersPosition() {
        this.colorAdjustButton.x = this.screenWidth - this.LAYOUT_CONSTANT.FLOAT_COLOR_BUTTON_POSITIONS[this.palleteNumber]/this.LAYOUT_CONSTANT.SCREEN_SIZE_COEF;
        this.colorAdjustDot.x = this.screenWidth - this.LAYOUT_CONSTANT.FLOAT_COLOR_BUTTON_POSITIONS[this.palleteNumber]/this.LAYOUT_CONSTANT.SCREEN_SIZE_COEF;
    }

    setColorFromSliderPosition(colorSliderPosition:integer) {
 
        this.setColorsToButtons();
        this.selectedColorButton.setMainSelectorColor();
        if(this.colorMode == CST.COLOR_MODE.CUSTOME){
            this.scene.bodyPartsManager.setCurrentBodyPartColor(this.colorPalletePanel.getPalleteNumber(), this.selectedColorButton);
        }else{
            this.scene.bodyPartsManager.setCurrentBodyPartColor(this.palleteNumber, this.selectedColorButton);
        }
        
    }

    setColorModelFromSelectedBodyPart(){

        var newPalletNumber = this.scene.bodyPartsManager.selectedBodyPart.colorModeNumber
        
        if(newPalletNumber > 9){
            this.colorPalletePanel.setPalleteNumber(newPalletNumber);
            this.setColorsToButtons();
            this.setColorMode(CST.COLOR_MODE.CUSTOME)
        }else{
            this.setColorMode(CST.COLOR_MODE.DEFAULT)
        }

        // var selectedButton = this.scene.bodyPartsManager.selectedBodyPart.colorButton
        // selectedButton.performSelection()
        
    }

    setColorsToButtons() {
        var startPos = 0
        var newPalletNumber = this.scene.bodyPartsManager.selectedBodyPart.palleteNumber
        
        if(this.colorMode == CST.COLOR_MODE.CUSTOME){
            newPalletNumber = this.colorPalletePanel.getPalleteNumber()
            startPos = this.colorButtons.length - 16;
        }else{
            this.palleteNumber = newPalletNumber;
        }

        for (var i = startPos; i < startPos + this.colorButtons.length; i++) {
            var index = i % this.colorButtons.length;
            const color = this.scene.getColorFromRange(index, newPalletNumber);
            this.colorButtons[i-startPos].setColor(color, newPalletNumber);
        
            this.colorButtons[i-startPos].makeMark();
        }
    }



    setSelectedTMenuButton(selectedTMenuButton:TMenuButton) {
        if(this.selectedTMenuButton) this.selectedTMenuButton.resetColorForBlinking();
        this.selectedTMenuButton = selectedTMenuButton;
        this.setTMenuSelector();
    }

    setSelectedChildBodyPartButton(selectedChildBodyPartButton:ChildBodyPartButton) {
        if(this.selectedChildBodyPartButton) this.selectedChildBodyPartButton.resetColorForBlinking();
        this.selectedChildBodyPartButton = selectedChildBodyPartButton;
        this.setChildBodyPartSelector();
    }

    setTMenuSelector(){

        if (this.currentlySelectedTMenuStack != null){
            //this.currentlySelectedTMenuStack.remove(this.tMenuSelector);
            this.removeAllBodyPartSelector()
        }

        this.currentlySelectedTMenuStack = this.selectedTMenuButton.stack;
        this.currentlySelectedTMenuStack.add(this.tMenuSelector);
        // this.colorsTable.scrollToTop(true);
    }

    //mark
    setChildBodyPartSelector(){

        if (this.currentlySelectedChildBodyPartStack != null){
            this.currentlySelectedChildBodyPartStack.remove(this.childBodyPartSelector);
        }

        this.currentlySelectedChildBodyPartStack = this.selectedChildBodyPartButton.stack;

        this.currentlySelectedChildBodyPartStack.add(this.childBodyPartSelector);
    }

    removeAllBodyPartSelector(){
        if (this.currentlySelectedChildBodyPartStack != null)
        this.currentlySelectedChildBodyPartStack.remove(this.childBodyPartSelector);

        if (this.currentlySelectedTMenuStack != null)
        this.currentlySelectedTMenuStack.remove(this.tMenuSelector);
    }

    hideFloatColorSlider() {
        this.floatColorSlider.setVisible(false);
        this.floatColorSliderBG.setVisible(false);
        this.floatColorSliderButton.setVisible(false);
    }

    setMaterialButtons(atlas:string) {
        
        /** Begin - Setting Materials */
        const tabRegions = this.scene.bodyPartsManager.selectedBodyPart.modelPart.getTabRegions();
        var newItems:MaterialButton[] = [];


        for (var i = 0; i < this.MATERIAL_BUTTONS_NUMBER; i++) {

            if (i >= tabRegions.length) {
                this.materialButtons[i].clearStack();
                continue;
            }
            const tabRegion = tabRegions[i];
            const materialButton = this.materialButtons[i];
            materialButton.setImage(tabRegion, atlas, i);
            newItems.push(materialButton);
        }
        
        /** End - Setting Materials */

        /** Begin - Setting Child BodyParts */
        var newChildItems = new Array<ChildBodyPartButton>(); // childrens in bodyPart
        // generate buttons for bodypart childrens
        const bodyParts = this.scene.bodyPartsManager.bodyParts;
        this.childBodyPartButtons = [];

        bodyParts.forEach(element => {
                if(element.modelPart.feature.isChildren && 
                    element.modelPart.feature.parent_id == this.scene.bodyPartsManager.selectedBodyPart.modelPart.feature.part_id){
                    const childButton = element.childBodyPartButton;
                    newChildItems.push(childButton);
                    this.childBodyPartButtons.push(childButton);
                }
        });

        this.materialTable.setItems(newItems, newChildItems);

        /** End - Setting Child BodyParts */

    }

    createTables(){
        this.createColorTable();
        this.createMaterialTable();
        this.createBodyPartTable();
        this.createUndoAndRedoButtons();

    }


    createUndoAndRedoButtons(){
        
        this.undoButton = new CustomButton(
            this.scene, 
            40 / this.LAYOUT_CONSTANT.ORIGIN_SCREEN_SIZE_COEF, 
            this.screenHeight - 1.4 * this.LAYOUT_CONSTANT.BUTTONS_SIZE_BASE/this.LAYOUT_CONSTANT.SCREEN_HEIGHT_COEF, 
            "Button_Undo", ()=>{
            this.undo();
        }, true);
        this.undoButton.setOrigin(0,1);
        this.undoButton.setScale(this.LAYOUT_CONSTANT.SCALE);
        this.undoButton.alpha = this.SEMITRANSPARENT;

        this.redoButton = new CustomButton(
            this.scene, 
            this.colorsTable.getX() - 40 / this.LAYOUT_CONSTANT.ORIGIN_SCREEN_SIZE_COEF, 
            this.screenHeight - 1.4 * this.LAYOUT_CONSTANT.BUTTONS_SIZE_BASE/this.LAYOUT_CONSTANT.SCREEN_HEIGHT_COEF, 
            "Button_redo", ()=>{
            this.redo();
        }, true);
        this.redoButton.setOrigin(1, 1);
        this.redoButton.setScale(this.LAYOUT_CONSTANT.SCALE);
        this.redoButton.alpha = this.SEMITRANSPARENT;
    }

    createBodyPartTable(){

        var context = this.scene;
        var self = this;
        var scrollMode = 1; // 0:vertical, 1:horizontal

        this.bodypartTable = new TmenuGridTable(this.scene, {
            x: this.LAYOUT_CONSTANT.BUTTONS_SIZE,
            y: 0,
            width: this.screenWidth - this.LAYOUT_CONSTANT.BUTTONS_SIZE,
            height: this.LAYOUT_CONSTANT.BUTTONS_SIZE * 1.2,
            background: context.add.image(0, 0, "TMenu_Back"),
            scrollMode: scrollMode,
                table: {
                    cellWidth: (scrollMode === 0) ? undefined : this.LAYOUT_CONSTANT.BUTTONS_SIZE * 0.8,
                    cellHeight: (scrollMode === 0) ? 300 : undefined,
                    columns: 1,
            },
        });

        // event when clicking tmenubutton
        this.bodypartTable.table.on("cell.click", (cellIndex)=>{
     
            self.tMenubuttons[cellIndex].onTapButton();
        });
    }

    listenBodyPartTouchEvent(cellIndex:integer, childFlag:boolean = false){

        if(childFlag){
            let childbodyParts = this.scene.bodyPartsManager.bodyParts.filter(it => it.modelPart.feature.part_id == cellIndex);

            if(childbodyParts.length > 0)
            {
                let childBP = childbodyParts[0];
                let selectedBP = this.scene.bodyPartsManager.selectedBodyPart;
                if(childBP.hasParent(selectedBP) || childBP.hasSibling(selectedBP))
                {
                    if(childBP.modelPart.feature.isChildren){
                
                        return childBP.childBodyPartButton.onTapButton();
                    }
                }else{
           
                    let parentBodyPart = childBP.getParnetBodyPart();
                    parentBodyPart.tMenuButton.performSelection()
                    return childBP.childBodyPartButton.onTapButton()
                }
       

            }
            
        }

        let tmenubtn = this.tMenubuttons[cellIndex];

        if(tmenubtn && !tmenubtn.bodyPart.modelPart.feature.isChildren){
            
            this.tMenubuttons[cellIndex].onTapButton();
        }
    }
    /**
     * generate Top menu buttons and child buttons
     */
    populateBodyPartTable(){
        var context = this.scene;
        const bodyParts = context.bodyPartsManager.bodyParts;
        this.tMenubuttons = new Array<TMenuButton>();
        
        bodyParts.forEach(element => {
            if(element.tMenuButton){
                if(!element.modelPart.feature.isChildren){
                    //this.tMenubuttons[+element.modelPart.tMenuOrder - 1] = element.tMenuButton;
                    this.tMenubuttons[element.modelPart.tMenuOrder] = element.tMenuButton;
                }
            }
        });
        
       
        this.bodypartTable.setItems(this.tMenubuttons);
        this.tMenubuttons.forEach(element => {
            if(element.bodyPart.modelPart.partName == "Background"){
                //
                this.backgroundSprite.linkTmenuButtonIndex(element.bodyPart.modelPart.tMenuOrder);
            }
        });
        
    }

    createColorTable(){

        var scrollMode = 0; // 0:vertical, 1:horizontal
        var context = this.scene;
        this.colorsTable = new ColorGridTable(this.scene, {
            x: this.screenWidth  - this.LAYOUT_CONSTANT.BUTTONS_SIZE * 2.7,
            y: this.LAYOUT_CONSTANT.BUTTONS_SIZE * 1.2 + this.colorSwitchBoard.getElementHeight(),
            width: 2 * this.LAYOUT_CONSTANT.BUTTONS_SIZE,
            height: this.screenHeight - this.LAYOUT_CONSTANT.BUTTONS_SIZE * 2 * 1.2 - this.colorSwitchBoard.getElementHeight(),
            background: context.add.image(0, 0, "solid_grey_8"),
            scrollMode: scrollMode,
                table: {
                cellWidth: (scrollMode === 0) ? undefined : this.LAYOUT_CONSTANT.BUTTONS_SIZE,
                cellHeight: (scrollMode === 0) ? this.LAYOUT_CONSTANT.BUTTONS_SIZE : undefined,
                columns: 2,
            },
        })
        this.colorsTable.table.on("cell.click", (cellIndex)=>{
            if(!this.floatColorSliderBG.visible){
                this.colorButtons[cellIndex].onTapButton();
            }
        });

        this.colorsTable.table.on("cell.press", (cellIndex)=>{
            this.hideFloatColorSlider();
            this.setFloatColorSlider(cellIndex);
            // this.setFloatColorTouchDownInputXOffset();
        });

        this.colorsTable.table.on("cell.pressup", (cellIndex)=>{
            this.hideFloatColorSlider();
        });

        // it causes this event when clicking preview button - Modified by SY
        this.scene.events.on("prevbutton.pontermove", (moveDis, islefttrend)=>{
            
        });
    }

    populateColorsTable(){

        var context = this.scene;
        this.colorButtons = new Array<ColorButton>();
        for(var i = 0; i < this.COLOR_BUTTONS_NUMBER; i++){
            var colorButton = new ColorButton(context,i);
            colorButton.setImagesStack();
            this.colorButtons.push(colorButton);
            if(i==0){
                colorButton.setSelected(true);
            }
        }
        this.colorsTable.setItems(this.colorButtons);
        
    }

    createMaterialTable(){
        var context = this.scene;
        var scrollMode = 0;
        this.materialTable = new MaterialGridTable(this.scene, {
            x: this.screenWidth - 0.92 * this.LAYOUT_CONSTANT.BUTTONS_SIZE,
            y: this.LAYOUT_CONSTANT.BUTTONS_SIZE * 1.2,
            width: this.LAYOUT_CONSTANT.BUTTONS_SIZE,
            height:this.screenHeight - this.LAYOUT_CONSTANT.BUTTONS_SIZE*2*1.2,
            background: context.add.image(0, 0, "solid_grey_8"),
            scrollMode: scrollMode,
                table: {
                cellWidth: (scrollMode === 0) ? undefined : this.LAYOUT_CONSTANT.BUTTONS_SIZE,
                cellHeight: (scrollMode === 0) ? this.LAYOUT_CONSTANT.BUTTONS_SIZE : undefined,
                columns: 1,
            },
        })
        this.materialTable.table.on("cell.click", (cellIndex)=>{
            this.materialButtons[cellIndex].onTapButton();
        });

        this.materialTable.table.on("cell.child.click", (cellIndex)=>{
            //console.log(this.childBodyPartButtons);
            let res = this.childBodyPartButtons.filter(it => it.bodyPart.modelPart.feature.part_id == cellIndex);
            if(res.length > 0)
            {
                
                res[0].onTapButton();
            }
        });

    }



    populateMaterialTable(){
        var materialButtons = new Array<MaterialButton>();
        

        // generate buttons for material
        for(var i = 0; i < this.MATERIAL_BUTTONS_NUMBER; i++){
            const materialButton = new MaterialButton(this.scene, this);
            materialButtons[i] = materialButton;
        }
        this.materialButtons = materialButtons;
        this.materialTable.setItems(materialButtons);
    }

    setActorsLocation() {

    }

    createImagesFromAssets(){

        this.colorTableBackgroundImage = this.scene.add.image(0,0, "Menu_SelectsBG");
        this.colorTableBackgroundImage.setDepth(1);
        this.colorTableBackgroundImage.displayWidth = 2.85 * this.LAYOUT_CONSTANT.BUTTONS_SIZE;
        this.colorTableBackgroundImage.displayHeight = this.screenHeight;
        this.colorTableBackgroundImage.setPosition(this.screenWidth - this.colorTableBackgroundImage.displayWidth/2, this.colorTableBackgroundImage.displayHeight/2);

        this.tMenuSelector = new Phaser.GameObjects.Image(this.scene,0,0,"TMenu_Selection");
        this.tMenuSelector.displayWidth = this.LAYOUT_CONSTANT.BODY_PART_BUTTON_WIDTH/this.LAYOUT_CONSTANT.SCREEN_SIZE_COEF;
        this.tMenuSelector.displayHeight = this.LAYOUT_CONSTANT.BUTTONS_SIZE * 1.2;


        this.childBodyPartSelector = new Phaser.GameObjects.Image(this.scene,0,0,"TMenu_Selection");
        this.childBodyPartSelector.displayWidth = this.LAYOUT_CONSTANT.BODY_PART_BUTTON_WIDTH/this.LAYOUT_CONSTANT.SCREEN_SIZE_COEF;
        this.childBodyPartSelector.displayHeight = this.LAYOUT_CONSTANT.BUTTONS_SIZE * 1.2;


        this.smallColorSelector = new Phaser.GameObjects.Image(this.scene,0,0,"SelectHoop");
        this.mediumColorSelector = new Phaser.GameObjects.Image(this.scene,0,0,"SelectHoop");
        this.mainColorSelector = new Phaser.GameObjects.Image(this.scene,0,0,"SelectHoop");
        this.largeColorSelector = new Phaser.GameObjects.Image(this.scene,0,0,"SelectHoop");

        this.smallColorSelector.setScale(this.LAYOUT_CONSTANT.SMALL_COLOR_SELECTION_SCALE);
        this.mediumColorSelector.setScale(this.LAYOUT_CONSTANT.MEDIUM_COLOR_SELECTION_SCALE);
        this.mainColorSelector.setScale(this.LAYOUT_CONSTANT.MAIN_COLOR_SELECTION_SCALE);
        this.largeColorSelector.setScale(this.LAYOUT_CONSTANT.LARGE_COLOR_SELECTION_SCALE);

        this.colorSelectorContainer.add(this.smallColorSelector)
        this.colorSelectorContainer.add(this.mediumColorSelector)
        this.colorSelectorContainer.add(this.mainColorSelector)
        this.colorSelectorContainer.add(this.largeColorSelector)

        this.colorAdjustFrame = this.scene.add.image(0,0,"BG_ValueNew");
        this.colorAdjustFrame.setScale(this.LAYOUT_CONSTANT.BUTTONS_SIZE * 3/this.colorAdjustFrame.displayWidth);
        this.colorAdjustFrame.setOrigin(1,1);
        this.colorAdjustFrame.setPosition(this.screenWidth, this.screenHeight);
        this.colorAdjustFrame.depth = 10;

        
        this.colorAdjustContainer = this.scene.add.container(0,0).setDepth(25);

        this.colorAdjustBG = this.scene.add.image(300,300,  "FiguromoBox_BG_extension");
        this.colorAdjustBG.setOrigin(1,1);
        this.colorAdjustBG.displayWidth = 2.8 * this.LAYOUT_CONSTANT.BUTTONS_SIZE;
        this.colorAdjustBG.displayHeight = this.LAYOUT_CONSTANT.BUTTONS_SIZE;
        this.colorAdjustBG.setPosition(this.screenWidth, this.screenHeight);
        this.colorAdjustBG.depth = 9;
        
        this.colorAdjustGradient = this.scene.add.image(300,300,  "BG_ValueNew_Gradient");
        this.colorAdjustGradient.setScale(this.LAYOUT_CONSTANT.BUTTONS_SIZE * 3/this.colorAdjustGradient.displayWidth);
        this.colorAdjustGradient.setOrigin(1,1);
        this.colorAdjustGradient.setPosition(this.screenWidth, this.screenHeight);
        this.colorAdjustGradient.depth = 9;
        
        
        this.colorAdjustButton = this.scene.add.image(300,300,"Button_ValueNew");
        this.colorAdjustButton.setScale(this.LAYOUT_CONSTANT.COLOR_ADJUST_BUTTON_SCALE * 0.8);
        this.colorAdjustButton.setPosition(this.screenWidth - 2.8 * this.LAYOUT_CONSTANT.BUTTONS_SIZE/2, this.screenHeight - this.LAYOUT_CONSTANT.BUTTONS_SIZE/2 - 5/this.LAYOUT_CONSTANT.SCREEN_HEIGHT_COEF);
        this.colorAdjustButton.setDepth(11);
        
        this.colorAdjustDot = this.scene.add.image(300,300,"ColorAdjust_Button_Dot");
        this.colorAdjustDot.setScale(this.LAYOUT_CONSTANT.COLOR_ADJUST_DOT_SCALE);
        this.colorAdjustDot.setPosition(this.screenWidth - 2.8 * (this.LAYOUT_CONSTANT.BUTTONS_SIZE/2)/this.LAYOUT_CONSTANT.SCREEN_SIZE_COEF, this.screenHeight - this.LAYOUT_CONSTANT.BUTTONS_SIZE + 6/this.LAYOUT_CONSTANT.SCREEN_HEIGHT_COEF);
        this.colorAdjustDot.setDepth(9);

        this.colorAdjustContainer.add(this.colorAdjustBG);
        this.colorAdjustContainer.add(this.colorAdjustGradient);
        this.colorAdjustContainer.add(this.colorAdjustDot);
        this.colorAdjustContainer.add(this.colorAdjustFrame);
        this.colorAdjustContainer.add(this.colorAdjustButton);
        // this.leftMovementContainer.add(this.colorAdjustContainer)

        this.colorPalletePanel = new ColorPalletePanel(this.scene, (index)=>{
            this.setColorsToButtons();
            this.colorsTable.scrollToTop(true);
            this.resetMarkInColorButtons();
        });


        this.colorSwitchBoard = new ColorSwitchBoard(this.scene, 0, 0, CST.COLOR_MODE.DEFAULT, this.scene.model.modeOff, (colormode)=>{
            this.setColorMode(colormode);
        });
        this.leftMovementContainer.add(this.colorSwitchBoard.stack);

        this.buttonPreview = this.scene.add.image(300, 300,  "Button_NewPreviewTab");
        this.buttonPreview.setDepth(1);
        this.buttonPreview.setScale(this.LAYOUT_CONSTANT.SCALE);
        this.buttonPreview.setPosition(this.screenWidth - 2.85 * this.LAYOUT_CONSTANT.BUTTONS_SIZE, this.LAYOUT_CONSTANT.BUTTONS_SIZE * 1.2);
        this.buttonPreview.setOrigin(1, 0);

        
        this.floatColorSliderBG = this.scene.add.image(0, 0,  "FloatSlider_BGTrans");
        this.floatColorSliderBG.setScale(this.LAYOUT_CONSTANT.SCALE);
        this.floatColorSliderBG.depth = 100;
        this.floatColorSlider = this.scene.add.image(0, 0,  "FloatSlider_Strip2");
        this.floatColorSlider.setScale(this.LAYOUT_CONSTANT.SCALE);
        this.floatColorSlider.depth = 100;
        this.floatColorSliderButton = this.scene.add.image(0, 0,  "ColorAdjust_Button");
        this.floatColorSliderButton.setScale(this.LAYOUT_CONSTANT.SCALE);
        this.floatColorSliderButton.depth = 100;

        this.tabSelector = new Phaser.GameObjects.Image(this.scene,0,0,"TMenu_Selection");
        this.tabSelector.displayWidth = this.LAYOUT_CONSTANT.BODY_PART_BUTTON_WIDTH/this.LAYOUT_CONSTANT.SCREEN_SIZE_COEF;
        this.tabSelector.displayHeight = this.LAYOUT_CONSTANT.BUTTONS_SIZE * 1.2;

        this.childBodyPartSelector = new Phaser.GameObjects.Image(this.scene,0,0,"TMenu_Selection");
        this.childBodyPartSelector.displayWidth = this.LAYOUT_CONSTANT.BODY_PART_BUTTON_WIDTH/this.LAYOUT_CONSTANT.SCREEN_SIZE_COEF;
        this.childBodyPartSelector.displayHeight = this.LAYOUT_CONSTANT.BUTTONS_SIZE * 1.2;


        

        this.goToSaveScreen = this.scene.add.image(300, 300,  "TMenu_SaveON").setVisible(false);
        this.bottomPlaceHolder = this.scene.add.image(200, 200,  "FiguromoBox_BG_extension").setVisible(false);

        this.goToEditBox = new CustomButton(this.scene, this.LAYOUT_CONSTANT.SCREEN_WIDTH - 2.75*this.LAYOUT_CONSTANT.BUTTONS_SIZE, this.LAYOUT_CONSTANT.BUTTONS_SIZE/2 + 5,  "Button_BackNEW",()=>{
            this.resetElementPositionByScreenMode(CST.LAYOUT.EDIT_MODE);
        });
        this.goToEditBox.depth = 10;
        this.goToEditBox.setScale(this.LAYOUT_CONSTANT.SCALE);
        this.goToEditBox.setSoundType(CST.SOUND.CANCEL_SOUND);

        // creating model save button
        this.saveButton = new CustomButton(this.scene, this.LAYOUT_CONSTANT.SCREEN_WIDTH/2, this.LAYOUT_CONSTANT.BUTTONS_SIZE/2 + 5,  "Button_SaveNEW", ()=>{
            this.saveCurrentModel();
        });
        this.saveButton.depth = 10;
        this.saveButton.setScale(this.LAYOUT_CONSTANT.SCALE);
        this.saveButton.setSoundType(CST.SOUND.CANCEL_SOUND);


        this.shelfSprite = this.scene.add.sprite(0, 0,  "shelf_adjusted")
        this.shelfSprite.setOrigin(0, 0);
        this.shelfSprite.displayHeight = this.screenHeight;
        this.shelfSprite.displayWidth = this.screenWidth;

        this.shopBackgroundSprite = this.scene.add.sprite(0, this.screenHeight/2 + 0.5 * this.LAYOUT_CONSTANT.BUTTONS_SIZE, "BoxBGFade").setVisible(false);
        this.shopBackgroundSprite.setOrigin(0, 1);
        this.shopBackgroundSprite.setScale(this.screenWidth/this.shopBackgroundSprite.displayWidth, this.screenHeight/this.shopBackgroundSprite.displayHeight);

        this.shopBoxSprite = this.scene.add.sprite(0, this.screenHeight/2 + 0.9 * this.LAYOUT_CONSTANT.BUTTONS_SIZE, "BoxFull").setVisible(false);
        this.shopBoxSprite.setOrigin(0.3, 1);
        this.shopBoxSprite.setScale(this.LAYOUT_CONSTANT.SCALE * 1.5);

        this.diceScreenTopBar = new DiceScreenTopBar(this.scene, 0, 0, `${this.scene.model.shopText}`, `${this.scene.model.shopPrice}`);


        this.extraSmallSizeButton = new CustomImageButton(this.scene, 
            this.screenWidth - 1.2 * this.LAYOUT_CONSTANT.BUTTONS_SIZE, 
            this.screenHeight/2 - 1.5 * this.LAYOUT_CONSTANT.BUTTONS_SIZE, 
            "Button_Size1", 
            ()=>{
                this.scene.playSound(CST.SOUND.COLOR_GENERAL_SOUND);
            this.onTapBuyButton(0);
        });
        this.extraSmallSizeButton.setTitle(this.scene.model.getExtraSmallSizeInCmOfString());
        this.extraSmallSizeButton.setVisible(false);
        this.extraSmallSizeButton.setHighLightTexture("Button_Size1SEL");



        this.smallSizeButton = new CustomImageButton(this.scene, 
            this.screenWidth - 1.2 * this.LAYOUT_CONSTANT.BUTTONS_SIZE, 
            this.screenHeight/2 - 0.5 * this.LAYOUT_CONSTANT.BUTTONS_SIZE, 
            "Button_Size1",
            ()=>{
                this.scene.playSound(CST.SOUND.COLOR_GENERAL_SOUND);
            this.onTapBuyButton(1);
        });
        this.smallSizeButton.setTitle(this.scene.model.getSmallSizeInCmOfString());
        this.smallSizeButton.setVisible(false);
        this.smallSizeButton.setHighLightTexture("Button_Size1SEL");

        this.mediumSizeButton = new CustomImageButton(this.scene, 
            this.screenWidth - 1.2 * this.LAYOUT_CONSTANT.BUTTONS_SIZE, 
            this.screenHeight/2 + 0.5* this.LAYOUT_CONSTANT.BUTTONS_SIZE, 
            "Button_Size1", 
            ()=>{
                this.scene.playSound(CST.SOUND.COLOR_GENERAL_SOUND);
            this.onTapBuyButton(2);
        });
        this.mediumSizeButton.setTitle(this.scene.model.getMediumSizeInCmOfString());
        this.mediumSizeButton.setVisible(false);
        this.mediumSizeButton.setHighLightTexture("Button_Size1SEL");

        this.largeSizeButton = new CustomImageButton(this.scene, 
            this.screenWidth - 1.2 * this.LAYOUT_CONSTANT.BUTTONS_SIZE, 
            this.screenHeight/2 + 1.5*this.LAYOUT_CONSTANT.BUTTONS_SIZE, 
            "Button_Size1", ()=>{
                this.scene.playSound(CST.SOUND.COLOR_GENERAL_SOUND);
            this.onTapBuyButton(3);
        });

        this.largeSizeButton.setTitle(this.scene.model.getLargeSizeInCmOfString());
        this.largeSizeButton.setVisible(false);
        this.largeSizeButton.setHighLightTexture("Button_Size1SEL");
        
        this.saveScreenUpperBackground = this.scene.add.image(100, 400,  "NewSettings_BG1");

        this.diceImage = this.scene.add.image(0,this.screenHeight,  "dice");
        this.diceImage.setOrigin(0,1);
        this.diceImage.setScale(1/this.LAYOUT_CONSTANT.SCREEN_HEIGHT_COEF);
        this.diceImage.depth = 100;

        //back button area
        this.topBarAreaContainer = this.scene.add.container(0,0);
        this.backButtonBG = this.scene.add.image(this.LAYOUT_CONSTANT.BUTTONS_SIZE/2, this.LAYOUT_CONSTANT.BUTTONS_SIZE * 1.2/2,  "FiguromoBox_BG_extension");
        this.backButtonBG.displayHeight = this.LAYOUT_CONSTANT.BUTTONS_SIZE * 1.2;
        this.backButtonBG.displayWidth = this.LAYOUT_CONSTANT.BUTTONS_SIZE;

        this.backButton = new CustomButton(this.scene, this.LAYOUT_CONSTANT.BUTTONS_SIZE/2, this.LAYOUT_CONSTANT.BUTTONS_SIZE * 1.2/2,  "back_button", ()=>{
            this.resetElementPositionByScreenMode(CST.LAYOUT.SAVE_PREVIEW_MODE);
        });
        this.backButton.setSoundType(CST.SOUND.CANCEL_SOUND);
        this.backButton.setScale(this.LAYOUT_CONSTANT.BUTTONS_SIZE/this.backButton.displayWidth);
        this.backButton.depth = 100;
        this.scene.sys.displayList.add(this.backButton);
        this.scene.sys.updateList.add(this.backButton);

        this.topBarAreaContainer.add(this.backButtonBG);
        // this.topBarAreaContainer.add(this.backButton);
        this.topBarAreaContainer.depth = 25;

        this.progressBar = new ProgressBar(this.scene, "Button_SlideGroove", 0x0bd447);
        
        
    }

    setImagesVisibility(){
        // this.solidGreySprite.setAlpha(0);
        this.shelfSprite.setVisible(false);

        this.shopBackgroundSprite.setVisible(false);
        this.diceScreenTopBar.setVisible(false);

        // Label.LabelStyle labelStyle = new Label.LabelStyle();
        // labelStyle.font = new BitmapFont();
        // labelStyle.font.getData().setScale(lABEL_SCALE, lABEL_SCALE);
        // labelStyle.fontColor = Color.BLACK;

        this.saveScreenUpperBackground.setVisible(false);
        // this.colorPallete.setVisible(false);
        // this.buttonNext.setVisible(false);
        // this.buttonPrevious.setVisible(false);
        // this.modeSwitch_1.setVisible(false);
        this.saveButton.setVisible(false);
        this.goToEditBox.setVisible(false);

        this.diceScreenTopBar.setVisible(false);
        this.smallSizeButton.setVisible(false);
        this.extraSmallSizeButton.setVisible(false);
        this.mediumSizeButton.setVisible(false);
        this.largeSizeButton.setVisible(false);
        this.diceImage.setVisible(false);



        this.buttonPreview.setVisible(true);
        // if (!game.isFramesLoaded()) {
        //     this.frameSliderButton.setTouchable(Touchable.disabled);
        // }
        // this.frameSliderButton.setAlpha(1);
        // this.framesSliderGroove.setAlpha(1);
        // this.framesAutoOn.setAlpha(1);
        // this.framesAutoOff.setAlpha(1);
        // this.saveButton.setAlpha(1);
        // this.goToEditBox.setAlpha(1);
        // this.backgroundBoxTopper.setAlpha(1);
        this.hideFloatColorSlider();
    }

    setBackground(backgroundTexture:string, fadeTexture:string) {
        
           this.fadeSprite.setTexture(fadeTexture);
            
    }

    getBackgroundSprite() {
        return this.backgroundSprite;
    }

    setInitialUserAction(){
        this.userActionsIndex = 0;
        var initialBodyPart = this.scene.bodyPartsManager.getFirstBodyPart();
        const initialPalleteNumber = initialBodyPart.palleteNumber;
        const initialColorButtonIndex = initialBodyPart.colorButton.buttonIndex;
        const initialColorPositionPair = new ColorPositionPair(initialPalleteNumber, initialColorButtonIndex);
        const initialTabIndex = initialBodyPart.selectedTabIndex;    
        
        const initialUserAction = new UserAction(this, initialBodyPart.bodyPartIndex, initialColorPositionPair, initialTabIndex);
        this.userActionsSequence.push(initialUserAction);
    }

    populateStage(){
        
        this.bottomSwitchBar = new BottomSwitchBar(this.scene, "AutoOn", "AutoOff","Button_FrameSlideNew", "Button_SlideGroove",this.scene.model, (switchstatus)=>{
            this.scene.animationDirection = switchstatus;
        });
        
    }
    

    setTabSelector(){
    
        
        if (this.currentlySelectedMaterialStack != null){
            this.currentlySelectedMaterialStack.remove(this.tabSelector);
        }

        if(this.scene.bodyPartsManager.selectedBodyPart.modelPart.feature.isChildren){
            let parentBodyPart = this.scene.bodyPartsManager.selectedBodyPart.getParnetBodyPart();
            var selectedMaterial = this.materialTable.items[parentBodyPart.selectedTabIndex];
            if(selectedMaterial){
                this.currentlySelectedMaterialStack = this.materialTable.items[parentBodyPart.selectedTabIndex].stack;
                this.currentlySelectedMaterialStack.add(this.tabSelector);
            }
        }else{
            var selectedMaterial = this.materialTable.items[this.scene.bodyPartsManager.selectedBodyPart.selectedTabIndex];
            //if(selectedMaterial.tabIndex == 1) debugger
            
            if(selectedMaterial){
                console.log('sssss',selectedMaterial.tabIndex)
                this.currentlySelectedMaterialStack = this.materialTable.items[this.scene.bodyPartsManager.selectedBodyPart.selectedTabIndex].stack;
                this.currentlySelectedMaterialStack.add(this.tabSelector);
            }
        }
    }


    correctColorAdjustbutton(button:Phaser.GameObjects.Image){
        this.setColorSlidersPosition();
        this.createUserAction();
    }

    onChangeColorAdjustment(button:Phaser.GameObjects.Image){
        var buttonx = this.screenWidth - button.x;
        var pos = 0;
        var deta = 50;
        for(var i = 0; i < this.LAYOUT_CONSTANT.COLOR_ADJUST_DOT_POSITIONS.length; i++){
            const element = this.LAYOUT_CONSTANT.COLOR_ADJUST_DOT_POSITIONS[i]/this.LAYOUT_CONSTANT.SCREEN_SIZE_COEF;
            if(deta > Math.abs(element - buttonx)){
                deta = Math.abs(element - buttonx)
                pos = i;
            }
        }
        
        var currentLevel = pos;
        
        if(currentLevel<0) currentLevel = 0;
        if(currentLevel>8) currentLevel = 8;
        this.palleteNumber = currentLevel;
        this.scene.bodyPartsManager.selectedBodyPart.setPalleteNumber(this.palleteNumber);
        this.setColorFromSliderPosition(this.palleteNumber);
    }

    chooseTMenuButton(bodyPartIndex:integer){
        var bodyPart = this.scene.bodyPartsManager.bodyParts[bodyPartIndex];

        if(bodyPart.modelPart.feature.isChildren){         
            // if bodyPart is children
            let parent = bodyPart.getParnetBodyPart();
            let tMenuButton = parent.tMenuButton;
            tMenuButton.performSelection(); // parent bodypart
            bodyPart.childBodyPartButton.performSelection(); // children bodypart
        }else{
            // if bodypart is parent
            let tMenuButton = bodyPart.tMenuButton;
            tMenuButton.performSelection();
        }
    }

    chooseTabButton(materialIndex:integer) {
        if (this.scene.bodyPartsManager.selectedBodyPart != null) {
            if(this.scene.bodyPartsManager.selectedBodyPart.modelPart.feature.isChildren){
                //debugger;
            }else{
              
                this.scene.bodyPartsManager.setMaterial(materialIndex);
            }
            
        }
    }

    chooseColorButton(colorPositionPair:ColorPositionPair) {
        const buttonNumber = colorPositionPair.buttonIndex;
        const palleteNumber = colorPositionPair.palletNumber;
        this.scene.bodyPartsManager.selectedBodyPart.setPalleteNumber(palleteNumber);

        if(palleteNumber < 9){
            this.palleteNumber = palleteNumber;
            this.setColorMode(CST.COLOR_MODE.DEFAULT)
        }else{
            this.colorPalletePanel.setPalleteNumber(palleteNumber)
            this.setColorMode(CST.COLOR_MODE.CUSTOME)
        }

        var colorButton = this.colorButtons[buttonNumber];
       
        colorButton.performSelection(true);
        
    }

    // chooseChildColorLink(colorlinks:integer[]) {


    //  // undo
    //     let childBPs:BodyPart[] = this.scene.bodyPartsManager.selectedBodyPart.getChildBodyParts();

    //     childBPs.forEach(element => {
    //         let res = colorlinks.filter(it => it == element.getPartId());
    //         if(res.length > 0) element.depend = true;
    //         else element.depend = false;
    //         // console.log('lihnk',element.getChildLink())
    //         if(element.depend){
    //             let colorModeNumber = this.getSelectedBodyPart().colorModeNumber;
    //             let newColorModeNumber = Math.min(8,colorModeNumber + element.modelPart.feature.colorModifier);     
    //             newColorModeNumber = Math.max(0, newColorModeNumber);         
    //             element.setPalleteNumber(newColorModeNumber);
    //             element.colorButton =this.selectedColorButton;
                
    //             element.color = this.scene.getColorFromRange(this.selectedColorButton.buttonIndex, newColorModeNumber);
    //             element.setSelectorColor(element.color);
            
    //         }            
    //     });
    // }

    setChildLinkedColor() {

        let childBPs:BodyPart[] = this.scene.bodyPartsManager.selectedBodyPart.getChildBodyParts();

        childBPs.forEach(element => {
            if(element.depend){
                // if(element.getChildLink()){
                    
                // }
                let colorModeNumber = this.getSelectedBodyPart().colorModeNumber;
                let newColorModeNumber = Math.min(8,colorModeNumber + element.modelPart.feature.colorModifier);     
                newColorModeNumber = Math.max(0, newColorModeNumber);         
                element.setPalleteNumber(newColorModeNumber);
                element.colorButton =this.selectedColorButton;
                
                element.color = this.scene.getColorFromRange(this.selectedColorButton.buttonIndex, newColorModeNumber);
                element.setSelectorColor(element.color);
                console.log('-------------------------------', element.color)

            }            
        });

 
        
    }

    hideElementsOnModelingMode(){
        this.backgroundSprite.setVisible(false);
        this.solidGreySprite.setVisible(false);
        this.backgroundBoxTopper.setVisible(false);
        this.undoButton.setVisible(false);
        this.redoButton.setVisible(false);
        this.bottomSwitchBar.setVisible(false);
        this.progressBar.setVisible(false);
        this.colorsTable.setVisible(false);
        this.materialTable.setVisible(false);
        this.bodypartTable.setVisible(false);
        this.topBarAreaContainer.setVisible(false);
        this.backButton.setVisible(false);
        this.colorTableBackgroundImage.setVisible(false);
        this.leftMovementContainer.setVisible(false);
        this.colorPalletePanel.setVisible(false);
        this.buttonPreview.setVisible(false);
        this.colorAdjustContainer.setVisible(false);

    }

    hideElementsOnPreviewMode(){
        this.shelfSprite.setVisible(false);
        this.goToEditBox.setVisible(false);
        this.saveButton.setVisible(false);
        this.solidGreySprite.setVisible(false);
    }

    hideElementsOnShopMode(){

        this.diceScreenTopBar.setVisible(false);
        this.shopBackgroundSprite.setVisible(false);
        this.shopBoxSprite.setVisible(false);

        this.diceImage.setVisible(false);
        this.extraSmallSizeButton.setVisible(false);
        this.smallSizeButton.setVisible(false);
        this.mediumSizeButton.setVisible(false);
        this.largeSizeButton.setVisible(false);
        
        
        this.fadeSprite.setVisible(true);
        this.backgroundSprite.setVisible(true);
        this.solidGreySprite.setVisible(true);
    }

    showElementsOnPrevMode(){
        this.shelfSprite.setVisible(true);
        this.goToEditBox.setVisible(true);
        this.saveButton.setVisible(true);
        this.solidGreySprite.setVisible(true);
        this.backgroundBoxTopper.displayWidth = this.screenWidth;
    }

    showElementsOnEditMode(){
        this.backgroundSprite.setVisible(true);
        this.solidGreySprite.setVisible(true);
        this.backgroundBoxTopper.setVisible(true);
        this.undoButton.setVisible(true);
        this.redoButton.setVisible(true);
        this.bottomSwitchBar.setVisible(true);
        this.colorsTable.setVisible(true);
        this.materialTable.setVisible(true);
        this.bodypartTable.setVisible(true);
        this.topBarAreaContainer.setVisible(true);
        this.backButton.setVisible(true);
        this.colorTableBackgroundImage.setVisible(true);
        this.leftMovementContainer.setVisible(true);
        this.colorPalletePanel.setVisible(true);
        this.buttonPreview.setVisible(true);
        this.backgroundBoxTopper.displayWidth = this.screenWidth - 2.85 * this.LAYOUT_CONSTANT.BUTTONS_SIZE;
        this.colorAdjustContainer.setVisible(true);
        this.setColorMode(this.colorMode);
    }

    resetElementPositionByScreenMode(viewmode:integer){
        this.viewMode = viewmode;
        if(viewmode == CST.LAYOUT.EDIT_MODE){
            this.hideElementsOnPreviewMode();
            this.showElementsOnEditMode();
            this.hideElementsOnShopMode();
            this.scene.animationDirection = SwitchStatus.RIGHT;
            this.bottomSwitchBar.setSwtichOn(SwitchStatus.RIGHT, true);
            this.updateGridTablesBasedOnPreviewBtn(0);
            this.bottomSwitchBar.setPingPongFlag(true);
            this.fadeInBackgroundSprite();

        }

        if(viewmode == CST.LAYOUT.SAVE_PREVIEW_MODE){ 
           
            this.hideElementsOnModelingMode();
            this.showElementsOnPrevMode();
            this.scene.animationDirection = SwitchStatus.STOP;
            this.scene.bodyPartsManager.resetbodyPartStackScale(viewmode);
            this.bottomSwitchBar.setPingPongFlag(false);
        }

        if(viewmode == CST.LAYOUT.PREVIEW_MODE){
            console.log("previewmode loaded")
            this.hideElementsOnPreviewMode();
            // this.showElementsOnEditMode();
            this.hideElementsOnShopMode();
            this.hideFloatColorSlider();
            this.hideElementsOnModelingMode();
            this.scene.animationDirection = SwitchStatus.RIGHT;
            this.bottomSwitchBar.setSwtichOn(SwitchStatus.RIGHT, true);
            this.updateGridTablesBasedOnPreviewBtn(0);
            this.bottomSwitchBar.setPingPongFlag(true);
            this.fadeInBackgroundSprite();
        }

        if(viewmode == CST.LAYOUT.SHOP_MODE){
            this.hideElementsOnPreviewMode();
            this.showElementsOnShopMode();
            this.bottomSwitchBar.setPingPongFlag(false);
            
        }

        this.scene.screenMode = viewmode;
        this.scene.setScreeMode(viewmode);
        
        
    }

    setFloatColorSlider(buttonNumber:integer) {

        this.colorIsAdjusted = true;
        this.selectedColorButton.setMarked(false);
        this.currentlySelectedColorStack = this.colorButtons[buttonNumber].stack;
        this.stackWithFloatingSlider = this.colorButtons[buttonNumber].stack;
        this.selectedColorButton = this.colorButtons[buttonNumber];

        this.setFloatingColorAdjustPosition();

        this.floatColorSlider.setVisible(true);
        this.floatColorSliderBG.setVisible(true);
        this.floatColorSliderButton.setVisible(true);
        

    }

    setFloatingColorAdjustPosition(){
        const x = this.colorsTable.stack.x + this.selectedColorButton.globalX;
        const y = this.colorsTable.stack.y + this.selectedColorButton.globalY;
        this.floatColorSlider.setPosition(x, y);
        this.floatColorSliderBG.setPosition(x, y);
        var xval = this.floatColorSliderBG.x + this.floatColorSliderBG.displayWidth - this.LAYOUT_CONSTANT.FLOAT_COLOR_BUTTON_POSITIONS[this.palleteNumber]/this.LAYOUT_CONSTANT.SCREEN_SIZE_COEF - this.LAYOUT_CONSTANT.X_OFFSET_BETWEEN_SLIDERS;
        this.floatColorSliderButton.setPosition(xval, y);
    }

    saveLastColorSelection(){
        this.createUserAction();
    }

    updateFloatingPosition(x:number){
        var temp = x;
        if(x < this.floatColorSliderBG.x - this.floatColorSliderBG.displayWidth/2){
            temp = this.floatColorSliderBG.x - this.floatColorSliderBG.displayWidth/2;
        }
        if(x > this.floatColorSliderBG.x + this.floatColorSliderBG.displayWidth/2){
            temp = this.floatColorSliderBG.x + this.floatColorSliderBG.displayWidth/2;
        }
        var buttonx = this.floatColorSliderBG.x + this.floatColorSliderBG.displayWidth/2 - temp;
        buttonx *=  this.colorAdjustFrame.displayWidth/this.floatColorSliderBG.displayWidth;
        
        // return;
        // var buttonx = temp + this.LAYOUT_CONSTANT.X_OFFSET_BETWEEN_SLIDERS + this.colorAdjustFrame.x - (this.floatColorSliderBG.x - this.floatColorSliderBG.displayWidth/2);
        // console.log(buttonx);
        var pos = 0;
        var deta = 50;
        for(var i = 0; i < this.LAYOUT_CONSTANT.COLOR_ADJUST_DOT_POSITIONS.length; i++){
            const element = this.LAYOUT_CONSTANT.COLOR_ADJUST_DOT_POSITIONS[i]/this.LAYOUT_CONSTANT.SCREEN_SIZE_COEF;
            if(deta > Math.abs(element - buttonx)){
                deta = Math.abs(element - buttonx)
                pos = i;
            }
        }
        var currentLevel = pos;
        if(currentLevel<0) currentLevel = 0;
        if(currentLevel>8) currentLevel = 8;
        this.palleteNumber = currentLevel;
        this.scene.bodyPartsManager.selectedBodyPart.setPalleteNumber(this.palleteNumber);
        this.setColorFromSliderPosition(this.palleteNumber);
        this.setColorSlidersPosition();
    }

    /**
     * listener binded model save button 
     * */ 
    saveCurrentModel(){
        // game.playSound(GENERAL_SOUND);
        this.incrementRateMeCount();
        this.scene.bodyPartsManager.saveModel();
        this.scene.screenMode = CST.LAYOUT.SHOP_MODE;
        // this.hideElementsOnPreviewMode();
        // this.showElementsOnShopMode();
        this.resetElementPositionByScreenMode(CST.LAYOUT.SHOP_MODE);
        // setDiceScale(smallSizeInCmText.getStr().toString());
        this.scene.getModelCharacteristics().setSize(`${this.scene.model.getExtraSmallSizeInCm()}`);
        
        if(this.scene.model.shopSkip){

            this.scene.goSloteScene();

        }else{
            this.resetElementPositionByScreenMode(CST.LAYOUT.SHOP_MODE);
        }

    }

    incrementRateMeCount() {
        var rateMeCount = +localStorage.getItem(this.RATE_ME_COUNT);
        rateMeCount ++;
        localStorage.setItem(this.RATE_ME_COUNT, `${rateMeCount}`);
    }

    showElementsOnShopMode(){
        this.diceScreenTopBar.setVisible(true);
        this.shopBackgroundSprite.setVisible(true);
        this.shopBoxSprite.setVisible(true);

        this.diceImage.setVisible(true);
        this.extraSmallSizeButton.setVisible(true);
        this.smallSizeButton.setVisible(true);
        this.mediumSizeButton.setVisible(true);
        this.largeSizeButton.setVisible(true);

        this.onTapBuyButton(0);
        
        //hide fade  sprete
        this.fadeSprite.setVisible(false);
        this.backgroundSprite.setVisible(false);
    }

    onTapBuyButton(index:integer){
        this.selectedSizeButtonIndex = index;
        this.extraSmallSizeButton.setStatus(false);
        this.smallSizeButton.setStatus(false);
        this.mediumSizeButton.setStatus(false);
        this.largeSizeButton.setStatus(false);
        if(index == 0) this.extraSmallSizeButton.setStatus(true);
        if(index == 1) this.smallSizeButton.setStatus(true);
        if(index == 2) this.mediumSizeButton.setStatus(true);
        if(index == 3) this.largeSizeButton.setStatus(true);

        

        this.scaleDiceImage(index);

    }

    scaleDiceImage(index:integer){
        var standardSize = this.scene.model.getSmallSizeInCm();
        var sizeInCm = this.scene.model.getExtraSmallSizeInCm();
        if(index == 1){
            sizeInCm = this.scene.model.getSmallSizeInCm();
        }

        if(index == 2){
            sizeInCm = this.scene.model.getMediumSizeInCm();
        }

        if(index == 3){
            sizeInCm = this.scene.model.getLargeSizeInCm();
        }

        this.scene.modelCharacteristics.setSize(`${sizeInCm}`);
        console.log(this.scene.modelCharacteristics);

        this.diceImage.setScale(0.4 * this.scene.bodyPartsManager.bodyPartStack.scale*(standardSize/this.LAYOUT_CONSTANT.SCREEN_HEIGHT_COEF)/sizeInCm);

    }


    setScaleDiceImageByDScale(){
        this.scaleDiceImage(this.selectedSizeButtonIndex)
    }

    setVisibleColorPalletPanelByColorMode(){
        if(this.colorMode == CST.COLOR_MODE.DEFAULT){
            
            if(this.viewMode == CST.LAYOUT.EDIT_MODE && this.buttonPreview.x != this.LAYOUT_CONSTANT.SCREEN_WIDTH) 
            this.colorAdjustContainer.setVisible(true);
        }

        if(this.colorMode == CST.COLOR_MODE.CUSTOME){
            
            if(this.viewMode == CST.LAYOUT.EDIT_MODE) this.colorPalletePanel.setVisible(true)
        }
    }


    setColorMode(colorMode:integer){

        this.colorMode = colorMode
        
        this.colorPalletePanel.setVisible(false)
        this.colorAdjustContainer.setVisible(false)
        
        this.setVisibleColorPalletPanelByColorMode();
        this.setColorsToButtons()
        this.resetMarkInColorButtons()
    }

    resetMarkInColorButtons(){

        this.colorSelectorContainer.setVisible(false);
        this.scene.bodyParts.forEach(element => {
            if(this.colorMode == CST.COLOR_MODE.CUSTOME){
                if(element.colorModeNumber != this.colorPalletePanel.getPalleteNumber()) element.removeMarkInColorButton();
                else element.makeMarkInColorButton();
            }else{
                if(element.colorModeNumber > 9) element.removeMarkInColorButton();
                else element.makeMarkInColorButton();
            }
        });

        this.scene.bodyParts.forEach(element => {
            if(this.colorMode == CST.COLOR_MODE.CUSTOME){
                if(element.colorModeNumber == this.colorPalletePanel.getPalleteNumber()){
                    element.makeMarkInColorButton();
                } 
            }else{
                if(element.colorModeNumber < 9){
                    element.makeMarkInColorButton();
                } 
            }
        });


        const colormodenumber = this.getSelectedBodyPart().colorModeNumber;
        if(this.colorMode == CST.COLOR_MODE.CUSTOME){
            if(colormodenumber == this.colorPalletePanel.getPalleteNumber()){
                this.colorSelectorContainer.setVisible(true);
            } 
        }else{
            if(colormodenumber < 9){
                this.colorSelectorContainer.setVisible(true);
            } 
        }


    }

    getSelectedBodyPart(){
        return this.scene.bodyPartsManager.selectedBodyPart;
    }

    updateGridTablesBasedOnPreviewBtn(dx:number){
        const prebtnOX = this.screenWidth - 2.85 * this.LAYOUT_CONSTANT.BUTTONS_SIZE;
        const prebtnOY = this.LAYOUT_CONSTANT.BUTTONS_SIZE * 1.2;
        const colorTblOY = this.LAYOUT_CONSTANT.BUTTONS_SIZE * 1.2 + this.colorSwitchBoard.getElementHeight();
        const mtTblOY = this.LAYOUT_CONSTANT.BUTTONS_SIZE * 1.2;

        //move bodypart table
        this.bodypartTable.setPosition({y:this.buttonPreview.y - prebtnOY});
        this.topBarAreaContainer.y = this.buttonPreview.y - prebtnOY;

        //move colors table
        this.colorsTable.setPosition({x:this.buttonPreview.x + 0.15 * this.LAYOUT_CONSTANT.BUTTONS_SIZE, y:this.buttonPreview.y - prebtnOY + colorTblOY});
        this.colorTableBackgroundImage.x = this.buttonPreview.x + this.colorTableBackgroundImage.displayWidth/2;
        this.materialTable.setPosition({x:this.screenWidth - 0.92 * this.LAYOUT_CONSTANT.BUTTONS_SIZE + this.buttonPreview.x - prebtnOX, y: this.buttonPreview.y - prebtnOY + mtTblOY});
        

        this.leftMovementContainer.x = this.buttonPreview.x - prebtnOX;
        this.leftMovementContainer.y = this.buttonPreview.y - prebtnOY;
        // this.colorPalletePanel.stack.x = this.colorPalletePanel.stackOriginX + this.buttonPreview.x - prebtnOX;
        this.backgroundSprite.displayWidth = this.buttonPreview.x;
        this.backgroundBoxTopper.displayWidth = this.buttonPreview.x;


        if(this.buttonPreview.x != this.LAYOUT_CONSTANT.SCREEN_WIDTH){
            this.backgroundSprite.setVisible(true);
            this.backgroundBoxTopper.setVisible(true);
            this.undoButton.setVisible(true);
            this.redoButton.setVisible(true);
            this.bottomSwitchBar.setVisible(true);
            this.setVisibleColorPalletPanelByColorMode();
        }
        if(this.buttonPreview.x == this.LAYOUT_CONSTANT.SCREEN_WIDTH){
            this.backgroundSprite.setVisible(false);
            this.backgroundBoxTopper.setVisible(false);
            this.undoButton.setVisible(false);
            this.redoButton.setVisible(false);
            this.bottomSwitchBar.setVisible(false);
            this.colorAdjustContainer.setVisible(false);
            this.colorPalletePanel.setVisible(false);
        }

        if(this.buttonPreview.y == 0 && this.buttonPreview.x == this.LAYOUT_CONSTANT.SCREEN_WIDTH){
            this.backButton.setVisible(false);
            this.viewMode = CST.LAYOUT.PREVIEW_MODE;
            
        }else{
            this.backButton.setVisible(true);
            this.viewMode = CST.LAYOUT.EDIT_MODE;
        }

        this.scene.bodyPartsManager.moveByX(dx/2);
        this.bottomSwitchBar.container.x = this.backgroundBoxTopper.displayWidth/2;
        this.redoButton.x = this.colorsTable.getX() - 40 / this.LAYOUT_CONSTANT.ORIGIN_SCREEN_SIZE_COEF,

        this.scene.bodyPartsManager.dynamicCenterX += dx/2;

        this.updateTouchZone();
    }

    fadeOutBackgroundSprite(){
        if(this.isBackgroundFadeOuted) return;
        this.isBackgroundFadeOuted = true;
        this.scene.tweens.add({
            targets: this.backgroundSprite,
            alpha: 0,
            ease: 'Cubic.easeOut',  
            duration: 1000,
        });

    }

    fadeInBackgroundSprite(){
        if(this.isBackgroundFadeOuted != true) return;
        this.isBackgroundFadeOuted = false;
        this.scene.tweens.add({
            targets: this.backgroundSprite,
            alpha: 1,
            ease: 'Cubic.easeIn',  
            duration: 1000,
        });
        this.scene.bodyPartsManager.resetBodyPartStack();
    }

    updateTouchZone(){
        var touchZoneW = this.buttonPreview.x;
        var touchZoneH = this.screenHeight;
        var touchZoneX = 0;
        var touchZoneY = this.LAYOUT_CONSTANT.BUTTONS_SIZE + this.bodypartTable.getTableY();

        if(this.viewMode != CST.LAYOUT.PREVIEW_MODE){
            touchZoneH = this.screenHeight - this.LAYOUT_CONSTANT.BUTTONS_SIZE - touchZoneY;
        }

        this.touchZone.setPosition(touchZoneX, touchZoneY);
        this.touchZone.displayWidth = touchZoneW;
        this.touchZone.displayHeight = touchZoneH;

    }
}