class CsvParser {

    constructor(){

    }

    static parsePartsCsv(csvPath:string, featuresCsvPath:string, callback: (modelparts: ModelPart[]) => void) {

        var result:ModelPart[] = [];

        this.parseFeaturesCsv(featuresCsvPath, (features)=>{
            Papa.parse(csvPath, {
                download: true,
                complete: function(results) {
                    var i = 0;
                    results.data.forEach(function(entry){
                        if(i > 0 && i < results.data.length - 1){
                            const model = new ModelPart(
                                entry[CST.CSVPARSER.PART_NAME_COL],
                                entry[CST.CSVPARSER.SORT_ORDER_COL],
                                entry[CST.CSVPARSER.TMENU_ORDER_COL],
                                entry[CST.CSVPARSER.PART_SIZE_X_COL],
                                entry[CST.CSVPARSER.PART_SIZE_Y_COL],
                                entry[CST.CSVPARSER.PART_POS_X_COL],
                                entry[CST.CSVPARSER.PART_POS_Y_COL],
                                entry[CST.CSVPARSER.SCALE_ADJUST_COL],
                                entry[CST.CSVPARSER.X_ADJUST_COL],
                                entry[CST.CSVPARSER.Y_ADJUST_COL],
                                entry[CST.CSVPARSER.PART_BASE_IMAGE_COL],
                                entry[CST.CSVPARSER.PART_GLOSS_IMAGE_COL],
                                entry[CST.CSVPARSER.GLOSS_ALPHA_COL],
                                entry[CST.CSVPARSER.COLOR_BUTTON_BASE_IMAGE_COL],
                                entry[CST.CSVPARSER.COLOR_BUTTON_GLOSS_IMAGE_COL],
                                entry[CST.CSVPARSER.BUTTON_GLOSS_ALPHA_COL],
                                fetchTabNames(entry),
                                fetchMaterials(entry),
                                features[i-1]
                            );
                           
                            result.push(model);
                        }
                        i++;
                    });
                    
                    var sortedArray: ModelPart[] = result.sort((obj1, obj2) => {
                        
                        if(obj1.partName.includes("Background")) obj1.sortOrder = "0";
                        else if(obj1.partName.includes("Shadow")) obj1.sortOrder = `${result.length}`;
                        

                        if(obj2.partName.includes("Background")) obj2.sortOrder = "0";
                        else if(obj2.partName.includes("Shadow")) obj2.sortOrder = `${result.length}`;
                       
                        return +obj1.sortOrder - +obj2.sortOrder;
                    });


                    callback(sortedArray);
                }
            });

        });



        function fetchTabNames(entry){
            var tabNames:string[] = [];
            for (var tabIndex = CST.CSVPARSER.FIRST_TAB_IMAGE_COL; tabIndex <= CST.CSVPARSER.LAST_TAB_IMAGE_COL; tabIndex++) {
                const tabName = entry[tabIndex];
                if (tabName != null) {
                    tabNames.push(tabName);
                }
            }
            return tabNames;
        }
    
    
        function fetchMaterials(entry){
    
            var index:integer;
            var indexGloss:integer;
            var indexGlossAlpha:integer;
            var indexButton:integer;
            var indexButtonGloss:integer;
            var indexButtonGlossAlpha:integer;
    
    //        Each Material corresponds to one Tab button.
    
            var materials:Material[] = [];
    
            for (var tabIndex = 0; tabIndex < CST.CSVPARSER.NUM_OF_TABS; tabIndex++) {
                index = CST.CSVPARSER.PART_BASE_IMAGE_COL + tabIndex * CST.CSVPARSER.NUM_OF_COLUMNS_BETWEEN_CORRESPONDING_IMAGES_IN_CSV;
                indexGloss = CST.CSVPARSER.PART_GLOSS_IMAGE_COL + tabIndex * CST.CSVPARSER.NUM_OF_COLUMNS_BETWEEN_CORRESPONDING_IMAGES_IN_CSV;
                indexGlossAlpha = CST.CSVPARSER.GLOSS_ALPHA_COL + tabIndex *CST.CSVPARSER.NUM_OF_COLUMNS_BETWEEN_CORRESPONDING_IMAGES_IN_CSV;
                indexButton = CST.CSVPARSER.COLOR_BUTTON_BASE_IMAGE_COL + tabIndex * CST.CSVPARSER.NUM_OF_COLUMNS_BETWEEN_CORRESPONDING_IMAGES_IN_CSV;
                indexButtonGloss = CST.CSVPARSER.COLOR_BUTTON_GLOSS_IMAGE_COL + tabIndex * CST.CSVPARSER.NUM_OF_COLUMNS_BETWEEN_CORRESPONDING_IMAGES_IN_CSV;
                indexButtonGlossAlpha = CST.CSVPARSER.BUTTON_GLOSS_ALPHA_COL + tabIndex * CST.CSVPARSER.NUM_OF_COLUMNS_BETWEEN_CORRESPONDING_IMAGES_IN_CSV;
    
                if (index < entry.length){
                    const clayRegion = entry[index];
                    const glossRegion = entry[indexGloss];
                    const glossAlpha = entry[indexGlossAlpha];
                    const clayButtonRegion = entry[indexButton];
                    const glossButtonRegion = entry[indexButtonGloss];
                    const glossButtonAlpha = entry[indexButtonGlossAlpha];
    
                    const material = new Material(
                            clayRegion,
                            glossRegion,
                            glossAlpha,
                            clayButtonRegion,
                            glossButtonRegion,
                            glossButtonAlpha
                            );
    
                    materials.push(material);
                }
            }
    
            return materials;
        }

        
    }


    static parseFeaturesCsv(csvPath:string, callback: (featuresarr: Feature[]) => void) {

        var result:Feature[] = [];

        Papa.parse(csvPath, {
            download: true,
            complete: function(results) {
                var i = 0;
                results.data.forEach(function(entry){
                    if(i > 0 && i < results.data.length - 1){
                        const feature = new Feature(
                            entry[0],
                            entry[1],
                            entry[2],
                            entry[8],
                            entry[12],
                            entry[13],
                            entry[15], // colorlink v1
                            entry[16], // colorlink v2
                            entry[17], // color modifier
                            entry[9],
                            i + 1, // set index as unique id
                            entry[18] // specify parent id
                        );
                        result.push(feature);
                    }
                    
                    i++;
                })

         
                callback(result);
            }
        });
    }


    static parseColorCsv(csvPath:string, callback: (results: ColorsRange[]) => void) {

        var result:ColorsRange[] = [];

        Papa.parse(csvPath, {
            download: true,
            complete: function(results) {
                var i = 0;
                results.data.forEach(function(entry){
                    if(i < results.data.length - 1){
                        var colorsRange = new ColorsRange(
                            entry[0],
                            entry[1],
                            [],
                            [],
                            [],
                        );
                        for (var k = 1; k < CST.CSVPARSER.NUM_OF_PALLETES; k++) {
                            colorsRange.red.push(+entry[CST.CSVPARSER.OFFSET_IN_COLORS_CSV * k - 1]);
                            colorsRange.green.push(+entry[CST.CSVPARSER.OFFSET_IN_COLORS_CSV * k]);
                            colorsRange.blue.push(+entry[CST.CSVPARSER.OFFSET_IN_COLORS_CSV * k + 1]);
                        }
                        result.push(colorsRange);
                    }
                    i++;
                })
                callback(result);
            }
        });
    }

}