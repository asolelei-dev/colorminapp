var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var TOKEN;
var CST = {
    FRONT_FRAME: 15,
    HOST_ADDRESS: "./",
    API_URL: "https://colorcloud.figuromomodels.com/api/",
    AUTH: {
        TOKEN_URL: "https://colorcloud.figuromomodels.com/api-token-auth/",
        USERNAME: "figuro5",
        PASSWORD: "figuro5pass1"
    },
    SHOPIFY: {
        PRIVATE_API_KEY: "cb4a7d42d28224712805f137d204ecb5",
        PRIVATE_API_KEY_PASSWORD: "e9d4504fcfbdbc06739d4b6f330964c7"
    },
    SCENES: {
        INITIAL: "InitialScene",
        PRELOAD: "PreloadScene",
        LAUNCH: "LaunchScene",
        MAIN: "MainScene",
        MODELING: "ModelingScene",
        SAVE_SLOTE: "Save_slote"
    },
    EVENTS: {
        LOADED: "loaded",
        CSV_LOADED: "csv_loaded"
    },
    ANIMATION: {
        DIRECTION: {
            RIGHT: 1,
            LEFT: 2,
            STOP: 3
        },
        RATE: 3
    },
    IMAGE: {
        LOGO: "assets/img/bootscreen/logo/AnimFill_2.png"
    },
    MODEL_TYPE: {
        GLOSS: 1,
        CLATY: 0
    },
    CSVPARSER: {
        PART_NAME_COL: 0,
        SORT_ORDER_COL: 2,
        TMENU_ORDER_COL: 3,
        PART_SIZE_X_COL: 4,
        PART_SIZE_Y_COL: 5,
        PART_POS_X_COL: 6,
        PART_POS_Y_COL: 7,
        SCALE_ADJUST_COL: 8,
        X_ADJUST_COL: 9,
        Y_ADJUST_COL: 10,
        FIRST_TAB_IMAGE_COL: 13,
        LAST_TAB_IMAGE_COL: 32,
        PART_BASE_IMAGE_COL: 34,
        PART_GLOSS_IMAGE_COL: 35,
        GLOSS_ALPHA_COL: 36,
        COLOR_BUTTON_BASE_IMAGE_COL: 39,
        COLOR_BUTTON_GLOSS_IMAGE_COL: 40,
        BUTTON_GLOSS_ALPHA_COL: 41,
        NUM_OF_TABS: 20,
        NUM_OF_COLUMNS_BETWEEN_CORRESPONDING_IMAGES_IN_CSV: 10,
        NUM_OF_PALLETES: 82,
        OFFSET_IN_COLORS_CSV: 4,
        MODEL_NAME_COL: 0,
        ICON_NAME_COL: 1,
        IS_ORBIT_COL: 6,
        DATA_8_HD_COL: 8,
        DATA_HD_COL: 9,
        DATA_8_SD_COL: 10,
        DATA_SD_COL: 11,
        EXTRA_SMALL_SIZE_COL: 12,
        SMALL_SIZE_COL: 13,
        MEDIUM_SIZE_COL: 14,
        LARGE_SIZE_COL: 15,
        SHOP_ID_COL: 16,
        FRAME_SETUP_COL: 29,
        CATEGORY_COL: 30,
        SHOP_TEXT_COL: 31,
        SHOP_PRICE_COL: 32,
        SHOP_IMAGE_COL: 33,
        SHOP_lINK_COL: 34,
        SHOP_SKIP_COL: 35,
        ZOOM_ADJUST_COL: 36,
        MADE_OFF_COL: 37
    },
    LAYOUT: {
        FLOAT_COLOR_BUTTON_POSITIONS: [210, 188, 166, 142, 120, 97, 73, 50, 28],
        COLOR_ADJUST_DOT_POSITIONS: [210, 188, 166, 142, 120, 97, 73, 50, 28],
        EDIT_BOX_BASE_WIDTH: 720,
        EDIT_BOX_BASE_HEIGHT: 1280,
        SCREEN_SIZE_COEF: this.EDIT_BOX_BASE_WIDTH / this.SCREEN_WIDTH,
        SCREEN_HEIGHT_COEF: this.EDIT_BOX_BASE_HEIGHT / this.SCREEN_HEIGHT,
        BASE_FPS: 60,
        FADE_SPRITE_ALPHA: 0.5,
        COLOR_ADJUST_BUTTON_SCALE: 0.40,
        COLOR_ADJUST_DOT_SCALE: 0.80,
        COLOR_ADJUST_SCALE: 0.455,
        SCALE: 0.49,
        BOOT_PROGRESSBAR_SCALE: 1.5,
        SETTINGS_BUTTONS_SCALE: 0.7,
        BUY_BUTTON_SCALE: 0.6,
        SELECT_BUTTON_SCALE: 0.5,
        SMALL_COLOR_SELECTION_SCALE: 0.09,
        MEDIUM_COLOR_SELECTION_SCALE: 0.18,
        MAIN_COLOR_SELECTION_SCALE: 0.27,
        LARGE_COLOR_SELECTION_SCALE: 0.36,
        LOADING_IMAGE_SCALE: 0.75,
        NET_WARNING_SCALE: 0.75,
        RATE_IMAGE_SCALE: 0.75,
        PRIVACY_SCALE: 0.4,
        lABEL_SCALE: 1.6,
        lOADING_BITMAP_FONT_EXTRA_WIDTH: 100,
        BUTTONS_SIZE_BASE: 85,
        BODY_PART_BUTTON_WIDTH: 80,
        BASIC_ASPECT_RATIO: 0.75,
        BUTTONS_SIZE: 85,
        COLOR_SLIDER_X_OFFSET: 22,
        FLOAT_COLOR_SLIDER_X_OFFSET: 20,
        X_OFFSET_BETWEEN_SLIDERS: 29,
        FLOAT_SLIDER_BG_OFFSET_X: 49,
        SLOTS_UPPER_OFFSET: 85 * 0.6,
        SLIDER_BG_OFFSET_Y: 109,
        SLIDER_Y_OFFSET: 27,
        SLIDER_BUTTON_Y_OFFSET: 19,
        SAVESLOT_OFFSET_Y_COEF: 0.0897,
        BUY_NOW_OFFSET: 45,
        SIZE_TEXT_OFFSET: 75,
        ORDER_NOW_TEXT_SIZE: 40,
        SIZE_TEXT_SIZE: 30,
        PERCENTAGE_SIZE: 25,
        MODEL_ICON_WIDTH_COEF: 1.99,
        MODEL_ICON_HEIGHT_COEF: 1.5,
        DICE_OFFSET_X: 0,
        DICE_OFFSET_Y: 100,
        SIZE_BUTTON_OFFSET: 110,
        DICE_SIZE_IN_CM: 1.6,
        EDIT_MODE: 0,
        PREVIEW_MODE: 1,
        SAVE_PREVIEW_MODE: 2,
        SHOP_MODE: 3,
    },
    SOUND: {
        BACKGROUND_SOUND: 0,
        COLOR_CHANGE_SOUND: 1,
        COLOR_GENERAL_SOUND: 2,
        CANCEL_SOUND: 3,
    },
    COLOR_MODE: {
        DEFAULT: 0,
        CUSTOME: 1
    }
};
/// <reference path='./phaser.d.ts'/>
var MyGame;
(function (MyGame) {
    var InitPhaser = /** @class */ (function () {
        function InitPhaser() {
        }
        InitPhaser.initGame = function (width, height, scale) {
            var config = {
                type: Phaser.AUTO,
                width: width,
                height: height,
                backgroundColor: '#dddddd',
                scene: [PreloadScene, LaunchScene, SaveSloteScene, MainScene, ModelingScene],
                banner: true,
                title: 'Color Minis',
                url: 'https://updatestage.littlegames.app',
                version: '1.0.0',
                parent: "game-canvas",
                fps: {
                    min: 1,
                    target: 60,
                    forceSetTimeOut: false,
                    deltaHistory: 1
                },
                input: {
                    activePointers: 3
                }
            };
            this.gameRef = new Phaser.Game(config);
            this.gameRef.global = {
                colorsRanges: [],
            };
            LayoutContants.getInstance().setScreenBounds(width, height);
            LayoutContants.getInstance().setRootScale(scale);
        };
        return InitPhaser;
    }());
    MyGame.InitPhaser = InitPhaser;
})(MyGame || (MyGame = {}));
window.onload = function () {
    setTimeout(function () {
        var scale = 1;
        if (window.innerWidth < 900)
            scale = 768 / window.innerWidth;
        var canvas = document.getElementById("game-canvas");
        canvas.style.transform = "scale(" + 1 / scale + ")";
        canvas.style.transformOrigin = "top left";
        MyGame.InitPhaser.initGame(window.innerWidth * scale, window.innerHeight * scale, scale);
    }, 1000);
};
var BodySprite = /** @class */ (function (_super) {
    __extends(BodySprite, _super);
    function BodySprite(scene, x, y, texture, frame) {
        var _this = _super.call(this, scene, x, y, texture, frame) || this;
        _this.moving = false;
        _this.initedTouchEvent = false;
        _this.enabledFlash = true;
        _this.systems = scene.sys;
        _this.scene = scene;
        _this.setName("bodypart sprite");
        return _this;
        // if(typeof frame === "string"){
        //     if(frame.includes("Clay")){
        //         // console.log(frame);
        //         // this.perfectPixelCallback = this.makePixelPerfect(1);
        //     }
        // }
    }
    BodySprite.prototype.linkTmenuButtonIndex = function (tIndex) {
        this.tIndex = tIndex;
    };
    BodySprite.prototype.linkChildBodyPartButtonIndex = function (childIndex) {
        this.childIndex = childIndex;
    };
    BodySprite.prototype.makePixcelPerfectTouchEvent = function () {
        if (parseInt(this.feature.paralax) != 1) {
            if (this.initedTouchEvent)
                return;
            this.initedTouchEvent = true;
            // this.setInteractive({
            //     pixelPerfect: true,
            //     alphaTolerance :150
            // })
            // // .on("pointerdown", (pointer, localX, localY, event)=>{
            // //     this.scene.touchZonePointerDown(pointer);
            // //     LayoutContants.getInstance().setModelMoving(false);
            // // }).on("pointermove", (pointer, localX, localY, event)=>{
            // //     if(pointer.isDown){
            // //         this.scene.touchZonePointerMove(pointer, localX);
            // //         this.moving = true;
            // //     }
            // // })
            // .on("pointerup", (pointer, x, y, event)=>{
            //     console.log(x, y, this.texture.key, this.frame.name);
            //     console.log("______________________________________");
            //     // if(this.scene.uiGenerator.viewMode == CST.LAYOUT.SHOP_MODE || this.scene.uiGenerator.viewMode == CST.LAYOUT.SAVE_PREVIEW_MODE) return;
            //     // this.scene.touchZonePointerUp(pointer);
            //     // if(!LayoutContants.getInstance().modelMoving){
            //     //     this.flash();
            //     //     this.scene.uiGenerator.listenBodyPartTouchEvent(this.tIndex);
            //     // }
            //     // this.moving = false;
            // }).on("pointerout", ()=>{});
        }
    };
    BodySprite.prototype.checkPixelPerfectPos = function (x, y, scale) {
        var scaleX = scale * this.scaleX;
        var scaleY = scale * this.scaleY;
        if (this.getBounds().contains(x, y)) {
            var localX = (x - this.getBounds().x) / scaleX;
            var localy = (y - this.getBounds().y) / scaleY;
            var textureManager = this.systems.textures;
            var alpha = textureManager.getPixelAlpha(localX, localy, this.texture.key, this.frame.name);
            if (alpha > 150) {
                if (this.scene.uiGenerator.viewMode == CST.LAYOUT.SHOP_MODE || this.scene.uiGenerator.viewMode == CST.LAYOUT.SAVE_PREVIEW_MODE)
                    return;
                // this.scene.touchZonePointerUp(pointer);
                if (!LayoutContants.getInstance().modelMoving) {
                    return this;
                }
            }
        }
        // const x = x1 - (this.x - this.displayWidth);
        // const y = y1 - (this.y - this.displayHeight);
        // console.log(x1, y1, x, y, this.texture.key, this.frame.name);
    };
    BodySprite.prototype.initflashSprite = function () {
        if (this.flashSprite != undefined) {
            return;
        }
        var bound = this.getBounds();
        this.flashSprite = this.scene.add.sprite(bound.x, bound.y, this.texture.key, this.frame.name);
        this.flashSprite.displayWidth = bound.width;
        this.flashSprite.displayHeight = bound.height;
        this.flashSprite.setOrigin(0, 0);
    };
    BodySprite.prototype.performSelection = function () {
        // this.initflashSprite();
        if (this.originTint == undefined)
            this.originTint = this.tint;
        this.flash();
        // this.scene.uiGenerator.listenBodyPartTouchEvent(this.tIndex);
    };
    BodySprite.prototype.CreatePixelPerfectHandler = function (textureManager, alphaTolerance) {
        return function (x, y, gameObject) {
            var alpha = textureManager.getPixelAlpha(x, y, gameObject.texture.key, gameObject.frame.name);
            return (alpha && alpha >= alphaTolerance);
        };
    };
    BodySprite.prototype.makePixelPerfect = function (alphaTolerance) {
        if (alphaTolerance === undefined) {
            alphaTolerance = 1;
        }
        var textureManager = this.systems.textures;
        return this.CreatePixelPerfectHandler(textureManager, alphaTolerance);
    };
    BodySprite.prototype.flash = function () {
        var _this = this;
        // if(this.enabledFlash) this.tint = 0xffffff;
        this.timerout = setTimeout(function () {
            if (_this.tIndex >= 0)
                _this.scene.uiGenerator.listenBodyPartTouchEvent(_this.tIndex);
            else {
                _this.scene.uiGenerator.listenBodyPartTouchEvent(_this.childIndex, true);
            }
        }, 100);
    };
    BodySprite.prototype.setFeature = function (feature) {
        this.feature = feature;
    };
    BodySprite.prototype.setFlashEnable = function (flag) {
        this.enabledFlash = flag;
    };
    BodySprite.prototype.resetColor = function () {
        this.tint = this.originTint;
    };
    BodySprite.prototype.setColor = function (color) {
        this.originTint = color;
        this.tint = color;
    };
    return BodySprite;
}(Phaser.GameObjects.Sprite));
var CategoryItem = /** @class */ (function () {
    function CategoryItem(scene, category) {
        this.category = category;
        this.scene = scene;
        this.stack = this.scene.add.container(0, 0);
        this.itemImage = new Phaser.GameObjects.Image(scene, 0, 0, category);
        this.stack.add(this.itemImage);
        this.stack.setScale(LayoutContants.getInstance().HEIGHT_SCALE);
    }
    CategoryItem.prototype.getDisplayHeight = function () {
        return this.itemImage.displayHeight * LayoutContants.getInstance().HEIGHT_SCALE;
    };
    CategoryItem.prototype.getDisplayWidth = function () {
        return this.itemImage.displayWidth * LayoutContants.getInstance().HEIGHT_SCALE;
    };
    CategoryItem.prototype.getOriginDisplaywidth = function () {
        return this.itemImage.displayWidth;
    };
    CategoryItem.prototype.addSelectector = function (selector) {
        this.stack.removeAll();
        this.stack.add(selector);
        this.stack.add(this.itemImage);
    };
    CategoryItem.prototype.getOriginDisplayHeight = function () {
        return this.itemImage.displayHeight;
    };
    return CategoryItem;
}());
var CommunityModel = /** @class */ (function () {
    function CommunityModel(scene, width, height, model, jsonData) {
        this.scene = scene;
        this.model = model;
        this.stack = new Phaser.GameObjects.Container(scene);
        this.jsonData = jsonData;
        this.counter = { download: 0, like: 0 };
        this.shelfSprite = { displayWidth: width, displayHeight: height };
        this.initGroup(width, height, model);
    }
    ;
    CommunityModel.prototype.initGroup = function (width, height, model) {
        this.solidBackgroundSprite = this.scene.add.image(0, 0, "Fade8");
        this.solidBackgroundSprite.setOrigin(0, 0);
        this.solidBackgroundSprite.displayHeight = this.shelfSprite.displayHeight;
        this.solidBackgroundSprite.displayWidth = this.shelfSprite.displayWidth;
        // this.solidBackgroundSprite.tint = 0x000000;
        this.fadeBackgroundSprite = this.scene.add.image(0, 0, "Fade8");
        this.fadeBackgroundSprite.displayHeight = this.shelfSprite.displayHeight;
        this.fadeBackgroundSprite.displayWidth = this.shelfSprite.displayWidth;
        this.fadeBackgroundSprite.setOrigin(0, 0);
        this.fadeBackgroundSprite.setAlpha(0.5);
        // added by asset
        this.stack.add(this.solidBackgroundSprite);
        this.stack.add(this.fadeBackgroundSprite);
    };
    CommunityModel.prototype.initBodyPart = function () {
        var width = this.shelfSprite.displayWidth;
        var height = this.shelfSprite.displayHeight;
        //init bodypart manager
        this.bodyPartManager = new BaseBodyPartManager(this.scene, this, this.scene.modelParts, width, height);
        this.bodyPartManager.setAtlasFront(this.model.modelName + "-front");
        this.bodyParts = this.bodyPartManager.createBodyParts(this.scene.firstFrame);
        this.bodyPartManager.setUtmostSprites();
        this.savedModel = new SavedModel(this.jsonData);
        this.bodyPartManager.setSavedProperties(this.savedModel);
    };
    CommunityModel.prototype.getDisplayWidth = function () {
        return this.shelfSprite.displayWidth * this.stack.scale;
    };
    CommunityModel.prototype.getDisplayHeight = function () {
        return this.shelfSprite.displayHeight * this.stack.scale;
    };
    CommunityModel.prototype.setDisplaySize = function (displayWidth, displayHeight, visibility) {
        if (visibility === void 0) { visibility = true; }
        var scaleX = displayWidth / this.shelfSprite.displayWidth;
        var scaleY = displayHeight / this.shelfSprite.displayHeight;
        this.solidBackgroundSprite.displayWidth = this.shelfSprite.displayWidth;
        this.solidBackgroundSprite.displayHeight = this.shelfSprite.displayHeight;
        this.fadeBackgroundSprite.displayWidth = this.shelfSprite.displayWidth;
        this.fadeBackgroundSprite.displayHeight = this.shelfSprite.displayHeight;
        // added by asset
    };
    CommunityModel.prototype.setModel = function (model) {
        this.model = model;
    };
    CommunityModel.prototype.setFadeTexture = function (fadeTexture) {
        this.fadeBackgroundSprite.setTexture(fadeTexture);
    };
    CommunityModel.prototype.updateData = function (savedModel) {
        this.savedModel = savedModel;
        this.bodyPartManager.setSavedProperties(this.savedModel);
    };
    CommunityModel.prototype.updateCloudButtons = function () {
        this.likeButton.updateState(this.counter.like);
        this.likeButton.setStatus(!!this.counter.like);
        this.downloadButton.updateState(this.counter.download);
    };
    return CommunityModel;
}());
var BaseBodyPart = /** @class */ (function () {
    function BaseBodyPart(scene, bodyPartsManager, modelPart, bodyPartIndex, frame) {
        this.scene = scene;
        this.bodyPartsManager = bodyPartsManager;
        this.modelPart = modelPart;
        this.bodyPartIndex = bodyPartIndex;
        this.material = modelPart.materials[0];
        this.frame = frame;
        this.setRegularSprites();
    }
    BaseBodyPart.prototype.setSpriteFromMaterial = function () {
        if (this.material == undefined) {
            return;
        }
        if (this.material.clayRegion != undefined && this.material.clayRegion != "") {
            this.setClaySpriteFromMaterial();
        }
        if (this.material.glossRegion != undefined && this.material.glossRegion != "") {
            this.setGlossSpriteFromMaterial();
        }
        if (this.material.glossAlpha != undefined) {
            var glossAlpha = this.material.glossAlpha;
            if (this.glossSprite != undefined) {
                this.glossSprite.setAlpha(glossAlpha);
            }
        }
    };
    BaseBodyPart.prototype.setClaySpriteFromMaterial = function () {
        if (this.material.clayRegion.includes(NOTHING)) {
            if (this.claySprite != undefined)
                this.claySprite.destroy();
            this.claySprite = undefined;
            return;
        }
        var clayRegion = "" + this.material.clayRegion + this.frame;
        this.claySprite = this.bodyPartsManager.setSprite(clayRegion, this.modelPart, this.claySprite);
    };
    BaseBodyPart.prototype.setGlossSpriteFromMaterial = function () {
        if (this.material.glossRegion.includes(NOTHING)) {
            if (this.glossSprite != undefined)
                this.glossSprite.destroy();
            this.glossSprite = undefined;
            return;
        }
        if (this.material.glossRegion.includes(FADE)) {
            var newFadeImage = "" + this.material.glossRegion;
            this.bodyPartsManager.setBackGround(newFadeImage);
            return;
        }
        var glossRegion = "" + this.material.glossRegion + this.frame;
        this.glossSprite = this.bodyPartsManager.setSprite(glossRegion, this.modelPart, this.glossSprite);
    };
    BaseBodyPart.prototype.setColor = function (color) {
        if (this.claySprite) {
            this.claySprite.tint = Phaser.Display.Color.GetColor(color.red, color.green, color.blue);
        }
    };
    BaseBodyPart.prototype.setColorVal = function (color) {
        if (this.claySprite) {
            this.claySprite.tint = color;
        }
    };
    BaseBodyPart.prototype.setRegularSprites = function () {
        if (!this.modelPart.partBaseImage.includes(NOTHING)) {
            if (this.modelPart.partBaseImage != "") {
                var clayRegionName = "" + this.modelPart.partBaseImage + this.frame;
                this.claySprite = this.bodyPartsManager.setSprite(clayRegionName, this.modelPart, this.claySprite);
            }
        }
        if (!this.modelPart.partGlossImage.includes(NOTHING) && !this.modelPart.partGlossImage.includes(BACKGROUND)) {
            if (this.modelPart.partGlossImage != "" && !this.modelPart.partGlossImage.includes(FADE)) {
                var glossRegionName = "" + this.modelPart.partGlossImage + this.frame;
                this.glossSprite = this.bodyPartsManager.setSprite(glossRegionName, this.modelPart, this.glossSprite);
            }
        }
    };
    BaseBodyPart.prototype.setMaterial = function (tabIndex) {
        this.material = this.modelPart.materials[tabIndex];
        this.setSpriteFromMaterial();
    };
    return BaseBodyPart;
}());
var BaseBodyPartManager = /** @class */ (function () {
    function BaseBodyPartManager(scene, sloteItem, modelParts, width, height) {
        this.userScale = 1;
        this.textures = [];
        this.scene = scene;
        this.modelParts = modelParts;
        this.VIEW_PORT_WIDTH = width;
        this.VIEW_PORT_HEIGHT = height;
        this.setAverageXY();
        this.sloteItem = sloteItem;
        this.bodyPartStack = this.scene.add.container(width / 2, height / 2);
        this.sloteItem.stack.add(this.bodyPartStack);
        this.yAdjust = this.modelParts[1].yAdjust;
        this.xAdjust = this.modelParts[1].xAdjust;
        this.userScale = this.modelParts[1].scaleAdjust;
        if (this.userScale == 0)
            this.userScale = 1;
        this.sloteItem = sloteItem;
    }
    BaseBodyPartManager.prototype.setAtlasFront = function (atlasFront) {
        this.atlasFront = atlasFront;
    };
    BaseBodyPartManager.prototype.dispose = function () {
    };
    BaseBodyPartManager.prototype.setAverageXY = function () {
        var total_X = 0;
        var total_Y = 0;
        var totals = 0;
        this.modelParts.forEach(function (element) {
            if (!element.partName.includes("Shadow")) {
                totals += 1;
                total_X += +element.partPosX;
                total_Y += +element.partPosY;
            }
        });
        this.averageX = total_X / totals;
        this.averageY = total_Y / totals;
    };
    BaseBodyPartManager.prototype.createBodyParts = function (frame) {
        var _this = this;
        this.bodyParts = [];
        this.frame = frame;
        this.modelParts.forEach(function (modelPart) {
            var modelPartIndex = _this.modelParts.indexOf(modelPart);
            var bodyPart = new BaseBodyPart(_this.scene, _this, modelPart, modelPartIndex, frame);
            _this.bodyParts[modelPartIndex] = bodyPart;
        });
        return this.bodyParts;
    };
    BaseBodyPartManager.prototype.setSprite = function (regionName, modelPart, target) {
        var atlas = this.atlasFront + "-SD";
        var xSize = +modelPart.partSizeX;
        var ySize = +modelPart.partSizeY;
        var xPos = +modelPart.partPosX - this.averageX;
        var yPos = -+modelPart.partPosY + this.averageY;
        var x = xPos / LayoutContants.getInstance().SCREEN_HEIGHT_COEF;
        var y = yPos / LayoutContants.getInstance().SCREEN_HEIGHT_COEF;
        if (target == undefined) {
            target = new Phaser.GameObjects.Sprite(this.scene, x, y, atlas, regionName + ".png");
            target.displayHeight = ySize / LayoutContants.getInstance().SCREEN_HEIGHT_COEF;
            target.displayWidth = xSize / LayoutContants.getInstance().SCREEN_HEIGHT_COEF;
            if (regionName.toLowerCase().includes("shadow")) {
                this.bodyPartStack.addAt(target, 0);
            }
            else {
                if (!modelPart.feature.isChildren) {
                    if (regionName.includes("Gloss")) {
                        this.bodyPartStack.add(target);
                        target.setName("" + modelPart.feature.part_id);
                    }
                    else {
                        this.bodyPartStack.add(target);
                    }
                }
                else {
                    var parentPart = this.bodyPartStack.getByName("" + modelPart.feature.parent_id);
                    var parentZindex = this.bodyPartStack.getIndex(parentPart);
                    this.bodyPartStack.addAt(target, parentZindex);
                }
            }
        }
        else {
            target.setTexture(atlas, regionName + ".png");
        }
        return target;
    };
    BaseBodyPartManager.prototype.getBackgroundTexture = function () {
        return this.backgroundTexture;
    };
    BaseBodyPartManager.prototype.getFadeTexture = function () {
        return this.fadeTexture;
    };
    BaseBodyPartManager.prototype.setUtmostSprites = function () {
        var _this = this;
        var frame = this.frame;
        this.bodyPartStack.setPosition(this.VIEW_PORT_WIDTH / 2, this.VIEW_PORT_HEIGHT / 2);
        var minX = 0;
        var maxX = 0;
        var minY = 0;
        var maxY = 0;
        var bodyParts = this.bodyParts.slice(1, this.bodyParts.length - 1);
        bodyParts.forEach(function (element) {
            var sprite = element.claySprite;
            if (sprite != null) {
                var tempLX = sprite.x - sprite.displayWidth / 2;
                var tempRX = sprite.x + sprite.displayWidth / 2;
                var tempBY = sprite.y - sprite.displayHeight / 2;
                var tempTY = sprite.y + sprite.displayHeight / 2;
                if (minX == 0) {
                    minX = tempLX;
                    _this.leftMostSprite = sprite;
                }
                else {
                    if (tempLX < minX) {
                        minX = tempLX;
                        _this.leftMostSprite = sprite;
                    }
                }
                if (maxX == 0) {
                    maxX = tempRX;
                    _this.rightMostSprite = sprite;
                }
                else {
                    if (tempRX > maxX) {
                        maxX = tempRX;
                        _this.rightMostSprite = sprite;
                    }
                }
                if (minY == 0) {
                    minY = tempBY;
                    _this.downMostSprite = sprite;
                }
                else {
                    if (tempBY < minY) {
                        minY = tempBY;
                        _this.downMostSprite = sprite;
                    }
                }
                if (maxY == 0) {
                    maxY = tempTY;
                    _this.upMostSprite = sprite;
                }
                else {
                    if (tempTY > maxY) {
                        maxY = tempTY;
                        _this.upMostSprite = sprite;
                    }
                }
            }
        });
        this.modelHeight = maxY - minY;
        this.modelWidth = maxX - minX;
        var leftX = this.getLeftX();
        var rightX = this.getRightX();
        var bottomY = this.getBottomYByScreenMode();
        var topY = this.getTopY();
        var userScale = this.userScale * (bottomY - topY) / this.modelHeight;
        // var userScale = this.userScale * Math.min((rightX - leftX) / this.modelWidth, (bottomY - topY) / this.modelHeight) * 0.8;
        // var userScale = this.userScale * (bottomY - topY) / this.modelHeight
        this.bodyPartStack.setScale(userScale);
        var deltaY = bottomY - (this.bodyPartStack.y + userScale * this.upMostSprite.y);
        this.bodyPartStack.y += deltaY;
        this.bodyPartStack.y += this.yAdjust / LayoutContants.getInstance().SCREEN_HEIGHT_COEF / 2;
        this.bodyPartStack.x += this.xAdjust / LayoutContants.getInstance().SCREEN_SIZE_COEF / 2;
    };
    BaseBodyPartManager.prototype.setSavedProperties = function (savedModel) {
        this.savedModel = savedModel;
        for (var i = 0; i < this.bodyParts.length - 1; i++) {
            this.setMaterialFromSavedModel(this.bodyParts[i]);
        }
        for (var i = 0; i < this.bodyParts.length - 1; i++) {
            this.setColorFromSavedModel(this.bodyParts[i]);
        }
    };
    BaseBodyPartManager.prototype.setMaterialFromSavedModel = function (bodyPart) {
        var modelPartName = bodyPart.modelPart.partName;
        if (this.savedModel.getActiveTabs().has(modelPartName)) {
            var tabIndex = this.savedModel.getActiveTabs().get(modelPartName);
            bodyPart.setMaterial(tabIndex);
            this.scene.getModelCharacteristics().setFinish(tabIndex);
        }
    };
    BaseBodyPartManager.prototype.setColorFromSavedModel = function (bodyPart) {
        var modelPartName = bodyPart.modelPart.partName;
        if (this.savedModel.getPartColors().has(modelPartName)) {
            var customColorData = this.savedModel.getPartColors().get(modelPartName);
            var a = 1;
            var r = customColorData.r * 255;
            var g = customColorData.g * 255;
            var b = customColorData.b * 255;
            var customColor = Phaser.Display.Color.GetColor(r, g, b);
            if (bodyPart != null) {
                bodyPart.setColorVal(customColor);
            }
            if (bodyPart.bodyPartIndex == 0) {
                this.sloteItem.solidBackgroundSprite.tint = customColor;
            }
        }
    };
    BaseBodyPartManager.prototype.setFrame = function (newframe) {
        this.frame = newframe;
    };
    BaseBodyPartManager.prototype.getBottomYByScreenMode = function () {
        return (this.VIEW_PORT_HEIGHT - 80 / LayoutContants.getInstance().SCREEN_HEIGHT_COEF);
    };
    BaseBodyPartManager.prototype.getTopY = function () {
        return LayoutContants.getInstance().BUTTONS_SIZE_BASE / 2 / LayoutContants.getInstance().SCREEN_HEIGHT_COEF;
    };
    BaseBodyPartManager.prototype.getRightX = function () {
        return (this.VIEW_PORT_WIDTH - 10 / LayoutContants.getInstance().SCREEN_SIZE_COEF);
    };
    BaseBodyPartManager.prototype.getLeftX = function () {
        return 10 / LayoutContants.getInstance().SCREEN_SIZE_COEF;
    };
    BaseBodyPartManager.prototype.resetbodyPartStackScale = function (screenMode) {
        if (screenMode == CST.LAYOUT.SAVE_PREVIEW_MODE) {
            var bottomY = this.getBottomYByScreenMode();
            this.userScale = this.modelParts[1].scaleAdjust;
            if (this.userScale == 0)
                this.userScale = 1;
            var userScale = this.userScale * (bottomY - LayoutContants.getInstance().BUTTONS_SIZE) / this.modelHeight;
        }
    };
    BaseBodyPartManager.prototype.setBackGround = function (fadeTexture) {
        this.fadeTexture = fadeTexture;
        this.sloteItem.setFadeTexture(fadeTexture);
    };
    return BaseBodyPartManager;
}());
var BodyPartsManager = /** @class */ (function () {
    function BodyPartsManager(scene, modelParts) {
        this.deltaScale = 0;
        this.userScale = 1;
        this.dy = 0;
        this.dx = 0;
        this.isPing = false;
        this.textures = [];
        this.scene = scene;
        this.uiGenerator = scene.uiGenerator;
        this.modelParts = modelParts;
        this.linkedByColorBodyParts = [];
        this.linkedByTabBodyParts = [];
        this.hiddenBodyParts = [];
        this.shownBodyParts = [];
        this.fadeTexture = this.uiGenerator.fadeTextures[0];
        this.backgroundTexture = this.uiGenerator.backgroundTexture;
        this.setAverageXY();
        this.VIEW_PORT_HEIGHT = +scene.game.config.height;
        this.VIEW_PORT_WIDTH = scene.uiGenerator.backgroundBoxTopper.displayWidth;
        this.bodyPartStack = this.scene.add.container(this.VIEW_PORT_WIDTH / 2, this.VIEW_PORT_HEIGHT / 2);
        this.bodyPartInnerStack = this.scene.add.container(0, 0);
        this.bodyPartStack.add(this.bodyPartInnerStack);
        // this.testImage1= this.scene.add.image(0, 0, "");
        // this.testImage2= this.scene.add.image(0, 0, "");
        this.dy = 0;
        this.dx = 0;
        this.yAdjust = this.modelParts[1].yAdjust;
        this.xAdjust = this.modelParts[1].xAdjust;
        this.scaleDy = 0;
        this.userScale = this.modelParts[1].scaleAdjust;
        if (this.userScale == 0)
            this.userScale = 1;
        var url = window.location.href;
        if (url.includes('color_debug'))
            this.addDebugFunction();
        this.originBPX = 0;
        this.zoomAdjust = this.scene.model.zoomAdjust;
    }
    BodyPartsManager.prototype.setAtlasFront = function (atlasFront) {
        this.atlasFront = atlasFront;
    };
    BodyPartsManager.prototype.dispose = function () {
    };
    BodyPartsManager.prototype.setAverageXY = function () {
        this.VIEW_PORT_HEIGHT = LayoutContants.getInstance().SCREEN_HEIGHT;
        this.VIEW_PORT_WIDTH = LayoutContants.getInstance().SCREEN_WIDTH;
        var total_X = 0;
        var total_Y = 0;
        var totals = 0;
        this.modelParts.forEach(function (element) {
            if (!element.partName.includes("Shadow")) {
                totals += 1;
                total_X += +element.partPosX;
                total_Y += +element.partPosY;
            }
        });
        this.averageX = total_X / totals;
        this.averageY = total_Y / totals;
        // this.averageY = this.VIEW_PORT_HEIGHT/2;
    };
    BodyPartsManager.prototype.setLinkedBodyParts = function () {
        this.setLinkedByTabBodyParts();
        this.setLinkedByColorSprites();
    };
    BodyPartsManager.prototype.setLinkedByTabBodyParts = function () {
        this.linkedByTabBodyParts = [];
        var currentTabLink = +this.getSelectedBodyPartTabLink();
        if (currentTabLink == 0)
            return;
        for (var i = 0; i < this.bodyParts.length; i++) {
            if (this.bodyParts[i].modelPart.partName == this.selectedBodyPart.modelPart.partName) {
                continue;
            }
            var tablLink = +this.bodyParts[i].modelPart.feature.tabLink;
            if (tablLink == null || tablLink != currentTabLink) {
                continue;
            }
            this.linkedByTabBodyParts.push(this.bodyParts[i]);
        }
    };
    BodyPartsManager.prototype.getSelectedBodyPartTabLink = function () {
        var chosenModelPart = this.modelParts[this.selectedBodyPart.bodyPartIndex];
        return chosenModelPart.feature.tabLink;
    };
    BodyPartsManager.prototype.setLinkedByColorSprites = function () {
        this.linkedByColorBodyParts = [];
        var currentColorLink = +this.getSelectedBodyPartColorLink();
        if (currentColorLink == 0)
            return;
        for (var i = 0; i < this.bodyParts.length; i++) {
            if (this.bodyParts[i].modelPart.partName == this.selectedBodyPart.modelPart.partName) {
                continue;
            }
            var colorLink = +this.bodyParts[i].modelPart.feature.colorLink;
            if (colorLink == null || colorLink != currentColorLink) {
                continue;
            }
            this.linkedByColorBodyParts.push(this.bodyParts[i]);
        }
    };
    BodyPartsManager.prototype.getSelectedBodyPartColorLink = function () {
        return this.modelParts[this.selectedBodyPart.bodyPartIndex].feature.colorLink;
    };
    /**
     * create bodyparts, child-bodypart/top-menu buttons and load sprites
     * @param atlas string
     * @param frame integer
     */
    BodyPartsManager.prototype.createBodyParts = function (atlas, frame) {
        var _this = this;
        this.bodyParts = [];
        this.frame = frame;
        this.modelParts.forEach(function (modelPart) {
            var modelPartIndex = _this.modelParts.indexOf(modelPart);
            var bodyPart = new BodyPart(_this.scene, _this, modelPart, modelPartIndex);
            _this.bodyParts[modelPartIndex] = bodyPart;
            if (atlas == _this.atlasFront) {
                if (bodyPart.modelPart.feature.isChildren) {
                    // if child bodypart
                    bodyPart.setChildBodyPartButton();
                }
                else {
                    // if parent bodypart
                    bodyPart.setTMenuButton();
                }
            }
            bodyPart.setSprites(frame);
        });
        return this.bodyParts;
    };
    BodyPartsManager.prototype.setMaterial = function (tabIndex) {
        // this.scene.stopBlinking();
        if (this.selectedBodyPart.isParent()) {
            // let parentBodyPart = this.selectedBodyPart.getParnetBodyPart();
            // parentBodyPart.setMaterial(tabIndex);
            // this.setlinkedSpritesMaterial(tabIndex);
            // this.setColorButtonImages(parentBodyPart.material);
            // }else{
            this.selectedBodyPart.setMaterial(tabIndex);
            this.setlinkedSpritesMaterial(tabIndex);
            this.setColorButtonImages(this.selectedBodyPart.material);
            this.uiGenerator.setTabSelector();
        }
    };
    BodyPartsManager.prototype.setlinkedSpritesMaterial = function (tabindex) {
        if (this.linkedByTabBodyParts.length == 0) {
            return;
        }
        this.linkedByTabBodyParts.forEach(function (bodyPart) {
            var linkedMaterials = bodyPart.modelPart.materials[tabindex];
            if (linkedMaterials.clayRegion != null) {
                bodyPart.setMaterial(tabindex);
            }
        });
    };
    // set sprites into bodypartinnerstack
    BodyPartsManager.prototype.setSprite = function (regionName, modelPart, target, frame) {
        var atlas;
        var resolution = this.scene.getResolution();
        if (regionName.endsWith("" + this.scene.firstFrame)) {
            atlas = this.scene.getAtlasfront();
        }
        else {
            atlas = this.atlasFrames + "-" + resolution;
        }
        var xSize = +modelPart.partSizeX;
        var ySize = +modelPart.partSizeY;
        var screenWidth = this.VIEW_PORT_WIDTH;
        var screenHeight = this.VIEW_PORT_HEIGHT;
        var xPos = +modelPart.partPosX - this.averageX;
        var yPos = -+modelPart.partPosY + this.averageY;
        var x = xPos / LayoutContants.getInstance().SCREEN_HEIGHT_COEF;
        var y = yPos / LayoutContants.getInstance().SCREEN_HEIGHT_COEF;
        var orderIndex = +modelPart.sortOrder;
        if (target == undefined) {
            if (this.scene.isFileLoaded) {
                if (regionName.includes(NOTHING)) {
                    target = new BodySprite(this.scene, x, y, "empty");
                    target.setName("empty");
                }
                else {
                    target = new BodySprite(this.scene, x, y, atlas, regionName + ".png");
                    target.setName('noempty');
                }
                target.displayHeight = ySize / LayoutContants.getInstance().SCREEN_HEIGHT_COEF;
                target.displayWidth = xSize / LayoutContants.getInstance().SCREEN_HEIGHT_COEF;
                target.setVisible(false);
                if (regionName.toLowerCase().includes("shadow")) {
                    this.bodyPartInnerStack.addAt(target, 0);
                }
                else {
                    this.insertBodyPart(regionName, modelPart, target, frame);
                }
            }
            else {
                if (atlas == this.atlasFront) {
                    if (regionName.includes(NOTHING)) {
                        target = new BodySprite(this.scene, x, y, "empty");
                        target.setName("empty");
                    }
                    else {
                        target = new BodySprite(this.scene, x, y, atlas, regionName + ".png");
                        target.setName('noempty');
                    }
                    target.displayHeight = ySize / LayoutContants.getInstance().SCREEN_HEIGHT_COEF;
                    target.displayWidth = xSize / LayoutContants.getInstance().SCREEN_HEIGHT_COEF;
                    if (regionName.toLowerCase().includes("shadow")) {
                        this.bodyPartInnerStack.addAt(target, 0);
                    }
                    else {
                        this.insertBodyPart(regionName, modelPart, target, frame);
                    }
                    target.setVisible(true);
                }
            }
        }
        else {
            var displayWidth = target.displayWidth;
            var displayHeight = target.displayHeight;
            if (regionName.includes(NOTHING)) {
                target.setTexture('empty');
                target.setName("empty");
            }
            else {
                target.setName('noempty');
                target.setTexture(atlas, regionName + ".png");
            }
            target.displayHeight = displayHeight;
            target.displayWidth = displayWidth;
            if (!this.scene.isFileLoaded) {
                target.setVisible(true);
            }
        }
        return target;
    };
    BodyPartsManager.prototype.insertBodyPart = function (regionName, modelPart, target, frame) {
        if (!modelPart.feature.isChildren) {
            if (regionName.includes("Gloss")) {
                this.bodyPartInnerStack.add(target);
                target.setName(modelPart.feature.part_id + "-" + frame);
            }
            else {
                this.bodyPartInnerStack.add(target);
            }
        }
        else {
            var parentPart = this.bodyPartInnerStack.getByName(modelPart.feature.parent_id + "-" + frame);
            var parentZindex = this.bodyPartInnerStack.getIndex(parentPart);
            this.bodyPartInnerStack.addAt(target, parentZindex);
        }
    };
    BodyPartsManager.prototype.updateSprite = function (regionName, modelPart, target) {
        var atlas;
        if (regionName.endsWith("" + this.scene.firstFrame)) {
            atlas = this.atlasFront;
        }
        else {
            var resolution = this.scene.getResolution();
            atlas = this.atlasFrames + "-" + resolution;
        }
        if (target) {
            var displayWidth = target.displayWidth;
            var displayHeight = target.displayHeight;
            if (regionName.includes(NOTHING)) {
                target.setTexture('empty');
                target.setName("empty");
            }
            else {
                target.setName('noempty');
                target.setTexture(atlas, regionName + ".png");
            }
            target.displayHeight = displayHeight;
            target.displayWidth = displayWidth;
        }
    };
    BodyPartsManager.prototype.showBodyParts = function () {
        var _this = this;
        var markedAsShown = [];
        this.hiddenBodyParts.forEach(function (bodyPart) {
            bodyPart.showSprites();
            markedAsShown.push(bodyPart);
            bodyPart.isHidden = false;
        });
        this.hiddenBodyParts = [];
        this.shownBodyParts.forEach(function (bodyPart) {
            bodyPart.hideSprites();
            _this.hiddenBodyParts.push(bodyPart);
            bodyPart.isHidden = true;
        });
        this.shownBodyParts = [];
        this.shownBodyParts = markedAsShown;
    };
    // set current selected bodypart object and bodypart index
    BodyPartsManager.prototype.setSelectedBodyPart = function (selectedBodyPart) {
        this.selectedBodyPart = selectedBodyPart;
        this.selectedBodyPartIndex = selectedBodyPart.bodyPartIndex;
    };
    BodyPartsManager.prototype.setCurrentBodyPartColor = function (colorMode, colorButton) {
        var _this = this;
        var color = colorButton.color;
        if (this.selectedBodyPart != null) {
            this.selectedBodyPart.setColor(colorButton);
            this.selectedBodyPart.setColorMode(colorMode);
        }
        if (this.linkedByColorBodyParts.length != 0) {
            this.linkedByColorBodyParts.forEach(function (bodyPart) {
                bodyPart.setColor(colorButton);
                _this.selectedBodyPart.setColorMode(colorMode);
                //if(checkIfContainsBodypart(self.hiddenBodyParts, bodyPart)){
                // self.hideBodyParts();
                //}
            });
        }
        if (this.selectedBodyPart.backgroundTexture != null) {
            this.uiGenerator.backgroundSprite.setColor(Phaser.Display.Color.GetColor(color.red, color.green, color.blue));
            this.uiGenerator.solidGreySprite.tint = Phaser.Display.Color.GetColor(color.red, color.green, color.blue);
            this.uiGenerator.bottomPlaceHolder.tint = Phaser.Display.Color.GetColor(color.red, color.green, color.blue);
        }
        this.uiGenerator.colorAdjustGradient.tint = Phaser.Display.Color.GetColor(color.red, color.green, color.blue);
    };
    BodyPartsManager.prototype.hideBodyParts = function () {
        this.hiddenBodyParts.forEach(function (element) {
            element.hideSprites();
        });
    };
    BodyPartsManager.prototype.setInitialSelectedBodyPart = function () {
        if (this.selectedBodyPart == null) {
            this.selectedBodyPartIndex = this.getFirstBodyPart().bodyPartIndex;
            this.selectedBodyPart = this.bodyParts[this.selectedBodyPartIndex];
            this.uiGenerator.setSelectedTMenuButton(this.selectedBodyPart.tMenuButton);
        }
    };
    BodyPartsManager.prototype.setColorButtonImages = function (colorButtonMaterial) {
        this.colorButtons = this.uiGenerator.colorButtons;
        this.colorButtons.forEach(function (element) {
            element.setImages(colorButtonMaterial);
        });
    };
    BodyPartsManager.prototype.getBackgroundTexture = function () {
        return this.backgroundTexture;
    };
    BodyPartsManager.prototype.getFadeTexture = function () {
        return this.fadeTexture;
    };
    BodyPartsManager.prototype.setUtmostSprites = function (screenMode) {
        var _this = this;
        var frame = this.frame;
        this.VIEW_PORT_WIDTH = this.scene.uiGenerator.backgroundBoxTopper.displayWidth;
        this.bodyPartStack.setPosition(this.VIEW_PORT_WIDTH / 2, this.VIEW_PORT_HEIGHT / 2);
        this.bodyPartInnerStack.setPosition(0, 0);
        var minX = 0;
        var maxX = 0;
        var minY = 0;
        var maxY = 0;
        var bodyParts = this.bodyParts.slice(1, this.bodyParts.length - 1);
        bodyParts.forEach(function (element) {
            var sprite = element.claySprites[frame];
            if (sprite != null) {
                if (sprite.name != "empty") {
                    var tempLX = sprite.x - sprite.displayWidth / 2;
                    var tempRX = sprite.x + sprite.displayWidth / 2;
                    var tempBY = sprite.y - sprite.displayHeight / 2;
                    var tempTY = sprite.y + sprite.displayHeight / 2;
                    if (minX == 0) {
                        minX = tempLX;
                        _this.leftMostSprite = sprite;
                    }
                    else {
                        if (tempLX < minX) {
                            minX = tempLX;
                            _this.leftMostSprite = sprite;
                        }
                    }
                    if (maxX == 0) {
                        maxX = tempRX;
                        _this.rightMostSprite = sprite;
                    }
                    else {
                        if (tempRX > maxX) {
                            maxX = tempRX;
                            _this.rightMostSprite = sprite;
                        }
                    }
                    if (minY == 0) {
                        minY = tempBY;
                        _this.downMostSprite = sprite;
                    }
                    else {
                        if (tempBY < minY) {
                            minY = tempBY;
                            _this.downMostSprite = sprite;
                        }
                    }
                    if (maxY == 0) {
                        maxY = tempTY;
                        _this.upMostSprite = sprite;
                    }
                    else {
                        if (tempTY > maxY) {
                            maxY = tempTY;
                            _this.upMostSprite = sprite;
                        }
                    }
                }
            }
        });
        this.modelHeight = maxY - minY;
        this.modelWidth = maxX - minX;
        var bottomY = this.getBottomYByScreenMode(screenMode);
        var topY = this.getTopY();
        var userScale = this.userScale * (bottomY - topY) / this.modelHeight;
        if (!this.initialScale)
            this.initialScale = this.userScale;
        this.bodyPartStack.setScale(userScale);
        this.deltaY = bottomY - (this.bodyPartStack.y + userScale * this.upMostSprite.y);
        this.bodyPartStack.y += this.deltaY;
        this.bodyPartStack.y += this.dy + this.yAdjust / LayoutContants.getInstance().SCREEN_HEIGHT_COEF;
        this.bodyPartStack.x += this.dx + this.xAdjust / LayoutContants.getInstance().SCREEN_SIZE_COEF;
        // detect bround of model background and protect overflow model's width and height - Edited by SY
        //console.log('bodypartmanager.setUtmostSprites', this.bodyPartStack.x, this.bodyPartStack.y);
        this.maxScale = 2.5 * this.bodyPartStack.scale * this.zoomAdjust;
        this.minScale = 0.5 * this.bodyPartStack.scale;
        //console.log("this.bodyPartStack.y", this.bodyPartStack.y);
        // if(!this.initialBodyStackY)
        // this.initialBodyStackY = this.bodyPartStack.y;
        // if(!this.initialBodyStackX)
        // this.initialBodyStackX = this.bodyPartStack.x;
        this.initializeOriginalPositionAndScale();
    };
    BodyPartsManager.prototype.initializeOriginalPositionAndScale = function () {
        console.log("initializeOriginalPositionAndScale");
        if (this.originBPX == 0) {
            this.dynamicCenterX = this.bodyPartStack.x;
            this.originBPX = this.bodyPartStack.x;
            this.originBPY = this.bodyPartStack.y;
            this.originScale = this.bodyPartStack.scale;
        }
    };
    BodyPartsManager.prototype.moveByY = function (dy) {
        this.bodyPartStack.y += dy;
        this.uiGenerator.fadeOutBackgroundSprite();
        //restrict pan zone.
        this.restrictPanZone();
    };
    BodyPartsManager.prototype.moveByX = function (dx) {
        this.bodyPartStack.x += dx;
    };
    BodyPartsManager.prototype.restrictPanZone = function () {
        var x = this.bodyPartStack.x;
        var y = this.bodyPartStack.y;
        var magnetStatus = this.scene.magnetStatus;
        var mdh = this.modelHeight * this.bodyPartStack.scale;
        var mdw = this.modelWidth * this.bodyPartStack.scale;
        var bndLX = mdw / 8;
        var bndRX = this.scene.uiGenerator.buttonPreview.x - mdw / 4;
        var bndTY = this.scene.uiGenerator.buttonPreview.y + mdh / 8;
        var bndBY = LayoutContants.getInstance().SCREEN_HEIGHT - mdh / 8;
        if (this.scene.magnetStatus != 3) {
            bndBY -= LayoutContants.getInstance().BUTTONS_SIZE;
        }
        if (x < bndLX - mdw / 2) {
            this.bodyPartStack.x = bndLX - mdw / 2;
        }
        if (x > bndRX + mdw / 2) {
            this.bodyPartStack.x = bndRX + mdw / 2;
        }
        if (y < bndTY - mdh / 2) {
            this.bodyPartStack.y = bndTY - mdh / 2;
        }
        if (y > bndBY + mdh / 2) {
            this.bodyPartStack.y = bndBY + mdh / 2;
        }
    };
    BodyPartsManager.prototype.setStackPosX = function (x) {
        this.bodyPartStack.x = x;
    };
    BodyPartsManager.prototype.scaleBy = function (dscale) {
        this.deltaScale += dscale;
        this.uiGenerator.fadeOutBackgroundSprite();
    };
    BodyPartsManager.prototype.setScaleMulti = function (dscale, posX, posY) {
        console.log(dscale);
        if (dscale == 0)
            return;
        var scale = this.bodyPartStack.scale * dscale;
        if (scale >= this.maxScale) {
            scale = this.maxScale;
        }
        if (scale <= this.minScale) {
            scale = this.minScale;
        }
        if (this.tween)
            this.tween.remove();
        this.bodyPartStack.scale = scale;
        this.uiGenerator.fadeOutBackgroundSprite();
    };
    BodyPartsManager.prototype.setScaleBy = function (dscale) {
        var newScale = this.bodyPartStack.scale + dscale;
        if (newScale == 0)
            return;
        if (newScale >= this.maxScale) {
            newScale = this.maxScale;
        }
        if (newScale <= this.minScale) {
            newScale = this.minScale;
        }
        if (this.tween)
            this.tween.remove();
        this.bodyPartStack.scale = newScale;
        this.uiGenerator.fadeOutBackgroundSprite();
        this.restrictPanZone();
    };
    BodyPartsManager.prototype.zoom = function (target, targetKey, value, targetIndex, totalTargets, tween) {
    };
    BodyPartsManager.prototype.setAtlasFrames = function (atlasFrames) {
        this.atlasFrames = atlasFrames;
    };
    BodyPartsManager.prototype.createFramesSprites = function (frame) {
        this.bodyParts.forEach(function (element) {
            if (element != null) {
                element.setSprites(frame);
                element.initColor();
            }
        });
    };
    BodyPartsManager.prototype.resetSprite = function () {
        //debugger
        var _this = this;
        this.bodyParts.forEach(function (element) {
            if (element.modelPart.partName != BACKGROUND) {
                for (var i = _this.scene.minFrame; i <= _this.scene.maxFrame; i++) {
                    element.updateBodyPart(i);
                    element.setSpriteFromMaterial(i);
                }
                element.initColor();
            }
            else {
                element.initColor();
                _this.uiGenerator.backgroundSprite.setColor(Phaser.Display.Color.GetColor(element.color.red, element.color.green, element.color.blue));
                _this.uiGenerator.solidGreySprite.tint = Phaser.Display.Color.GetColor(element.color.red, element.color.green, element.color.blue);
            }
        });
    };
    BodyPartsManager.prototype.setSavedProperties = function (savedModel) {
        this.savedModel = savedModel;
        for (var i = 0; i < this.bodyParts.length - 1; i++) {
            this.setPalleteNumbersFromSavedModel(this.bodyParts[i]);
            this.setMaterialFromSavedModel(this.bodyParts[i]);
        }
        this.uiGenerator.palleteNumber = this.bodyParts[1].palleteNumber;
        for (var i = 0; i < this.bodyParts.length - 1; i++) {
            this.setColorFromSavedModel(this.bodyParts[i]);
            this.setButtonIndexFromSavedModel(this.bodyParts[i]);
        }
        this.selectedBodyPart = this.getFirstBodyPart();
        this.selectedBodyPartIndex = this.selectedBodyPart.bodyPartIndex;
        if (this.selectedBodyPart.colorModeNumber > 9) {
            this.uiGenerator.setColorMode(CST.COLOR_MODE.CUSTOME);
            this.uiGenerator.colorPalletePanel.setPalleteNumber(this.selectedBodyPart.colorModeNumber);
        }
        else {
            this.uiGenerator.setColorMode(CST.COLOR_MODE.DEFAULT);
        }
        this.uiGenerator.setColorSelection(this.bodyParts[this.selectedBodyPartIndex].colorButton.buttonIndex);
    };
    BodyPartsManager.prototype.setPalleteNumbersFromSavedModel = function (bodyPart) {
        var modelPartName = bodyPart.modelPart.partName;
        if (this.savedModel.getSliderValues().has(modelPartName)) {
            var sliderPosition = +this.savedModel.getSliderValues().get(modelPartName);
            bodyPart.setInitialPalletNumber(sliderPosition);
        }
    };
    BodyPartsManager.prototype.setMaterialFromSavedModel = function (bodyPart) {
        var modelPartName = bodyPart.modelPart.partName;
        if (this.savedModel.getActiveTabs().has(modelPartName)) {
            var tabIndex = this.savedModel.getActiveTabs().get(modelPartName);
            this.selectedBodyPart = bodyPart;
            this.selectedBodyPartIndex = bodyPart.bodyPartIndex;
            this.setMaterial(tabIndex);
            this.scene.getModelCharacteristics().setFinish(tabIndex);
        }
    };
    BodyPartsManager.prototype.setColorFromSavedModel = function (bodyPart) {
        var modelPartName = bodyPart.modelPart.partName;
        if (bodyPart.colorButton) {
            bodyPart.colorButton.isMarked = false;
            bodyPart.colorButton.resetStack();
        }
        if (this.savedModel.getPartColors().has(modelPartName)) {
            var customColorData = this.savedModel.getPartColors().get(modelPartName);
            var a = 1;
            var customColor = new Phaser.Display.Color(customColorData.r * 255, customColorData.g * 255, customColorData.b * 255, a * 255);
            if (bodyPart != null) {
                bodyPart.setSelectorColor(customColor);
            }
            if (bodyPart.bodyPartIndex == 0) {
                this.uiGenerator.bottomPlaceHolder.tint = Phaser.Display.Color.GetColor(customColor.red, customColor.green, customColor.blue);
                this.uiGenerator.solidGreySprite.tint = Phaser.Display.Color.GetColor(customColor.red, customColor.green, customColor.blue);
            }
        }
    };
    BodyPartsManager.prototype.setButtonIndexFromSavedModel = function (bodyPart) {
        var modelPartName = bodyPart.modelPart.partName;
        if (this.savedModel.getButtonsIndex().has(modelPartName)) {
            var buttonsIndex = this.savedModel.getButtonsIndex().get(modelPartName);
            var colorButton = this.uiGenerator.colorButtons[buttonsIndex];
            bodyPart.colorButton = colorButton;
            colorButton.addToColoredBodyPartsCount();
        }
    };
    BodyPartsManager.prototype.renderBodyPart = function () {
        this.setBodyPartByFrame(this.frame);
    };
    BodyPartsManager.prototype.setBodyPartByFrame = function (frame) {
        if (this.bodyParts) {
            for (var i = 0; i < this.bodyParts.length; i++) {
                if (this.bodyParts[i] != null) {
                    this.bodyParts[i].render(frame);
                }
            }
        }
    };
    BodyPartsManager.prototype.rotateLeft = function () {
        var newframe = this.frame + 1;
        this.frame = newframe;
        if (this.scene.model.isOrbit == "0") {
            if (this.frame > this.scene.maxFrame)
                this.frame = this.scene.maxFrame;
        }
        else {
            if (this.frame == this.scene.maxFrame + 1) {
                this.frame = 1;
            }
        }
    };
    BodyPartsManager.prototype.rotateRight = function () {
        var newframe = this.frame - 1;
        this.frame = newframe;
        if (this.scene.model.isOrbit == "0") {
            if (this.frame < 0)
                this.frame = 0;
        }
        else {
            if (this.frame == 0) {
                this.frame = this.scene.maxFrame;
            }
        }
    };
    BodyPartsManager.prototype.updateModel = function () {
        this.setBodyPartByFrame(this.frame);
    };
    BodyPartsManager.prototype.setFrame = function (newframe) {
        this.frame = newframe;
    };
    BodyPartsManager.prototype.getCurrentSelectedPartPos = function () {
        var selectedbodyPart = this.selectedBodyPart.claySprites[this.frame];
        if (selectedbodyPart) {
            var defaultX = this.VIEW_PORT_HEIGHT / 2;
            var defaultY = this.VIEW_PORT_WIDTH / 2;
            if (selectedbodyPart != null) {
                // const posX =  selectedbodyPart.x + this.averageX;
                // const posY =  selectedbodyPart.y - this.averageX;
                var scale = this.bodyPartStack.scale;
                return { x: selectedbodyPart.x * scale + this.bodyPartStack.x, y: selectedbodyPart.y * scale + this.bodyPartStack.y };
            }
            else {
                return { x: defaultX, y: defaultY };
            }
        }
    };
    BodyPartsManager.prototype.saveModel = function () {
        var activeTabsMap = new Map();
        var partColorsMap = new Map();
        var sliderValuesMap = new Map();
        var buttonsIndexMap = new Map();
        for (var i = 0; i < this.bodyParts.length; i++) {
            var modelPartName = this.bodyParts[i].modelPart.partName;
            activeTabsMap.set(modelPartName, this.bodyParts[i].selectedTabIndex);
            var color = this.bodyParts[i].color;
            if (color != null) {
                var customColor = {
                    r: color.red / 255,
                    g: color.green / 255,
                    b: color.blue / 255,
                    a: color.alpha / 255
                };
                partColorsMap.set(modelPartName, customColor);
            }
            var bodyPart = this.bodyParts[i];
            var palleteNumber = this.bodyParts[i].palleteNumber;
            if (bodyPart.colorModeNumber > 9) { //custom color mode
                palleteNumber = bodyPart.colorModeNumber;
            }
            sliderValuesMap.set(modelPartName, palleteNumber);
            var colorButton = this.bodyParts[i].colorButton;
            if (colorButton != null) {
                buttonsIndexMap.set(modelPartName, this.bodyParts[i].colorButton.buttonIndex);
            }
        }
        this.savedModel.setActiveTabs(activeTabsMap);
        this.savedModel.setPartColors(partColorsMap);
        this.savedModel.setSliderValues(sliderValuesMap);
        this.savedModel.setButtonsIndex(buttonsIndexMap);
        Global.getInstance().saveCurrentModel(this.savedModel);
    };
    BodyPartsManager.prototype.getBottomYByScreenMode = function (screenMode) {
        if (screenMode == CST.LAYOUT.EDIT_MODE || screenMode == CST.LAYOUT.PREVIEW_MODE) {
            return (this.VIEW_PORT_HEIGHT - 300 / LayoutContants.getInstance().SCREEN_HEIGHT_COEF);
        }
        if (screenMode == CST.LAYOUT.SAVE_PREVIEW_MODE || screenMode == CST.LAYOUT.SHOP_MODE) {
            return (this.VIEW_PORT_HEIGHT - 220 / LayoutContants.getInstance().SCREEN_HEIGHT_COEF);
        }
    };
    BodyPartsManager.prototype.getTopY = function () {
        return LayoutContants.getInstance().BUTTONS_SIZE_BASE / LayoutContants.getInstance().SCREEN_HEIGHT_COEF;
    };
    BodyPartsManager.prototype.resetbodyPartStackScale = function (screenMode) {
        if (screenMode == CST.LAYOUT.SAVE_PREVIEW_MODE) {
            var bottomY = this.getBottomYByScreenMode(screenMode);
            this.userScale = this.modelParts[1].scaleAdjust;
            if (this.userScale == 0)
                this.userScale = 1;
            var userScale = this.userScale * (bottomY - LayoutContants.getInstance().BUTTONS_SIZE) / this.modelHeight;
        }
    };
    BodyPartsManager.prototype.resetDXDY = function () {
        this.dx = 0;
        this.dy = 0;
        if (this.initialScale)
            this.userScale = this.initialScale;
        this.scaleDy = 0;
        this.scaleDx = 0;
    };
    BodyPartsManager.prototype.getFirstBodyPart = function () {
        var firstBodyPart = this.bodyParts[1];
        this.bodyParts.forEach(function (element) {
            if (+element.modelPart.tMenuOrder > 0 && firstBodyPart.modelPart.tMenuOrder > element.modelPart.tMenuOrder)
                firstBodyPart = element;
        });
        return firstBodyPart;
    };
    BodyPartsManager.prototype.addDebugFunction = function () {
        var _this = this;
        var data = {
            xAdjustment: this.xAdjust,
            yAdjustment: this.yAdjust,
            scaleAdjustment: this.userScale
        };
        var event = new CustomEvent('setDebugFrom', { detail: data });
        document.dispatchEvent(event);
        document.addEventListener('setCharacterAdjustment', function (e) {
            _this.xAdjust = e.detail.xAdjustment;
            _this.yAdjust = e.detail.yAdjustment;
            _this.userScale = e.detail.scaleAdjustment;
            //console.log("bodyspartmanager.adddebugfunction 1", this.uiGenerator.backgroundSprite.displayWidth, this.uiGenerator.backgroundSprite.displayHeight)
            //console.log("bodyspartmanager.adddebugfunction 2", this.xAdjust, this.yAdjust)
            //if(this.xAdjust > this.uiGenerator.backgroundSprite)
            _this.setUtmostSprites(_this.scene.screenMode);
        });
    };
    BodyPartsManager.prototype.getBodyPartPos = function () {
        return {
            x: this.bodyPartInnerStack.x + this.bodyPartStack.x,
            y: this.bodyPartInnerStack.y + this.bodyPartStack.y,
            scale: this.bodyPartInnerStack.scale * this.bodyPartStack.scale
        };
    };
    BodyPartsManager.prototype.checkPixelPerfectPos = function (x, y) {
        var _this = this;
        var touchedElements = [];
        var scale = this.bodyPartInnerStack.scale * this.bodyPartStack.scale;
        for (var _i = 0, _a = this.bodyParts; _i < _a.length; _i++) {
            var element = _a[_i];
            if (element.modelPart.feature.paralax != "1") {
                var touchedElement = element.checkPixelPerfectPos(x, y, scale, this.frame);
                if (touchedElement != undefined) {
                    touchedElements.push(touchedElement);
                }
            }
        }
        if (touchedElements.length > 0) {
            touchedElements[touchedElements.length - 1].performSelection();
        }
        if (touchedElements.length == 0) {
            if (this.scene.uiGenerator.viewMode == CST.LAYOUT.SHOP_MODE || this.scene.uiGenerator.viewMode == CST.LAYOUT.SAVE_PREVIEW_MODE)
                return;
            if (LayoutContants.getInstance().modelMoving)
                return;
            if (this.uiGenerator.backgroundSprite.getBounds().contains(x, y)) {
                this.uiGenerator.backgroundSprite.performSelection();
            }
        }
        setTimeout(function () {
            _this.uiGenerator.backgroundSprite.resetColor();
            _this.uiGenerator.solidGreySprite.tint = _this.uiGenerator.backgroundSprite.originTint;
        }, 100);
    };
    BodyPartsManager.prototype.resetBodyPartStack = function () {
        // let x = 0;
        // switch(this.scene.screenMode){
        //     case CST.LAYOUT.EDIT_MODE:
        //             x = this.originBPX;
        //         break;
        //     case CST.LAYOUT.PREVIEW_MODE:
        //     default:
        //         x = LayoutContants.getInstance().SCREEN_WIDTH/2; 
        // }
        this.scene.tweens.add({
            targets: this.bodyPartStack,
            x: this.dynamicCenterX,
            y: this.originBPY,
            scale: this.originScale,
            ease: 'Cubic.easeIn',
            duration: 500,
        });
    };
    return BodyPartsManager;
}());
var CsvParser = /** @class */ (function () {
    function CsvParser() {
    }
    CsvParser.parsePartsCsv = function (csvPath, featuresCsvPath, callback) {
        var result = [];
        this.parseFeaturesCsv(featuresCsvPath, function (features) {
            Papa.parse(csvPath, {
                download: true,
                complete: function (results) {
                    var i = 0;
                    results.data.forEach(function (entry) {
                        if (i > 0 && i < results.data.length - 1) {
                            var model = new ModelPart(entry[CST.CSVPARSER.PART_NAME_COL], entry[CST.CSVPARSER.SORT_ORDER_COL], entry[CST.CSVPARSER.TMENU_ORDER_COL], entry[CST.CSVPARSER.PART_SIZE_X_COL], entry[CST.CSVPARSER.PART_SIZE_Y_COL], entry[CST.CSVPARSER.PART_POS_X_COL], entry[CST.CSVPARSER.PART_POS_Y_COL], entry[CST.CSVPARSER.SCALE_ADJUST_COL], entry[CST.CSVPARSER.X_ADJUST_COL], entry[CST.CSVPARSER.Y_ADJUST_COL], entry[CST.CSVPARSER.PART_BASE_IMAGE_COL], entry[CST.CSVPARSER.PART_GLOSS_IMAGE_COL], entry[CST.CSVPARSER.GLOSS_ALPHA_COL], entry[CST.CSVPARSER.COLOR_BUTTON_BASE_IMAGE_COL], entry[CST.CSVPARSER.COLOR_BUTTON_GLOSS_IMAGE_COL], entry[CST.CSVPARSER.BUTTON_GLOSS_ALPHA_COL], fetchTabNames(entry), fetchMaterials(entry), features[i - 1]);
                            result.push(model);
                        }
                        i++;
                    });
                    var sortedArray = result.sort(function (obj1, obj2) {
                        if (obj1.partName.includes("Background"))
                            obj1.sortOrder = "0";
                        else if (obj1.partName.includes("Shadow"))
                            obj1.sortOrder = "" + result.length;
                        if (obj2.partName.includes("Background"))
                            obj2.sortOrder = "0";
                        else if (obj2.partName.includes("Shadow"))
                            obj2.sortOrder = "" + result.length;
                        return +obj1.sortOrder - +obj2.sortOrder;
                    });
                    callback(sortedArray);
                }
            });
        });
        function fetchTabNames(entry) {
            var tabNames = [];
            for (var tabIndex = CST.CSVPARSER.FIRST_TAB_IMAGE_COL; tabIndex <= CST.CSVPARSER.LAST_TAB_IMAGE_COL; tabIndex++) {
                var tabName = entry[tabIndex];
                if (tabName != null) {
                    tabNames.push(tabName);
                }
            }
            return tabNames;
        }
        function fetchMaterials(entry) {
            var index;
            var indexGloss;
            var indexGlossAlpha;
            var indexButton;
            var indexButtonGloss;
            var indexButtonGlossAlpha;
            //        Each Material corresponds to one Tab button.
            var materials = [];
            for (var tabIndex = 0; tabIndex < CST.CSVPARSER.NUM_OF_TABS; tabIndex++) {
                index = CST.CSVPARSER.PART_BASE_IMAGE_COL + tabIndex * CST.CSVPARSER.NUM_OF_COLUMNS_BETWEEN_CORRESPONDING_IMAGES_IN_CSV;
                indexGloss = CST.CSVPARSER.PART_GLOSS_IMAGE_COL + tabIndex * CST.CSVPARSER.NUM_OF_COLUMNS_BETWEEN_CORRESPONDING_IMAGES_IN_CSV;
                indexGlossAlpha = CST.CSVPARSER.GLOSS_ALPHA_COL + tabIndex * CST.CSVPARSER.NUM_OF_COLUMNS_BETWEEN_CORRESPONDING_IMAGES_IN_CSV;
                indexButton = CST.CSVPARSER.COLOR_BUTTON_BASE_IMAGE_COL + tabIndex * CST.CSVPARSER.NUM_OF_COLUMNS_BETWEEN_CORRESPONDING_IMAGES_IN_CSV;
                indexButtonGloss = CST.CSVPARSER.COLOR_BUTTON_GLOSS_IMAGE_COL + tabIndex * CST.CSVPARSER.NUM_OF_COLUMNS_BETWEEN_CORRESPONDING_IMAGES_IN_CSV;
                indexButtonGlossAlpha = CST.CSVPARSER.BUTTON_GLOSS_ALPHA_COL + tabIndex * CST.CSVPARSER.NUM_OF_COLUMNS_BETWEEN_CORRESPONDING_IMAGES_IN_CSV;
                if (index < entry.length) {
                    var clayRegion = entry[index];
                    var glossRegion = entry[indexGloss];
                    var glossAlpha = entry[indexGlossAlpha];
                    var clayButtonRegion = entry[indexButton];
                    var glossButtonRegion = entry[indexButtonGloss];
                    var glossButtonAlpha = entry[indexButtonGlossAlpha];
                    var material = new Material(clayRegion, glossRegion, glossAlpha, clayButtonRegion, glossButtonRegion, glossButtonAlpha);
                    materials.push(material);
                }
            }
            return materials;
        }
    };
    CsvParser.parseFeaturesCsv = function (csvPath, callback) {
        var result = [];
        Papa.parse(csvPath, {
            download: true,
            complete: function (results) {
                var i = 0;
                results.data.forEach(function (entry) {
                    if (i > 0 && i < results.data.length - 1) {
                        var feature = new Feature(entry[0], entry[1], entry[2], entry[8], entry[12], entry[13], entry[15], // colorlink v1
                        entry[16], // colorlink v2
                        entry[17], // color modifier
                        entry[9], i + 1, // set index as unique id
                        entry[18] // specify parent id
                        );
                        result.push(feature);
                    }
                    i++;
                });
                callback(result);
            }
        });
    };
    CsvParser.parseColorCsv = function (csvPath, callback) {
        var result = [];
        Papa.parse(csvPath, {
            download: true,
            complete: function (results) {
                var i = 0;
                results.data.forEach(function (entry) {
                    if (i < results.data.length - 1) {
                        var colorsRange = new ColorsRange(entry[0], entry[1], [], [], []);
                        for (var k = 1; k < CST.CSVPARSER.NUM_OF_PALLETES; k++) {
                            colorsRange.red.push(+entry[CST.CSVPARSER.OFFSET_IN_COLORS_CSV * k - 1]);
                            colorsRange.green.push(+entry[CST.CSVPARSER.OFFSET_IN_COLORS_CSV * k]);
                            colorsRange.blue.push(+entry[CST.CSVPARSER.OFFSET_IN_COLORS_CSV * k + 1]);
                        }
                        result.push(colorsRange);
                    }
                    i++;
                });
                callback(result);
            }
        });
    };
    return CsvParser;
}());
function checkIfContainsBodypart(source, taget) {
    var result = false;
    source.forEach(function (element) {
        if (element.modelPart.partName == taget.modelPart.partName)
            result = true;
    });
    return result;
}
var SlotItem = /** @class */ (function () {
    function SlotItem(scene, width, height, scrollMode, model, jsonData, sloteItemInfos) {
        this.scene = scene;
        this.model = model;
        this.scrollMode = scrollMode;
        this.stack = new Phaser.GameObjects.Container(scene);
        this.jsonData = jsonData;
        var sloteItemInfo = sloteItemInfos.sloteItemInfo, tabIndex = sloteItemInfos.tabIndex, shareFlag = sloteItemInfos.shareFlag;
        this.sloteItemInfo = sloteItemInfo;
        this.tabIndex = tabIndex;
        this.shareFlag = shareFlag;
        this.counter = { download: 0, like: 0 };
        this.initGroup(width, height, model);
    }
    ;
    SlotItem.prototype.initGroup = function (width, height, model) {
        this.shelfSprite = this.scene.add.image(0, 0, "shelf_adjusted");
        this.shelfSprite.setOrigin(0, 0);
        this.solidBackgroundSprite = this.scene.add.image(0, 0, "solid");
        this.solidBackgroundSprite.setOrigin(0, 0);
        this.solidBackgroundSprite.displayHeight = this.shelfSprite.displayHeight;
        this.solidBackgroundSprite.displayWidth = this.shelfSprite.displayWidth;
        // this.solidBackgroundSprite.tint = 0x000000;
        this.fadeBackgroundSprite = this.scene.add.image(0, 0, "Fade0");
        this.fadeBackgroundSprite.displayHeight = this.shelfSprite.displayHeight;
        this.fadeBackgroundSprite.displayWidth = this.shelfSprite.displayWidth;
        this.fadeBackgroundSprite.setOrigin(0, 0);
        this.fadeBackgroundSprite.setAlpha(0.5);
        // added by asset
        this.stack.add(this.solidBackgroundSprite);
        this.stack.add(this.fadeBackgroundSprite);
        this.stack.add(this.shelfSprite);
        this.cloudBoard = this.scene.add.container(0, 0);
        this.cloudBoard.depth = 499;
        this.likeButton = new CustomImageTextureButton(this.scene, this.cloudBoard, 0, 0, 120, 150, "Button_CloudLike", this.counter.like, function () {
        });
        this.likeButton.setTopLeftOrigin();
        this.likeButton.setHighLightTexture("Button_CloudLike_Red");
        this.downloadButton = new CustomImageTextureButton(this.scene, this.cloudBoard, 0, 200, 120, 150, "Button_CloudDownload", this.counter.download, function () {
        });
        this.downloadButton.setTopLeftOrigin();
        this.stack.add(this.cloudBoard);
    };
    SlotItem.prototype.initBodyPart = function () {
        var width = this.shelfSprite.displayWidth;
        var height = this.shelfSprite.displayHeight;
        //init bodypart manager
        this.bodyPartManager = new BaseBodyPartManager(this.scene, this, this.scene.modelParts, width, height);
        this.bodyPartManager.setAtlasFront(this.model.modelName + "-front");
        this.bodyParts = this.bodyPartManager.createBodyParts(this.scene.firstFrame);
        this.bodyPartManager.setUtmostSprites();
        this.savedModel = new SavedModel(this.jsonData);
        this.bodyPartManager.setSavedProperties(this.savedModel);
    };
    SlotItem.prototype.getDisplayWidth = function () {
        return this.shelfSprite.displayWidth * this.stack.scale;
    };
    SlotItem.prototype.getDisplayHeight = function () {
        return this.shelfSprite.displayHeight * this.stack.scale;
    };
    SlotItem.prototype.setDisplaySize = function (displayWidth, displayHeight, visibility) {
        if (visibility === void 0) { visibility = true; }
        var scaleX = displayWidth / this.shelfSprite.displayWidth;
        var scaleY = displayHeight / this.shelfSprite.displayHeight;
        this.shelfSprite.setScale(scaleX, scaleY);
        this.solidBackgroundSprite.displayWidth = this.shelfSprite.displayWidth;
        this.solidBackgroundSprite.displayHeight = this.shelfSprite.displayHeight;
        this.fadeBackgroundSprite.displayWidth = this.shelfSprite.displayWidth;
        this.fadeBackgroundSprite.displayHeight = this.shelfSprite.displayHeight;
        // added by asset
        // this.cloudBoard.setVisible(false);
        this.likeButton.setVisible(visibility);
        this.downloadButton.setVisible(visibility);
        this.cloudBoard.setScale(0.3);
        this.cloudBoard.setPosition(displayWidth * 0.7, displayHeight * 0.2);
        this.cloudBoard.setAlpha(.5);
    };
    SlotItem.prototype.setModel = function (model) {
        this.model = model;
    };
    SlotItem.prototype.setFadeTexture = function (fadeTexture) {
        this.fadeBackgroundSprite.setTexture(fadeTexture);
    };
    SlotItem.prototype.updateData = function (savedModel) {
        this.savedModel = savedModel;
        this.bodyPartManager.setSavedProperties(this.savedModel);
    };
    SlotItem.prototype.updateCloudButtons = function () {
        this.likeButton.updateState(this.counter.like);
        this.likeButton.setStatus(!!this.counter.like);
        this.downloadButton.updateState(this.counter.download);
    };
    return SlotItem;
}());
var UIGenerator = /** @class */ (function () {
    function UIGenerator(scene) {
        this.INITIAL_ALFA = 0.01;
        this.ALFA_INCREASE_VALUE = 0.030;
        this.ALFA_DECREASE_VALUE = 0.05;
        this.SECOND = 1000;
        this.ACTORS_MOVEMENT_VELOCITY = 15;
        this.COLOR_BUTTONS_NUMBER = 50;
        this.MATERIAL_BUTTONS_NUMBER = 20;
        this.NUMBER_OF_FRAME_SLIDER_POSITIONS = 4;
        this.FRAME_BUTTON_SLIDER_Y = 10;
        this.PREVIEW_BUTTON_WIDTH = 48;
        this.MOVE_RIGHT = "moveRight";
        this.MOVE_UP = "moveUp";
        this.TOPPER = "topper";
        this.BUY_IT_NOW = "Buy it Now";
        this.RATE_ME_COUNT = "RateMeCount";
        this.RATE_SHOWN = "RateShown";
        this.TRANSPARENT = 0;
        this.SEMITRANSPARENT = 0.5;
        this.OPAQUE = 1;
        this.NUMBER_OF_FADE_TEXTURES = 9;
        this.palleteNumber = 4;
        this.tMenubuttons = [];
        this.bodyPartChildButtons = []; // included into meterialGridTable container
        this.colorButtons = [];
        this.materialButtons = [];
        this.childBodyPartButtons = [];
        this.LAYOUT_CONSTANT = LayoutContants.getInstance();
        this.undoButtonAlpha = this.SEMITRANSPARENT;
        this.redoButtonAlpha = this.SEMITRANSPARENT;
        this.diceImageDeltaScale = 0;
        this.selectedSizeButtonIndex = 1;
        this.scene = scene;
        this.screenWidth = +this.scene.game.config.width;
        this.screenHeight = +this.scene.game.config.height;
        this.solidGreySprite = this.scene.add.sprite(0, this.screenHeight, "solid");
        this.solidGreySprite.displayWidth = this.screenWidth;
        this.solidGreySprite.displayHeight = this.screenHeight;
        this.solidGreySprite.setOrigin(0, 1);
        this.leftMovementContainer = this.scene.add.container(0, 0).setDepth(10);
        this.colorSelectorContainer = this.scene.add.container(0, 0);
        this.colorMode = CST.COLOR_MODE.DEFAULT;
        this.initBackgroundTextures();
        // this.initLoadingImage();
        // this.initRateTextures();
        this.colorButtons = [];
        this.materialButtons = [];
        this.framesSliderCoordinates = new Array();
        this.userActionsSequence = new Array();
        this.texts = new Array();
    }
    UIGenerator.prototype.initBackgroundTextures = function () {
        this.backgroundTexture = "EditBox_Model_1";
        this.backgroundSprite = new BodySprite(this.scene, 0, this.LAYOUT_CONSTANT.SCREEN_HEIGHT, "EditBox_Model_1");
        this.backgroundSprite.displayHeight = this.LAYOUT_CONSTANT.SCREEN_HEIGHT;
        this.backgroundSprite.displayWidth = this.screenWidth - 2.85 * this.LAYOUT_CONSTANT.BUTTONS_SIZE;
        this.backgroundSprite.setOrigin(0, 1);
        this.backgroundSprite.setFlashEnable(false);
        this.scene.sys.displayList.add(this.backgroundSprite);
        this.scene.sys.updateList.add(this.backgroundSprite);
        this.fadeSprite = this.scene.add.image(0, 0, "Fade0");
        this.fadeSprite.displayHeight = this.screenHeight;
        this.fadeSprite.displayWidth = this.screenWidth;
        this.fadeSprite.setOrigin(0, 0);
        this.fadeSprite.setAlpha(0.5);
        this.touchZone = this.scene.add.image(0, this.LAYOUT_CONSTANT.BUTTONS_SIZE, "Fade0");
        this.touchZone.displayHeight = this.screenHeight - 2 * this.LAYOUT_CONSTANT.BUTTONS_SIZE;
        this.touchZone.displayWidth = this.screenWidth;
        this.touchZone.setOrigin(0, 0);
        this.backgroundBoxTopper = this.scene.add.image(0, this.screenHeight, "EditBox_ModelMask");
        this.backgroundBoxTopper.setDepth(1);
        this.backgroundBoxTopper.setOrigin(0, 1);
        this.backgroundBoxTopper.displayWidth = this.screenWidth - 2.85 * this.LAYOUT_CONSTANT.BUTTONS_SIZE;
        this.backgroundBoxTopper.displayHeight = this.LAYOUT_CONSTANT.SCREEN_HEIGHT;
        //this.touchZoneCamera = this.scene.cameras.add(0,  this.LAYOUT_CONSTANT.BUTTONS_SIZE, this.backgroundSprite.displayHeight, this.backgroundSprite.displayWidth );
        //this.touchZoneCamera.setViewport();
        //this.touchZoneCamera.setBounds(0,  this.LAYOUT_CONSTANT.BUTTONS_SIZE, this.backgroundSprite.displayWidth, this.backgroundSprite.displayHeight)
        //this.touchZoneCamera.setZoom(1.5)
        this.fadeTextures = [];
        for (var i = 0; i < this.NUMBER_OF_FADE_TEXTURES; i++) {
            var texture = "Fade" + i;
            this.fadeTextures.push(texture);
        }
    };
    UIGenerator.prototype.getSelectedColorButton = function () {
        return this.selectedColorButton;
    };
    UIGenerator.prototype.createUserAction = function () {
        var pnToS = this.palleteNumber;
        if (this.colorMode == CST.COLOR_MODE.CUSTOME) {
            pnToS = this.colorPalletePanel.getPalleteNumber();
        }
        var colorPositionPair = new ColorPositionPair(pnToS, this.selectedColorButton.buttonIndex);
        var selectedBodyPart = this.scene.bodyPartsManager.selectedBodyPart;
        // let childColorLinks = [];
        var userAction = null;
        if (selectedBodyPart.isParent()) {
            // selectedBodyPart.getChildBodyParts().forEach(element => {
            //     //if(element.depend == true)
            //         childColorLinks.push(element.getPartId());
            // });
            userAction = new UserAction(this, selectedBodyPart.bodyPartIndex, colorPositionPair, selectedBodyPart.selectedTabIndex);
        }
        else {
            userAction = new UserAction(this, selectedBodyPart.bodyPartIndex, colorPositionPair, selectedBodyPart.getParnetBodyPart().selectedTabIndex);
        }
        this.addToUserActionsSequence(userAction);
    };
    UIGenerator.prototype.addToUserActionsSequence = function (userAction) {
        this.undoButton.alpha = this.OPAQUE;
        this.userActionsSequence = this.userActionsSequence.slice(0, this.userActionsIndex + 1);
        this.userActionsSequence.push(userAction);
        this.userActionsIndex++;
    };
    UIGenerator.prototype.undo = function () {
        this.scene.setUndoRedoCooldowned(true);
        if (this.userActionsIndex > 0) {
            this.userActionsIndex--;
            var userAction = this.userActionsSequence[this.userActionsIndex];
            userAction.performAction();
        }
        if (this.userActionsIndex == 0) {
            this.undoButton.alpha = this.SEMITRANSPARENT;
            this.undoButtonAlpha = this.SEMITRANSPARENT;
        }
        if (this.redoButton.alpha < 1 && this.userActionsSequence.length > 1) {
            this.redoButton.alpha = this.OPAQUE;
            this.redoButtonAlpha = this.OPAQUE;
        }
    };
    UIGenerator.prototype.redo = function () {
        this.scene.setUndoRedoCooldowned(true);
        if (this.userActionsIndex < this.userActionsSequence.length - 1) {
            this.userActionsIndex++;
            var userAction = this.userActionsSequence[this.userActionsIndex];
            userAction.performAction();
        }
        if (this.userActionsIndex == this.userActionsSequence.length - 1) {
            this.redoButton.alpha = this.SEMITRANSPARENT;
            this.redoButtonAlpha = this.SEMITRANSPARENT;
        }
        if (this.undoButton.alpha < 1 && this.userActionsSequence.length > 1) {
            this.undoButton.alpha = this.OPAQUE;
            this.undoButtonAlpha = this.OPAQUE;
        }
    };
    UIGenerator.prototype.setColorSelection = function (buttonNumber) {
        this.selectedColorButton = this.colorButtons[buttonNumber];
        this.setColorSlidersPosition();
        this.setColorFromSliderPosition(this.palleteNumber);
        this.setColorSelectors();
    };
    UIGenerator.prototype.setColorSelectors = function () {
        this.scene.setColorSelectorsAlfa(this.INITIAL_ALFA);
        this.currentlySelectedColorStack = this.selectedColorButton.stack;
        this.currentlySelectedColorStack.add(this.colorSelectorContainer);
        this.colorSelectorContainer.setVisible(true);
    };
    UIGenerator.prototype.setColorSlidersPosition = function () {
        this.colorAdjustButton.x = this.screenWidth - this.LAYOUT_CONSTANT.FLOAT_COLOR_BUTTON_POSITIONS[this.palleteNumber] / this.LAYOUT_CONSTANT.SCREEN_SIZE_COEF;
        this.colorAdjustDot.x = this.screenWidth - this.LAYOUT_CONSTANT.FLOAT_COLOR_BUTTON_POSITIONS[this.palleteNumber] / this.LAYOUT_CONSTANT.SCREEN_SIZE_COEF;
    };
    UIGenerator.prototype.setColorFromSliderPosition = function (colorSliderPosition) {
        this.setColorsToButtons();
        this.selectedColorButton.setMainSelectorColor();
        if (this.colorMode == CST.COLOR_MODE.CUSTOME) {
            this.scene.bodyPartsManager.setCurrentBodyPartColor(this.colorPalletePanel.getPalleteNumber(), this.selectedColorButton);
        }
        else {
            this.scene.bodyPartsManager.setCurrentBodyPartColor(this.palleteNumber, this.selectedColorButton);
        }
    };
    UIGenerator.prototype.setColorModelFromSelectedBodyPart = function () {
        var newPalletNumber = this.scene.bodyPartsManager.selectedBodyPart.colorModeNumber;
        if (newPalletNumber > 9) {
            this.colorPalletePanel.setPalleteNumber(newPalletNumber);
            this.setColorsToButtons();
            this.setColorMode(CST.COLOR_MODE.CUSTOME);
        }
        else {
            this.setColorMode(CST.COLOR_MODE.DEFAULT);
        }
        // var selectedButton = this.scene.bodyPartsManager.selectedBodyPart.colorButton
        // selectedButton.performSelection()
    };
    UIGenerator.prototype.setColorsToButtons = function () {
        var startPos = 0;
        var newPalletNumber = this.scene.bodyPartsManager.selectedBodyPart.palleteNumber;
        if (this.colorMode == CST.COLOR_MODE.CUSTOME) {
            newPalletNumber = this.colorPalletePanel.getPalleteNumber();
            startPos = this.colorButtons.length - 16;
        }
        else {
            this.palleteNumber = newPalletNumber;
        }
        for (var i = startPos; i < startPos + this.colorButtons.length; i++) {
            var index = i % this.colorButtons.length;
            var color = this.scene.getColorFromRange(index, newPalletNumber);
            this.colorButtons[i - startPos].setColor(color, newPalletNumber);
            this.colorButtons[i - startPos].makeMark();
        }
    };
    UIGenerator.prototype.setSelectedTMenuButton = function (selectedTMenuButton) {
        if (this.selectedTMenuButton)
            this.selectedTMenuButton.resetColorForBlinking();
        this.selectedTMenuButton = selectedTMenuButton;
        this.setTMenuSelector();
    };
    UIGenerator.prototype.setSelectedChildBodyPartButton = function (selectedChildBodyPartButton) {
        if (this.selectedChildBodyPartButton)
            this.selectedChildBodyPartButton.resetColorForBlinking();
        this.selectedChildBodyPartButton = selectedChildBodyPartButton;
        this.setChildBodyPartSelector();
    };
    UIGenerator.prototype.setTMenuSelector = function () {
        if (this.currentlySelectedTMenuStack != null) {
            //this.currentlySelectedTMenuStack.remove(this.tMenuSelector);
            this.removeAllBodyPartSelector();
        }
        this.currentlySelectedTMenuStack = this.selectedTMenuButton.stack;
        this.currentlySelectedTMenuStack.add(this.tMenuSelector);
        // this.colorsTable.scrollToTop(true);
    };
    //mark
    UIGenerator.prototype.setChildBodyPartSelector = function () {
        if (this.currentlySelectedChildBodyPartStack != null) {
            this.currentlySelectedChildBodyPartStack.remove(this.childBodyPartSelector);
        }
        this.currentlySelectedChildBodyPartStack = this.selectedChildBodyPartButton.stack;
        this.currentlySelectedChildBodyPartStack.add(this.childBodyPartSelector);
    };
    UIGenerator.prototype.removeAllBodyPartSelector = function () {
        if (this.currentlySelectedChildBodyPartStack != null)
            this.currentlySelectedChildBodyPartStack.remove(this.childBodyPartSelector);
        if (this.currentlySelectedTMenuStack != null)
            this.currentlySelectedTMenuStack.remove(this.tMenuSelector);
    };
    UIGenerator.prototype.hideFloatColorSlider = function () {
        this.floatColorSlider.setVisible(false);
        this.floatColorSliderBG.setVisible(false);
        this.floatColorSliderButton.setVisible(false);
    };
    UIGenerator.prototype.setMaterialButtons = function (atlas) {
        var _this = this;
        /** Begin - Setting Materials */
        var tabRegions = this.scene.bodyPartsManager.selectedBodyPart.modelPart.getTabRegions();
        var newItems = [];
        for (var i = 0; i < this.MATERIAL_BUTTONS_NUMBER; i++) {
            if (i >= tabRegions.length) {
                this.materialButtons[i].clearStack();
                continue;
            }
            var tabRegion = tabRegions[i];
            var materialButton = this.materialButtons[i];
            materialButton.setImage(tabRegion, atlas, i);
            newItems.push(materialButton);
        }
        /** End - Setting Materials */
        /** Begin - Setting Child BodyParts */
        var newChildItems = new Array(); // childrens in bodyPart
        // generate buttons for bodypart childrens
        var bodyParts = this.scene.bodyPartsManager.bodyParts;
        this.childBodyPartButtons = [];
        bodyParts.forEach(function (element) {
            if (element.modelPart.feature.isChildren &&
                element.modelPart.feature.parent_id == _this.scene.bodyPartsManager.selectedBodyPart.modelPart.feature.part_id) {
                var childButton = element.childBodyPartButton;
                newChildItems.push(childButton);
                _this.childBodyPartButtons.push(childButton);
            }
        });
        this.materialTable.setItems(newItems, newChildItems);
        /** End - Setting Child BodyParts */
    };
    UIGenerator.prototype.createTables = function () {
        this.createColorTable();
        this.createMaterialTable();
        this.createBodyPartTable();
        this.createUndoAndRedoButtons();
    };
    UIGenerator.prototype.createUndoAndRedoButtons = function () {
        var _this = this;
        this.undoButton = new CustomButton(this.scene, 40 / this.LAYOUT_CONSTANT.ORIGIN_SCREEN_SIZE_COEF, this.screenHeight - 1.4 * this.LAYOUT_CONSTANT.BUTTONS_SIZE_BASE / this.LAYOUT_CONSTANT.SCREEN_HEIGHT_COEF, "Button_Undo", function () {
            _this.undo();
        }, true);
        this.undoButton.setOrigin(0, 1);
        this.undoButton.setScale(this.LAYOUT_CONSTANT.SCALE);
        this.undoButton.alpha = this.SEMITRANSPARENT;
        this.redoButton = new CustomButton(this.scene, this.colorsTable.getX() - 40 / this.LAYOUT_CONSTANT.ORIGIN_SCREEN_SIZE_COEF, this.screenHeight - 1.4 * this.LAYOUT_CONSTANT.BUTTONS_SIZE_BASE / this.LAYOUT_CONSTANT.SCREEN_HEIGHT_COEF, "Button_redo", function () {
            _this.redo();
        }, true);
        this.redoButton.setOrigin(1, 1);
        this.redoButton.setScale(this.LAYOUT_CONSTANT.SCALE);
        this.redoButton.alpha = this.SEMITRANSPARENT;
    };
    UIGenerator.prototype.createBodyPartTable = function () {
        var context = this.scene;
        var self = this;
        var scrollMode = 1; // 0:vertical, 1:horizontal
        this.bodypartTable = new TmenuGridTable(this.scene, {
            x: this.LAYOUT_CONSTANT.BUTTONS_SIZE,
            y: 0,
            width: this.screenWidth - this.LAYOUT_CONSTANT.BUTTONS_SIZE,
            height: this.LAYOUT_CONSTANT.BUTTONS_SIZE * 1.2,
            background: context.add.image(0, 0, "TMenu_Back"),
            scrollMode: scrollMode,
            table: {
                cellWidth: (scrollMode === 0) ? undefined : this.LAYOUT_CONSTANT.BUTTONS_SIZE * 0.8,
                cellHeight: (scrollMode === 0) ? 300 : undefined,
                columns: 1,
            },
        });
        // event when clicking tmenubutton
        this.bodypartTable.table.on("cell.click", function (cellIndex) {
            self.tMenubuttons[cellIndex].onTapButton();
        });
    };
    UIGenerator.prototype.listenBodyPartTouchEvent = function (cellIndex, childFlag) {
        if (childFlag === void 0) { childFlag = false; }
        if (childFlag) {
            var childbodyParts = this.scene.bodyPartsManager.bodyParts.filter(function (it) { return it.modelPart.feature.part_id == cellIndex; });
            if (childbodyParts.length > 0) {
                var childBP = childbodyParts[0];
                var selectedBP = this.scene.bodyPartsManager.selectedBodyPart;
                if (childBP.hasParent(selectedBP) || childBP.hasSibling(selectedBP)) {
                    if (childBP.modelPart.feature.isChildren) {
                        return childBP.childBodyPartButton.onTapButton();
                    }
                }
                else {
                    var parentBodyPart = childBP.getParnetBodyPart();
                    parentBodyPart.tMenuButton.performSelection();
                    return childBP.childBodyPartButton.onTapButton();
                }
            }
        }
        var tmenubtn = this.tMenubuttons[cellIndex];
        if (tmenubtn && !tmenubtn.bodyPart.modelPart.feature.isChildren) {
            this.tMenubuttons[cellIndex].onTapButton();
        }
    };
    /**
     * generate Top menu buttons and child buttons
     */
    UIGenerator.prototype.populateBodyPartTable = function () {
        var _this = this;
        var context = this.scene;
        var bodyParts = context.bodyPartsManager.bodyParts;
        this.tMenubuttons = new Array();
        bodyParts.forEach(function (element) {
            if (element.tMenuButton) {
                if (!element.modelPart.feature.isChildren) {
                    //this.tMenubuttons[+element.modelPart.tMenuOrder - 1] = element.tMenuButton;
                    _this.tMenubuttons[element.modelPart.tMenuOrder] = element.tMenuButton;
                }
            }
        });
        this.bodypartTable.setItems(this.tMenubuttons);
        this.tMenubuttons.forEach(function (element) {
            if (element.bodyPart.modelPart.partName == "Background") {
                //
                _this.backgroundSprite.linkTmenuButtonIndex(element.bodyPart.modelPart.tMenuOrder);
            }
        });
    };
    UIGenerator.prototype.createColorTable = function () {
        var _this = this;
        var scrollMode = 0; // 0:vertical, 1:horizontal
        var context = this.scene;
        this.colorsTable = new ColorGridTable(this.scene, {
            x: this.screenWidth - this.LAYOUT_CONSTANT.BUTTONS_SIZE * 2.7,
            y: this.LAYOUT_CONSTANT.BUTTONS_SIZE * 1.2 + this.colorSwitchBoard.getElementHeight(),
            width: 2 * this.LAYOUT_CONSTANT.BUTTONS_SIZE,
            height: this.screenHeight - this.LAYOUT_CONSTANT.BUTTONS_SIZE * 2 * 1.2 - this.colorSwitchBoard.getElementHeight(),
            background: context.add.image(0, 0, "solid_grey_8"),
            scrollMode: scrollMode,
            table: {
                cellWidth: (scrollMode === 0) ? undefined : this.LAYOUT_CONSTANT.BUTTONS_SIZE,
                cellHeight: (scrollMode === 0) ? this.LAYOUT_CONSTANT.BUTTONS_SIZE : undefined,
                columns: 2,
            },
        });
        this.colorsTable.table.on("cell.click", function (cellIndex) {
            if (!_this.floatColorSliderBG.visible) {
                _this.colorButtons[cellIndex].onTapButton();
            }
        });
        this.colorsTable.table.on("cell.press", function (cellIndex) {
            _this.hideFloatColorSlider();
            _this.setFloatColorSlider(cellIndex);
            // this.setFloatColorTouchDownInputXOffset();
        });
        this.colorsTable.table.on("cell.pressup", function (cellIndex) {
            _this.hideFloatColorSlider();
        });
        // it causes this event when clicking preview button - Modified by SY
        this.scene.events.on("prevbutton.pontermove", function (moveDis, islefttrend) {
        });
    };
    UIGenerator.prototype.populateColorsTable = function () {
        var context = this.scene;
        this.colorButtons = new Array();
        for (var i = 0; i < this.COLOR_BUTTONS_NUMBER; i++) {
            var colorButton = new ColorButton(context, i);
            colorButton.setImagesStack();
            this.colorButtons.push(colorButton);
            if (i == 0) {
                colorButton.setSelected(true);
            }
        }
        this.colorsTable.setItems(this.colorButtons);
    };
    UIGenerator.prototype.createMaterialTable = function () {
        var _this = this;
        var context = this.scene;
        var scrollMode = 0;
        this.materialTable = new MaterialGridTable(this.scene, {
            x: this.screenWidth - 0.92 * this.LAYOUT_CONSTANT.BUTTONS_SIZE,
            y: this.LAYOUT_CONSTANT.BUTTONS_SIZE * 1.2,
            width: this.LAYOUT_CONSTANT.BUTTONS_SIZE,
            height: this.screenHeight - this.LAYOUT_CONSTANT.BUTTONS_SIZE * 2 * 1.2,
            background: context.add.image(0, 0, "solid_grey_8"),
            scrollMode: scrollMode,
            table: {
                cellWidth: (scrollMode === 0) ? undefined : this.LAYOUT_CONSTANT.BUTTONS_SIZE,
                cellHeight: (scrollMode === 0) ? this.LAYOUT_CONSTANT.BUTTONS_SIZE : undefined,
                columns: 1,
            },
        });
        this.materialTable.table.on("cell.click", function (cellIndex) {
            _this.materialButtons[cellIndex].onTapButton();
        });
        this.materialTable.table.on("cell.child.click", function (cellIndex) {
            //console.log(this.childBodyPartButtons);
            var res = _this.childBodyPartButtons.filter(function (it) { return it.bodyPart.modelPart.feature.part_id == cellIndex; });
            if (res.length > 0) {
                res[0].onTapButton();
            }
        });
    };
    UIGenerator.prototype.populateMaterialTable = function () {
        var materialButtons = new Array();
        // generate buttons for material
        for (var i = 0; i < this.MATERIAL_BUTTONS_NUMBER; i++) {
            var materialButton = new MaterialButton(this.scene, this);
            materialButtons[i] = materialButton;
        }
        this.materialButtons = materialButtons;
        this.materialTable.setItems(materialButtons);
    };
    UIGenerator.prototype.setActorsLocation = function () {
    };
    UIGenerator.prototype.createImagesFromAssets = function () {
        var _this = this;
        this.colorTableBackgroundImage = this.scene.add.image(0, 0, "Menu_SelectsBG");
        this.colorTableBackgroundImage.setDepth(1);
        this.colorTableBackgroundImage.displayWidth = 2.85 * this.LAYOUT_CONSTANT.BUTTONS_SIZE;
        this.colorTableBackgroundImage.displayHeight = this.screenHeight;
        this.colorTableBackgroundImage.setPosition(this.screenWidth - this.colorTableBackgroundImage.displayWidth / 2, this.colorTableBackgroundImage.displayHeight / 2);
        this.tMenuSelector = new Phaser.GameObjects.Image(this.scene, 0, 0, "TMenu_Selection");
        this.tMenuSelector.displayWidth = this.LAYOUT_CONSTANT.BODY_PART_BUTTON_WIDTH / this.LAYOUT_CONSTANT.SCREEN_SIZE_COEF;
        this.tMenuSelector.displayHeight = this.LAYOUT_CONSTANT.BUTTONS_SIZE * 1.2;
        this.childBodyPartSelector = new Phaser.GameObjects.Image(this.scene, 0, 0, "TMenu_Selection");
        this.childBodyPartSelector.displayWidth = this.LAYOUT_CONSTANT.BODY_PART_BUTTON_WIDTH / this.LAYOUT_CONSTANT.SCREEN_SIZE_COEF;
        this.childBodyPartSelector.displayHeight = this.LAYOUT_CONSTANT.BUTTONS_SIZE * 1.2;
        this.smallColorSelector = new Phaser.GameObjects.Image(this.scene, 0, 0, "SelectHoop");
        this.mediumColorSelector = new Phaser.GameObjects.Image(this.scene, 0, 0, "SelectHoop");
        this.mainColorSelector = new Phaser.GameObjects.Image(this.scene, 0, 0, "SelectHoop");
        this.largeColorSelector = new Phaser.GameObjects.Image(this.scene, 0, 0, "SelectHoop");
        this.smallColorSelector.setScale(this.LAYOUT_CONSTANT.SMALL_COLOR_SELECTION_SCALE);
        this.mediumColorSelector.setScale(this.LAYOUT_CONSTANT.MEDIUM_COLOR_SELECTION_SCALE);
        this.mainColorSelector.setScale(this.LAYOUT_CONSTANT.MAIN_COLOR_SELECTION_SCALE);
        this.largeColorSelector.setScale(this.LAYOUT_CONSTANT.LARGE_COLOR_SELECTION_SCALE);
        this.colorSelectorContainer.add(this.smallColorSelector);
        this.colorSelectorContainer.add(this.mediumColorSelector);
        this.colorSelectorContainer.add(this.mainColorSelector);
        this.colorSelectorContainer.add(this.largeColorSelector);
        this.colorAdjustFrame = this.scene.add.image(0, 0, "BG_ValueNew");
        this.colorAdjustFrame.setScale(this.LAYOUT_CONSTANT.BUTTONS_SIZE * 3 / this.colorAdjustFrame.displayWidth);
        this.colorAdjustFrame.setOrigin(1, 1);
        this.colorAdjustFrame.setPosition(this.screenWidth, this.screenHeight);
        this.colorAdjustFrame.depth = 10;
        this.colorAdjustContainer = this.scene.add.container(0, 0).setDepth(25);
        this.colorAdjustBG = this.scene.add.image(300, 300, "FiguromoBox_BG_extension");
        this.colorAdjustBG.setOrigin(1, 1);
        this.colorAdjustBG.displayWidth = 2.8 * this.LAYOUT_CONSTANT.BUTTONS_SIZE;
        this.colorAdjustBG.displayHeight = this.LAYOUT_CONSTANT.BUTTONS_SIZE;
        this.colorAdjustBG.setPosition(this.screenWidth, this.screenHeight);
        this.colorAdjustBG.depth = 9;
        this.colorAdjustGradient = this.scene.add.image(300, 300, "BG_ValueNew_Gradient");
        this.colorAdjustGradient.setScale(this.LAYOUT_CONSTANT.BUTTONS_SIZE * 3 / this.colorAdjustGradient.displayWidth);
        this.colorAdjustGradient.setOrigin(1, 1);
        this.colorAdjustGradient.setPosition(this.screenWidth, this.screenHeight);
        this.colorAdjustGradient.depth = 9;
        this.colorAdjustButton = this.scene.add.image(300, 300, "Button_ValueNew");
        this.colorAdjustButton.setScale(this.LAYOUT_CONSTANT.COLOR_ADJUST_BUTTON_SCALE * 0.8);
        this.colorAdjustButton.setPosition(this.screenWidth - 2.8 * this.LAYOUT_CONSTANT.BUTTONS_SIZE / 2, this.screenHeight - this.LAYOUT_CONSTANT.BUTTONS_SIZE / 2 - 5 / this.LAYOUT_CONSTANT.SCREEN_HEIGHT_COEF);
        this.colorAdjustButton.setDepth(11);
        this.colorAdjustDot = this.scene.add.image(300, 300, "ColorAdjust_Button_Dot");
        this.colorAdjustDot.setScale(this.LAYOUT_CONSTANT.COLOR_ADJUST_DOT_SCALE);
        this.colorAdjustDot.setPosition(this.screenWidth - 2.8 * (this.LAYOUT_CONSTANT.BUTTONS_SIZE / 2) / this.LAYOUT_CONSTANT.SCREEN_SIZE_COEF, this.screenHeight - this.LAYOUT_CONSTANT.BUTTONS_SIZE + 6 / this.LAYOUT_CONSTANT.SCREEN_HEIGHT_COEF);
        this.colorAdjustDot.setDepth(9);
        this.colorAdjustContainer.add(this.colorAdjustBG);
        this.colorAdjustContainer.add(this.colorAdjustGradient);
        this.colorAdjustContainer.add(this.colorAdjustDot);
        this.colorAdjustContainer.add(this.colorAdjustFrame);
        this.colorAdjustContainer.add(this.colorAdjustButton);
        // this.leftMovementContainer.add(this.colorAdjustContainer)
        this.colorPalletePanel = new ColorPalletePanel(this.scene, function (index) {
            _this.setColorsToButtons();
            _this.colorsTable.scrollToTop(true);
            _this.resetMarkInColorButtons();
        });
        this.colorSwitchBoard = new ColorSwitchBoard(this.scene, 0, 0, CST.COLOR_MODE.DEFAULT, this.scene.model.modeOff, function (colormode) {
            _this.setColorMode(colormode);
        });
        this.leftMovementContainer.add(this.colorSwitchBoard.stack);
        this.buttonPreview = this.scene.add.image(300, 300, "Button_NewPreviewTab");
        this.buttonPreview.setDepth(1);
        this.buttonPreview.setScale(this.LAYOUT_CONSTANT.SCALE);
        this.buttonPreview.setPosition(this.screenWidth - 2.85 * this.LAYOUT_CONSTANT.BUTTONS_SIZE, this.LAYOUT_CONSTANT.BUTTONS_SIZE * 1.2);
        this.buttonPreview.setOrigin(1, 0);
        this.floatColorSliderBG = this.scene.add.image(0, 0, "FloatSlider_BGTrans");
        this.floatColorSliderBG.setScale(this.LAYOUT_CONSTANT.SCALE);
        this.floatColorSliderBG.depth = 100;
        this.floatColorSlider = this.scene.add.image(0, 0, "FloatSlider_Strip2");
        this.floatColorSlider.setScale(this.LAYOUT_CONSTANT.SCALE);
        this.floatColorSlider.depth = 100;
        this.floatColorSliderButton = this.scene.add.image(0, 0, "ColorAdjust_Button");
        this.floatColorSliderButton.setScale(this.LAYOUT_CONSTANT.SCALE);
        this.floatColorSliderButton.depth = 100;
        this.tabSelector = new Phaser.GameObjects.Image(this.scene, 0, 0, "TMenu_Selection");
        this.tabSelector.displayWidth = this.LAYOUT_CONSTANT.BODY_PART_BUTTON_WIDTH / this.LAYOUT_CONSTANT.SCREEN_SIZE_COEF;
        this.tabSelector.displayHeight = this.LAYOUT_CONSTANT.BUTTONS_SIZE * 1.2;
        this.childBodyPartSelector = new Phaser.GameObjects.Image(this.scene, 0, 0, "TMenu_Selection");
        this.childBodyPartSelector.displayWidth = this.LAYOUT_CONSTANT.BODY_PART_BUTTON_WIDTH / this.LAYOUT_CONSTANT.SCREEN_SIZE_COEF;
        this.childBodyPartSelector.displayHeight = this.LAYOUT_CONSTANT.BUTTONS_SIZE * 1.2;
        this.goToSaveScreen = this.scene.add.image(300, 300, "TMenu_SaveON").setVisible(false);
        this.bottomPlaceHolder = this.scene.add.image(200, 200, "FiguromoBox_BG_extension").setVisible(false);
        this.goToEditBox = new CustomButton(this.scene, this.LAYOUT_CONSTANT.SCREEN_WIDTH - 2.75 * this.LAYOUT_CONSTANT.BUTTONS_SIZE, this.LAYOUT_CONSTANT.BUTTONS_SIZE / 2 + 5, "Button_BackNEW", function () {
            _this.resetElementPositionByScreenMode(CST.LAYOUT.EDIT_MODE);
        });
        this.goToEditBox.depth = 10;
        this.goToEditBox.setScale(this.LAYOUT_CONSTANT.SCALE);
        this.goToEditBox.setSoundType(CST.SOUND.CANCEL_SOUND);
        // creating model save button
        this.saveButton = new CustomButton(this.scene, this.LAYOUT_CONSTANT.SCREEN_WIDTH / 2, this.LAYOUT_CONSTANT.BUTTONS_SIZE / 2 + 5, "Button_SaveNEW", function () {
            _this.saveCurrentModel();
        });
        this.saveButton.depth = 10;
        this.saveButton.setScale(this.LAYOUT_CONSTANT.SCALE);
        this.saveButton.setSoundType(CST.SOUND.CANCEL_SOUND);
        this.shelfSprite = this.scene.add.sprite(0, 0, "shelf_adjusted");
        this.shelfSprite.setOrigin(0, 0);
        this.shelfSprite.displayHeight = this.screenHeight;
        this.shelfSprite.displayWidth = this.screenWidth;
        this.shopBackgroundSprite = this.scene.add.sprite(0, this.screenHeight / 2 + 0.5 * this.LAYOUT_CONSTANT.BUTTONS_SIZE, "BoxBGFade").setVisible(false);
        this.shopBackgroundSprite.setOrigin(0, 1);
        this.shopBackgroundSprite.setScale(this.screenWidth / this.shopBackgroundSprite.displayWidth, this.screenHeight / this.shopBackgroundSprite.displayHeight);
        this.shopBoxSprite = this.scene.add.sprite(0, this.screenHeight / 2 + 0.9 * this.LAYOUT_CONSTANT.BUTTONS_SIZE, "BoxFull").setVisible(false);
        this.shopBoxSprite.setOrigin(0.3, 1);
        this.shopBoxSprite.setScale(this.LAYOUT_CONSTANT.SCALE * 1.5);
        this.diceScreenTopBar = new DiceScreenTopBar(this.scene, 0, 0, "" + this.scene.model.shopText, "" + this.scene.model.shopPrice);
        this.extraSmallSizeButton = new CustomImageButton(this.scene, this.screenWidth - 1.2 * this.LAYOUT_CONSTANT.BUTTONS_SIZE, this.screenHeight / 2 - 1.5 * this.LAYOUT_CONSTANT.BUTTONS_SIZE, "Button_Size1", function () {
            _this.scene.playSound(CST.SOUND.COLOR_GENERAL_SOUND);
            _this.onTapBuyButton(0);
        });
        this.extraSmallSizeButton.setTitle(this.scene.model.getExtraSmallSizeInCmOfString());
        this.extraSmallSizeButton.setVisible(false);
        this.extraSmallSizeButton.setHighLightTexture("Button_Size1SEL");
        this.smallSizeButton = new CustomImageButton(this.scene, this.screenWidth - 1.2 * this.LAYOUT_CONSTANT.BUTTONS_SIZE, this.screenHeight / 2 - 0.5 * this.LAYOUT_CONSTANT.BUTTONS_SIZE, "Button_Size1", function () {
            _this.scene.playSound(CST.SOUND.COLOR_GENERAL_SOUND);
            _this.onTapBuyButton(1);
        });
        this.smallSizeButton.setTitle(this.scene.model.getSmallSizeInCmOfString());
        this.smallSizeButton.setVisible(false);
        this.smallSizeButton.setHighLightTexture("Button_Size1SEL");
        this.mediumSizeButton = new CustomImageButton(this.scene, this.screenWidth - 1.2 * this.LAYOUT_CONSTANT.BUTTONS_SIZE, this.screenHeight / 2 + 0.5 * this.LAYOUT_CONSTANT.BUTTONS_SIZE, "Button_Size1", function () {
            _this.scene.playSound(CST.SOUND.COLOR_GENERAL_SOUND);
            _this.onTapBuyButton(2);
        });
        this.mediumSizeButton.setTitle(this.scene.model.getMediumSizeInCmOfString());
        this.mediumSizeButton.setVisible(false);
        this.mediumSizeButton.setHighLightTexture("Button_Size1SEL");
        this.largeSizeButton = new CustomImageButton(this.scene, this.screenWidth - 1.2 * this.LAYOUT_CONSTANT.BUTTONS_SIZE, this.screenHeight / 2 + 1.5 * this.LAYOUT_CONSTANT.BUTTONS_SIZE, "Button_Size1", function () {
            _this.scene.playSound(CST.SOUND.COLOR_GENERAL_SOUND);
            _this.onTapBuyButton(3);
        });
        this.largeSizeButton.setTitle(this.scene.model.getLargeSizeInCmOfString());
        this.largeSizeButton.setVisible(false);
        this.largeSizeButton.setHighLightTexture("Button_Size1SEL");
        this.saveScreenUpperBackground = this.scene.add.image(100, 400, "NewSettings_BG1");
        this.diceImage = this.scene.add.image(0, this.screenHeight, "dice");
        this.diceImage.setOrigin(0, 1);
        this.diceImage.setScale(1 / this.LAYOUT_CONSTANT.SCREEN_HEIGHT_COEF);
        this.diceImage.depth = 100;
        //back button area
        this.topBarAreaContainer = this.scene.add.container(0, 0);
        this.backButtonBG = this.scene.add.image(this.LAYOUT_CONSTANT.BUTTONS_SIZE / 2, this.LAYOUT_CONSTANT.BUTTONS_SIZE * 1.2 / 2, "FiguromoBox_BG_extension");
        this.backButtonBG.displayHeight = this.LAYOUT_CONSTANT.BUTTONS_SIZE * 1.2;
        this.backButtonBG.displayWidth = this.LAYOUT_CONSTANT.BUTTONS_SIZE;
        this.backButton = new CustomButton(this.scene, this.LAYOUT_CONSTANT.BUTTONS_SIZE / 2, this.LAYOUT_CONSTANT.BUTTONS_SIZE * 1.2 / 2, "back_button", function () {
            _this.resetElementPositionByScreenMode(CST.LAYOUT.SAVE_PREVIEW_MODE);
        });
        this.backButton.setSoundType(CST.SOUND.CANCEL_SOUND);
        this.backButton.setScale(this.LAYOUT_CONSTANT.BUTTONS_SIZE / this.backButton.displayWidth);
        this.backButton.depth = 100;
        this.scene.sys.displayList.add(this.backButton);
        this.scene.sys.updateList.add(this.backButton);
        this.topBarAreaContainer.add(this.backButtonBG);
        // this.topBarAreaContainer.add(this.backButton);
        this.topBarAreaContainer.depth = 25;
        this.progressBar = new ProgressBar(this.scene, "Button_SlideGroove", 0x0bd447);
    };
    UIGenerator.prototype.setImagesVisibility = function () {
        // this.solidGreySprite.setAlpha(0);
        this.shelfSprite.setVisible(false);
        this.shopBackgroundSprite.setVisible(false);
        this.diceScreenTopBar.setVisible(false);
        // Label.LabelStyle labelStyle = new Label.LabelStyle();
        // labelStyle.font = new BitmapFont();
        // labelStyle.font.getData().setScale(lABEL_SCALE, lABEL_SCALE);
        // labelStyle.fontColor = Color.BLACK;
        this.saveScreenUpperBackground.setVisible(false);
        // this.colorPallete.setVisible(false);
        // this.buttonNext.setVisible(false);
        // this.buttonPrevious.setVisible(false);
        // this.modeSwitch_1.setVisible(false);
        this.saveButton.setVisible(false);
        this.goToEditBox.setVisible(false);
        this.diceScreenTopBar.setVisible(false);
        this.smallSizeButton.setVisible(false);
        this.extraSmallSizeButton.setVisible(false);
        this.mediumSizeButton.setVisible(false);
        this.largeSizeButton.setVisible(false);
        this.diceImage.setVisible(false);
        this.buttonPreview.setVisible(true);
        // if (!game.isFramesLoaded()) {
        //     this.frameSliderButton.setTouchable(Touchable.disabled);
        // }
        // this.frameSliderButton.setAlpha(1);
        // this.framesSliderGroove.setAlpha(1);
        // this.framesAutoOn.setAlpha(1);
        // this.framesAutoOff.setAlpha(1);
        // this.saveButton.setAlpha(1);
        // this.goToEditBox.setAlpha(1);
        // this.backgroundBoxTopper.setAlpha(1);
        this.hideFloatColorSlider();
    };
    UIGenerator.prototype.setBackground = function (backgroundTexture, fadeTexture) {
        this.fadeSprite.setTexture(fadeTexture);
    };
    UIGenerator.prototype.getBackgroundSprite = function () {
        return this.backgroundSprite;
    };
    UIGenerator.prototype.setInitialUserAction = function () {
        this.userActionsIndex = 0;
        var initialBodyPart = this.scene.bodyPartsManager.getFirstBodyPart();
        var initialPalleteNumber = initialBodyPart.palleteNumber;
        var initialColorButtonIndex = initialBodyPart.colorButton.buttonIndex;
        var initialColorPositionPair = new ColorPositionPair(initialPalleteNumber, initialColorButtonIndex);
        var initialTabIndex = initialBodyPart.selectedTabIndex;
        var initialUserAction = new UserAction(this, initialBodyPart.bodyPartIndex, initialColorPositionPair, initialTabIndex);
        this.userActionsSequence.push(initialUserAction);
    };
    UIGenerator.prototype.populateStage = function () {
        var _this = this;
        this.bottomSwitchBar = new BottomSwitchBar(this.scene, "AutoOn", "AutoOff", "Button_FrameSlideNew", "Button_SlideGroove", this.scene.model, function (switchstatus) {
            _this.scene.animationDirection = switchstatus;
        });
    };
    UIGenerator.prototype.setTabSelector = function () {
        if (this.currentlySelectedMaterialStack != null) {
            this.currentlySelectedMaterialStack.remove(this.tabSelector);
        }
        if (this.scene.bodyPartsManager.selectedBodyPart.modelPart.feature.isChildren) {
            var parentBodyPart = this.scene.bodyPartsManager.selectedBodyPart.getParnetBodyPart();
            var selectedMaterial = this.materialTable.items[parentBodyPart.selectedTabIndex];
            if (selectedMaterial) {
                this.currentlySelectedMaterialStack = this.materialTable.items[parentBodyPart.selectedTabIndex].stack;
                this.currentlySelectedMaterialStack.add(this.tabSelector);
            }
        }
        else {
            var selectedMaterial = this.materialTable.items[this.scene.bodyPartsManager.selectedBodyPart.selectedTabIndex];
            //if(selectedMaterial.tabIndex == 1) debugger
            if (selectedMaterial) {
                console.log('sssss', selectedMaterial.tabIndex);
                this.currentlySelectedMaterialStack = this.materialTable.items[this.scene.bodyPartsManager.selectedBodyPart.selectedTabIndex].stack;
                this.currentlySelectedMaterialStack.add(this.tabSelector);
            }
        }
    };
    UIGenerator.prototype.correctColorAdjustbutton = function (button) {
        this.setColorSlidersPosition();
        this.createUserAction();
    };
    UIGenerator.prototype.onChangeColorAdjustment = function (button) {
        var buttonx = this.screenWidth - button.x;
        var pos = 0;
        var deta = 50;
        for (var i = 0; i < this.LAYOUT_CONSTANT.COLOR_ADJUST_DOT_POSITIONS.length; i++) {
            var element = this.LAYOUT_CONSTANT.COLOR_ADJUST_DOT_POSITIONS[i] / this.LAYOUT_CONSTANT.SCREEN_SIZE_COEF;
            if (deta > Math.abs(element - buttonx)) {
                deta = Math.abs(element - buttonx);
                pos = i;
            }
        }
        var currentLevel = pos;
        if (currentLevel < 0)
            currentLevel = 0;
        if (currentLevel > 8)
            currentLevel = 8;
        this.palleteNumber = currentLevel;
        this.scene.bodyPartsManager.selectedBodyPart.setPalleteNumber(this.palleteNumber);
        this.setColorFromSliderPosition(this.palleteNumber);
    };
    UIGenerator.prototype.chooseTMenuButton = function (bodyPartIndex) {
        var bodyPart = this.scene.bodyPartsManager.bodyParts[bodyPartIndex];
        if (bodyPart.modelPart.feature.isChildren) {
            // if bodyPart is children
            var parent_1 = bodyPart.getParnetBodyPart();
            var tMenuButton = parent_1.tMenuButton;
            tMenuButton.performSelection(); // parent bodypart
            bodyPart.childBodyPartButton.performSelection(); // children bodypart
        }
        else {
            // if bodypart is parent
            var tMenuButton = bodyPart.tMenuButton;
            tMenuButton.performSelection();
        }
    };
    UIGenerator.prototype.chooseTabButton = function (materialIndex) {
        if (this.scene.bodyPartsManager.selectedBodyPart != null) {
            if (this.scene.bodyPartsManager.selectedBodyPart.modelPart.feature.isChildren) {
                //debugger;
            }
            else {
                this.scene.bodyPartsManager.setMaterial(materialIndex);
            }
        }
    };
    UIGenerator.prototype.chooseColorButton = function (colorPositionPair) {
        var buttonNumber = colorPositionPair.buttonIndex;
        var palleteNumber = colorPositionPair.palletNumber;
        this.scene.bodyPartsManager.selectedBodyPart.setPalleteNumber(palleteNumber);
        if (palleteNumber < 9) {
            this.palleteNumber = palleteNumber;
            this.setColorMode(CST.COLOR_MODE.DEFAULT);
        }
        else {
            this.colorPalletePanel.setPalleteNumber(palleteNumber);
            this.setColorMode(CST.COLOR_MODE.CUSTOME);
        }
        var colorButton = this.colorButtons[buttonNumber];
        colorButton.performSelection(true);
    };
    // chooseChildColorLink(colorlinks:integer[]) {
    //  // undo
    //     let childBPs:BodyPart[] = this.scene.bodyPartsManager.selectedBodyPart.getChildBodyParts();
    //     childBPs.forEach(element => {
    //         let res = colorlinks.filter(it => it == element.getPartId());
    //         if(res.length > 0) element.depend = true;
    //         else element.depend = false;
    //         // console.log('lihnk',element.getChildLink())
    //         if(element.depend){
    //             let colorModeNumber = this.getSelectedBodyPart().colorModeNumber;
    //             let newColorModeNumber = Math.min(8,colorModeNumber + element.modelPart.feature.colorModifier);     
    //             newColorModeNumber = Math.max(0, newColorModeNumber);         
    //             element.setPalleteNumber(newColorModeNumber);
    //             element.colorButton =this.selectedColorButton;
    //             element.color = this.scene.getColorFromRange(this.selectedColorButton.buttonIndex, newColorModeNumber);
    //             element.setSelectorColor(element.color);
    //         }            
    //     });
    // }
    UIGenerator.prototype.setChildLinkedColor = function () {
        var _this = this;
        var childBPs = this.scene.bodyPartsManager.selectedBodyPart.getChildBodyParts();
        childBPs.forEach(function (element) {
            if (element.depend) {
                // if(element.getChildLink()){
                // }
                var colorModeNumber = _this.getSelectedBodyPart().colorModeNumber;
                var newColorModeNumber = Math.min(8, colorModeNumber + element.modelPart.feature.colorModifier);
                newColorModeNumber = Math.max(0, newColorModeNumber);
                element.setPalleteNumber(newColorModeNumber);
                element.colorButton = _this.selectedColorButton;
                element.color = _this.scene.getColorFromRange(_this.selectedColorButton.buttonIndex, newColorModeNumber);
                element.setSelectorColor(element.color);
                console.log('-------------------------------', element.color);
            }
        });
    };
    UIGenerator.prototype.hideElementsOnModelingMode = function () {
        this.backgroundSprite.setVisible(false);
        this.solidGreySprite.setVisible(false);
        this.backgroundBoxTopper.setVisible(false);
        this.undoButton.setVisible(false);
        this.redoButton.setVisible(false);
        this.bottomSwitchBar.setVisible(false);
        this.progressBar.setVisible(false);
        this.colorsTable.setVisible(false);
        this.materialTable.setVisible(false);
        this.bodypartTable.setVisible(false);
        this.topBarAreaContainer.setVisible(false);
        this.backButton.setVisible(false);
        this.colorTableBackgroundImage.setVisible(false);
        this.leftMovementContainer.setVisible(false);
        this.colorPalletePanel.setVisible(false);
        this.buttonPreview.setVisible(false);
        this.colorAdjustContainer.setVisible(false);
    };
    UIGenerator.prototype.hideElementsOnPreviewMode = function () {
        this.shelfSprite.setVisible(false);
        this.goToEditBox.setVisible(false);
        this.saveButton.setVisible(false);
        this.solidGreySprite.setVisible(false);
    };
    UIGenerator.prototype.hideElementsOnShopMode = function () {
        this.diceScreenTopBar.setVisible(false);
        this.shopBackgroundSprite.setVisible(false);
        this.shopBoxSprite.setVisible(false);
        this.diceImage.setVisible(false);
        this.extraSmallSizeButton.setVisible(false);
        this.smallSizeButton.setVisible(false);
        this.mediumSizeButton.setVisible(false);
        this.largeSizeButton.setVisible(false);
        this.fadeSprite.setVisible(true);
        this.backgroundSprite.setVisible(true);
        this.solidGreySprite.setVisible(true);
    };
    UIGenerator.prototype.showElementsOnPrevMode = function () {
        this.shelfSprite.setVisible(true);
        this.goToEditBox.setVisible(true);
        this.saveButton.setVisible(true);
        this.solidGreySprite.setVisible(true);
        this.backgroundBoxTopper.displayWidth = this.screenWidth;
    };
    UIGenerator.prototype.showElementsOnEditMode = function () {
        this.backgroundSprite.setVisible(true);
        this.solidGreySprite.setVisible(true);
        this.backgroundBoxTopper.setVisible(true);
        this.undoButton.setVisible(true);
        this.redoButton.setVisible(true);
        this.bottomSwitchBar.setVisible(true);
        this.colorsTable.setVisible(true);
        this.materialTable.setVisible(true);
        this.bodypartTable.setVisible(true);
        this.topBarAreaContainer.setVisible(true);
        this.backButton.setVisible(true);
        this.colorTableBackgroundImage.setVisible(true);
        this.leftMovementContainer.setVisible(true);
        this.colorPalletePanel.setVisible(true);
        this.buttonPreview.setVisible(true);
        this.backgroundBoxTopper.displayWidth = this.screenWidth - 2.85 * this.LAYOUT_CONSTANT.BUTTONS_SIZE;
        this.colorAdjustContainer.setVisible(true);
        this.setColorMode(this.colorMode);
    };
    UIGenerator.prototype.resetElementPositionByScreenMode = function (viewmode) {
        this.viewMode = viewmode;
        if (viewmode == CST.LAYOUT.EDIT_MODE) {
            this.hideElementsOnPreviewMode();
            this.showElementsOnEditMode();
            this.hideElementsOnShopMode();
            this.scene.animationDirection = SwitchStatus.RIGHT;
            this.bottomSwitchBar.setSwtichOn(SwitchStatus.RIGHT, true);
            this.updateGridTablesBasedOnPreviewBtn(0);
            this.bottomSwitchBar.setPingPongFlag(true);
            this.fadeInBackgroundSprite();
        }
        if (viewmode == CST.LAYOUT.SAVE_PREVIEW_MODE) {
            this.hideElementsOnModelingMode();
            this.showElementsOnPrevMode();
            this.scene.animationDirection = SwitchStatus.STOP;
            this.scene.bodyPartsManager.resetbodyPartStackScale(viewmode);
            this.bottomSwitchBar.setPingPongFlag(false);
        }
        if (viewmode == CST.LAYOUT.PREVIEW_MODE) {
            console.log("previewmode loaded");
            this.hideElementsOnPreviewMode();
            // this.showElementsOnEditMode();
            this.hideElementsOnShopMode();
            this.hideFloatColorSlider();
            this.hideElementsOnModelingMode();
            this.scene.animationDirection = SwitchStatus.RIGHT;
            this.bottomSwitchBar.setSwtichOn(SwitchStatus.RIGHT, true);
            this.updateGridTablesBasedOnPreviewBtn(0);
            this.bottomSwitchBar.setPingPongFlag(true);
            this.fadeInBackgroundSprite();
        }
        if (viewmode == CST.LAYOUT.SHOP_MODE) {
            this.hideElementsOnPreviewMode();
            this.showElementsOnShopMode();
            this.bottomSwitchBar.setPingPongFlag(false);
        }
        this.scene.screenMode = viewmode;
        this.scene.setScreeMode(viewmode);
    };
    UIGenerator.prototype.setFloatColorSlider = function (buttonNumber) {
        this.colorIsAdjusted = true;
        this.selectedColorButton.setMarked(false);
        this.currentlySelectedColorStack = this.colorButtons[buttonNumber].stack;
        this.stackWithFloatingSlider = this.colorButtons[buttonNumber].stack;
        this.selectedColorButton = this.colorButtons[buttonNumber];
        this.setFloatingColorAdjustPosition();
        this.floatColorSlider.setVisible(true);
        this.floatColorSliderBG.setVisible(true);
        this.floatColorSliderButton.setVisible(true);
    };
    UIGenerator.prototype.setFloatingColorAdjustPosition = function () {
        var x = this.colorsTable.stack.x + this.selectedColorButton.globalX;
        var y = this.colorsTable.stack.y + this.selectedColorButton.globalY;
        this.floatColorSlider.setPosition(x, y);
        this.floatColorSliderBG.setPosition(x, y);
        var xval = this.floatColorSliderBG.x + this.floatColorSliderBG.displayWidth - this.LAYOUT_CONSTANT.FLOAT_COLOR_BUTTON_POSITIONS[this.palleteNumber] / this.LAYOUT_CONSTANT.SCREEN_SIZE_COEF - this.LAYOUT_CONSTANT.X_OFFSET_BETWEEN_SLIDERS;
        this.floatColorSliderButton.setPosition(xval, y);
    };
    UIGenerator.prototype.saveLastColorSelection = function () {
        this.createUserAction();
    };
    UIGenerator.prototype.updateFloatingPosition = function (x) {
        var temp = x;
        if (x < this.floatColorSliderBG.x - this.floatColorSliderBG.displayWidth / 2) {
            temp = this.floatColorSliderBG.x - this.floatColorSliderBG.displayWidth / 2;
        }
        if (x > this.floatColorSliderBG.x + this.floatColorSliderBG.displayWidth / 2) {
            temp = this.floatColorSliderBG.x + this.floatColorSliderBG.displayWidth / 2;
        }
        var buttonx = this.floatColorSliderBG.x + this.floatColorSliderBG.displayWidth / 2 - temp;
        buttonx *= this.colorAdjustFrame.displayWidth / this.floatColorSliderBG.displayWidth;
        // return;
        // var buttonx = temp + this.LAYOUT_CONSTANT.X_OFFSET_BETWEEN_SLIDERS + this.colorAdjustFrame.x - (this.floatColorSliderBG.x - this.floatColorSliderBG.displayWidth/2);
        // console.log(buttonx);
        var pos = 0;
        var deta = 50;
        for (var i = 0; i < this.LAYOUT_CONSTANT.COLOR_ADJUST_DOT_POSITIONS.length; i++) {
            var element = this.LAYOUT_CONSTANT.COLOR_ADJUST_DOT_POSITIONS[i] / this.LAYOUT_CONSTANT.SCREEN_SIZE_COEF;
            if (deta > Math.abs(element - buttonx)) {
                deta = Math.abs(element - buttonx);
                pos = i;
            }
        }
        var currentLevel = pos;
        if (currentLevel < 0)
            currentLevel = 0;
        if (currentLevel > 8)
            currentLevel = 8;
        this.palleteNumber = currentLevel;
        this.scene.bodyPartsManager.selectedBodyPart.setPalleteNumber(this.palleteNumber);
        this.setColorFromSliderPosition(this.palleteNumber);
        this.setColorSlidersPosition();
    };
    /**
     * listener binded model save button
     * */
    UIGenerator.prototype.saveCurrentModel = function () {
        // game.playSound(GENERAL_SOUND);
        this.incrementRateMeCount();
        this.scene.bodyPartsManager.saveModel();
        this.scene.screenMode = CST.LAYOUT.SHOP_MODE;
        // this.hideElementsOnPreviewMode();
        // this.showElementsOnShopMode();
        this.resetElementPositionByScreenMode(CST.LAYOUT.SHOP_MODE);
        // setDiceScale(smallSizeInCmText.getStr().toString());
        this.scene.getModelCharacteristics().setSize("" + this.scene.model.getExtraSmallSizeInCm());
        if (this.scene.model.shopSkip) {
            this.scene.goSloteScene();
        }
        else {
            this.resetElementPositionByScreenMode(CST.LAYOUT.SHOP_MODE);
        }
    };
    UIGenerator.prototype.incrementRateMeCount = function () {
        var rateMeCount = +localStorage.getItem(this.RATE_ME_COUNT);
        rateMeCount++;
        localStorage.setItem(this.RATE_ME_COUNT, "" + rateMeCount);
    };
    UIGenerator.prototype.showElementsOnShopMode = function () {
        this.diceScreenTopBar.setVisible(true);
        this.shopBackgroundSprite.setVisible(true);
        this.shopBoxSprite.setVisible(true);
        this.diceImage.setVisible(true);
        this.extraSmallSizeButton.setVisible(true);
        this.smallSizeButton.setVisible(true);
        this.mediumSizeButton.setVisible(true);
        this.largeSizeButton.setVisible(true);
        this.onTapBuyButton(0);
        //hide fade  sprete
        this.fadeSprite.setVisible(false);
        this.backgroundSprite.setVisible(false);
    };
    UIGenerator.prototype.onTapBuyButton = function (index) {
        this.selectedSizeButtonIndex = index;
        this.extraSmallSizeButton.setStatus(false);
        this.smallSizeButton.setStatus(false);
        this.mediumSizeButton.setStatus(false);
        this.largeSizeButton.setStatus(false);
        if (index == 0)
            this.extraSmallSizeButton.setStatus(true);
        if (index == 1)
            this.smallSizeButton.setStatus(true);
        if (index == 2)
            this.mediumSizeButton.setStatus(true);
        if (index == 3)
            this.largeSizeButton.setStatus(true);
        this.scaleDiceImage(index);
    };
    UIGenerator.prototype.scaleDiceImage = function (index) {
        var standardSize = this.scene.model.getSmallSizeInCm();
        var sizeInCm = this.scene.model.getExtraSmallSizeInCm();
        if (index == 1) {
            sizeInCm = this.scene.model.getSmallSizeInCm();
        }
        if (index == 2) {
            sizeInCm = this.scene.model.getMediumSizeInCm();
        }
        if (index == 3) {
            sizeInCm = this.scene.model.getLargeSizeInCm();
        }
        this.scene.modelCharacteristics.setSize("" + sizeInCm);
        console.log(this.scene.modelCharacteristics);
        this.diceImage.setScale(0.4 * this.scene.bodyPartsManager.bodyPartStack.scale * (standardSize / this.LAYOUT_CONSTANT.SCREEN_HEIGHT_COEF) / sizeInCm);
    };
    UIGenerator.prototype.setScaleDiceImageByDScale = function () {
        this.scaleDiceImage(this.selectedSizeButtonIndex);
    };
    UIGenerator.prototype.setVisibleColorPalletPanelByColorMode = function () {
        if (this.colorMode == CST.COLOR_MODE.DEFAULT) {
            if (this.viewMode == CST.LAYOUT.EDIT_MODE && this.buttonPreview.x != this.LAYOUT_CONSTANT.SCREEN_WIDTH)
                this.colorAdjustContainer.setVisible(true);
        }
        if (this.colorMode == CST.COLOR_MODE.CUSTOME) {
            if (this.viewMode == CST.LAYOUT.EDIT_MODE)
                this.colorPalletePanel.setVisible(true);
        }
    };
    UIGenerator.prototype.setColorMode = function (colorMode) {
        this.colorMode = colorMode;
        this.colorPalletePanel.setVisible(false);
        this.colorAdjustContainer.setVisible(false);
        this.setVisibleColorPalletPanelByColorMode();
        this.setColorsToButtons();
        this.resetMarkInColorButtons();
    };
    UIGenerator.prototype.resetMarkInColorButtons = function () {
        var _this = this;
        this.colorSelectorContainer.setVisible(false);
        this.scene.bodyParts.forEach(function (element) {
            if (_this.colorMode == CST.COLOR_MODE.CUSTOME) {
                if (element.colorModeNumber != _this.colorPalletePanel.getPalleteNumber())
                    element.removeMarkInColorButton();
                else
                    element.makeMarkInColorButton();
            }
            else {
                if (element.colorModeNumber > 9)
                    element.removeMarkInColorButton();
                else
                    element.makeMarkInColorButton();
            }
        });
        this.scene.bodyParts.forEach(function (element) {
            if (_this.colorMode == CST.COLOR_MODE.CUSTOME) {
                if (element.colorModeNumber == _this.colorPalletePanel.getPalleteNumber()) {
                    element.makeMarkInColorButton();
                }
            }
            else {
                if (element.colorModeNumber < 9) {
                    element.makeMarkInColorButton();
                }
            }
        });
        var colormodenumber = this.getSelectedBodyPart().colorModeNumber;
        if (this.colorMode == CST.COLOR_MODE.CUSTOME) {
            if (colormodenumber == this.colorPalletePanel.getPalleteNumber()) {
                this.colorSelectorContainer.setVisible(true);
            }
        }
        else {
            if (colormodenumber < 9) {
                this.colorSelectorContainer.setVisible(true);
            }
        }
    };
    UIGenerator.prototype.getSelectedBodyPart = function () {
        return this.scene.bodyPartsManager.selectedBodyPart;
    };
    UIGenerator.prototype.updateGridTablesBasedOnPreviewBtn = function (dx) {
        var prebtnOX = this.screenWidth - 2.85 * this.LAYOUT_CONSTANT.BUTTONS_SIZE;
        var prebtnOY = this.LAYOUT_CONSTANT.BUTTONS_SIZE * 1.2;
        var colorTblOY = this.LAYOUT_CONSTANT.BUTTONS_SIZE * 1.2 + this.colorSwitchBoard.getElementHeight();
        var mtTblOY = this.LAYOUT_CONSTANT.BUTTONS_SIZE * 1.2;
        //move bodypart table
        this.bodypartTable.setPosition({ y: this.buttonPreview.y - prebtnOY });
        this.topBarAreaContainer.y = this.buttonPreview.y - prebtnOY;
        //move colors table
        this.colorsTable.setPosition({ x: this.buttonPreview.x + 0.15 * this.LAYOUT_CONSTANT.BUTTONS_SIZE, y: this.buttonPreview.y - prebtnOY + colorTblOY });
        this.colorTableBackgroundImage.x = this.buttonPreview.x + this.colorTableBackgroundImage.displayWidth / 2;
        this.materialTable.setPosition({ x: this.screenWidth - 0.92 * this.LAYOUT_CONSTANT.BUTTONS_SIZE + this.buttonPreview.x - prebtnOX, y: this.buttonPreview.y - prebtnOY + mtTblOY });
        this.leftMovementContainer.x = this.buttonPreview.x - prebtnOX;
        this.leftMovementContainer.y = this.buttonPreview.y - prebtnOY;
        // this.colorPalletePanel.stack.x = this.colorPalletePanel.stackOriginX + this.buttonPreview.x - prebtnOX;
        this.backgroundSprite.displayWidth = this.buttonPreview.x;
        this.backgroundBoxTopper.displayWidth = this.buttonPreview.x;
        if (this.buttonPreview.x != this.LAYOUT_CONSTANT.SCREEN_WIDTH) {
            this.backgroundSprite.setVisible(true);
            this.backgroundBoxTopper.setVisible(true);
            this.undoButton.setVisible(true);
            this.redoButton.setVisible(true);
            this.bottomSwitchBar.setVisible(true);
            this.setVisibleColorPalletPanelByColorMode();
        }
        if (this.buttonPreview.x == this.LAYOUT_CONSTANT.SCREEN_WIDTH) {
            this.backgroundSprite.setVisible(false);
            this.backgroundBoxTopper.setVisible(false);
            this.undoButton.setVisible(false);
            this.redoButton.setVisible(false);
            this.bottomSwitchBar.setVisible(false);
            this.colorAdjustContainer.setVisible(false);
            this.colorPalletePanel.setVisible(false);
        }
        if (this.buttonPreview.y == 0 && this.buttonPreview.x == this.LAYOUT_CONSTANT.SCREEN_WIDTH) {
            this.backButton.setVisible(false);
            this.viewMode = CST.LAYOUT.PREVIEW_MODE;
        }
        else {
            this.backButton.setVisible(true);
            this.viewMode = CST.LAYOUT.EDIT_MODE;
        }
        this.scene.bodyPartsManager.moveByX(dx / 2);
        this.bottomSwitchBar.container.x = this.backgroundBoxTopper.displayWidth / 2;
        this.redoButton.x = this.colorsTable.getX() - 40 / this.LAYOUT_CONSTANT.ORIGIN_SCREEN_SIZE_COEF,
            this.scene.bodyPartsManager.dynamicCenterX += dx / 2;
        this.updateTouchZone();
    };
    UIGenerator.prototype.fadeOutBackgroundSprite = function () {
        if (this.isBackgroundFadeOuted)
            return;
        this.isBackgroundFadeOuted = true;
        this.scene.tweens.add({
            targets: this.backgroundSprite,
            alpha: 0,
            ease: 'Cubic.easeOut',
            duration: 1000,
        });
    };
    UIGenerator.prototype.fadeInBackgroundSprite = function () {
        if (this.isBackgroundFadeOuted != true)
            return;
        this.isBackgroundFadeOuted = false;
        this.scene.tweens.add({
            targets: this.backgroundSprite,
            alpha: 1,
            ease: 'Cubic.easeIn',
            duration: 1000,
        });
        this.scene.bodyPartsManager.resetBodyPartStack();
    };
    UIGenerator.prototype.updateTouchZone = function () {
        var touchZoneW = this.buttonPreview.x;
        var touchZoneH = this.screenHeight;
        var touchZoneX = 0;
        var touchZoneY = this.LAYOUT_CONSTANT.BUTTONS_SIZE + this.bodypartTable.getTableY();
        if (this.viewMode != CST.LAYOUT.PREVIEW_MODE) {
            touchZoneH = this.screenHeight - this.LAYOUT_CONSTANT.BUTTONS_SIZE - touchZoneY;
        }
        this.touchZone.setPosition(touchZoneX, touchZoneY);
        this.touchZone.displayWidth = touchZoneW;
        this.touchZone.displayHeight = touchZoneH;
    };
    return UIGenerator;
}());
// class BitmapTexture {
//     scene:Phaser.Scene;
//     defaultTexture:string;
//     hightlightTexture:string;
//     stack:Phaser.GameObjects.Container;
//     backgroundImage:Phaser.GameObjects.Image;
//     callback:(boolean)=>void;
//     isON:boolean;
//     constructor(scene:Phaser.Scene, x:number, y:number, text:string, callback){
//         this.scene = scene;
//         this.text = text;
//         this.callback = callback;
//         this.backgroundImage = this.scene.add.image(0, 0, text);
//         this.stack = this.scene.add.container(x, y);
//         this.stack.add(this.backgroundImage);
//         this.initElements();
//         this.initEvent();
//     }
//     initElements(){
//             this.backgroundImage.setTexture(this.text);
//     }
//     initEvent(){
//         this.backgroundImage.setInteractive().on('pointerdown', (pointer, localX, localY, event)=>{
//         }).on('pointermove', (pointer, localX, localY, event)=>{
//         }).on('pointerup', (pointer, localX, localY, event)=>{
//             this.setToggle();
//             this.callback(this.isON);
//         });
//     }
//     setToggle(){
//         const isOn = !this.isON;
//         if(isOn){
//             this.backgroundImage.setTexture(this.hightlightTexture);
//         }else{
//             this.backgroundImage.setTexture(this.defaultTexture);
//         }
//         this.isON = isOn;
//     }
//     setActive(flag:boolean){        
//         this.isON = flag;
//         if(this.isON){
//             this.backgroundImage.setTexture(this.hightlightTexture);
//         }else{
//             this.backgroundImage.setTexture(this.defaultTexture);
//         }
//     }
// }
// const _LEFTMOST_SLIDER_POSITION = 0;
// const _RIGHTMOST_SLIDER_POSITION = 8;
var ChildBodyPartButton = /** @class */ (function () {
    function ChildBodyPartButton(bodyPart) {
        this.blinkCurColor = new Phaser.Display.Color();
        this.bodyPart = bodyPart;
        this.scene = this.bodyPart.scene;
        this.bodyPartsManager = bodyPart.bodyPartsManager;
        this.stack = new Phaser.GameObjects.Container(this.scene);
        this.lightBlinkingColor = new Phaser.Display.Color(255, 255, 255);
        this.darkBlinkingColor = new Phaser.Display.Color(255, 255, 255);
        this.uiGenerator = this.bodyPart.scene.uiGenerator;
        this.createImage();
        this.interpolatingCoef = 0;
        this.interpolatingCoefIncrement = 1;
        // blink features Edied by SY
        this.blinkProgress = 0;
        this.blinkRepeatNum = 0;
        this.blinkRepeatLimit = 3;
        this.blinkIncrement = 14;
    }
    ChildBodyPartButton.prototype.createImage = function () {
        this.atlasFront = this.scene.getHDAtalasFront(); //model_name-front
        var tabRegions = this.bodyPart.modelPart.getTabRegions();
        var tabRegion = tabRegions[0];
        this.image = new Phaser.GameObjects.Image(this.scene, 0, 0, this.atlasFront, tabRegion + ".png");
        this.image.setScale(LayoutContants.getInstance().BUTTONS_SIZE / this.image.displayHeight);
        this.stack.add(this.image);
        // const buttonName = this.bodyPart.modelPart.feature.partName;
        // this.image = new Phaser.GameObjects.Image(this.scene, 0, 0, `${this.atlasFront}`, `${buttonName}.png`);
        // this.image.displayHeight = LayoutContants.getInstance().BUTTONS_SIZE*1.3;
        // this.image.displayWidth = LayoutContants.getInstance().BODY_PART_BUTTON_WIDTH/LayoutContants.getInstance().SCREEN_SIZE_COEF;
        // this.stack.add(this.image);
        // this.stack.setPosition(this.image.displayWidth/2, this.image.displayHeight/2);
    };
    ChildBodyPartButton.prototype.onTapButton = function () {
        this.performSelection();
        // setting blink start
        this.resetColorForBlinking();
        this.scene.setBlinkingEnabled(true);
        this.scene.playSound(CST.SOUND.COLOR_GENERAL_SOUND);
        this.uiGenerator.setColorModelFromSelectedBodyPart();
        var colorButton = this.uiGenerator.getSelectedBodyPart().colorButton;
        var buttonIndex = 0;
        if (colorButton)
            buttonIndex = colorButton.buttonIndex; // set bodypart's color button
        this.uiGenerator.setColorSelection(buttonIndex); // set color for current selected bodypart 
        this.uiGenerator.resetMarkInColorButtons();
        this.uiGenerator.createUserAction();
    };
    ChildBodyPartButton.prototype.performSelection = function () {
        this.bodyPart.depend = false;
        this.bodyPartsManager.setSelectedBodyPart(this.bodyPart); // set focused bodypart
        this.uiGenerator.setSelectedChildBodyPartButton(this); // set selected ui button
        // set marker 
        var colorButton = this.uiGenerator.getSelectedColorButton();
        if (!colorButton.isMarked) {
            colorButton.setMarked(true);
        }
        this.uiGenerator.hideFloatColorSlider();
        //this.bodyPartsManager.setLinkedByColorSprites();
        //this.uiGenerator.setMaterialButtons(this.atlasFront);
        this.uiGenerator.setColorsToButtons();
        this.bodyPartsManager.setMaterial(this.bodyPartsManager.selectedBodyPart.getParnetBodyPart().bodyPartIndex);
        // if (checkIfContainsBodypart(this.bodyPartsManager.hiddenBodyParts, this.bodyPart)) {
        //     // this.bodyPartsManager.showBodyParts();
        // }
    };
    ChildBodyPartButton.prototype.resetColorForBlinking = function () {
        //set initilal values Edited by SY
        this.blinkProgress = this.blinkIncrement;
        this.blinkRepeatNum = 0;
        this.blinkCurColor = this.color;
        this.blinkInitialColors();
        this.bodyPart.initColor();
        //setTimeout(()=>{this.stopBlinkWorking()}, 3000);
    };
    // Created funtion by SY
    // blinkInitialColors(){
    //     let s_v = 0.5, r_v= 0.5;
    //     let hsv = Phaser.Display.Color.RGBToHSV(this.blinkCurColor.red, this.blinkCurColor.green, this.blinkCurColor.blue);
    //     //V
    //     if(hsv.v + 0.25 > 1)
    //     {
    //         s_v = 1;
    //         r_v = 0.5;
    //     }
    //     else if(hsv.v - 0.25< 0)
    //     {
    //         s_v= 0;
    //         r_v = 0.5;
    //     }
    //     else
    //     {
    //         s_v = hsv.v + 0.25;
    //         r_v = hsv.v - 0.25;
    //     }
    //     this.blinkStartColor = new Phaser.Display.Color();
    //     this.blinkreqColor = new Phaser.Display.Color();
    //     Phaser.Display.Color.HSVToRGB(hsv.h, hsv.s, s_v, this.blinkStartColor);
    //     Phaser.Display.Color.HSVToRGB(hsv.h, hsv.s, r_v, this.blinkreqColor);
    // }
    ChildBodyPartButton.prototype.blinkInitialColors = function () {
        if (this.blinkCurColor == undefined)
            this.blinkCurColor = new Phaser.Display.Color();
        var r_v = 0.5;
        var hsv = Phaser.Display.Color.RGBToHSV(this.blinkCurColor.red, this.blinkCurColor.green, this.blinkCurColor.blue);
        //V
        if (hsv.v + 0.5 > 1) {
            r_v = 0;
        }
        else if (hsv.v - 0.5 < 0) {
            r_v = 1;
        }
        else {
            r_v = 1;
        }
        this.blinkStartColor = new Phaser.Display.Color();
        this.blinkreqColor = new Phaser.Display.Color();
        Phaser.Display.Color.HSVToRGB(hsv.h, hsv.s, hsv.v, this.blinkStartColor);
        Phaser.Display.Color.HSVToRGB(hsv.h, hsv.s, r_v, this.blinkreqColor);
    };
    ChildBodyPartButton.prototype.setColor = function (color) {
        this.color = color;
        this.image.tint = Phaser.Display.Color.GetColor(color.red, color.green, color.blue);
        this.resetColorForBlinking();
    };
    ChildBodyPartButton.prototype.blink = function (callback) {
        if (this.blinkRepeatNum > this.blinkRepeatLimit) {
            this.bodyPart.setBodyPartColor(this.blinkCurColor);
            callback();
            return;
        }
        else {
            if (this.blinkProgress < 100) {
                var colorObject = Phaser.Display.Color.Interpolate.ColorWithColor(this.blinkStartColor, this.blinkreqColor, 100, this.blinkProgress);
                this.bodyPart.setBodyPartColor(Phaser.Display.Color.ObjectToColor(colorObject));
                this.blinkProgress += this.blinkIncrement;
            }
            else {
                var temp = this.blinkStartColor;
                this.blinkStartColor = this.blinkreqColor;
                this.blinkreqColor = temp;
                this.blinkProgress = this.blinkIncrement;
                this.blinkRepeatNum += 1;
            }
        }
    };
    ChildBodyPartButton.prototype.getDisplayHeight = function () {
        return this.image.displayHeight;
    };
    ChildBodyPartButton.prototype.getDisplayWidth = function () {
        return this.image.displayWidth;
    };
    return ChildBodyPartButton;
}());
var BottomSwitchBar = /** @class */ (function () {
    function BottomSwitchBar(scene, onImageTexture, offImageTexture, thumImageTexuture, sliderPathImageTexture, model, callback) {
        this.isPingPong = true;
        this.isPing = true;
        this.offImageTexture = offImageTexture;
        this.onImageTexture = onImageTexture;
        this.sliderPathImageTexture = sliderPathImageTexture;
        this.thumImageTexuture = thumImageTexuture;
        this.callback = callback;
        this.scene = scene;
        this.model = model;
        this.switchStatus = SwitchStatus.RIGHT;
        this.container = this.scene.add.container(this.scene.uiGenerator.backgroundBoxTopper.displayWidth / 2, +this.scene.game.config.height - CST.LAYOUT.BUTTONS_SIZE_BASE / LayoutContants.getInstance().SCREEN_HEIGHT_COEF / 2);
        this.container.setDepth(2);
        this.createElement();
        this.bindSwitchEvent();
        this.minFrame = 0;
        this.maxFrame = parseInt(this.model.frameSetup.split(",")[1]);
        this.currentFrame = parseInt(this.model.frameSetup.split(",")[2]);
        this.stepDuration = 1000 / (this.maxFrame - this.minFrame);
        this.initTimer();
    }
    BottomSwitchBar.prototype.createElement = function () {
        this.sliderPathImage = this.scene.add.image(0, 0, this.sliderPathImageTexture);
        this.sliderPathImage.displayWidth = (+this.scene.game.config.width - 3 * CST.LAYOUT.BUTTONS_SIZE) * 0.5;
        this.sliderPathImage.setScale(LayoutContants.getInstance().SCALE);
        this.thumbButton = this.scene.add.image(0, 0, this.thumImageTexuture);
        this.thumbButton.setScale(LayoutContants.getInstance().SCALE);
        // this.thumbButton.x = this.sliderPathImage.displayWidth/2  - this.thumbButton.displayWidth/4;
        if (this.switchStatus != SwitchStatus.STOP) {
            this.indicator = this.scene.add.image(-this.sliderPathImage.displayWidth / 2 - 50, 0, this.onImageTexture);
        }
        else {
            this.indicator = this.scene.add.image(-this.sliderPathImage.displayWidth / 2 - 50, 0, this.offImageTexture);
        }
        this.indicator.setScale(LayoutContants.getInstance().SCALE);
        this.container.add(this.indicator);
        this.container.add(this.sliderPathImage);
        this.container.add(this.thumbButton);
    };
    BottomSwitchBar.prototype.bindSwitchEvent = function () {
        var _this = this;
        if (this.model.isOrbit == "1") {
            var isDown = false;
            var lastPosX = 0;
            this.thumbButton.setInteractive({
                hitAreaCallback: function () { }
            }).on('pointerdown', function (pointer, localX, localY, event) {
                isDown = true;
            }).on('pointermove', function (pointer, localX, localY, event) {
                // console.log(pointer.x);
                if (isDown) {
                    if (lastPosX == 0) {
                        lastPosX = pointer.x;
                    }
                    var dx = pointer.x - lastPosX;
                    _this.thumbButton.x += dx;
                    lastPosX = pointer.x;
                    var delta = 30;
                    if (_this.thumbButton.x - _this.sliderPathImage.x > delta) {
                        if (_this.switchStatus != SwitchStatus.RIGHT) {
                            // console.log("swich right!");
                            _this.switchStatus = SwitchStatus.RIGHT;
                            _this.callback(SwitchStatus.RIGHT);
                            _this.setSwtichOn(SwitchStatus.RIGHT, false);
                            _this.scene.playSound(CST.SOUND.COLOR_GENERAL_SOUND);
                        }
                    }
                    if (_this.sliderPathImage.x - _this.thumbButton.x > delta) {
                        if (_this.switchStatus != SwitchStatus.LEFT) {
                            // console.log("swich left!");
                            _this.switchStatus = SwitchStatus.LEFT;
                            _this.callback(SwitchStatus.LEFT);
                            _this.setSwtichOn(SwitchStatus.LEFT, false);
                            _this.scene.playSound(CST.SOUND.COLOR_GENERAL_SOUND);
                        }
                    }
                    if (Math.abs(_this.sliderPathImage.x - _this.thumbButton.x) < delta) {
                        if (_this.switchStatus != SwitchStatus.STOP) {
                            // console.log("swich off!");
                            _this.switchStatus = SwitchStatus.STOP;
                            _this.callback(SwitchStatus.STOP);
                            _this.setSwtichOn(SwitchStatus.STOP, false);
                            _this.scene.playSound(CST.SOUND.COLOR_GENERAL_SOUND);
                        }
                    }
                    // detect bound and fix position
                    if (_this.thumbButton.x <= -(_this.sliderPathImage.displayWidth / 2 - _this.thumbButton.displayWidth / 2)) {
                        _this.thumbButton.x = -(_this.sliderPathImage.displayWidth / 2 - _this.thumbButton.displayWidth / 2);
                    }
                    if (_this.thumbButton.x >= +(_this.sliderPathImage.displayWidth / 2 - _this.thumbButton.displayWidth / 2)) {
                        _this.thumbButton.x = _this.sliderPathImage.displayWidth / 2 - _this.thumbButton.displayWidth / 2;
                    }
                }
            }).on('pointerup', function (pointer, localX, localY, event) {
                // console.log('pointerup');
                isDown = false;
                if (_this.switchStatus == SwitchStatus.STOP) {
                    _this.thumbButton.x = 0;
                }
                if (_this.switchStatus == SwitchStatus.RIGHT) {
                    _this.thumbButton.x = (_this.sliderPathImage.displayWidth - _this.thumbButton.displayWidth) / 2;
                }
                if (_this.switchStatus == SwitchStatus.LEFT) {
                    _this.thumbButton.x = -(_this.sliderPathImage.displayWidth - _this.thumbButton.displayWidth) / 2;
                }
                lastPosX = 0;
            }).on('pointerout', function () {
                isDown = false;
            });
            this.indicator.setInteractive()
                .on('ponterdown', function (pointer, localX, localY, event) {
            })
                .on('pointerup', function (pointer, localX, localY, event) {
                if (_this.switchStatus == SwitchStatus.STOP) {
                    _this.setSwtichOn(SwitchStatus.RIGHT, true);
                    _this.callback(SwitchStatus.RIGHT);
                }
                else {
                    _this.setSwtichOn(SwitchStatus.STOP, true);
                    _this.callback(SwitchStatus.STOP);
                }
            });
        }
        else {
            var lastX_1 = 0;
            this.sliderPathImage.setInteractive()
                .on('pointerdown', function (pointer, localX, localY, event) {
            }).on('pointermove', function (pointer, localX, localY, event) {
                if (pointer.isDown) {
                    if (lastX_1 == 0)
                        lastX_1 = pointer.x;
                    var dx = pointer.x - lastX_1;
                    if (_this.thumbButton.x + dx >= _this.thumbButton.displayWidth / 2 - _this.sliderPathImage.displayWidth / 2) {
                    }
                    else {
                        _this.thumbButton.x = _this.thumbButton.displayWidth / 2 - _this.sliderPathImage.displayWidth / 2;
                        dx = 0;
                    }
                    if (_this.thumbButton.x + dx <= -_this.thumbButton.displayWidth / 2 + _this.sliderPathImage.displayWidth / 2) {
                    }
                    else {
                        _this.thumbButton.x = -_this.thumbButton.displayWidth / 2 + _this.sliderPathImage.displayWidth / 2;
                        dx = 0;
                    }
                    _this.thumbButton.x += dx;
                    lastX_1 = pointer.x;
                    if (!_this.isPingPong) {
                        _this.updateBodyPartFrame();
                    }
                }
            }).on('pointerup', function (pointer, localX, localY, event) {
                lastX_1 = 0;
            }).on('pointerout', function () {
                lastX_1 = 0;
            });
            this.indicator.setInteractive()
                .on('ponterdown', function (pointer, localX, localY, event) {
            })
                .on('pointerup', function (pointer, localX, localY, event) {
                _this.togglePingPongIndicator();
            });
        }
    };
    BottomSwitchBar.prototype.updateBodyPartFrame = function () {
        var frame = this.getPingPongFrame();
        this.currentFrame = frame;
        this.scene.bodyPartsManager.setFrame(frame);
    };
    BottomSwitchBar.prototype.getPingPongFrame = function () {
        var distance = this.sliderPathImage.displayWidth - this.thumbButton.displayWidth;
        var bx = this.thumbButton.x + (this.sliderPathImage.displayWidth - this.thumbButton.displayWidth) / 2;
        var ud = distance / (this.maxFrame - this.minFrame);
        var frame = this.maxFrame - ((this.isPing) ? Math.ceil(bx / ud) : Math.floor(bx / ud));
        if (frame > this.maxFrame)
            return this.maxFrame;
        if (frame < this.minFrame)
            return this.minFrame;
        return frame;
    };
    BottomSwitchBar.prototype.updateThumbButtonPositionByFrame = function (frame) {
        this.currentFrame = frame;
        var stdFrame = this.maxFrame / 2;
        console.log("stdFrame =====> ", stdFrame);
        var distance = this.sliderPathImage.displayWidth - this.thumbButton.displayWidth;
        var ud = distance / (this.maxFrame - this.minFrame - 1);
        this.thumbButton.x = ud * (-frame + stdFrame);
        // if(this.tween) this.tween.remove();
        // this.tween = this.scene.tweens.add({
        //     targets: this.thumbButton,
        //     x: ud * (- frame + stdFrame),
        //     ease: 'Linear',
        // 	duration: this.stepDuration,
        // 	repeat:0,
        // 	onComplete:()=>{
        // 		console.log(this.timer);
        // 	}
        // });
    };
    BottomSwitchBar.prototype.setSwtichOn = function (status, movingButton) {
        if (this.model.isOrbit == "1") {
            this.switchStatus = status;
            if (status == SwitchStatus.RIGHT) {
                this.indicator.setTexture(this.onImageTexture);
                if (movingButton)
                    this.thumbButton.x = this.sliderPathImage.displayWidth / 2 - this.thumbButton.displayWidth / 2;
            }
            else if (status == SwitchStatus.LEFT) {
                this.indicator.setTexture(this.onImageTexture);
                if (movingButton)
                    this.thumbButton.x = -(this.sliderPathImage.displayWidth / 2 - this.thumbButton.displayWidth / 2);
            }
            else {
                this.indicator.setTexture(this.offImageTexture);
                if (movingButton)
                    this.thumbButton.x = 0;
            }
        }
        if (status == SwitchStatus.STOP) {
            this.setPingPongFlag(false);
            this.indicator.setTexture(this.offImageTexture);
        }
    };
    BottomSwitchBar.prototype.togglePingPongIndicator = function () {
        if (!this.isPingPong) {
            this.indicator.setTexture(this.onImageTexture);
            this.isPingPong = true;
        }
        else {
            this.indicator.setTexture(this.offImageTexture);
            this.isPingPong = false;
        }
    };
    BottomSwitchBar.prototype.updatePingPong = function () {
        var _this = this;
        if (!this.scene.isFileLoaded)
            return;
        var minFrame = 0;
        var maxFrame = parseInt(this.model.frameSetup.split(",")[1]);
        if (this.isPingPong) {
            var newFrame = 0;
            if (this.isPing) {
                newFrame = this.currentFrame - 1;
                if (newFrame < minFrame) {
                    newFrame = minFrame;
                    setTimeout(function () {
                        _this.isPing = false;
                    }, 1000);
                }
            }
            else {
                newFrame = this.currentFrame + 1;
                console.log("newFrame for pong ==> ", newFrame);
                if (newFrame > maxFrame) {
                    newFrame = maxFrame;
                    setTimeout(function () {
                        _this.isPing = true;
                    }, 1000);
                }
            }
            console.log("newFrame ==> ", newFrame);
            this.updateThumbButtonPositionByFrame(newFrame);
            this.scene.bodyPartsManager.setFrame(newFrame);
            this.scene.bodyPartsManager.updateModel();
        }
    };
    BottomSwitchBar.prototype.initTimer = function () {
        var _this = this;
        if (this.scene.model.isOrbit == "0") {
            this.timer = this.scene.time.addEvent({
                delay: this.stepDuration,
                callback: function () {
                    _this.updatePingPong();
                },
                //args: [],
                loop: true
            });
        }
    };
    BottomSwitchBar.prototype.setPingPongFlag = function (flag) {
        this.isPingPong = flag;
    };
    BottomSwitchBar.prototype.setFrame = function (frame) {
        if (this.scene.model.isOrbit == "0") {
            this.updateThumbButtonPositionByFrame(frame);
        }
    };
    BottomSwitchBar.prototype.setVisible = function (visible) {
        this.indicator.setVisible(visible);
        this.thumbButton.setVisible(visible);
        this.sliderPathImage.setVisible(visible);
    };
    return BottomSwitchBar;
}());
var ColorModeButton = /** @class */ (function () {
    function ColorModeButton(scene, x, y, defaultTexture, hightLightTexture, callback) {
        this.scene = scene;
        this.stack = this.scene.add.container(x, y);
        this.stack.setScale(LayoutContants.getInstance().SCALE);
        this.backGroundImage = this.scene.add.image(0, 0, defaultTexture);
        this.stack.depth = 500;
        this.callback = callback;
        this.highlightImage = this.scene.add.image(0, 0, hightLightTexture).setVisible(false);
        this.stack.add(this.backGroundImage);
        this.stack.add(this.highlightImage);
        this.stack.setScale(LayoutContants.getInstance().BUTTONS_SIZE / this.backGroundImage.displayWidth);
        this.bindEvent();
    }
    ColorModeButton.prototype.bindEvent = function () {
        var _this = this;
        var isDown = false;
        this.backGroundImage.setInteractive().on('pointerdown', function (pointer, localX, localY, event) {
            isDown = true;
        }).on('pointermove', function (pointer, localX, localY, event) {
        }).on('pointerup', function (pointer, localX, localY, event) {
            if (isDown) {
                _this.setStatus(true);
                _this.callback();
                isDown = false;
            }
        }).on('pointerout', function () {
            if (isDown) {
                isDown = false;
            }
        });
    };
    ColorModeButton.prototype.setStatus = function (selected) {
        if (selected) {
            this.highlightImage.setVisible(true);
        }
        else {
            this.highlightImage.setVisible(false);
        }
    };
    ColorModeButton.prototype.setVisible = function (visible) {
        this.stack.setVisible(visible);
    };
    return ColorModeButton;
}());
var ColorPalletePanel = /** @class */ (function () {
    function ColorPalletePanel(scene, callback) {
        this.palleteNumber = 10;
        this.scene = scene;
        this.stack = this.scene.add.container(0, 0).setDepth(25);
        this.callback = callback;
        this.initElement();
        this.bindEvent();
    }
    ColorPalletePanel.prototype.initElement = function () {
        var layout = LayoutContants.getInstance();
        this.backgroundBg = this.scene.add.image(0, 0, "BG_PaletteNew");
        this.buttonNext = this.scene.add.image(170, 30, "PaletteArrowR");
        this.buttonPrevious = this.scene.add.image(-130, 30, "PaletteArrowL");
        this.text = this.scene.add.text(0, -25, "1");
        this.text.setColor("#807f7d");
        this.text.setFontSize(100);
        this.text.setFontFamily("Nevis");
        this.stack.add(this.backgroundBg);
        this.stack.add(this.buttonNext);
        this.stack.add(this.buttonPrevious);
        this.stack.add(this.text);
        var scale = layout.BUTTONS_SIZE * 3 / this.backgroundBg.displayWidth;
        this.stackOriginX = layout.SCREEN_WIDTH - this.backgroundBg.displayWidth / 2 * scale;
        this.stack.setPosition(this.stackOriginX, layout.SCREEN_HEIGHT - this.backgroundBg.displayHeight / 2 * scale);
        this.stack.setScale(scale);
        //this.stack.setDepth(99);
    };
    ColorPalletePanel.prototype.correctTextCenter = function () {
        this.text.x = 20 - this.text.displayWidth * 0.5;
    };
    ColorPalletePanel.prototype.setVisible = function (flag) {
        this.stack.setVisible(flag);
    };
    ColorPalletePanel.prototype.bindEvent = function () {
        var _this = this;
        this.backgroundBg.setInteractive().on('poniterdown', function () { });
        this.buttonNext.setInteractive()
            .on('pointerdown', function (pointer, localX, localY, event) {
            _this.timerEvent = _this.scene.time.addEvent({
                delay: 1000,
                callback: function () {
                    _this.autoChangePalleteNumber(true);
                },
                loop: false
            });
        })
            .on('pointerup', function (pointer, localX, localY, event) {
            _this.increasePalleteNumber();
            _this.timerEvent.destroy();
            _this.callback(_this.palleteNumber);
        });
        this.buttonPrevious.setInteractive()
            .on('pointerdown', function (pointer, localX, localY, event) {
            _this.timerEvent = _this.scene.time.addEvent({
                delay: 1000,
                callback: function () {
                    _this.autoChangePalleteNumber(false);
                },
                loop: false
            });
        })
            .on('pointerup', function (pointer, localX, localY, event) {
            _this.decreasePalleteNumber();
            _this.timerEvent.destroy();
            _this.callback(_this.palleteNumber);
        });
    };
    ColorPalletePanel.prototype.getPalleteNumber = function () {
        return this.palleteNumber;
    };
    ColorPalletePanel.prototype.setPalleteNumber = function (pallete) {
        this.palleteNumber = pallete;
        this.text.setText("" + (this.palleteNumber - 9));
        this.correctTextCenter();
    };
    ColorPalletePanel.prototype.increasePalleteNumber = function () {
        if (this.palleteNumber < 80) {
            this.palleteNumber++;
        }
        else {
            this.palleteNumber = 10;
        }
        this.text.setText("" + (this.palleteNumber - 9));
        this.correctTextCenter();
    };
    ColorPalletePanel.prototype.decreasePalleteNumber = function () {
        if (this.palleteNumber > 10) {
            this.palleteNumber--;
        }
        else {
            this.palleteNumber = 80;
        }
        this.text.setText("" + (this.palleteNumber - 9));
        this.correctTextCenter();
    };
    ColorPalletePanel.prototype.autoChangePalleteNumber = function (increase) {
        var _this = this;
        this.timerEvent = this.scene.time.addEvent({
            delay: 200,
            callback: function () {
                if (increase)
                    _this.increasePalleteNumber();
                else
                    _this.decreasePalleteNumber();
                _this.callback(_this.palleteNumber);
            },
            loop: true
        });
    };
    return ColorPalletePanel;
}());
var ColorSwitchBoard = /** @class */ (function () {
    function ColorSwitchBoard(scene, x, y, colormode, madeOff, callback) {
        this.scene = scene;
        this.colorMode = colormode;
        this.stack = this.scene.add.container(x, y);
        this.callback = callback;
        this.madeOff = madeOff;
        if (!madeOff) {
            this.initElement();
        }
    }
    ColorSwitchBoard.prototype.initElement = function () {
        var _this = this;
        //background
        this.colorPalleteModeBG = this.scene.add.image(300, 300, "ModeBG");
        this.colorPalleteModeBG.displayHeight = LayoutContants.getInstance().BUTTONS_SIZE * 1.2;
        this.colorPalleteModeBG.displayWidth = LayoutContants.getInstance().BUTTONS_SIZE * 2.1;
        this.colorPalleteModeBG.setPosition(LayoutContants.getInstance().SCREEN_WIDTH - LayoutContants.getInstance().BUTTONS_SIZE * 2.85, LayoutContants.getInstance().BUTTONS_SIZE * 1.2);
        this.colorPalleteModeBG.setOrigin(0, 0);
        this.stack.add(this.colorPalleteModeBG);
        //default color button
        this.DefaultColorModeBtn = new ColorModeButton(this.scene, this.colorPalleteModeBG.x + this.colorPalleteModeBG.displayWidth * 0.29, this.colorPalleteModeBG.y + this.colorPalleteModeBG.displayHeight / 2, "ModeRainbow", "ModeGlow", function () {
            _this.callback(CST.COLOR_MODE.DEFAULT);
            _this.CustomColorModeBtn.setStatus(false);
            _this.DefaultColorModeBtn.setStatus(true);
        });
        this.DefaultColorModeBtn.setStatus(true);
        this.stack.add(this.DefaultColorModeBtn.stack);
        //custom color button
        this.CustomColorModeBtn = new ColorModeButton(this.scene, this.colorPalleteModeBG.x + this.colorPalleteModeBG.displayWidth * 0.78, this.colorPalleteModeBG.y + this.colorPalleteModeBG.displayHeight / 2, "ModePalette", "ModeGlow", function () {
            _this.callback(CST.COLOR_MODE.CUSTOME);
            _this.DefaultColorModeBtn.setStatus(false);
            _this.CustomColorModeBtn.setStatus(true);
        });
        this.stack.add(this.CustomColorModeBtn.stack);
    };
    ColorSwitchBoard.prototype.getElementHeight = function () {
        if (!this.madeOff) {
            return LayoutContants.getInstance().BUTTONS_SIZE * 1.2;
        }
        else {
            return 0;
        }
    };
    return ColorSwitchBoard;
}());
var CustomButton = /** @class */ (function (_super) {
    __extends(CustomButton, _super);
    function CustomButton(scene, x, y, texture, callback, repeativeInLongPress) {
        var _this = _super.call(this, scene, x, y, texture) || this;
        _this.callback = callback;
        var isDown = false;
        var isPress = false;
        var repeative_task = null; // handler to perform interval task
        _this.scene = scene;
        _this.setInteractive().on('pointerdown', function (pointer, localX, localY, event) {
            isDown = true;
            //check long press event if repeativeInLongPress = true
            if (repeativeInLongPress) {
                if (_this.timer)
                    _this.timer.remove();
                _this.timer = _this.scene.time.addEvent({
                    delay: 600,
                    callback: function () {
                        if (isDown)
                            repeative_task = setInterval(callback, 400);
                    },
                    loop: false
                });
            }
        }).on('pointermove', function (pointer, localX, localY, event) {
        }).on('pointerup', function (pointer, localX, localY, event) {
            if (isDown) {
                if (!repeative_task) // check if it's long press event 
                    callback(); // trigger callback if it's one clicking
                isDown = false; // set pointer down flag as false
                // remove repeative task
                clearInterval(repeative_task);
                repeative_task = null;
                // play effect sound
                if (!_this.soundID) {
                    _this.scene.playSound(CST.SOUND.COLOR_GENERAL_SOUND);
                }
                else {
                    _this.scene.playSound(_this.soundID);
                }
            }
        }).on('pointerout', function () {
            if (isDown) {
                isDown = false;
            }
        });
        _this.scene.add.existing(_this);
        return _this;
    }
    CustomButton.prototype.setSoundType = function (soundID) {
        this.soundID = soundID;
    };
    CustomButton.prototype.setTextre = function (texture) {
        this.setTexture(texture);
        // this.update()
    };
    return CustomButton;
}(Phaser.GameObjects.Sprite));
var CustomImageButton = /** @class */ (function () {
    function CustomImageButton(scene, x, y, texture, callback) {
        this.scene = scene;
        this.stack = this.scene.add.container(x, y);
        this.backGroundImage = this.scene.add.image(0, 0, texture);
        this.text = this.scene.add.text(0, 0, "");
        this.stack.add(this.backGroundImage);
        this.stack.add(this.text);
        this.stack.setScale(LayoutContants.getInstance().HEIGHT_SCALE);
        this.stack.depth = 500;
        this.callback = callback;
        this.backgroundTexture = texture;
        this.highlightTexture = texture;
        this.bindEvent();
    }
    CustomImageButton.prototype.getDisplayWidth = function () {
        return this.backGroundImage.displayWidth * LayoutContants.getInstance().HEIGHT_SCALE;
    };
    CustomImageButton.prototype.setTitle = function (title) {
        this.text.text = title;
        this.text.setColor("0x000000");
        this.text.setFontSize(50);
        this.text.setOrigin(0.5, 0.5);
        this.text.setFontFamily("Nevis");
        if (title == "")
            this.stack.removeAll();
    };
    CustomImageButton.prototype.setHighLightTexture = function (texture) {
        this.highlightTexture = texture;
    };
    CustomImageButton.prototype.bindEvent = function () {
        var _this = this;
        var isDown = false;
        this.backGroundImage.setInteractive().on('pointerdown', function (pointer, localX, localY, event) {
            isDown = true;
        }).on('pointermove', function (pointer, localX, localY, event) {
        }).on('pointerup', function (pointer, localX, localY, event) {
            if (isDown) {
                _this.setStatus(true);
                _this.callback();
                isDown = false;
            }
        }).on('pointerout', function () {
            if (isDown) {
                isDown = false;
            }
        });
    };
    CustomImageButton.prototype.setStatus = function (selected) {
        if (selected) {
            this.backGroundImage.setTexture(this.highlightTexture);
        }
        else {
            this.backGroundImage.setTexture(this.backgroundTexture);
        }
    };
    CustomImageButton.prototype.setVisible = function (visible) {
        this.stack.setVisible(visible);
    };
    CustomImageButton.prototype.setTopLeftOrigin = function () {
        this.backGroundImage.setOrigin(0, 0);
        var dx = this.backGroundImage.displayWidth / 2;
        var dy = this.backGroundImage.displayHeight / 2;
        // this.stack.y += dy;
        // this.stack.x += dx;
    };
    CustomImageButton.prototype.setTopRightOrigin = function () {
        this.backGroundImage.setOrigin(1, 0);
    };
    CustomImageButton.prototype.setRightOrigin = function () {
        this.backGroundImage.setOrigin(1, 0.5);
    };
    //  * added by asset *
    CustomImageButton.prototype.setLeftOrigin = function () {
        this.backGroundImage.setOrigin(0, 0.5);
    };
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    CustomImageButton.prototype.setScale = function (scale) {
        var os = this.stack.scale;
        this.stack.setScale(os * scale);
    };
    return CustomImageButton;
}());
var CustomImageTextureButton = /** @class */ (function () {
    function CustomImageTextureButton(scene, stack, x, y, width, height, texture, counter, callback) {
        this.scene = scene;
        this.stack = stack;
        this.originX = x;
        this.originY = y;
        this.width = width;
        this.height = height;
        this.textHeight = 35;
        this.fontSize = 30;
        this.wrapWidth = 220;
        this.backGroundImage = this.scene.add.image(this.originX, this.originY, texture);
        this.backGroundImage.setDisplaySize(this.width, this.height - this.textHeight);
        this.stack.add(this.backGroundImage);
        var textWidth = this.fontSize * ("" + counter).length;
        var textDx = this.originX + (this.width - textWidth / 2) / 2;
        var textDy = this.originY + this.height - this.fontSize;
        this.text = this.scene.add.text(textDx, textDy, "", { font: "bold " + this.fontSize + "pt Arial" });
        this.text.setText(this.text.getWrappedText(String(counter)));
        // this.text.setFontSize(this.fontSize)
        this.text.setPosition(textDx, textDy);
        this.text.depth = 600;
        // this.text.lineSpacing = 1
        this.text.setWordWrapWidth(this.width);
        this.text.setAlign("center");
        this.stack.add(this.text);
        this.stack.setScale(LayoutContants.getInstance().HEIGHT_SCALE);
        // this.stack.depth = 500;
        this.callback = callback;
        this.backgroundTexture = texture;
        this.highlightTexture = texture;
        this.bindEvent();
    }
    CustomImageTextureButton.prototype.getDisplayWidth = function () {
        return this.backGroundImage.displayWidth * LayoutContants.getInstance().HEIGHT_SCALE;
    };
    CustomImageTextureButton.prototype.updateState = function (title) {
        this.text.text = String(title);
        if (String(title).length) {
            var textWidth = this.fontSize * ("" + title).length;
            var textDx, textDy;
            if (textWidth > this.wrapWidth) {
                textDx = this.originX;
                textDy = this.originY + this.height - this.fontSize;
                this.text.setWordWrapWidth(this.wrapWidth);
                this.text.setText(this.text.getWrappedText(String(title)));
            }
            else {
                textDx = this.originX + (this.width - textWidth) / 2;
                textDy = this.originY + this.height - this.fontSize;
                this.text.setText(String(title));
            }
            this.text.setPosition(textDx, textDy);
            this.text.depth = 600;
            this.text.setAlign("center");
        }
    };
    CustomImageTextureButton.prototype.setTitle = function (title, x, y) {
        this.text.text = title;
        this.text.setColor("0x000000");
        this.text.setFontSize(50);
        this.text.setFontFamily("Nevis");
        if (title == "")
            this.stack.removeAll();
    };
    CustomImageTextureButton.prototype.setHighLightTexture = function (texture) {
        this.highlightTexture = texture;
    };
    CustomImageTextureButton.prototype.bindEvent = function () {
        var _this = this;
        var isDown = false;
        this.backGroundImage.setInteractive().on('pointerdown', function (pointer, localX, localY, event) {
            isDown = true;
        }).on('pointermove', function (pointer, localX, localY, event) {
        }).on('pointerup', function (pointer, localX, localY, event) {
            if (isDown) {
                _this.setStatus(true);
                _this.callback();
                isDown = false;
            }
        }).on('pointerout', function () {
            if (isDown) {
                isDown = false;
            }
        });
    };
    CustomImageTextureButton.prototype.setStatus = function (selected) {
        if (selected) {
            this.backGroundImage.setTexture(this.highlightTexture);
        }
        else {
            this.backGroundImage.setTexture(this.backgroundTexture);
        }
    };
    CustomImageTextureButton.prototype.setVisible = function (visible) {
        // console.log('visible', visible)
        this.backGroundImage.setVisible(visible);
        this.text.setVisible(visible);
    };
    CustomImageTextureButton.prototype.setTopLeftOrigin = function () {
        this.backGroundImage.setOrigin(0, 0);
        var dx = this.backGroundImage.displayWidth / 2;
        var dy = this.backGroundImage.displayHeight / 2;
        // this.stack.y += dy;
        // this.stack.x += dx;
    };
    CustomImageTextureButton.prototype.setTopRightOrigin = function () {
        this.backGroundImage.setOrigin(1, 0);
    };
    CustomImageTextureButton.prototype.setRightOrigin = function () {
        this.backGroundImage.setOrigin(1, 0.5);
    };
    CustomImageTextureButton.prototype.setLeftOrigin = function () {
        this.backGroundImage.setOrigin(0, 0.5);
    };
    CustomImageTextureButton.prototype.setScale = function (scale) {
        var os = this.stack.scale;
        this.stack.setScale(os * scale);
    };
    return CustomImageTextureButton;
}());
var DiceScreenTopBar = /** @class */ (function () {
    function DiceScreenTopBar(scene, x, y, text, price) {
        var _this = this;
        this.scene = scene;
        this.stack = this.scene.add.container(x, y);
        this.backButton = new CustomImageButton(this.scene, 0, 10 * LayoutContants.getInstance().HEIGHT_SCALE, "back_button", function () {
            _this.scene.goSloteScene();
            _this.scene.playSound(CST.SOUND.COLOR_GENERAL_SOUND);
        });
        this.backButton.setTopLeftOrigin();
        this.shopingCartButton = new CustomImageButton(this.scene, LayoutContants.getInstance().SCREEN_WIDTH, 0, "StoreButton", function () {
            _this.scene.playSound(CST.SOUND.COLOR_GENERAL_SOUND);
            if (_this.scene.model.shopID == "") {
                location.href = _this.scene.model.shopLink;
            }
            else {
                var jsonData = Global.getInstance().getSavedModel().getJSONData();
                var event = new CustomEvent('web.start.shoping-cart', { detail: { modelCharacteristics: _this.scene.getModelCharacteristics(), savedModel: jsonData } });
                document.dispatchEvent(event);
            }
        });
        this.shopingCartButton.setTopRightOrigin();
        this.shopingCartButton.setScale(1.3);
        this.backgroundImage = this.scene.add.image(0, 0, "TMenu_Back");
        this.backgroundImage.setOrigin(0, 0);
        this.backgroundImage.setScale(LayoutContants.getInstance().HEIGHT_SCALE);
        this.backgroundImage.displayWidth = LayoutContants.getInstance().SCREEN_WIDTH;
        this.blueBG = this.scene.add.graphics();
        this.blueBG.fillStyle(0x007bff);
        var blueBGX = this.backButton.getDisplayWidth() + 5;
        var blueBGY = this.backgroundImage.displayHeight * 0.05;
        var blueBGW = LayoutContants.getInstance().SCREEN_WIDTH - 2 * blueBGX;
        var blueBGH = this.backgroundImage.displayHeight * 0.9;
        this.blueBG.fillRect(blueBGX, blueBGY, blueBGW, blueBGH);
        this.text = this.scene.add.text(0, 0, text);
        this.priceText = this.scene.add.text(0, 0, price);
        this.setTitle(text, price);
        this.initShopImage();
        this.stack.add(this.backgroundImage);
        this.stack.add(this.blueBG);
        this.stack.add(this.backButton.stack);
        this.stack.add(this.shopingCartButton.stack);
        this.stack.add(this.text);
        this.stack.add(this.priceText);
        this.stack.depth = 100;
    }
    DiceScreenTopBar.prototype.initShopImage = function () {
        if (!this.scene.model.shopSkip && this.scene.model.shopID == "") {
            this.grayBG = this.scene.add.graphics();
            this.grayBG.fillStyle(0xdddddd);
            this.grayBG.fillRect(0, 0, LayoutContants.getInstance().SCREEN_WIDTH, LayoutContants.getInstance().SCREEN_HEIGHT);
            this.shopImage = this.scene.add.image(0, this.backgroundImage.displayHeight, this.scene.model.shopImage);
            if (!LayoutContants.getInstance().isLandScape) {
                this.shopImage.scale = LayoutContants.getInstance().SCREEN_WIDTH / this.shopImage.displayWidth;
            }
            else {
                this.shopImage.scale = (LayoutContants.getInstance().SCREEN_HEIGHT - this.backgroundImage.displayHeight) / this.shopImage.displayHeight;
                this.shopImage.x = LayoutContants.getInstance().SCREEN_WIDTH / 2 - this.shopImage.displayWidth / 2;
            }
            this.shopImage.setOrigin(0, 0);
            this.stack.add(this.grayBG);
            this.stack.add(this.shopImage);
        }
    };
    DiceScreenTopBar.prototype.setTitle = function (title, price) {
        this.text.text = title;
        this.priceText.text = price;
        this.text.setColor("#ffffff");
        this.priceText.setColor("#ffffff");
        this.text.setFontSize(60 * LayoutContants.getInstance().SCALE);
        this.priceText.setFontSize(70 * LayoutContants.getInstance().SCALE);
        this.text.setOrigin(1, 0.5);
        this.priceText.setOrigin(1, 0.5);
        this.text.setFontFamily("Nevis");
        this.priceText.setFontFamily("Nevis");
        this.priceText.x = this.backgroundImage.displayWidth - 1.3 * this.shopingCartButton.getDisplayWidth();
        this.text.x = this.backgroundImage.displayWidth - 1.5 * this.shopingCartButton.getDisplayWidth() - this.priceText.displayWidth - 10;
        this.text.y = this.backgroundImage.displayHeight / 2;
        this.priceText.y = this.backgroundImage.displayHeight / 2;
        this.text.setAlign("right");
        this.priceText.setAlign("right");
    };
    DiceScreenTopBar.prototype.setVisible = function (flag) {
        this.stack.setVisible(flag);
    };
    return DiceScreenTopBar;
}());
var HelperContainer = /** @class */ (function () {
    function HelperContainer(scene, helperIndex) {
        this.helpers = [];
        this.currentHelperIndex = 0;
        this.isFinishedHelp = false;
        this.scene = scene;
        this.helpers = Global.getInstance().getHelperImages();
        this.stack = this.scene.add.container(0, 0);
        this.stack.depth = 1000;
        this.helperIndex = helperIndex;
        this.helperImage = this.scene.add.image(0, 0, this.helpers[helperIndex]);
        if (!LayoutContants.getInstance().isLandScape) {
            this.helperImage.scale = LayoutContants.getInstance().SCREEN_WIDTH * 0.75 / this.helperImage.displayWidth;
        }
        else {
            this.helperImage.scale = LayoutContants.getInstance().SCREEN_HEIGHT * 0.75 / this.helperImage.displayHeight;
        }
        this.helperImage.x = LayoutContants.getInstance().SCREEN_WIDTH / 2;
        this.helperImage.y = LayoutContants.getInstance().SCREEN_HEIGHT / 2;
        this.stack.add(this.helperImage);
        this.addEventLisnter();
    }
    HelperContainer.prototype.addEventLisnter = function () {
        var _this = this;
        if (localStorage.getItem("helper" + this.helperIndex) == "true") {
            this.stack.setVisible(false);
        }
        this.helperImage.setInteractive()
            .on("pointerup", function () {
            if (_this.helperIndex == 2) {
                _this.fadeOut();
            }
            else {
                if (_this.pendingAction) {
                    _this.pendingAction();
                }
                else {
                    _this.stack.setVisible(false);
                }
            }
            localStorage.setItem("helper" + _this.helperIndex, "true");
        });
    };
    HelperContainer.prototype.fadeOut = function () {
        this.scene.tweens.add({
            targets: this.stack,
            alpha: 0,
            ease: 'Cubic.easeOut',
            duration: 1000,
        });
    };
    HelperContainer.prototype.setHelperIndex = function (index) {
        var _this = this;
        if (index == 2 && localStorage.getItem("helper1") != "true") {
            this.pendingAction = function () {
                _this.setIndex(index);
            };
        }
        else {
            this.setIndex(index);
        }
    };
    HelperContainer.prototype.setIndex = function (index) {
        this.helperIndex = index;
        this.helperImage.setTexture(this.helpers[index]);
        this.stack.setVisible(true);
        this.pendingAction = null;
    };
    return HelperContainer;
}());
var ProgressBar = /** @class */ (function () {
    function ProgressBar(scene, backgroundTexture, color) {
        this.scene = scene;
        this.backgroundSlider = this.scene.add.image(0, 0, backgroundTexture);
        this.backgroundSlider.setScale(LayoutContants.getInstance().SCALE);
        var width = this.backgroundSlider.displayWidth - 5;
        var height = this.backgroundSlider.displayHeight - 5;
        this.width = width;
        this.height = height;
        var x = this.scene.uiGenerator.backgroundBoxTopper.displayWidth / 2;
        var y = +this.scene.game.config.height - CST.LAYOUT.BUTTONS_SIZE_BASE / LayoutContants.getInstance().SCREEN_HEIGHT_COEF / 2;
        this.stack = this.scene.add.container(x, y);
        this.color = color;
        this.percent = 0;
        this.progress = this.scene.add.graphics();
        this.progress.fillStyle(color);
        var indicator = this.scene.add.image(-this.backgroundSlider.displayWidth / 2 - 50, 0, "AutoOff");
        indicator.setScale(LayoutContants.getInstance().SCALE);
        this.text = this.scene.add.text(0, 0, "loading 3D");
        this.text.setColor("0x000000");
        this.text.setOrigin(0.5, 0.5);
        this.text.setFontFamily("Nevis");
        this.stack.add(indicator);
        this.stack.add(this.backgroundSlider);
        this.stack.add(this.progress);
        this.stack.add(this.text);
        this.stack.setDepth(2);
        this.setPercent(0);
    }
    ProgressBar.prototype.setPercent = function (percent) {
        this.percent = percent;
        var newwidth = this.width * this.percent;
        if (newwidth < this.height)
            newwidth = this.height;
        this.progress.fillRoundedRect(-this.width / 2, -this.height / 2, newwidth, this.height, this.height / 2);
    };
    ProgressBar.prototype.increasePercent = function () {
        var dp = (1 - this.percent) * 0.1;
        var percent = this.percent + dp;
        this.setPercent(percent);
    };
    ProgressBar.prototype.setVisible = function (flag) {
        this.stack.setVisible(flag);
    };
    return ProgressBar;
}());
var SaveSloteItem = /** @class */ (function () {
    function SaveSloteItem() {
    }
    SaveSloteItem.prototype.setIndex = function (index) {
        this.index = index;
    };
    return SaveSloteItem;
}());
var SettingPanel = /** @class */ (function () {
    function SettingPanel(scene, height, backgroundTexture, callback) {
        var _this = this;
        this.scene = scene;
        this.height = height;
        this.visibleBarHeight = 32 * LayoutContants.getInstance().root_scale;
        this.stack = this.scene.add.container(0, 0);
        this.backgroundImage = this.scene.add.image(0, 0, backgroundTexture);
        this.backgroundImage.displayWidth = LayoutContants.getInstance().SCREEN_WIDTH;
        this.backgroundImage.displayHeight = height;
        this.backgroundImage.setOrigin(0, 0);
        this.deltaY = 202;
        this.settingButton = new ToggleButton(scene, LayoutContants.getInstance().SCREEN_WIDTH / 2, height - (70 + this.deltaY) * LayoutContants.getInstance().HEIGHT_SCALE, "Button_GoToSettings", "Button_GoToSettingsON", callback);
        this.isOn = false;
        this.RateAppButton = new CustomImageButton(scene, LayoutContants.getInstance().SCREEN_WIDTH / 2, height - (320 + this.deltaY) * LayoutContants.getInstance().HEIGHT_SCALE, "Button_RateMe02", function () {
            if (_this.isOn) {
                var event = new CustomEvent('app-link', { detail: "rate" });
                document.dispatchEvent(event);
            }
        });
        this.PrivacyPolicyButton = new CustomImageButton(scene, LayoutContants.getInstance().SCREEN_WIDTH - 200 * LayoutContants.getInstance().SCALE, height - (350 + this.deltaY) * LayoutContants.getInstance().HEIGHT_SCALE, "GDRP", function () {
            if (_this.isOn) {
                var event = new CustomEvent('app-link', { detail: "privacy" });
                document.dispatchEvent(event);
            }
        });
        this.PrivacyPolicyButton.setScale(0.8);
        this.musicButton = new ToggleButton(scene, 300 * LayoutContants.getInstance().SCALE, (400 - this.deltaY) * LayoutContants.getInstance().HEIGHT_SCALE, "ButtonSwitch_OFF", "ButtonSwitch_ON", function (isOn) {
            if (isOn)
                localStorage.setItem("music", 'on');
            else
                localStorage.setItem("music", 'off');
        });
        // initialize toggle button for show or hide debug form on theyes page.
        var url = window.location.href;
        if (url.includes("color_debug")) {
            this.debugButton = new ToggleButton(scene, 300 * LayoutContants.getInstance().SCALE, (600 - 202) * LayoutContants.getInstance().HEIGHT_SCALE, "ButtonSwitch_OFF", "ButtonSwitch_ON", function (isOn) {
                if (isOn)
                    localStorage.setItem("debug", 'on');
                else
                    localStorage.setItem("debug", 'off');
            });
            this.fpsButton = new ToggleButton(scene, LayoutContants.getInstance().SCREEN_WIDTH / 2, (400 - this.deltaY) * LayoutContants.getInstance().HEIGHT_SCALE, "ButtonSwitch_OFF", "ButtonSwitch_ON", function (isOn) {
                if (isOn)
                    localStorage.setItem("fps", 'on');
                else
                    localStorage.setItem("fps", 'off');
            });
        }
        this.soundButton = new ToggleButton(scene, LayoutContants.getInstance().SCREEN_WIDTH - 300 * LayoutContants.getInstance().SCALE, (400 - this.deltaY) * LayoutContants.getInstance().HEIGHT_SCALE, "ButtonSwitch_OFF", "ButtonSwitch_ON", function (isOn) {
            if (isOn)
                localStorage.setItem("sound", 'on');
            else
                localStorage.setItem("sound", 'off');
        });
        this.musicButton.setActive(((localStorage.getItem("music") || 'on') === 'on'));
        this.soundButton.setActive(((localStorage.getItem("sound") || 'on') === 'on'));
        if (this.debugButton)
            this.debugButton.setActive(((localStorage.getItem("debug") || 'off') === 'on')); // intialize value at startup for debug form
        if (this.fpsButton)
            this.fpsButton.setActive(((localStorage.getItem("fps") || 'off') === 'on')); // intialize value at startup for debug form
        this.stack.add(this.backgroundImage);
        this.stack.add(this.settingButton.stack);
        this.stack.add(this.PrivacyPolicyButton.stack);
        this.stack.add(this.RateAppButton.stack);
        this.stack.add(this.musicButton.stack);
        this.stack.add(this.soundButton.stack);
        if (this.debugButton)
            this.stack.add(this.debugButton.stack); // add debug toggle button in view
        if (this.fpsButton)
            this.stack.add(this.fpsButton.stack); // add fps toggle button in view
        this.initText();
        this.initSetting();
    }
    SettingPanel.prototype.initText = function () {
        var textRateApp = this.scene.add.text(0, -200, "Rate App");
        textRateApp.setColor("#807f7d");
        textRateApp.setFontSize(100);
        textRateApp.setFontFamily("Nevis");
        textRateApp.x = -textRateApp.displayWidth / 2;
        this.RateAppButton.stack.add(textRateApp);
        var textPrivacy = this.scene.add.text(0, -180, "Privacy");
        textPrivacy.setColor("#807f7d");
        textPrivacy.setFontSize(50);
        textPrivacy.setFontFamily("Nevis");
        textPrivacy.x = -this.PrivacyPolicyButton.backGroundImage.displayWidth / 2;
        this.PrivacyPolicyButton.stack.add(textPrivacy);
        var textMusic = this.scene.add.text(0, -150, "Music");
        textMusic.setColor("#807f7d");
        textMusic.setFontSize(100);
        textMusic.setFontFamily("Nevis");
        textMusic.x = -textMusic.displayWidth / 2;
        this.musicButton.stack.add(textMusic);
        if (this.debugButton) {
            var textDebug = this.scene.add.text(0, -150, "Debug");
            textDebug.setColor("#807f7d");
            textDebug.setFontSize(100);
            textDebug.setFontFamily("Nevis");
            textDebug.x = -textDebug.displayWidth / 2;
            this.debugButton.stack.add(textDebug);
        }
        if (this.fpsButton) {
            var textFPS = this.scene.add.text(0, -150, "FPS");
            textFPS.setColor("#807f7d");
            textFPS.setFontSize(100);
            textFPS.setFontFamily("Nevis");
            textFPS.x = -textFPS.displayWidth / 2;
            this.fpsButton.stack.add(textFPS);
        }
        var textSound = this.scene.add.text(0, -150, "Sound FX");
        textSound.setColor("#807f7d");
        textSound.setFontSize(100);
        textSound.setFontFamily("Nevis");
        textSound.x = -textSound.displayWidth / 2;
        this.soundButton.stack.add(textSound);
        this.availableDistance = (-850 + this.deltaY) * LayoutContants.getInstance().HEIGHT_SCALE;
        this.stack.y = this.availableDistance + this.visibleBarHeight;
    };
    SettingPanel.prototype.getHeight = function () {
        return this.height;
    };
    SettingPanel.prototype.setDepth = function (depth) {
        this.stack.depth = depth;
    };
    SettingPanel.prototype.initSetting = function () {
        var isMusicOn = (localStorage.getItem("music") || 'on') === 'on';
        var isSoundOn = (localStorage.getItem("music") || 'on') === 'on';
    };
    SettingPanel.prototype.setOn = function (isOn) {
        this.isOn = isOn;
    };
    return SettingPanel;
}());
var LEFTMOST_SLIDER_POSITION = 0;
var RIGHTMOST_SLIDER_POSITION = 8;
var TMenuButton = /** @class */ (function () {
    function TMenuButton(bodyPart) {
        this.LEFTMOST_SLIDER_POSITION = 0;
        this.RIGHTMOST_SLIDER_POSITION = 8;
        this.blinkCurColor = new Phaser.Display.Color();
        this.bodyPart = bodyPart;
        this.scene = this.bodyPart.scene;
        this.bodyPartsManager = bodyPart.bodyPartsManager;
        this.stack = new Phaser.GameObjects.Container(this.scene);
        this.lightBlinkingColor = new Phaser.Display.Color(255, 255, 255);
        this.darkBlinkingColor = new Phaser.Display.Color(255, 255, 255);
        this.uiGenerator = this.bodyPart.scene.uiGenerator;
        this.createImage();
        this.interpolatingCoef = 0;
        this.interpolatingCoefIncrement = 1;
        // blink features Edied by SY
        this.blinkProgress = 0;
        this.blinkRepeatNum = 0;
        this.blinkRepeatLimit = 3;
        this.blinkIncrement = 14;
    }
    TMenuButton.prototype.createImage = function () {
        this.atlasFront = this.scene.getHDAtalasFront(); //model_name-front
        var buttonName = this.bodyPart.modelPart.feature.tMenuButton;
        this.image = new Phaser.GameObjects.Image(this.scene, 0, 0, "" + this.atlasFront, buttonName + ".png");
        this.image.displayHeight = LayoutContants.getInstance().BUTTONS_SIZE * 1.2;
        this.image.displayWidth = LayoutContants.getInstance().BODY_PART_BUTTON_WIDTH / LayoutContants.getInstance().SCREEN_SIZE_COEF;
        this.stack.add(this.image);
        // this.stack.setPosition(this.image.displayWidth/2, this.image.displayHeight/2);
    };
    TMenuButton.prototype.onTapButton = function () {
        this.performSelection();
        // setting blink start
        this.resetColorForBlinking();
        this.scene.setBlinkingEnabled(true);
        this.scene.playSound(CST.SOUND.COLOR_GENERAL_SOUND);
        this.uiGenerator.setColorModelFromSelectedBodyPart();
        var colorButton = this.uiGenerator.getSelectedBodyPart().colorButton;
        var buttonIndex = 0;
        if (colorButton)
            buttonIndex = colorButton.buttonIndex;
        this.uiGenerator.setColorSelection(buttonIndex); // set color for current selected bodypart 
        this.uiGenerator.resetMarkInColorButtons();
        this.uiGenerator.createUserAction();
    };
    TMenuButton.prototype.performSelection = function () {
        this.bodyPartsManager.setSelectedBodyPart(this.bodyPart);
        this.uiGenerator.setSelectedTMenuButton(this);
        var colorButton = this.uiGenerator.getSelectedColorButton();
        if (!colorButton.isMarked) {
            colorButton.setMarked(true);
        }
        this.uiGenerator.hideFloatColorSlider();
        this.bodyPartsManager.setLinkedBodyParts();
        this.uiGenerator.setMaterialButtons(this.atlasFront);
        this.uiGenerator.setColorsToButtons();
        this.bodyPartsManager.setMaterial(this.bodyPartsManager.selectedBodyPart.selectedTabIndex);
        // if (checkIfContainsBodypart(this.bodyPartsManager.hiddenBodyParts, this.bodyPart)) {
        //     // this.bodyPartsManager.showBodyParts();
        // }
    };
    TMenuButton.prototype.resetColorForBlinking = function () {
        //set initilal values Edited by SY
        this.blinkProgress = this.blinkIncrement;
        this.blinkRepeatNum = 0;
        this.blinkCurColor = this.uiGenerator.selectedTMenuButton.color;
        this.blinkInitialColors();
        this.bodyPart.initColor();
        //setTimeout(()=>{this.stopBlinkWorking()}, 3000);
    };
    // Created funtion by SY
    // blinkInitialColors(){
    //     let s_v = 0.5, r_v= 0.5;
    //     let hsv = Phaser.Display.Color.RGBToHSV(this.blinkCurColor.red, this.blinkCurColor.green, this.blinkCurColor.blue);
    //     //V
    //     if(hsv.v + 0.25 > 1)
    //     {
    //         s_v = 1;
    //         r_v = 0.5;
    //     }
    //     else if(hsv.v - 0.25< 0)
    //     {
    //         s_v= 0;
    //         r_v = 0.5;
    //     }
    //     else
    //     {
    //         s_v = hsv.v + 0.25;
    //         r_v = hsv.v - 0.25;
    //     }
    //     this.blinkStartColor = new Phaser.Display.Color();
    //     this.blinkreqColor = new Phaser.Display.Color();
    //     Phaser.Display.Color.HSVToRGB(hsv.h, hsv.s, s_v, this.blinkStartColor);
    //     Phaser.Display.Color.HSVToRGB(hsv.h, hsv.s, r_v, this.blinkreqColor);
    // }
    TMenuButton.prototype.blinkInitialColors = function () {
        if (this.blinkCurColor == undefined)
            this.blinkCurColor = new Phaser.Display.Color();
        var r_v = 0.5;
        var hsv = Phaser.Display.Color.RGBToHSV(this.blinkCurColor.red, this.blinkCurColor.green, this.blinkCurColor.blue);
        //V
        if (hsv.v + 0.5 > 1) {
            r_v = 0;
        }
        else if (hsv.v - 0.5 < 0) {
            r_v = 1;
        }
        else {
            r_v = 1;
        }
        this.blinkStartColor = new Phaser.Display.Color();
        this.blinkreqColor = new Phaser.Display.Color();
        Phaser.Display.Color.HSVToRGB(hsv.h, hsv.s, hsv.v, this.blinkStartColor);
        Phaser.Display.Color.HSVToRGB(hsv.h, hsv.s, r_v, this.blinkreqColor);
    };
    TMenuButton.prototype.setColor = function (color) {
        this.color = color;
        this.image.tint = Phaser.Display.Color.GetColor(color.red, color.green, color.blue);
        this.resetColorForBlinking();
    };
    TMenuButton.prototype.blink = function (callback) {
        if (this.blinkRepeatNum > this.blinkRepeatLimit) {
            this.bodyPart.setBodyPartColor(this.blinkCurColor);
            callback();
            return;
        }
        else {
            if (this.blinkProgress < 100) {
                var colorObject = Phaser.Display.Color.Interpolate.ColorWithColor(this.blinkStartColor, this.blinkreqColor, 100, this.blinkProgress);
                this.bodyPart.setBodyPartColor(Phaser.Display.Color.ObjectToColor(colorObject));
                this.blinkProgress += this.blinkIncrement;
            }
            else {
                var temp = this.blinkStartColor;
                this.blinkStartColor = this.blinkreqColor;
                this.blinkreqColor = temp;
                this.blinkProgress = this.blinkIncrement;
                this.blinkRepeatNum += 1;
            }
        }
    };
    TMenuButton.prototype.getDisplayHeight = function () {
        return this.image.displayHeight;
    };
    TMenuButton.prototype.getDisplayWidth = function () {
        return this.image.displayWidth;
    };
    return TMenuButton;
}());
var ToggleButton = /** @class */ (function () {
    function ToggleButton(scene, x, y, defaultTexture, highlightTexture, callback) {
        this.scene = scene;
        this.defaultTexture = defaultTexture;
        this.hightlightTexture = highlightTexture;
        this.callback = callback;
        this.backgroundImage = this.scene.add.image(0, 0, defaultTexture);
        this.stack = this.scene.add.container(x, y);
        this.stack.add(this.backgroundImage);
        this.stack.setScale(LayoutContants.getInstance().HEIGHT_SCALE);
        this.isON = false;
        this.initElements();
        this.initEvent();
    }
    ToggleButton.prototype.initElements = function () {
        if (this.isON) {
            this.backgroundImage.setTexture(this.hightlightTexture);
        }
        else {
            this.backgroundImage.setTexture(this.defaultTexture);
        }
    };
    ToggleButton.prototype.initEvent = function () {
        var _this = this;
        this.backgroundImage.setInteractive().on('pointerdown', function (pointer, localX, localY, event) {
        }).on('pointermove', function (pointer, localX, localY, event) {
        }).on('pointerup', function (pointer, localX, localY, event) {
            _this.setToggle();
            _this.callback(_this.isON);
        });
    };
    ToggleButton.prototype.setToggle = function () {
        var isOn = !this.isON;
        if (isOn) {
            this.backgroundImage.setTexture(this.hightlightTexture);
        }
        else {
            this.backgroundImage.setTexture(this.defaultTexture);
        }
        this.isON = isOn;
    };
    ToggleButton.prototype.setActive = function (flag) {
        this.isON = flag;
        if (this.isON) {
            this.backgroundImage.setTexture(this.hightlightTexture);
        }
        else {
            this.backgroundImage.setTexture(this.defaultTexture);
        }
    };
    return ToggleButton;
}());
var ColorButton = /** @class */ (function () {
    function ColorButton(scene, buttonIndex) {
        this.SECOND = 1000;
        this.color = new Phaser.Display.Color(255, 255, 255);
        this.mainSelectorColor = new Phaser.Display.Color(255, 255, 255);
        this.scene = scene;
        this.uiGenerator = scene.uiGenerator;
        this.buttonIndex = buttonIndex;
        this.bodyPartsManager = scene.bodyPartsManager;
        this.stack = new Phaser.GameObjects.Container(scene);
        this.colorSelector = new Phaser.GameObjects.Image(scene, 0, 0, "SelectHoop");
        this.coloredBodyPartsCount = [];
        this.depth = 30;
    }
    ColorButton.prototype.setImages = function (colorButtonMaterial) {
        this.material = colorButtonMaterial;
        this.setImage(colorButtonMaterial);
        this.setGlossImage(colorButtonMaterial);
        this.resetStack();
    };
    ColorButton.prototype.setImage = function (colorbuttonMaterial) {
        var atlasFront = this.scene.getHDAtalasFront();
        this.clayImage = new Phaser.GameObjects.Image(this.scene, 0, 0, "" + atlasFront, colorbuttonMaterial.clayButtonRegion + ".png");
        // this.clayImage.texture.setFilter(Phaser.Textures.FilterMode.LINEAR);
        //this.clayImage.setDepth(21);
        this.clayImage.setScale(LayoutContants.getInstance().BUTTONS_SIZE / this.clayImage.displayWidth);
        this.setColor(this.color, this.colorModeNumber);
    };
    ColorButton.prototype.setGlossImage = function (colorbuttonMaterial) {
        var atlasFront = this.scene.getHDAtalasFront();
        this.glossImage = new Phaser.GameObjects.Image(this.scene, 0, 0, "" + atlasFront, colorbuttonMaterial.glossButtonRegion + ".png");
        //this.glossImage.setDepth(20);
        // this.glossImage.texture.setFilter(Phaser.Textures.FilterMode.LINEAR);
        this.glossImage.setScale(LayoutContants.getInstance().BUTTONS_SIZE / this.glossImage.displayWidth);
        this.glossImage.setAlpha(+colorbuttonMaterial.glossButtonAlpha);
    };
    ColorButton.prototype.resetStack = function () {
        this.stack.removeAll();
        this.stack.add(this.clayImage);
        this.stack.add(this.glossImage);
        if (this.isMarked) {
            this.addMainSelector();
        }
    };
    ColorButton.prototype.setImagesStack = function () {
        if (this.isMarked) {
            this.addMainSelector();
        }
    };
    ColorButton.prototype.addMainSelector = function () {
        this.setMainSelectorColor();
        var _r = Math.min(255, this.color.red + 150);
        var _g = Math.min(255, this.color.green + 150);
        var _b = Math.min(255, this.color.blue + 150);
        this.colorSelector.setTint(Phaser.Display.Color.GetColor(_r, _g, _b));
        this.colorSelector.setScale(LayoutContants.getInstance().MAIN_COLOR_SELECTION_SCALE);
        this.stack.add(this.colorSelector);
    };
    ColorButton.prototype.removeMark = function () {
        this.colorSelector.setVisible(false);
    };
    ColorButton.prototype.makeMark = function () {
        var _r = Math.min(255, this.color.red + 150);
        var _g = Math.min(255, this.color.green + 150);
        var _b = Math.min(255, this.color.blue + 150);
        this.colorSelector.setTint(Phaser.Display.Color.GetColor(_r, _g, _b));
        this.colorSelector.setVisible(true);
    };
    ColorButton.prototype.performSelection = function (isUndo) {
        if (isUndo === void 0) { isUndo = false; }
        this.scene.stopBlinking(); // stop blinking effect of bodypart
        this.deselectCurrentlySelectedColorButton(); //setting selected status
        this.setSelected(true); // set selected flag var as true
        var sprite = this.bodyPartsManager.selectedBodyPart.claySprites[this.scene.firstFrame];
        if (!this.bodyPartsManager.selectedBodyPart.isBackground()) {
            if (sprite == undefined) {
                this.bodyPartsManager.setMaterial(1);
            }
            else if (sprite != undefined) {
                if (sprite.name == "empty") {
                    if (!isUndo) {
                        this.bodyPartsManager.setMaterial(1);
                    }
                    else {
                        this.bodyPartsManager.setMaterial(0);
                    }
                }
            }
        }
        this.uiGenerator.setColorSelection(this.buttonIndex);
        if (this.bodyPartsManager.selectedBodyPart.isParent()) // if has childs, change them together
            this.uiGenerator.setChildLinkedColor();
    };
    ColorButton.prototype.setMarked = function (isMarked) {
        this.isMarked = isMarked;
        if (isMarked) {
            this.addMainSelector();
            return;
        }
        this.removeMainSelector();
    };
    ColorButton.prototype.deselectCurrentlySelectedColorButton = function () {
        var selectedColorButton = this.uiGenerator.getSelectedColorButton();
        selectedColorButton.removeFromColoredBodyPartsCount();
    };
    ColorButton.prototype.addToColoredBodyPartsCount = function () {
        this.coloredBodyPartsCount.push(1);
        this.setMarked(true);
    };
    ColorButton.prototype.removeFromColoredBodyPartsCount = function () {
        if (this.coloredBodyPartsCount.length != 0) {
            this.coloredBodyPartsCount.pop();
        }
        if (this.coloredBodyPartsCount.length == 0) {
            this.setMarked(false);
        }
    };
    ColorButton.prototype.removeMainSelector = function () {
        this.stack.remove(this.colorSelector);
    };
    ColorButton.prototype.setMainSelectorColor = function () {
        this.mainSelectorColor = new Phaser.Display.Color(this.color.red, this.color.green, this.color.blue, this.color.alpha);
    };
    ColorButton.prototype.setColor = function (color, colorModeNumber) {
        this.colorModeNumber = colorModeNumber;
        this.color = color;
        this.clayImage.tint = Phaser.Display.Color.GetColor32(color.red, color.green, color.blue, color.alpha);
    };
    ColorButton.prototype.setSelected = function (selected) {
        this.isSelected = selected;
    };
    ColorButton.prototype.onTapButton = function () {
        this.performSelection();
        this.scene.callColorPoofer();
        this.uiGenerator.createUserAction();
        this.scene.playSound(CST.SOUND.COLOR_CHANGE_SOUND);
    };
    ColorButton.prototype.onPressButton = function (start) {
        var _this = this;
        if (start) {
            this.isPressed = true;
            this.scene.time.addEvent({
                delay: 500,
                callback: function () {
                    // spawn a new apple
                    if (_this.isPressed) {
                        // console.log("press start"+start);        //this.startMainScene();
                    }
                },
                loop: false
            });
        }
        else {
            // if (!uiGenerator.getFloatColorSlider().isVisible() && x < 0 && y < 0) {
            //     isPressed = false;
            //     return;
            // }
            this.isPressed = false;
            this.performSelection();
            this.scene.callColorPoofer();
            this.uiGenerator.createUserAction();
            this.scene.stopBlinking();
        }
    };
    ColorButton.prototype.setPosition = function (x, y) {
        this.globalX = x;
        this.globalY = y;
    };
    return ColorButton;
}());
var ColorProoferSprite = /** @class */ (function (_super) {
    __extends(ColorProoferSprite, _super);
    function ColorProoferSprite(scene, texture, frame) {
        var _this = _super.call(this, scene, 500, 500, texture, frame) || this;
        scene.add.existing(_this);
        _this.scene = scene;
        // this.animate(scene);
        _this.depth = 100;
        _this.scale = 3;
        _this.setVisible(false);
        _this.scene = _this.scene;
        _this.scene.anims.create({
            key: 'color-proof',
            repeat: 0,
            frameRate: 23,
            frames: _this.scene.anims.generateFrameNames('proof', {
                prefix: 'Poof.',
                suffix: '.png',
                start: 1,
                end: 23,
                zeroPad: 1
            }),
        });
        _this.on('animationcomplete', function (animation, frame) {
            _this.setVisible(false);
        }, _this);
        return _this;
    }
    ;
    ColorProoferSprite.prototype.startColorProof = function (color, posX, posY) {
        this.tint = Phaser.Display.Color.GetColor(color.red, color.green, color.blue);
        this.setVisible(true);
        this.x = posX;
        this.y = posY;
        this.play("color-proof");
    };
    return ColorProoferSprite;
}(Phaser.GameObjects.Sprite));
;
var ModelItem = /** @class */ (function () {
    function ModelItem(scene, scrollMode, model) {
        this.scene = scene;
        this.model = model;
        this.scrollMode = scrollMode;
        this.fileNames = new FileNames().generateFileNames(model, true);
        this.stack = new Phaser.GameObjects.Container(scene);
        this.initGroup(model);
    }
    ;
    ModelItem.prototype.initGroup = function (model) {
        if (this.getSelectedModelArray().includes(this.model.iconName)) {
            this.backgroundImage = new Phaser.GameObjects.Image(this.scene, 0, 0, 'IAPicon_BGBlue');
        }
        else {
            this.backgroundImage = new Phaser.GameObjects.Image(this.scene, 0, 0, 'IAPicon_BGGrey');
        }
        this.backgroundImage.setOrigin(0, 0);
        this.modelButton = new Phaser.GameObjects.Image(this.scene, 0, 0, model.iconName);
        this.modelButton.setOrigin(0, 0);
        this.stack.add(this.backgroundImage);
        this.stack.add(this.modelButton);
    };
    ModelItem.prototype.updateBackground = function (flag) {
        if (flag) {
            this.backgroundImage.setTexture("IAPicon_BGBlue");
        }
        else {
            this.backgroundImage.setTexture("IAPicon_BGGrey");
        }
        if (!this.getSelectedModelArray().includes(this.model.iconName)) {
            var new_selected_models_str = localStorage.getItem("selected_models") + "," + this.model.iconName;
            localStorage.setItem("selected_models", new_selected_models_str);
        }
    };
    ModelItem.prototype.getSelectedModelArray = function () {
        var selected_models_str = localStorage.getItem("selected_models");
        if (selected_models_str) {
            return selected_models_str.split(",");
        }
        else {
            return [];
        }
    };
    ModelItem.prototype.getDisplayWidth = function () {
        return this.backgroundImage.width * this.stack.scale;
    };
    ModelItem.prototype.getDisplayHeight = function () {
        return this.backgroundImage.height * this.stack.scale;
    };
    ModelItem.prototype.setDisplayWidth = function (displayWidth) {
        var scale = displayWidth / this.backgroundImage.displayWidth;
        this.stack.setScale(scale);
    };
    return ModelItem;
}());
var BACKGROUND = "Background";
var FADE = "Fade";
var SHADOW = "Shadow";
var NOTHING = "tmp_Nada";
var BodyPart = /** @class */ (function () {
    function BodyPart(scene, bodyPartsManager, modelPart, bodyPartIndex) {
        this.selectedTabIndex = 0;
        this.selectedChildBodyPartIndex = -1;
        this.depend = true;
        this.color = new Phaser.Display.Color(255, 255, 255);
        this.palleteNumber = 4;
        this.dx = 0;
        this.dy = 0;
        this.colorModeNumber = 0;
        this.scene = scene;
        this.bodyPartsManager = bodyPartsManager;
        this.modelPart = modelPart;
        this.bodyPartIndex = bodyPartIndex;
        this.material = modelPart.materials[0];
        this.claySprites = [];
        this.glossSprites = [];
        this.solidGreySprite = scene.uiGenerator.solidGreySprite;
    }
    BodyPart.prototype.getChildLink = function () {
        return this.modelPart.feature.colorLink;
    };
    BodyPart.prototype.setChildLink = function (colorLink) {
        this.modelPart.feature.colorLink = colorLink;
    };
    BodyPart.prototype.getParnetBodyPart = function () {
        var parentId = this.modelPart.feature.parent_id;
        var result = this.bodyPartsManager.bodyParts.filter(function (it) { return it.modelPart.feature.part_id == parentId; });
        if (result.length > 0) {
            return result[0];
        }
    };
    BodyPart.prototype.setTMenuButton = function () {
        var buttonName = this.modelPart.feature.tMenuButton;
        if (buttonName != null && buttonName != "") {
            this.tMenuButton = new TMenuButton(this);
        }
    };
    BodyPart.prototype.setChildBodyPartButton = function () {
        var buttonName = this.modelPart.feature.tMenuButton;
        if (buttonName != null && buttonName != "") {
            this.childBodyPartButton = new ChildBodyPartButton(this);
        }
    };
    BodyPart.prototype.setMaterial = function (tabIndex) {
        this.material = this.modelPart.materials[tabIndex];
        this.selectedTabIndex = tabIndex;
        // if (game.isSavesSlotsScreen()) {
        //     setSpriteFromMaterial(game.getFirstFrame());
        //     setColor();
        //     return;
        // }
        for (var frame = this.scene.minFrame; frame < this.scene.maxFrame + 1; frame++) {
            this.setSpriteFromMaterial(frame);
            this.initColor();
        }
    };
    BodyPart.prototype.setChildBodyPart = function (childBodyPartIndex) {
        this.selectedChildBodyPartIndex = childBodyPartIndex;
        var results = this.bodyPartsManager.bodyParts.filter(function (it) { return it.modelPart.feature.part_id == childBodyPartIndex; });
        if (results.length > 0) {
            this.childBodyPart = results[0];
        }
    };
    BodyPart.prototype.isBackground = function () {
        return this.modelPart.partName.includes("Background");
    };
    BodyPart.prototype.setSpriteFromMaterial = function (frame) {
        if (this.material == null) {
            return;
        }
        if (this.material.clayRegion != null && this.material.clayRegion != "") {
            this.setClaySpriteFromMaterial(frame);
        }
        if (this.material.glossRegion != null && this.material.glossRegion != "") {
            this.setGlossSpriteFromMaterial(frame);
        }
        if (this.material.glossAlpha != null) {
            var glossAlpha = this.material.glossAlpha;
            if (this.glossSprites[frame] != null) {
                this.glossSprites[frame].setAlpha(glossAlpha);
            }
        }
    };
    BodyPart.prototype.updateBodyPart = function (frame) {
        var clayRegion = "" + this.material.clayRegion + frame;
        this.bodyPartsManager.updateSprite(clayRegion, this.modelPart, this.claySprites[frame]);
        var glossRegion = "" + this.material.glossRegion + frame;
        this.bodyPartsManager.updateSprite(glossRegion, this.modelPart, this.glossSprites[frame]);
    };
    BodyPart.prototype.setClaySpriteFromMaterial = function (frame) {
        // if (this.material.clayRegion.includes(NOTHING)) {
        //     if(this.claySprites[frame]){
        //         this.claySprites[frame].destroy();
        //         this.claySprites[frame] = null;
        //     }
        //     return;
        // }
        var clayRegion = "" + this.material.clayRegion + frame;
        this.claySprites[frame] = this.bodyPartsManager.setSprite(clayRegion, this.modelPart, this.claySprites[frame], frame);
        if (this.claySprites[frame]) {
            if (this.modelPart.feature.isChildren) {
                // 
                this.claySprites[frame].linkChildBodyPartButtonIndex(this.childBodyPartButton.bodyPart.modelPart.feature.part_id);
            }
            else {
                this.claySprites[frame].linkTmenuButtonIndex(this.tMenuButton.bodyPart.modelPart.tMenuOrder);
            }
            this.claySprites[frame].setFeature(this.modelPart.feature);
            this.claySprites[frame].makePixcelPerfectTouchEvent();
        }
    };
    BodyPart.prototype.setGlossSpriteFromMaterial = function (frame) {
        // if (this.material.glossRegion.includes(NOTHING)) {
        //     if(this.glossSprites[frame]){
        //         this.glossSprites[frame].destroy();
        //         this.glossSprites[frame] = null;
        //     }
        //     return;
        // }
        if (this.material.glossRegion.includes(FADE)) {
            var newFadeImage = "" + this.material.glossRegion;
            this.scene.uiGenerator.setBackground(this.bodyPartsManager.backgroundTexture, newFadeImage);
            return;
        }
        var glossRegion = "" + this.material.glossRegion + frame;
        this.glossSprites[frame] = this.bodyPartsManager.setSprite(glossRegion, this.modelPart, this.glossSprites[frame], frame);
    };
    BodyPart.prototype.showSprites = function () {
        this.claySprites.forEach(function (element) {
            if (element != null) {
                element.visible = true;
            }
        });
        this.isHidden = false;
    };
    BodyPart.prototype.hideSprites = function () {
        this.claySprites.forEach(function (element) {
            if (element != null) {
                element.setVisible(false);
            }
        });
        this.glossSprites.forEach(function (element) {
            if (element != null) {
                element.setVisible(false);
            }
        });
        this.isHidden = true;
    };
    BodyPart.prototype.setColor = function (colorButton) {
        this.colorButton = colorButton;
        this.color = colorButton.color;
        this.setSelectorColor(this.color);
    };
    BodyPart.prototype.setSelectorColor = function (color) {
        this.color = color;
        if (this.isChild()) {
            if (this.childBodyPartButton != null) {
                this.childBodyPartButton.setColor(color);
            }
        }
        else {
            if (this.tMenuButton != null) {
                this.tMenuButton.setColor(color);
            }
        }
        this.initColor();
    };
    BodyPart.prototype.initColor = function () {
        var _this = this;
        this.claySprites.forEach(function (element) {
            if (element != undefined) {
                element.tint = Phaser.Display.Color.GetColor(_this.color.red, _this.color.green, _this.color.blue);
            }
        });
    };
    BodyPart.prototype.setSprites = function (frame) {
        if (this.material != null) {
            this.setSpriteFromMaterial(frame);
            // this.setSpriteFromMaterial(frame, false);
        }
        if (this.modelPart.partName.includes(BACKGROUND)) {
            this.backgroundTexture = this.bodyPartsManager.getBackgroundTexture();
            this.fadeTexture = this.bodyPartsManager.getFadeTexture();
            this.scene.uiGenerator.setBackground(this.backgroundTexture, this.fadeTexture);
            // (ScreenManager.getNewScreen()).setBackgroundCoordinates(backgroundSprite, BACKGROUND_SPRITE);
            // (ScreenManager.getNewScreen()).setBackgroundCoordinates(fadeSprite, FADE_SPRITE);
            return;
        }
        if (this.modelPart.partName.includes(SHADOW)) {
            // const glossRegionName = `${this.modelPart.partGlossImage}${frame}`;
            // this.glossSprites[frame] = this.bodyPartsManager.setSprite(glossRegionName, this.modelPart, this.glossSprites[frame]);
            return;
        }
        if (this.modelPart.partBaseImage != null) {
            this.setRegularSprites(frame);
            this.checkPartToggle(frame);
        }
    };
    BodyPart.prototype.setRegularSprites = function (frame) {
        if (!this.modelPart.partBaseImage.includes(NOTHING)) {
            if (this.modelPart.partBaseImage != "") {
                var clayRegionName = "" + this.modelPart.partBaseImage + frame;
                this.claySprites[frame] = this.bodyPartsManager.setSprite(clayRegionName, this.modelPart, this.claySprites[frame], frame);
            }
        }
        if (this.modelPart.partBaseImage != null) {
            if (!this.modelPart.partGlossImage.includes(NOTHING) && !this.modelPart.partGlossImage.includes(BACKGROUND)) {
                if (this.modelPart.partGlossImage != "") {
                    var glossRegionName = "" + this.modelPart.partGlossImage + frame;
                    this.glossSprites[frame] = this.bodyPartsManager.setSprite(glossRegionName, this.modelPart, this.glossSprites[frame], frame);
                }
            }
        }
    };
    BodyPart.prototype.checkPartToggle = function (frame) {
        var partToggle = this.modelPart.feature.partToggle;
        if (partToggle == null) {
            return;
        }
        if (frame == this.scene.firstFrame) {
            if (!partToggle.includes("1")) {
                // this.hideSprites();
            }
            else {
                this.showSprites();
            }
            return;
        }
        if (this.isHidden) {
            this.hideSprites();
            this.bodyPartsManager.hiddenBodyParts.push(this);
        }
        else {
            this.showSprites();
            this.bodyPartsManager.shownBodyParts.push(this);
        }
    };
    BodyPart.prototype.setPalleteNumber = function (palleteNumber) {
        if (palleteNumber < 9) {
            this.palleteNumber = palleteNumber;
        }
        else {
            this.colorModeNumber = palleteNumber;
        }
    };
    BodyPart.prototype.setInitialPalletNumber = function (palleteNumber) {
        if (palleteNumber < 9) {
            this.palleteNumber = palleteNumber;
            this.colorModeNumber = 0;
        }
        else {
            this.palleteNumber = 4;
            this.colorModeNumber = palleteNumber;
        }
    };
    BodyPart.prototype.render = function (frame) {
        // console.log(frame)
        // if(this.scene.isFileLoaded){
        //     this.hideSprites();
        // }
        this.hideSprites();
        // if (this.fadeTexture != null) {
        //     if(!this.scene.blinkingEnabled){
        //         this.bodyPartsManager.uiGenerator.backgroundSprite.tint = Phaser.Display.Color.GetColor(this.color.red, this.color.green, this.color.blue);
        //         this.bodyPartsManager.uiGenerator.solidGreySprite.tint = Phaser.Display.Color.GetColor(this.color.red, this.color.green, this.color.blue);
        //     }
        // }
        if (this.glossSprites[frame] != null) {
            var element = this.glossSprites[frame];
            element.setDepth(21);
            element.setVisible(true);
        }
        if (this.claySprites[frame] != null) {
            var element = this.claySprites[frame];
            //console.log("bodypart.render", element);
            element.setDepth(20);
            element.setVisible(true);
            if (!this.scene.blinkingEnabled) {
                element.tint = Phaser.Display.Color.GetColor(this.color.red, this.color.green, this.color.blue);
            }
        }
    };
    // Edited by SY
    BodyPart.prototype.setBodyPartColor = function (color) {
        if (this.backgroundTexture != null) {
            this.bodyPartsManager.uiGenerator.backgroundSprite.tint = Phaser.Display.Color.GetColor(color.red, color.green, color.blue);
            this.bodyPartsManager.uiGenerator.solidGreySprite.tint = Phaser.Display.Color.GetColor(color.red, color.green, color.blue);
            // this.bodyPartsManager.uiGenerator.fadeSprite.tint = Phaser.Display.Color.GetColor(color.red, color.green, color.blue);
            return;
        }
        this.claySprites.forEach(function (element) {
            if (element != undefined) {
                element.tint = Phaser.Display.Color.GetColor(color.red, color.green, color.blue);
            }
        });
    };
    BodyPart.prototype.setScale = function (scale, frame) {
        if (this.claySprites[frame] != null) {
            var element = this.claySprites[frame];
            // element.setScale(2);
        }
        if (this.glossSprites[frame] != null) {
            var element = this.glossSprites[frame];
        }
    };
    BodyPart.prototype.setColorMode = function (colormode) {
        this.colorModeNumber = colormode;
    };
    BodyPart.prototype.removeMarkInColorButton = function () {
        if (this.colorButton) {
            this.colorButton.removeMark();
        }
    };
    BodyPart.prototype.makeMarkInColorButton = function () {
        if (this.colorButton) {
            this.colorButton.makeMark();
        }
    };
    BodyPart.prototype.checkPixelPerfectPos = function (x, y, scale, frame) {
        if (this.claySprites[frame]) {
            return this.claySprites[frame].checkPixelPerfectPos(x, y, scale);
        }
    };
    BodyPart.prototype.hasParent = function (parent) {
        if (parent.modelPart.feature.part_id == this.modelPart.feature.parent_id) {
            return true;
        }
        return false;
    };
    BodyPart.prototype.hasSibling = function (sibling) {
        if (sibling.modelPart.feature.parent_id == this.modelPart.feature.parent_id) {
            return true;
        }
        return false;
    };
    BodyPart.prototype.isParent = function () {
        return this.modelPart.feature.parent_id == 0 ? true : false;
    };
    BodyPart.prototype.isChild = function () {
        return this.modelPart.feature.parent_id != 0 ? true : false;
    };
    BodyPart.prototype.getPartId = function () {
        return this.modelPart.feature.part_id;
    };
    BodyPart.prototype.getParentId = function () {
        return this.modelPart.feature.parent_id;
    };
    BodyPart.prototype.getChildBodyParts = function () {
        var childBPs = [];
        var parentId = this.getPartId();
        if (this.isParent()) {
            childBPs = this.scene.bodyPartsManager.bodyParts.filter(function (it) { return it.getParentId() == parentId; });
        }
        return childBPs;
    };
    return BodyPart;
}());
var BodyPartGroup = /** @class */ (function (_super) {
    __extends(BodyPartGroup, _super);
    function BodyPartGroup(scene, parts_arr, modelName) {
        var _this = _super.call(this, scene, 0, 0) || this;
        _this.clayPartsArr = [];
        _this.glossPartsArr = [];
        scene.add.existing(_this);
        _this.scene = scene;
        _this.currentIndex = 1;
        _this.initBodyPart(parts_arr, modelName);
        return _this;
    }
    BodyPartGroup.prototype.initBodyPart = function (parts_arr, modelName) {
        var _this = this;
        var average_X = 0;
        var average_Y = 0;
        var total_X = 0;
        var total_Y = 0;
        var totals = 0;
        var self = this;
        parts_arr.forEach(function (element) {
            if (!element.partName.includes("Shadow")) {
                totals += 1;
                total_X += +element.partPosX;
                total_Y += +element.partPosY;
            }
        });
        average_X = total_X / totals;
        average_Y = total_Y / totals;
        parts_arr.forEach(function (element) {
            if (!element.partName.includes("Shadow") && !element.partName.includes("Background")) {
                if (!element.partBaseImage.includes("tmp_Nada")) {
                    var clay_bodyPart = new MyBodyPart(_this.scene, element, modelName, average_X, average_Y);
                    var gloss_bodyPart = new MyBodyPart(_this.scene, element, modelName, average_X, average_Y);
                    _this.add(clay_bodyPart);
                    _this.add(gloss_bodyPart);
                    _this.clayPartsArr.push(clay_bodyPart);
                    _this.glossPartsArr.push(gloss_bodyPart);
                }
            }
        });
        this.setFrameIndex(this.currentIndex);
    };
    BodyPartGroup.prototype.setFrameIndex = function (frameindex) {
        this.clayPartsArr.forEach(function (element) {
            element.setFramIndex(frameindex);
        });
        this.glossPartsArr.forEach(function (element) {
            element.setFramIndex(frameindex);
        });
    };
    BodyPartGroup.prototype.moveLeft = function () {
        var newIndex = this.currentIndex + 1;
        this.currentIndex = Math.abs(newIndex % 29);
        if (this.currentIndex == 0) {
            this.currentIndex = 1;
        }
        this.setFrameIndex(this.currentIndex);
    };
    BodyPartGroup.prototype.moveRight = function () {
        var newIndex = this.currentIndex - 1;
        this.currentIndex = Math.abs(newIndex % 29);
        if (this.currentIndex == 0) {
            this.currentIndex = 28;
        }
        this.setFrameIndex(this.currentIndex);
    };
    return BodyPartGroup;
}(Phaser.GameObjects.Container));
var MyBodyPart = /** @class */ (function (_super) {
    __extends(MyBodyPart, _super);
    function MyBodyPart(scene, modelPart, modelName, averageX, averageY) {
        var _this = this;
        var SCREEN_SIZE_COEF = 1;
        var xSize = +modelPart.partSizeX / SCREEN_SIZE_COEF;
        var ySize = +modelPart.partSizeY / SCREEN_SIZE_COEF;
        var xPos = +scene.game.config.width - (averageX - +modelPart.partPosX / SCREEN_SIZE_COEF + +scene.game.config.width / 2);
        var yPos = averageY - +modelPart.partPosY / SCREEN_SIZE_COEF + +scene.game.config.height / 2;
        var x = xPos;
        var y = yPos;
        _this = _super.call(this, scene, x, y, modelName, "" + modelPart.partBaseImage + 1 + ".png") || this;
        _this.tint = 0x4287f5;
        _this.currentFrameIndex = 1;
        _this.scene = scene;
        _this.modelPart = modelPart;
        _this.modelName = modelName;
        _this.displayHeight = ySize;
        _this.displayWidth = xSize;
        _this.startFrame = 0;
        scene.sys.updateList.add(_this);
        scene.sys.displayList.add(_this);
        //this.animate(scene);
        var self = _this;
        scene.events.on("stop-animation", function () {
            _this.startFrame = 10;
            _this.anims.pause();
        });
        scene.events.on("start-animation", function () {
            _this.anims.resume();
        });
        return _this;
    }
    ;
    MyBodyPart.prototype.animate = function (scene) {
        // const hair = this.add.sprite(100, 100, "model", "08Hair_Clay1.1.png");
        // hair.tint = 0x000000;
        var _this = this;
        // this.anims.create({
        //     key:'rotate-hair',
        //     repeat:-1,
        //     frameRate:10,
        //     frames: this.anims.generateFrameNames('model', {
        //         prefix:'08Hair_Clay1.',
        //         suffix:'.png',
        //         start:1,
        //         end:29,
        //         zeroPad:1
        //     }),
        // });
        // hair.play("rotate-hair");
        if (this.modelPart.partName.includes("Background")) {
            return;
        }
        scene.anims.create({
            key: "" + this.modelPart.partName,
            repeat: -1,
            frameRate: 2,
            frames: scene.anims.generateFrameNames("" + this.modelName, {
                prefix: "" + this.modelPart.partBaseImage,
                suffix: '.png',
                start: this.startFrame,
                end: 29,
                zeroPad: 1
            }),
        });
        this.play("" + this.modelPart.partName);
        this.on('animationcomplete', function (animation, frame) {
        }, this);
        this.on('animationupdate', function (animation, frame) {
            _this.startFrame = 0;
        }, this);
    };
    MyBodyPart.prototype.setFramIndex = function (index) {
        this.currentFrameIndex = 1;
        if (index == CST.FRONT_FRAME) {
            this.setTexture(this.modelName + "-front", "" + this.modelPart.partBaseImage + index + ".png");
        }
        else {
            this.setTexture("" + this.modelName, "" + this.modelPart.partBaseImage + index + ".png");
        }
    };
    return MyBodyPart;
}(Phaser.GameObjects.Sprite));
// // added by asset
// class SideBar {
//     scene:Phaser.Scene;
//     headerStack: Phaser.GameObjects.Container;// added by asset
//     UImargin: Phaser.GameObjects.Image; // added by asset
//     titleImage: Phaser.GameObjects.Image; // added by asset
//     cloudBoard: CloudIconButtonContainer; //added by asset
//     cloudBoardConfig: { x: number, y: number, width: number, height: number }
//     communityBoard: CommuntyIconButtonContainer; //added by asset
//     communityBoardConfig: { x: number, y: number, width: number, height: number }
//     constructor(){
//     }
//     init(){
//     }
// }
// created by asset
var CloudIconButtonContainer = /** @class */ (function () {
    function CloudIconButtonContainer(scene, config) {
        this.likeButtonTexture = "Button_CloudLike";
        this.LAYOUT_CONSTANT = LayoutContants.getInstance();
        this.scene = scene;
        this.counter = {
            download: config.counter.download,
            like: config.counter.like,
        };
        this.origin = { x: config.x, y: 0 };
        this.scale = { x: 1, y: 1 };
        this.size = { width: config.width, height: config.height };
        this.buttonSize = 5;
        this.visibility = config.visibility;
        this.stack = scene.add.container(config.x, config.y);
        this.board = scene.add.container(config.x, config.y);
        // this.board.setPosition(config.x, config.y);
        // this.board.setDisplaySize(config.width, config.height);
        this.stack.depth = 499; // under the cloud button
        this.board.depth = 500; // under the cloud button
        // this.board.setScale( this.scale.x, this.scale.y);
        this.setShareFlag = config.setShareFlag;
        this.likeIncrement = config.likeIncrement;
        this.downloadIncrement = config.downloadIncrement;
        this.is_shared = config.is_shared;
        this.init();
    }
    ;
    CloudIconButtonContainer.prototype.init = function () {
        var _this = this;
        this.backgroundImage = this.scene.add.image(this.origin.x, this.origin.y, "solid-new");
        this.backgroundImage.setOrigin(0, 0);
        this.backgroundImage.setDisplaySize(this.size.width, this.size.height);
        // this.backgroundImage.setScale( this.scale.x, this.scale.y);
        this.stack.add(this.backgroundImage);
        var px = 50;
        this.likeButton = new CustomImageTextureButton(this.scene, this.board, this.size.width / 2, this.size.height * 1 / 5, 185, 200, this.likeButtonTexture, this.counter.like, function () {
            _this.likeIncrement();
            if (_this.counter.like) {
                _this.likeButton.setStatus(true);
            }
            else {
                _this.likeButton.setStatus(false);
            }
            console.log('click the like button', _this.counter.like);
            _this.likeButton.updateState(_this.counter.like);
            _this.setDataFromAPI();
        });
        this.likeButton.setHighLightTexture("Button_CloudLike_Red");
        this.likeButton.setTopLeftOrigin();
        this.downloadButton = new CustomImageTextureButton(this.scene, this.board, this.size.width / 2, this.size.height * 3 / 5, 185, 200, "Button_CloudDownload", this.counter.download, function () {
            _this.downloadIncrement();
            console.log('click the download button', _this.counter.download);
            _this.downloadButton.updateState(_this.counter.download);
            _this.setDataFromAPI();
        });
        this.downloadButton.setTopLeftOrigin();
        this.shareButton = new CustomImageTextureButton(this.scene, this.board, this.size.width / 2, this.size.height * 3 / 7, 185, 185, "Button_CloudShare", "Share", function () {
            var res = _this.setShareFlag();
            if (res) {
                _this.is_shared = true;
                _this.updateButton();
            }
        });
        this.shareButton.setTopLeftOrigin();
        this.stack.add(this.board);
        this.board.setVisible(this.visibility);
    };
    CloudIconButtonContainer.prototype.updateButton = function () {
        console.log("this.is_shared", this.is_shared);
        this.shareButton.setVisible(!this.is_shared);
        this.downloadButton.setVisible(this.is_shared);
        this.likeButton.setVisible(this.is_shared);
        console.log('!!this.counter.like', !!this.counter.like);
        this.likeButton.setStatus(!!this.counter.like);
        this.likeButton.updateState(this.counter.like);
        this.downloadButton.updateState(this.counter.download);
    };
    CloudIconButtonContainer.prototype.setDataFromAPI = function () {
        console.log('call set-data-from-api function');
        // var event = new CustomEvent('setModelData', { detail: {model_name: this.model.modelName, counter: this.counter, callback:(res)=>{
        //     // we need to refresh the counter 
        //     // using call back data 
        //     this.apiData = res;
        //     this.update()
        // }}});
        // document.dispatchEvent(event);
    };
    return CloudIconButtonContainer;
}());
;
// created by asset
var communityModelButtonContainer = /** @class */ (function () {
    function communityModelButtonContainer(scene, config) {
        this.likeButtonTexture = "Button_CloudLike";
        this.scene = scene;
        this.counter = {
            download: config.counter.download,
            like: config.counter.like,
        };
        this.origin = { x: 0, y: config.y };
        this.scale = { x: 1, y: 1 };
        this.size = { width: config.width, height: config.height };
        this.buttonSize = 5;
        this.stack = scene.add.container(config.x, config.y);
        this.likeIncrement = config.likeIncrement;
        this.downloadIncrement = config.downloadIncrement;
        this.init();
    }
    ;
    communityModelButtonContainer.prototype.init = function () {
        var _this = this;
        this.backgroundImage = this.scene.add.image(this.origin.x, this.origin.y, "solid-new");
        this.backgroundImage.setOrigin(0, 0);
        // this.backgroundImage.setDepth(1002)
        this.backgroundImage.setDisplaySize(this.size.width, this.size.height);
        // this.backgroundImage.setScale( this.scale.x, this.scale.y);
        this.stack.add(this.backgroundImage);
        var buttons = { width: 185, height: 200 };
        this.likeButton = new CustomImageTextureButton(this.scene, this.stack, (this.size.width - buttons.width) / 2, this.size.height * 1 / 5, buttons.width, buttons.height, this.likeButtonTexture, this.counter.like, function () {
            _this.likeIncrement();
        });
        this.likeButton.setHighLightTexture("Button_CloudLike_Red");
        this.likeButton.setTopLeftOrigin();
        this.likeButton.stack.setDepth(1002);
        this.downloadButton = new CustomImageTextureButton(this.scene, this.stack, (this.size.width - buttons.width) / 2, this.size.height * 3 / 5, buttons.width, buttons.height, "Button_CloudDownload", this.counter.download, function () {
        });
        this.downloadButton.stack.setDepth(1002);
        this.downloadButton.setTopLeftOrigin();
    };
    communityModelButtonContainer.prototype.updateButton = function () {
        this.likeButton.setStatus(!!this.counter.like);
        this.likeButton.updateState(this.counter.like);
        this.downloadButton.updateState(this.counter.download);
    };
    communityModelButtonContainer.prototype.setDataFromAPI = function () {
        console.log('call set-data-from-api function');
    };
    return communityModelButtonContainer;
}());
;
// created by asset
var CommuntyIconButtonContainer = /** @class */ (function () {
    function CommuntyIconButtonContainer(scene, config) {
        this.likeButtonTexture = "Button_CloudLike";
        this.LAYOUT_CONSTANT = LayoutContants.getInstance();
        this.scene = scene;
        this.counter = {
            download: 0,
            like: 0,
            flag: false
        };
        this.origin = { x: config.x, y: config.y - 20 };
        this.scale = { x: 0.6, y: 0.75 };
        this.size = { width: config.width, height: config.height };
        this.buttonSize = 5;
        this.visibility = config.visibility;
        this.selected = "";
        this.stack = scene.add.container(config.x, config.y);
        this.board = scene.add.container(config.x, config.y);
        // this.board.setPosition(config.x, config.y);
        // this.board.setDisplaySize(config.width, config.height);
        this.stack.depth = 499; // under the cloud button
        this.board.depth = 499; // under the cloud button
        this.sortFlag = config.sortFlag;
        this.setSortFlag = config.setSortFlag;
        // this.board.setScale( this.scale.x, this.scale.y);
        this.init();
    }
    ;
    CommuntyIconButtonContainer.prototype.init = function () {
        var _this = this;
        this.backgroundImage = this.scene.add.image(this.origin.x, this.origin.y, "solid-new");
        this.backgroundImage.setOrigin(0, 0);
        this.backgroundImage.setDisplaySize(this.size.width, this.size.height);
        // this.backgroundImage.setScale( this.scale.x, this.scale.y);
        this.stack.add(this.backgroundImage);
        var px = 50;
        // community button
        this.featureButton = new CustomImageTextureButton(this.scene, this.board, this.size.width / 2 + px, this.size.height * 2 / 30, 160, 180, "Button_CloudFeatured", this.sortFlag === "feature" ? "Sort by feature" : "", function () {
            if (_this.sortFlag !== "feature") {
                _this.featureButton.setStatus(true);
                _this.likeButton.setStatus(false);
                _this.downloadButton.setStatus(false);
                _this.dateButton.setStatus(false);
                _this.randomButton.setStatus(false);
                _this.featureButton.updateState("Sort by feature");
                _this.likeButton.updateState("");
                _this.downloadButton.updateState("");
                _this.randomButton.updateState("");
                _this.dateButton.updateState("");
                _this.setSortFlag("feature");
            }
        });
        this.featureButton.setTopLeftOrigin();
        this.featureButton.setHighLightTexture("Button_CloudFeatured_Blue");
        this.featureButton.setStatus(this.sortFlag === "feature");
        this.featureButton.updateState(this.sortFlag === "feature" ? "Sort by feature" : "");
        this.likeButton = new CustomImageTextureButton(this.scene, this.board, this.size.width / 2 + px, this.size.height * 10 / 30, 160, 180, "Button_CloudLike", this.sortFlag === "like" ? "Sort by like" : "", function () {
            if (_this.sortFlag !== "like") {
                _this.featureButton.setStatus(false);
                _this.likeButton.setStatus(true);
                _this.downloadButton.setStatus(false);
                _this.dateButton.setStatus(false);
                _this.randomButton.setStatus(false);
                _this.likeButton.updateState("Sort by like");
                _this.dateButton.updateState("");
                _this.downloadButton.updateState("");
                _this.randomButton.updateState("");
                _this.featureButton.updateState("");
                _this.setSortFlag("like");
            }
        });
        this.likeButton.setTopLeftOrigin();
        this.likeButton.setHighLightTexture("Button_CloudLike_Blue");
        this.likeButton.setStatus(this.sortFlag === "like");
        this.likeButton.updateState(this.sortFlag === "like" ? "Sort by like" : "");
        this.downloadButton = new CustomImageTextureButton(this.scene, this.board, this.size.width / 2 + px, this.size.height * 18 / 30, 160, 180, "Button_CloudDownload", this.sortFlag === "download" ? "Sort by download" : "", function () {
            if (_this.sortFlag !== "download") {
                _this.featureButton.setStatus(false);
                _this.likeButton.setStatus(false);
                _this.downloadButton.setStatus(true);
                _this.dateButton.setStatus(false);
                _this.randomButton.setStatus(false);
                _this.downloadButton.updateState("Sort by download");
                _this.likeButton.updateState("");
                _this.dateButton.updateState("");
                _this.randomButton.updateState("");
                _this.featureButton.updateState("");
                _this.setSortFlag("download");
            }
        });
        this.downloadButton.setHighLightTexture("Button_CloudDownload_Blue");
        this.downloadButton.setTopLeftOrigin();
        this.downloadButton.setStatus(this.sortFlag === "download");
        this.downloadButton.updateState(this.sortFlag === "download" ? "Sort by download" : "");
        this.dateButton = new CustomImageTextureButton(this.scene, this.board, this.size.width / 2 + px, this.size.height * 26 / 30, 160, 180, "Button_CloudDate", this.sortFlag === "date" ? "Sort by date" : "", function () {
            if (_this.sortFlag !== "date") {
                _this.featureButton.setStatus(false);
                _this.likeButton.setStatus(false);
                _this.downloadButton.setStatus(false);
                _this.dateButton.setStatus(true);
                _this.randomButton.setStatus(false);
                _this.dateButton.updateState("Sort by date");
                _this.likeButton.updateState("");
                _this.downloadButton.updateState("");
                _this.randomButton.updateState("");
                _this.featureButton.updateState("");
                _this.setSortFlag("date");
            }
        });
        this.dateButton.setTopLeftOrigin();
        this.dateButton.setHighLightTexture("Button_CloudDate_Blue");
        this.dateButton.setStatus(this.sortFlag === "date");
        this.dateButton.updateState(this.sortFlag === "date" ? "Sort by date" : "");
        this.randomButton = new CustomImageTextureButton(this.scene, this.board, this.size.width / 2 + px, this.size.height * 34 / 30, 160, 180, "Button_CloudRandom", this.sortFlag === "random" ? "Sort by random" : "", function () {
            if (_this.sortFlag !== "random") {
                _this.featureButton.setStatus(false);
                _this.likeButton.setStatus(false);
                _this.downloadButton.setStatus(false);
                _this.dateButton.setStatus(false);
                _this.randomButton.setStatus(true);
                _this.randomButton.updateState("Sort by random");
                _this.likeButton.updateState("");
                _this.downloadButton.updateState("");
                _this.dateButton.updateState("");
                _this.featureButton.updateState("");
                _this.setSortFlag("random");
            }
        });
        this.randomButton.setTopLeftOrigin();
        this.randomButton.setHighLightTexture("Button_CloudRandom_Blue");
        this.randomButton.setStatus(this.sortFlag === "random");
        this.randomButton.updateState(this.sortFlag === "random" ? "Sort by random" : "");
        this.stack.add(this.board);
        this.board.setVisible(this.visibility);
        this.board.setScale(0.3);
    };
    CommuntyIconButtonContainer.prototype.setDataFromAPI = function () {
        console.log('call set-data-from-api function');
        // var event = new CustomEvent('setModelData', { detail: {model_name: this.model.modelName, counter: this.counter, callback:(res)=>{
        //     // we need to refresh the counter 
        //     // using call back data 
        //     this.apiData = res;
        //     this.update()
        // }}});
        // document.dispatchEvent(event);
    };
    return CommuntyIconButtonContainer;
}());
;
var IconGroupContainer = /** @class */ (function (_super) {
    __extends(IconGroupContainer, _super);
    function IconGroupContainer(scene, width, height, scrollMode, model) {
        var _this = _super.call(this, scene, 0, 0) || this;
        scene.add.existing(_this);
        _this.scene = scene;
        _this.model = model;
        _this.scrollMode = scrollMode;
        _this.fileNames = new FileNames().generateFileNames(model, true);
        _this.initGroup(width, height, model);
        return _this;
    }
    ;
    IconGroupContainer.prototype.initGroup = function (width, height, model) {
        if (this.getSelectedModelArray().includes(this.model.iconName)) {
            this.backgroundImage = new Phaser.GameObjects.Image(this.scene, 0, 0, 'IAPicon_BGBlue');
        }
        else {
            this.backgroundImage = new Phaser.GameObjects.Image(this.scene, 0, 0, 'IAPicon_BGGrey');
        }
        this.modelButton = new Phaser.GameObjects.Image(this.scene, 0, 0, model.iconName);
        if (this.scrollMode == 1) {
            this.setScale(width / this.modelButton.width);
        }
        else {
            this.setScale(height / this.modelButton.height);
        }
        this.add(this.backgroundImage);
        this.add(this.modelButton);
        // this.modelButton.setInteractive().on('pointerdown',()=>{
        //     alert("model"+model.iconName);
        // })
    };
    IconGroupContainer.prototype.updateBackground = function (flag) {
        if (flag) {
            this.backgroundImage.setTexture("IAPicon_BGBlue");
        }
        else {
            this.backgroundImage.setTexture("IAPicon_BGGrey");
        }
        //localStorage.setItem();
        if (!this.getSelectedModelArray().includes(this.model.iconName)) {
            var new_selected_models_str = localStorage.getItem("selected_models") + "," + this.model.iconName;
            localStorage.setItem("selected_models", new_selected_models_str);
        }
    };
    IconGroupContainer.prototype.getSelectedModelArray = function () {
        var selected_models_str = localStorage.getItem("selected_models");
        if (selected_models_str) {
            return selected_models_str.split(",");
        }
        else {
            return [];
        }
    };
    return IconGroupContainer;
}(Phaser.GameObjects.Container));
;
/// <reference path='../../../phaser.d.ts'/>
var LogoSprite = /** @class */ (function (_super) {
    __extends(LogoSprite, _super);
    function LogoSprite(scene, texture, frame) {
        var _this = _super.call(this, scene, 0, 0, texture, frame) || this;
        scene.sys.updateList.add(_this);
        scene.sys.displayList.add(_this);
        _this.scene = scene;
        _this.animate(scene);
        return _this;
    }
    ;
    LogoSprite.prototype.animate = function (scene) {
        var _this = this;
        var loading = scene.anims.create({
            key: 'loading',
            repeat: 0,
            frameRate: 1,
            frames: scene.anims.generateFrameNames('logo', {
                prefix: 'AnimFill_',
                suffix: '.png',
                start: 2,
                end: 7,
                zeroPad: 1
            }),
        });
        this.play("loading");
        this.on('animationcomplete', function (animation, frame) {
        }, this);
        this.on('animationupdate', function (animation, frame) {
            if (frame.index == 6) {
                _this.scene.events.emit(CST.EVENTS.LOADED, true);
            }
        }, this);
    };
    return LogoSprite;
}(Phaser.GameObjects.Sprite));
;
/// <reference path='../../../phaser.d.ts'/>
var LogoContainer = /** @class */ (function (_super) {
    __extends(LogoContainer, _super);
    function LogoContainer(scene) {
        var _this = this;
        var yOrigin = (Number)(scene.game.config.height) / 2;
        var xOrigin = (Number)(scene.game.config.width) / 2;
        _this = _super.call(this, scene, xOrigin, yOrigin) || this;
        _this.scene = scene;
        scene.add.existing(_this);
        _this.addLogo();
        _this.scene.events.on(CST.EVENTS.LOADED, function () {
            _this.logoFrame.setTexture('logoFrame2');
        });
        return _this;
    }
    ;
    LogoContainer.prototype.addLogo = function () {
        this.logo = new LogoSprite(this.scene, 'logo');
        this.logoFrame = new Phaser.GameObjects.Image(this.scene, 0, 0, 'logoFrame1');
        this.add(this.logo);
        this.add(this.logoFrame);
    };
    return LogoContainer;
}(Phaser.GameObjects.Container));
;
var CategoryGridTable = /** @class */ (function () {
    function CategoryGridTable(scene, config) {
        this.table = config.background;
        this.table.setPosition(0, 0);
        this.table.displayHeight = config.height;
        this.table.displayWidth = config.width;
        this.table.setOrigin(0, 0).setDepth(22).setAlpha(0.01);
        this.stack = scene.add.container(config.x, config.y).setDepth(22);
        this.stack.add(this.table);
        this.bindEvent(scene);
        this.scene = scene;
        this.config = config;
        this.tableMovement = false;
    }
    CategoryGridTable.prototype.bindEvent = function (scene) {
        var _this = this;
        var isDown = false;
        var lastPosX = 0;
        var travelDistance = 0;
        var velocityX = 0;
        this.table.setInteractive().on('pointerdown', function (pointer, localX, localY, event) {
            isDown = true;
        }).on('pointermove', function (pointer, localX, localY, event) {
            if (isDown) {
                if (lastPosX == 0) {
                    lastPosX = pointer.x;
                }
                _this.stack.x += pointer.x - lastPosX;
                travelDistance += Math.abs(pointer.x - lastPosX);
                lastPosX = pointer.x;
                if (travelDistance > 10) {
                    _this.tableMovement = true;
                }
            }
        }).on('pointerup', function (pointer, localX, localY, event) {
            lastPosX = 0;
            if (isDown) {
                isDown = false;
                _this.correctPosition();
                _this.detectEvent(pointer, event);
            }
            travelDistance = 0;
            if (_this.tableMovement) {
                _this.doInertiaMovement(velocityX);
            }
            _this.tableMovement = false;
        }).on('pointerout', function (pointer, localX, localY, event) {
            lastPosX = 0;
            if (isDown) {
                isDown = false;
                _this.correctPosition();
                _this.detectEvent(pointer, event);
            }
            travelDistance = 0;
            _this.tableMovement = false;
        });
        scene.events.on("velocity", function (vx, vy) {
            velocityX = vx;
        });
    };
    CategoryGridTable.prototype.setItems = function (items) {
        var _this = this;
        this.items = items;
        var index = 0;
        var twidth = 0;
        items.forEach(function (element) {
            element.index = index;
            var itemH = items[index].getDisplayHeight();
            var itemW = items[index].getDisplayWidth();
            var x = twidth + itemW / 2 + 20;
            var y = itemH / 2;
            var cellContainer = _this.scene.add.container(x, y);
            cellContainer.add(element.stack);
            _this.stack.add(cellContainer);
            index++;
            twidth += itemW + 20;
        });
        if (this.config.scrollMode == 1) {
            this.limitLeftX = this.config.x;
            this.table.displayWidth = twidth;
            this.limitRightX = -Math.max(twidth + 20 - this.config.width, 0) + this.config.x;
            // this.table.displayWidth = LayoutContants.getInstance().SCREEN_WIDTH;
        }
    };
    CategoryGridTable.prototype.detectEvent = function (pointer, event) {
        var _this = this;
        if (!this.tableMovement) {
            this.items.forEach(function (element) {
                if (element.itemImage.getBounds().contains(pointer.x, pointer.y)) {
                    _this.table.emit("cell.click", element.index);
                }
            });
        }
    };
    CategoryGridTable.prototype.correctPosition = function () {
        if (this.config.scrollMode == 1) {
            if (this.stack.x > this.limitLeftX) {
                this.scrollTo(this.limitLeftX);
            }
            if (this.stack.x < this.limitRightX) {
                this.scrollTo(this.limitRightX);
            }
        }
    };
    CategoryGridTable.prototype.scrollTo = function (targetPosX) {
        var _this = this;
        var frames = 15;
        var unit = (targetPosX - this.stack.x) / frames;
        if (unit == 0) {
            return;
        }
        var duration = 100;
        var delay = duration / frames;
        if (this.timer)
            this.timer.destroy();
        this.timer = this.scene.time.addEvent({
            delay: delay,
            callback: function () {
                if (duration > 0) {
                    _this.stack.x += unit;
                    duration -= delay;
                }
            },
            repeat: frames
        });
    };
    CategoryGridTable.prototype.moveByX = function (dx) {
        this.stack.x += dx;
        this.table.x += dx;
    };
    CategoryGridTable.prototype.setVisible = function (visible) {
        this.stack.setVisible(visible);
        this.table.setVisible(visible);
    };
    CategoryGridTable.prototype.setMoveByY = function (dy) {
        this.stack.y += dy;
        this.table.y += dy;
    };
    CategoryGridTable.prototype.doInertiaMovement = function (v) {
        var _this = this;
        var steps = 0;
        if (this.smoothMovTimer != undefined)
            this.smoothMovTimer.remove();
        var velocity = v;
        this.smoothMovTimer = this.scene.time.addEvent({
            delay: 10,
            callback: function () {
                velocity -= velocity / 10;
                _this.stack.x += velocity;
                if (velocity < 1 && velocity > -1)
                    _this.smoothMovTimer.remove();
                if (_this.stack.x > _this.limitLeftX) {
                    _this.smoothMovTimer.remove();
                    _this.correctPosition();
                }
                if (_this.stack.x < _this.limitRightX) {
                    _this.smoothMovTimer.remove();
                    _this.correctPosition();
                }
                steps++;
            },
            loop: true
        });
    };
    return CategoryGridTable;
}());
var ColorGridTable = /** @class */ (function () {
    function ColorGridTable(scene, config) {
        this.lastPosY = 0;
        this.table = config.background;
        this.table.setPosition(config.x, config.y);
        this.table.displayHeight = config.height - 100;
        this.table.displayWidth = config.width;
        this.table.setOrigin(0, 0);
        this.table.tint = 0xffffff;
        this.table.alpha = 0.01;
        this.stack = scene.add.container(config.x, config.y);
        this.stack.setDepth(1);
        this.bindEvent(scene);
        this.scene = scene;
        this.config = config;
        this.tableMovement = false;
        this.PrevTableMovement = false;
    }
    ColorGridTable.prototype.bindEvent = function (scene) {
        var _this = this;
        var isDown = false;
        var lastPosY = 0;
        var travelDistance = 0;
        var velocityY = 0;
        var startPosX;
        var startPosY;
        this.table.setInteractive().on('pointerdown', function (pointer, localX, localY, event) {
            if (_this.smoothMovTimer != undefined)
                _this.smoothMovTimer.remove();
            isDown = true;
            if (_this.tableMovement) {
                _this.PrevTableMovement = true;
            }
            else {
                _this.PrevTableMovement = false;
            }
            _this.tableMovement = false;
            startPosX = pointer.x;
            startPosY = pointer.y;
            if (_this.scene.uiGenerator.colorMode != CST.COLOR_MODE.CUSTOME)
                _this.detectPressDown(pointer, event);
        }).on('pointermove', function (pointer, localX, localY, event) {
            if (isDown) {
                if (lastPosY == 0) {
                    lastPosY = pointer.y;
                }
                _this.stack.y += pointer.y - lastPosY;
                travelDistance += Math.abs(pointer.y - lastPosY);
                lastPosY = pointer.y;
                if (travelDistance > 5) {
                    _this.tableMovement = true;
                    if (_this.timer)
                        _this.timer.remove();
                }
            }
        }).on('pointerup', function (pointer, localX, localY, event) {
            lastPosY = 0;
            if (isDown && !_this.tableMovement) {
                isDown = false;
                _this.correctPosition();
                _this.detectEvent(startPosX, startPosY, event);
            }
            travelDistance = 0;
            if (_this.tableMovement) {
                isDown = false;
                _this.doInertiaMovement(velocityY);
            }
            _this.emitPressUpEvent();
        }).on('pointerout', function (pointer, localX, localY, event) {
            lastPosY = 0;
            if (isDown) {
                isDown = false;
                _this.correctPosition();
                // this.detectEvent(startPosX, startPosY, event);
            }
            travelDistance = 0;
        });
        scene.events.on("velocity", function (vx, vy) {
            velocityY = vy;
        });
    };
    ColorGridTable.prototype.setItems = function (items) {
        var _this = this;
        this.items = items;
        var index = 0;
        var sizeVal = 0;
        items.forEach(function (element) {
            var col = index % _this.config.table.columns;
            sizeVal = (_this.config.scrollMode == 0) ? _this.config.table.cellHeight : _this.config.table.cellWidth;
            var x = col * sizeVal + 0.5 * sizeVal;
            var y = Math.floor(index / _this.config.table.columns) * sizeVal + 0.5 * sizeVal;
            var cellContainer = _this.scene.add.container(x, y);
            element.setPosition(x, y);
            cellContainer.add(element.stack);
            _this.stack.add(cellContainer);
            index++;
        });
        if (this.config.scrollMode == 0) {
            this.table.displayHeight = Math.ceil(sizeVal * items.length / this.config.table.columns);
            this.limitTopY = this.stack.y;
            this.limitBottomY = -Math.max(this.table.displayHeight - this.config.height, 0) + this.config.y;
        }
    };
    ColorGridTable.prototype.emitPressUpEvent = function () {
        var _this = this;
        setTimeout(function () {
            _this.table.emit("cell.pressup");
        }, 10);
    };
    ColorGridTable.prototype.detectPressDown = function (pointer, event) {
        var _this = this;
        if (this.tableMovement) {
            if (this.timer)
                this.timer.remove();
            return;
        }
        if (this.timer)
            this.timer.remove();
        this.items.forEach(function (element) {
            if (element.clayImage.getBounds().contains(pointer.x, pointer.y)) {
                _this.currentItemIndex = element.buttonIndex;
                _this.timer = _this.scene.time.addEvent({
                    delay: 600,
                    callback: function () {
                        if (_this.currentItemIndex == element.buttonIndex && !_this.tableMovement && !_this.PrevTableMovement) {
                            _this.table.emit("cell.press", element.buttonIndex);
                        }
                    },
                    loop: false
                });
            }
        });
    };
    ColorGridTable.prototype.detectEvent = function (x, y, event) {
        var _this = this;
        if (!this.tableMovement && !this.PrevTableMovement) {
            this.items.forEach(function (element) {
                if (element.clayImage.getBounds().contains(x, y)) {
                    _this.table.emit("cell.click", element.buttonIndex);
                    _this.currentItemIndex = -1;
                }
            });
        }
    };
    ColorGridTable.prototype.emitMoveEvent = function (x, y, event) {
        this.table.emit("cell.move", x, y);
    };
    ColorGridTable.prototype.correctPosition = function () {
        if (this.config.scrollMode == 0) {
            if (this.stack.y > this.limitTopY) {
                this.scrollTo(this.limitTopY);
            }
            if (this.stack.y < this.limitBottomY) {
                this.scrollTo(this.limitBottomY);
            }
        }
    };
    ColorGridTable.prototype.scrollTo = function (targetPosY) {
        var _this = this;
        var frames = 15;
        var unit = (targetPosY - this.stack.y) / frames;
        if (unit == 0) {
            return;
        }
        var duration = 100;
        var delay = duration / frames;
        if (this.timer)
            this.timer.destroy();
        this.timer = this.scene.time.addEvent({
            delay: delay,
            callback: function () {
                if (duration > 0) {
                    _this.stack.y += unit;
                    duration -= delay;
                }
            },
            repeat: frames
        });
    };
    ColorGridTable.prototype.scrollToTop = function (flag) {
        if (flag) {
            this.scrollTo(this.limitTopY);
        }
    };
    ColorGridTable.prototype.getX = function () {
        return this.stack.x;
    };
    ColorGridTable.prototype.moveByX = function (dx) {
        this.stack.x += dx;
        this.table.x += dx;
    };
    ColorGridTable.prototype.setPosition = function (pos) {
        if (pos.x != undefined) {
            this.stack.x = pos.x;
            this.table.x = pos.x;
        }
        if (pos.y != undefined) {
            var dy = 0;
            if (this.lastPosY == 0)
                this.lastPosY = pos.y;
            dy = pos.y - this.lastPosY;
            this.lastPosY = pos.y;
            // var dy = pos.y - this.stack.y;
            this.stack.y += dy;
            this.table.y += dy;
            this.limitTopY = pos.y;
        }
    };
    ColorGridTable.prototype.setVisible = function (visible) {
        this.stack.setVisible(visible);
        this.table.setVisible(visible);
    };
    ColorGridTable.prototype.doInertiaMovement = function (v) {
        var _this = this;
        if (this.smoothMovTimer != undefined)
            this.smoothMovTimer.remove();
        var velocity = v;
        this.smoothMovTimer = this.scene.time.addEvent({
            delay: 8,
            callback: function () {
                velocity -= velocity / 9;
                if (Math.abs(velocity) < 1) {
                    _this.tableMovement = false;
                }
                if (Math.abs(velocity) < 0.01) {
                    //this.tableMovement = false;
                    _this.smoothMovTimer.remove();
                    _this.correctPosition();
                }
                _this.stack.y += velocity;
                if (_this.stack.y > _this.limitTopY) {
                    _this.tableMovement = false;
                    _this.smoothMovTimer.remove();
                    _this.correctPosition();
                }
                if (_this.stack.y < _this.limitBottomY) {
                    _this.tableMovement = false;
                    _this.smoothMovTimer.remove();
                    _this.correctPosition();
                }
            },
            loop: true
        });
    };
    return ColorGridTable;
}());
var CommunityModelBoard = /** @class */ (function () {
    function CommunityModelBoard(scene, config) {
        var _this = this;
        this.scene = scene;
        this.config = config;
        this.modelName = config.modelName;
        this.stack = scene.add.container(config.x, config.y);
        this.stack.setDepth(1000);
        this.setShowFlag = config.changeCommunityModelBoardFlag;
        this.editModel = config.editCommunityModel;
        this.table = config.background;
        this.table.displayHeight = config.height;
        this.table.displayWidth = config.width;
        this.table.tint = 0xCCCCCC;
        this.table.alpha = 0.01;
        this.table.setPosition(config.x, config.y);
        this.table.setOrigin(0, 0);
        this.exitButton = new CustomImageButton(this.scene, 0, 0, "back_button", function () {
            _this.goMainScene();
        });
        this.exitButton.stack.setDepth(1001);
        this.exitButton.stack.setAlpha(.5);
        this.exitButton.setTopLeftOrigin();
        this.cloudBoardConfig = { x: this.config.width - 120, y: 0, width: 350, height: config.height };
        var cloudBoardConfig = __assign(__assign({}, this.cloudBoardConfig), { visibility: true, counter: {
                like: 1,
                download: 1,
            }, setShareFlag: function () {
            }, likeIncrement: function () {
                console.log("like increment");
                config.editCommunityModel();
            }, downloadIncrement: function () {
            } });
        this.cloudBoard = new communityModelButtonContainer(this.scene, cloudBoardConfig);
        // this.stack.add(this.cloudBoard.stack);
        this.cloudBoard.stack.setDepth(1001);
    }
    CommunityModelBoard.prototype.setItems = function (item) {
        this.item = item;
        var cellWidth = this.config.width;
        var cellHeight = this.config.height;
        item.setDisplaySize(cellWidth, cellHeight);
        this.itemH = item.getDisplayHeight();
        this.itemW = item.getDisplayWidth();
        this.table.displayWidth = this.itemW;
        // console.log("this.itemH", this.itemH, this.config.height)
        // this.cloudBoard.stack.displayHeight = this.config.height;
        var x = 0;
        var y = 0;
        var cellContainer = this.scene.add.container(x, y);
        item.initBodyPart();
        cellContainer.add(item.stack);
        this.stack.add(cellContainer);
        this.stack.x = this.config.x;
    };
    CommunityModelBoard.prototype.goMainScene = function () {
        this.setShowFlag(true);
        this.setVisible(false);
    };
    CommunityModelBoard.prototype.setVisible = function (visibility) {
        this.stack.setVisible(visibility);
        this.exitButton.setVisible(visibility);
        this.cloudBoard.stack.setVisible(visibility);
    };
    return CommunityModelBoard;
}());
var MaterialGridTable = /** @class */ (function () {
    function MaterialGridTable(scene, config) {
        this.table = config.background;
        this.table.setPosition(config.x, config.y);
        this.table.displayHeight = config.height;
        this.table.displayWidth = config.width;
        this.table.setOrigin(0, 0);
        this.table.tint = 0x000000;
        this.table.alpha = 0.01;
        this.stack = scene.add.container(config.x, config.y);
        this.stack.depth = 11;
        this.bindEvent(scene);
        this.scene = scene;
        this.config = config;
        this.tableMovement = false;
    }
    MaterialGridTable.prototype.bindEvent = function (scene) {
        var _this = this;
        var isDown = false;
        var lastPosY = 0;
        var travelDistance = 0;
        var velocityY = 0;
        this.table.setInteractive().on('pointerdown', function (pointer, localX, localY, event) {
            isDown = true;
        }).on('pointermove', function (pointer, localX, localY, event) {
            if (isDown) {
                if (lastPosY == 0) {
                    lastPosY = pointer.y;
                }
                _this.stack.y += pointer.y - lastPosY;
                travelDistance += Math.abs(pointer.y - lastPosY);
                lastPosY = pointer.y;
                if (travelDistance > 10) {
                    _this.tableMovement = true;
                }
            }
        }).on('pointerup', function (pointer, localX, localY, event) {
            lastPosY = 0;
            if (isDown) {
                isDown = false;
                _this.correctPosition();
                _this.detectEvent(pointer, event);
            }
            travelDistance = 0;
            if (_this.tableMovement) {
                _this.doInertiaMovement(velocityY);
            }
            _this.tableMovement = false;
        }).on('pointerout', function (pointer, localX, localY, event) {
            lastPosY = 0;
            if (isDown) {
                isDown = false;
                _this.correctPosition();
                _this.detectEvent(pointer, event);
            }
            travelDistance = 0;
            _this.tableMovement = false;
        });
        scene.events.on("velocity", function (vx, vy) {
            velocityY = vy;
        });
    };
    MaterialGridTable.prototype.setItems = function (items, childBodyParts) {
        var _this = this;
        if (childBodyParts === void 0) { childBodyParts = []; }
        this.items = items;
        this.childBodyParts = childBodyParts;
        var index = 0;
        this.stack.removeAll();
        var sizeVal = 0;
        items.forEach(function (element) {
            var col = index % _this.config.table.columns;
            sizeVal = (_this.config.scrollMode == 0) ? _this.config.table.cellHeight : _this.config.table.cellWidth;
            var x = col * sizeVal + 0.5 * _this.config.width;
            var y = Math.floor(index / _this.config.table.columns) * sizeVal + 0.5 * sizeVal;
            var cellContainer = _this.scene.add.container(x, y);
            cellContainer.add(element.stack);
            element.stack.setInteractive;
            _this.stack.add(cellContainer);
            index++;
        });
        // var graphic = this.scene.add.graphics();
        // graphic.lineStyle(5, 0xFF00FF, 1.0);
        // graphic.fillStyle(0xFFFFFF, 1.0);
        // graphic.fillRect(50, 50, 400, 200);        
        //graphics.strokeRect(50, 50, 400, 200);
        if (items.length > 0 && childBodyParts.length > 0) {
            //make spacer
            var col = index % this.config.table.columns;
            sizeVal = (this.config.scrollMode == 0) ? this.config.table.cellHeight : this.config.table.cellWidth;
            var x = col * sizeVal + 0.5 * this.config.width;
            var y = Math.floor(index / this.config.table.columns) * sizeVal + 0.1 * sizeVal;
            var cellContainer = this.scene.add.container(x, y);
            var spliter_img = this.scene.add.image(0, 0, "tab-splitter");
            spliter_img.displayWidth = items[0].tabImage.displayWidth;
            spliter_img.displayHeight = 10;
            //var graphics = this.scene.add.graphics();
            // graphics.fillRect(0,0,items[0].tabImage.displayWidth,5);
            // graphics.fillGradientStyle(0,0,0,0);
            // graphics.fillStyle(0x000000);
            //graphics.lineGradientStyle(items[0].tabImage.displayWidth, 0xff0000, 0xff0000, 0x0000ff, 0x0000ff, 1);
            //graphics.lineBetween(0, 0, 0 , 5);
            cellContainer.add(spliter_img);
            this.stack.add(cellContainer);
        }
        // set child bodyparts continuously
        childBodyParts.forEach(function (element) {
            var col = index % _this.config.table.columns;
            sizeVal = (_this.config.scrollMode == 0) ? _this.config.table.cellHeight : _this.config.table.cellWidth;
            var x = col * sizeVal + 0.5 * _this.config.width;
            var y = Math.floor(index / _this.config.table.columns) * sizeVal + 0.7 * sizeVal;
            var cellContainer = _this.scene.add.container(x, y);
            cellContainer.add(element.stack);
            element.stack.setInteractive;
            _this.stack.add(cellContainer);
            index++;
        });
        if (this.config.scrollMode == 0) {
            this.table.displayHeight = Math.ceil(sizeVal * (items.length + childBodyParts.length) / this.config.table.columns);
            this.limitTopY = this.config.y;
            this.limitBottomY = -Math.max(this.table.displayHeight - this.config.height, 0) + this.config.y;
        }
        this.stack.y = this.config.y;
    };
    MaterialGridTable.prototype.detectEvent = function (pointer, event) {
        var _this = this;
        if (!this.tableMovement) {
            this.items.forEach(function (element) {
                if (element.tabImage.getBounds().contains(pointer.x, pointer.y)) {
                    _this.table.emit("cell.click", element.tabIndex);
                }
            });
            this.childBodyParts.forEach(function (element) {
                if (element.image.getBounds().contains(pointer.x, pointer.y)) {
                    _this.table.emit("cell.child.click", element.bodyPart.modelPart.feature.part_id);
                }
            });
        }
    };
    MaterialGridTable.prototype.correctPosition = function () {
        if (this.config.scrollMode == 0) {
            if (this.stack.y > this.limitTopY) {
                this.scrollTo(this.limitTopY);
            }
            if (this.stack.y < this.limitBottomY) {
                this.scrollTo(this.limitBottomY);
            }
        }
    };
    MaterialGridTable.prototype.scrollTo = function (targetPosY) {
        if (this.tween)
            this.tween.remove();
        this.tween = this.scene.tweens.add({
            targets: this.stack,
            y: targetPosY,
            // alpha: { start: 0, to: 1 },
            // alpha: 1,
            // alpha: '+=1',
            ease: 'Back.easeIn',
            duration: 300,
            repeat: 0,
            yoyo: false
        });
        // var frames = 15;
        // const unit = (targetPosY - this.stack.y)/frames;
        // if(unit == 0){
        //     return;
        // }
        // var duration = 100;
        // var delay = duration/frames;
        // if(this.timer) this.timer.destroy();
        // this.timer = this.scene.time.addEvent({
        //     delay: delay,
        //     callback: ()=>{
        //         if(duration > 0){
        //             this.stack.y += unit;
        //             duration -= delay;
        //         }
        //     },
        //     repeat: frames
        // });
    };
    MaterialGridTable.prototype.scrollToTop = function (flag) {
        if (flag) {
            this.scrollTo(this.limitTopY);
        }
    };
    MaterialGridTable.prototype.moveByX = function (dx) {
        this.stack.x += dx;
        this.table.x += dx;
    };
    MaterialGridTable.prototype.setPosition = function (pos) {
        if (pos.x != undefined) {
            this.stack.x = pos.x;
            this.table.x = pos.x;
            this.config.x = pos.x;
        }
        if (pos.y != undefined) {
            this.stack.y = pos.y;
            this.table.y = pos.y;
            this.config.y = pos.y;
            this.limitTopY = this.stack.y;
            this.limitBottomY = -Math.max(this.table.displayHeight - this.config.height, 0) + this.stack.y;
        }
    };
    MaterialGridTable.prototype.setVisible = function (visible) {
        this.stack.setVisible(visible);
        this.table.setVisible(visible);
    };
    MaterialGridTable.prototype.doInertiaMovement = function (v) {
        var _this = this;
        if (this.smoothMovTimer != undefined)
            this.smoothMovTimer.remove();
        var velocity = v;
        this.smoothMovTimer = this.scene.time.addEvent({
            delay: 10,
            callback: function () {
                velocity -= velocity / 10;
                _this.stack.y += velocity;
                if (_this.stack.y > _this.limitTopY) {
                    _this.smoothMovTimer.remove();
                    _this.correctPosition();
                }
                if (_this.stack.y < _this.limitBottomY) {
                    _this.smoothMovTimer.remove();
                    _this.correctPosition();
                }
            },
            loop: true
        });
    };
    return MaterialGridTable;
}());
var ModelGridTable = /** @class */ (function () {
    function ModelGridTable(scene, config) {
        this.table = config.background;
        this.table.setPosition(config.x, config.y);
        this.table.displayHeight = config.height;
        this.table.displayWidth = config.width;
        this.table.setOrigin(0, 0);
        this.table.tint = 0xCCCCCC;
        this.table.alpha = 0.01;
        this.stack = scene.add.container(config.x, config.y);
        this.stack.add(this.table);
        this.bindEvent(scene);
        this.scene = scene;
        this.config = config;
        this.tableMovement = false;
    }
    ModelGridTable.prototype.bindEvent = function (scene) {
        var _this = this;
        var isDown = false;
        var lastPosY = 0;
        var travelDistance = 0;
        var velocityY = 0;
        var closeSettingPannel = function () {
            // check if setting pannel is opened
            if (_this.config.settingPannel.isOn) {
                // clear smooth timer 
                if (_this.smoothMovTimer != undefined)
                    _this.smoothMovTimer.remove();
                // setting pannel is closed
                _this.config.toggleSettingPannel(false);
                _this.config.settingPannel.settingButton.setActive(false); // make active mark as false
            }
        };
        var movingTableByPy = function (py) {
            // init last pos
            if (lastPosY == 0) {
                lastPosY = py;
            }
            // moving table by pointer
            _this.stack.y += py - lastPosY;
            // get moving distance
            travelDistance += Math.abs(py - lastPosY);
            lastPosY = py; // backup last position
            // check if pointer is moved by 10
            if (travelDistance > 10) {
                _this.tableMovement = true; // make tableMovement as true
            }
        };
        var _t = null;
        var movingTableByDy = function (dy) {
            // moving table by pointer
            _this.stack.y = _this.stack.y - dy;
            // get moving distance
            travelDistance += Math.abs(dy);
            _this.correctPosition();
        };
        // mouse scroll event
        this.table.on('wheel', function (pointer, dx, dy, dz, event) {
            closeSettingPannel(); // close setting pannel if it's opened already
            //console.log('dy', pointer);
            movingTableByDy(dy);
        });
        this.table.setInteractive().on('pointerdown', function (pointer, localX, localY, event) {
            closeSettingPannel(); // close setting pannel if it's opened already
            isDown = true; // mark down flag as true
        }).on('pointermove', function (pointer, localX, localY, event) {
            if (isDown) {
                //console.log(pointer);
                movingTableByPy(pointer.y); // moving table                
            }
        }).on('pointerup', function (pointer, localX, localY, event) {
            lastPosY = 0; // remove last position 
            if (isDown) {
                isDown = false;
                _this.correctPosition();
                _this.detectEvent(pointer, event);
            }
            travelDistance = 0;
            if (_this.tableMovement) {
                _this.doInertiaMovement(velocityY);
            }
            _this.tableMovement = false;
        }).on('pointerout', function (pointer, localX, localY, event) {
            lastPosY = 0;
            if (isDown) {
                isDown = false;
                _this.correctPosition();
                _this.detectEvent(pointer, event);
            }
            travelDistance = 0;
            _this.tableMovement = false;
        });
        scene.events.on("velocity", function (vx, vy) {
            velocityY = vy;
        });
    };
    ModelGridTable.prototype.setItems = function (items) {
        var _this = this;
        this.stack.removeAll();
        this.items = items;
        var columns = this.config.table.columns;
        var width = this.config.width;
        var height = this.config.height;
        var cellWidth = Math.max(this.config.table.cellWidth, this.config.width / columns);
        items.forEach(function (element) {
            element.setDisplayWidth(cellWidth);
        });
        var itemH = items[0].getDisplayHeight();
        var itemW = items[0].getDisplayWidth();
        // var paddingTop = Math.max((height - itemH * Math.ceil(items.length / columns))/2, 0);
        // var marginLeft = (width - columns * itemW) /(columns+1);
        var paddingTop = 0;
        var marginLeft = 0;
        if (this.config.scrollMode == 0) {
            this.table.displayHeight = itemH * Math.ceil(items.length / columns) + 2 * paddingTop;
            this.limitTopY = this.config.y;
            this.limitBottomY = -Math.max(this.table.displayHeight - this.config.height, 0) + this.config.y;
        }
        var index = 0;
        items.forEach(function (element) {
            element.tabIndex = index;
            var col = index % columns;
            var x = col * itemW + (col + 1) * marginLeft;
            var y = Math.floor(index / columns) * itemH + paddingTop;
            var cellContainer = _this.scene.add.container(x, y);
            cellContainer.add(element.stack);
            _this.stack.add(cellContainer);
            index++;
        });
        this.stack.y = this.config.y;
    };
    ModelGridTable.prototype.detectEvent = function (pointer, event) {
        var _this = this;
        if (!this.tableMovement) {
            this.items.forEach(function (element) {
                if (element.backgroundImage.getBounds().contains(pointer.x, pointer.y)) {
                    if (LayoutContants.getInstance().SCREEN_HEIGHT - (parseFloat(localStorage.getItem("bottom-height")) || 0) >= pointer.y) {
                        _this.table.emit("cell.click", element.tabIndex);
                    }
                }
            });
        }
    };
    ModelGridTable.prototype.correctPosition = function () {
        if (this.config.scrollMode == 0) {
            if (this.stack.y > this.limitTopY) {
                this.scrollTo(this.limitTopY);
            }
            if (this.stack.y < this.limitBottomY) {
                this.scrollTo(this.limitBottomY);
            }
        }
    };
    ModelGridTable.prototype.scrollTo = function (targetPosY) {
        if (this.tween)
            this.tween.remove();
        this.tween = this.scene.tweens.add({
            targets: this.stack,
            y: targetPosY,
            // alpha: { start: 0, to: 1 },
            // alpha: 1,
            // alpha: '+=1',
            ease: 'Back.easeIn',
            duration: 300,
            repeat: 0,
            yoyo: false
        });
        // var frames = 15;
        // const unit = (targetPosY - this.stack.y)/frames;
        // if(unit == 0){
        //     return;
        // }
        // var duration = 100;
        // var delay = duration/frames;
        // if(this.timer) this.timer.destroy();
        // this.timer = this.scene.time.addEvent({
        //     delay: delay,
        //     callback: ()=>{
        //         if(duration > 0){
        //             this.stack.y += unit;
        //             duration -= delay;
        //         }
        //     },
        //     repeat: frames
        // });
    };
    ModelGridTable.prototype.scrollToTop = function (flag) {
        if (flag) {
            this.scrollTo(this.limitTopY);
        }
    };
    ModelGridTable.prototype.moveByX = function (dx) {
        this.stack.x += dx;
        this.table.x += dx;
    };
    ModelGridTable.prototype.setVisible = function (visible) {
        this.stack.setVisible(visible);
        this.table.setVisible(visible);
    };
    ModelGridTable.prototype.doInertiaMovement = function (v) {
        var _this = this;
        if (this.smoothMovTimer != undefined)
            this.smoothMovTimer.remove();
        var velocity = v;
        this.smoothMovTimer = this.scene.time.addEvent({
            delay: 8,
            callback: function () {
                velocity -= velocity / 9;
                _this.stack.y += velocity;
                if (_this.stack.y > _this.limitTopY) {
                    _this.smoothMovTimer.remove();
                    _this.correctPosition();
                }
                if (_this.stack.y < _this.limitBottomY) {
                    _this.smoothMovTimer.remove();
                    _this.correctPosition();
                }
            },
            loop: true
        });
    };
    ModelGridTable.prototype.increaseDY = function (y) {
        this.stack.y += y;
        //this.table.y += y;
        this.limitTopY += y;
        this.limitBottomY += y;
    };
    ModelGridTable.prototype.increaseTableDY = function (y) {
        this.table.y += y;
    };
    return ModelGridTable;
}());
var SlotGridTable = /** @class */ (function () {
    function SlotGridTable(scene, config) {
        this.stack = [];
        this.table = [];
        this.tableMovement = [false, false];
        this.items = [];
        this.itemW = [];
        this.itemH = [];
        this.limitLeftX = [];
        this.limitRightX = [];
        // this.limitLeftX = []
        // this.limitLeftX = []
        for (var i = 0; i < 2; i++) {
            this.table[i] = config.background[i];
            this.table[i].displayHeight = config.height / 2;
            this.table[i].displayWidth = config.width;
            this.table[i].tint = 0xCCCCCC;
            this.table[i].alpha = 0.01;
            this.tableMovement[i] = false;
        }
        this.table[0].setPosition(config.x, config.y);
        this.table[0].setOrigin(0, 0);
        this.table[1].setPosition(config.x, config.y + config.height / 2);
        this.table[1].setOrigin(0, 0);
        var bindConfig = { isDown: [false, false], lastPosX: [0, 0], travelDistance: [0, 0], velocityX: [0, 0] };
        this.bindEvent(scene, bindConfig, 0);
        this.scene = scene;
        this.config = config;
        this.modelName = config.modelName;
        console.log('modelName', this.modelName);
        // added by asset
        this.overlapWidth = 30;
        this.cloudBoardConfig = { x: 0, y: config.y, width: config.x + this.overlapWidth, height: config.height };
        this.communityBoardConfig = { x: 0, y: config.y + 220, width: config.x + this.overlapWidth, height: config.height };
        this.stack = [];
        this.stack[0] = scene.add.container(config.x, config.y);
        this.stack[1] = scene.add.container(config.x, config.y + config.height / 2);
        this.headerStack = scene.add.container(config.x, config.y);
        this.headerStack.depth = 510;
        this.communityHeaderStack = scene.add.container(0, config.height / 2 + 30);
        this.communityHeaderStack.depth = 510;
        // this.headerStack.setDisplaySize(300, 100)
        this.sloteItemsData = config.sloteItemsData;
        this.sloteItemsInfo = [];
        this.currentSloteIndex = 0;
        this.containerAry = [];
        this.sortFlag = "feature";
        this.isLoggedIn = false;
        this.user_id = "";
        this.checkUserLoggedIn();
    }
    SlotGridTable.prototype.checkUserLoggedIn = function () {
        var _this = this;
        var event = new CustomEvent('checkUserLoggedIn', { detail: { callback: function (res) {
                    console.log("checkUserLoggedIn", res);
                    var loginFlag = res[0];
                    _this.isLoggedIn = loginFlag;
                    _this.user_id = _this.isLoggedIn ? res[1] : _this.user_id;
                } } });
        document.dispatchEvent(event);
    };
    SlotGridTable.prototype.updateSloteItemData = function () {
        console.log('updateSloteItemData', this.items, this.currentSloteIndex);
        var _a = this.items[0][this.currentSloteIndex].sloteItemInfo, mid = _a.id, rest = __rest(_a, ["id"]);
        var _b = this.sloteItemsInfo[this.currentSloteIndex], is_shared = _b.is_shared, download = _b.download, like = _b.like, shared_date = _b.shared_date;
        var model_data = __assign(__assign({}, rest), { is_shared: is_shared, download_count: download, like_count: like, shared_date: shared_date });
        this.config.updateSloteItemData(mid, model_data);
    };
    SlotGridTable.prototype.bindEvent = function (scene, config, tableIndex) {
        var _this = this;
        var isDown = config.isDown, lastPosX = config.lastPosX, travelDistance = config.travelDistance, velocityX = config.velocityX;
        // const i = tableIndex
        // isDown[i] = false;
        // lastPosX[i] = 0;
        // travelDistance[i] = 0;
        // velocityX[i] = 0;
        this.table[0].setInteractive().on('pointerdown', function (pointer, localX, localY, event) {
            if (pointer.y < (_this.config.height / 2 + _this.config.y)) {
                isDown[0] = true;
                console.log('pointdown', 0);
            }
            // isDown[0] = true;
            console.log('pointdown', 0);
        }).on('pointermove', function (pointer, localX, localY, event) {
            // console.log('pointermove', 0)
            if (isDown[0]) {
                // console.log('pointermove', isDown, 0)
                if (lastPosX[0] == 0) {
                    lastPosX[0] = pointer.x;
                }
                _this.stack[0].x += pointer.x - lastPosX[0];
                travelDistance[0] += Math.abs(pointer.x - lastPosX[0]);
                lastPosX[0] = pointer.x;
                if (travelDistance[0] > 10) {
                    _this.tableMovement[0] = true;
                    // added by asset
                    if (_this.cloudBoard) {
                        _this.cloudBoard.board.setVisible(!_this.tableMovement[0]);
                    }
                }
            }
        }).on('pointerup', function (pointer, localX, localY, event) {
            // console.log('pointerup', 0)
            lastPosX[0] = 0;
            if (isDown[0]) {
                isDown[0] = false;
                _this.correctPosition(0);
                _this.detectEvent(pointer, event, 0);
            }
            travelDistance[0] = 0;
            if (_this.tableMovement[0]) {
                _this.doInertiaMovement(velocityX[0], 0);
            }
        }).on('pointerout', function (pointer, localX, localY, event) {
            lastPosX[0] = 0;
            if (isDown[0]) {
                isDown[0] = false;
                // console.log('pointerout', 0)
                _this.correctPosition(0);
                _this.detectEvent(pointer, event, 0);
            }
            travelDistance[0] = 0;
            _this.tableMovement[0] = false;
        });
        scene.events.on("velocity", function (vx, vy) {
            velocityX[0] = vx;
        });
        this.table[1].setInteractive().on('pointerdown', function (pointer, localX, localY, event) {
            if (pointer.y > (_this.config.height / 2 + _this.config.y)) {
                isDown[1] = true;
                // console.log('pointdown', 1, pointer.y, this.config.height / 2)
            }
            // isDown[1] = true;
            // console.log('pointdown', 1)
        }).on('pointermove', function (pointer, localX, localY, event) {
            // console.log('pointermove', 1)
            if (isDown[1]) {
                // console.log('pointermove', isDown, 1)
                if (lastPosX[1] == 0) {
                    lastPosX[1] = pointer.x;
                }
                _this.stack[1].x += pointer.x - lastPosX[1];
                travelDistance[1] += Math.abs(pointer.x - lastPosX[1]);
                lastPosX[1] = pointer.x;
                if (travelDistance[1] > 10) {
                    _this.tableMovement[1] = true;
                    // added by asset
                }
            }
        }).on('pointerup', function (pointer, localX, localY, event) {
            // console.log('pointerup', 1)
            lastPosX[1] = 0;
            if (isDown[1]) {
                isDown[1] = false;
                _this.correctPosition(1);
                _this.detectEvent(pointer, event, 1);
            }
            travelDistance[1] = 0;
            if (_this.tableMovement[1]) {
                _this.doInertiaMovement(velocityX[1], 1);
            }
            _this.tableMovement[1] = false;
            // added by asset
        }).on('pointerout', function (pointer, localX, localY, event) {
            lastPosX[1] = 0;
            if (isDown[1]) {
                isDown[1] = false;
                // console.log('pointerout', 1)
                _this.correctPosition(1);
                _this.detectEvent(pointer, event, 1);
            }
            travelDistance[1] = 0;
            _this.tableMovement[1] = false;
        });
        scene.events.on("velocity", function (vx, vy) {
            velocityX[1] = vx;
        });
    };
    // added by asset
    SlotGridTable.prototype.setItems = function (items, tableIndex) {
        var _this = this;
        this.items[tableIndex] = items;
        var columns = this.config.table.columns;
        // console.log("setItems", items)
        items.forEach(function (element, index) {
            var cellWidth = _this.config.table.cellWidth;
            var cellHeight = _this.config.table.cellHeight;
            // console.log('foreach', index, element)
            // added by asset
            element.setDisplaySize(cellWidth, cellHeight, tableIndex === 1);
        });
        var itemH = items[0].getDisplayHeight();
        var itemW = items[0].getDisplayWidth();
        this.itemW[tableIndex] = itemW;
        this.itemH[tableIndex] = itemH;
        this.table[tableIndex].displayWidth = itemW * Math.ceil(items.length / columns);
        this.limitLeftX[tableIndex] = this.config.x;
        this.limitRightX[tableIndex] = -Math.max(this.table[tableIndex].displayWidth - this.config.width, 0) + this.config.x;
        if (tableIndex === 0) {
            items.forEach(function (element, index) {
                var _a = element.sloteItemInfo, is_shared = _a.is_shared, like_count = _a.like_count, download_count = _a.download_count, shared_date = _a.shared_date;
                _this.sloteItemsInfo[index] = { is_shared: is_shared, like: like_count, download: download_count, shared_date: shared_date };
            });
        }
        var index = 0;
        items.forEach(function (element) {
            var col = index % columns;
            var x = Math.floor(index / columns) * itemW;
            var y = col * itemH;
            var cellContainer = _this.scene.add.container(x, y);
            if (tableIndex) {
                _this.containerAry.push(cellContainer);
            }
            element.initBodyPart();
            var model_visibility = false;
            if (element.shareFlag) {
                if (element.sloteItemInfo.is_shared) {
                    model_visibility = true;
                }
                else {
                    model_visibility = false;
                }
            }
            else {
                model_visibility = true;
            }
            cellContainer.add(element.stack);
            _this.stack[tableIndex].add(cellContainer);
            element.bodyPartManager.bodyPartStack.setVisible(model_visibility);
            element.cloudBoard.setVisible(model_visibility);
            index++;
        });
        this.stack[tableIndex].x = this.config.x;
        if (tableIndex === 0) {
            // added by asset
            var _a = this.sloteItemsInfo[this.currentSloteIndex], download = _a.download, like = _a.like, is_shared = _a.is_shared;
            var cloudBoardConfig = __assign(__assign({}, this.cloudBoardConfig), { visibility: !this.tableMovement[0], counter: {
                    like: like,
                    download: download,
                }, is_shared: is_shared, setShareFlag: function () {
                    _this.checkUserLoggedIn();
                    if (_this.isLoggedIn) {
                        console.log("shared slote");
                        // const currentSlote = this.items[1][this.currentSloteIndex]
                        _this.sloteItemsInfo[_this.currentSloteIndex].is_shared = true;
                        _this.sloteItemsInfo[_this.currentSloteIndex].like = 1;
                        _this.cloudBoard.counter.like = _this.sloteItemsInfo[_this.currentSloteIndex].like;
                        // this.items[1][this.currentSloteIndex].counter.like = this.sloteItemsInfo[this.currentSloteIndex].like;
                        // this.items[1][this.currentSloteIndex].updateCloudButtons();
                        _this.sloteItemsInfo[_this.currentSloteIndex].shared_date = new Date().toISOString();
                        // currentSlote.bodyPartManager.bodyPartStack.setVisible(true);
                        // currentSlote.like_button.setVisible(true);
                        // currentSlote.download_button.setVisible(true);
                        var sortedSharedSloteIndexs = _this.getSortedSharedSloteIndexs(_this.sortFlag);
                        _this.sortSharedSlotes(sortedSharedSloteIndexs);
                        _this.updateSloteItemData();
                        return true;
                    }
                    else {
                        document.getElementById("btn-login").click();
                        return false;
                    }
                }, likeIncrement: function () {
                    // this.checkUserLoggedIn();
                    // if (this.isLoggedIn) {
                    //     const { like } = this.sloteItemsInfo[this.currentSloteIndex]
                    //     this.sloteItemsInfo[this.currentSloteIndex].like = like?like - 1:like + 1; 
                    //     this.cloudBoard.counter.like = this.sloteItemsInfo[this.currentSloteIndex].like
                    //     this.items[1][this.currentSloteIndex].counter.like = this.sloteItemsInfo[this.currentSloteIndex].like;
                    //     this.items[1][this.currentSloteIndex].updateCloudButtons();
                    //     if(this.sortFlag === 'like') {
                    //         const sortedSharedSloteIndexs = this.getSortedSharedSloteIndexs(this.sortFlag);
                    //         this.sortSharedSlotes(sortedSharedSloteIndexs);
                    //     }
                    //     this.updateSloteItemData();
                    // } else {
                    //     document.getElementById("btn-login").click();
                    // }
                }, downloadIncrement: function () {
                    _this.checkUserLoggedIn();
                    if (_this.isLoggedIn) {
                        var download_1 = _this.sloteItemsInfo[_this.currentSloteIndex].download;
                        _this.sloteItemsInfo[_this.currentSloteIndex].download = download_1 + 1;
                        _this.cloudBoard.counter.download = _this.sloteItemsInfo[_this.currentSloteIndex].download;
                        _this.items[1][_this.currentSloteIndex].counter.download = _this.sloteItemsInfo[_this.currentSloteIndex].download;
                        _this.items[1][_this.currentSloteIndex].updateCloudButtons();
                        if (_this.sortFlag === 'download') {
                            var sortedSharedSloteIndexs = _this.getSortedSharedSloteIndexs(_this.sortFlag);
                            _this.sortSharedSlotes(sortedSharedSloteIndexs);
                        }
                        _this.updateSloteItemData();
                    }
                    else {
                        document.getElementById("btn-login").click();
                    }
                } });
            this.cloudBoard = new CloudIconButtonContainer(this.scene, cloudBoardConfig);
            var titleImageID = "" + this.modelName; // gonna use this variable as modal title image identifier
            this.titleImage = this.scene.add.image(0, 0, this.modelName + "_Title");
            this.titleImage.setOrigin(0, 0);
            // this.titleImage.setScale(.6)
            this.titleImage.setDisplaySize(itemW * 3 / 5, 70);
            this.titleImage.setPosition(itemW / 5, -20);
            this.headerStack.add(this.titleImage);
            this.cloudBoard.updateButton();
        }
        else {
            // added by asset
            var communityBoardConfig = __assign(__assign({}, this.communityBoardConfig), { sortFlag: this.sortFlag, visibility: !this.tableMovement[1], setSortFlag: function (flag) {
                    _this.sortFlag = flag;
                    _this.communityBoard.sortFlag = _this.sortFlag;
                    var sortedSharedSloteIndexs = _this.getSortedSharedSloteIndexs(_this.sortFlag);
                    console.log("communityBoardConfigsortedSharedSloteIndexs", sortedSharedSloteIndexs);
                    _this.sortSharedSlotes(sortedSharedSloteIndexs);
                } });
            this.communityBoard = new CommuntyIconButtonContainer(this.scene, communityBoardConfig);
            // added by asset
            this.communitytitleImage = this.scene.add.image(0, 0, "UISaveslotCommunity");
            this.communitytitleImage.setOrigin(0, 0);
            // this.communitytitleImage.setScale(.4)
            this.communitytitleImage.setDisplaySize(this.config.x + this.overlapWidth, 70);
            this.communitytitleImage.setPosition(0, 10);
            this.communityHeaderStack.add(this.communitytitleImage);
            this.UImargin = this.scene.add.image(0, 0, "UISaveslotMiddle");
            this.UImargin.setOrigin(0, 0);
            this.UImargin.setScale(.6);
            this.UImargin.setPosition(0, 0);
            this.UImargin.setDisplaySize(this.config.width + this.config.x, 10);
            this.communityHeaderStack.add(this.UImargin);
            this.items[tableIndex].forEach(function (element, index) {
                // console.log('updatecloudbutton', element)
                if (element.shareFlag && element.sloteItemInfo.is_shared) {
                    var _a = _this.sloteItemsInfo[index], download = _a.download, like = _a.like;
                    element.counter = { download: download, like: like };
                    element.updateCloudButtons();
                }
            });
            var sortedSharedSloteIndexs = this.getSortedSharedSloteIndexs(this.sortFlag);
            this.sortSharedSlotes(sortedSharedSloteIndexs);
        }
    };
    SlotGridTable.prototype.addItems = function (items, tableIndex) {
        var _this = this;
        var columns = this.config.table.columns;
        items.forEach(function (element, index) {
            var cellWidth = _this.config.table.cellWidth;
            var cellHeight = _this.config.table.cellHeight;
            // added by asset
            element.setDisplaySize(cellWidth, cellHeight, index % 2 === 1);
        });
        this.table[tableIndex].displayWidth += this.itemW[tableIndex] * Math.ceil(items.length / columns);
        this.limitLeftX[tableIndex] = this.config.x;
        this.limitRightX[tableIndex] = -Math.max(this.table[tableIndex].displayWidth - this.config.width, 0) + this.config.x;
        var index = this.items[tableIndex].length;
        items.forEach(function (element) {
            var col = index % columns;
            var x = Math.floor(index / columns) * _this.itemW[tableIndex];
            var y = col * _this.itemH[tableIndex];
            var cellContainer = _this.scene.add.container(x, y);
            element.initBodyPart();
            cellContainer.add(element.stack);
            _this.stack[tableIndex].add(cellContainer);
            index++;
        });
        this.items[tableIndex] = this.items[tableIndex].concat(items);
    };
    SlotGridTable.prototype.detectEvent = function (pointer, event, tableIndex) {
        var _this = this;
        // console.log('detectEvent', tableIndex, this.tableMovement)
        if (!this.tableMovement[tableIndex]) {
            this.items[tableIndex].forEach(function (element, cellIndex) {
                if (element.shelfSprite.getBounds().contains(pointer.x, pointer.y)) {
                    _this.checkUserLoggedIn();
                    if (tableIndex) {
                        if (_this.sloteItemsInfo[cellIndex].is_shared) {
                            _this.table[tableIndex].emit("cell.click", element.tabIndex);
                        }
                    }
                    else {
                        if (_this.isLoggedIn) {
                            _this.table[tableIndex].emit("cell.click", element.tabIndex);
                        }
                        else {
                            document.getElementById("btn-login").click();
                        }
                    }
                }
            });
        }
    };
    SlotGridTable.prototype.correctPosition = function (tableIndex) {
        // console.log('correctPosition', tableIndex)
        if (this.config.scrollMode == 1) {
            if (this.stack[tableIndex].x > this.limitLeftX[tableIndex]) {
                this.scrollTo(this.limitLeftX[tableIndex], tableIndex);
            }
            else if (this.stack[tableIndex].x < this.limitRightX[tableIndex]) {
                this.scrollTo(this.limitRightX[tableIndex], tableIndex);
            }
            else {
                if (this.itemW && this.config.width) {
                    var gapIndex = Math.round((this.stack[tableIndex].x - (this.config.x + this.overlapWidth)) / this.itemW[tableIndex]);
                    var posX = this.itemW[tableIndex] * gapIndex + this.config.x;
                    this.scrollTo(posX, tableIndex);
                }
            }
        }
    };
    // Original fn: modified by asset
    // correctPosition(){
    //     if(this.config.scrollMode == 1){
    //         if(this.stack.x > this.limitLeftX){
    //             this.scrollTo(this.limitLeftX);
    //         }
    //         if(this.stack.x < this.limitRightX){
    //             this.scrollTo(this.limitRightX);
    //         }
    //     }
    // }
    SlotGridTable.prototype.scrollTo = function (targetPosX, tableIndex) {
        var _this = this;
        // console.log('scrollTo', tableIndex)
        var frames = 15;
        var unit = (targetPosX - this.stack[tableIndex].x) / frames;
        // console.log('unit', unit, targetPosX, tableIndex)
        if (unit == 0) {
            return;
        }
        var duration = 100;
        var delay = duration / frames;
        if (this.timer)
            this.timer.destroy();
        this.timer = this.scene.time.addEvent({
            delay: delay,
            callback: function () {
                if (duration > 0) {
                    // console.log('scrollTo', duration)
                    _this.stack[tableIndex].x += unit;
                    duration -= delay;
                }
                else {
                    if (tableIndex === 0) {
                        _this.tableMovement[0] = false;
                        // added by asset
                        if (_this.cloudBoard) {
                            _this.cloudBoard.board.setVisible(!_this.tableMovement[0]);
                        }
                    }
                }
            },
            repeat: frames
        });
    };
    SlotGridTable.prototype.scrollToLeft = function (flag, tableIndex) {
        if (flag) {
            this.scrollTo(this.limitLeftX[tableIndex], tableIndex);
        }
    };
    SlotGridTable.prototype.moveByX = function (dx, tableIndex) {
        this.stack[tableIndex].x += dx;
        this.table[tableIndex].x += dx;
    };
    SlotGridTable.prototype.setVisible = function (visible, tableIndex) {
        this.stack[tableIndex].setVisible(visible);
        this.table[tableIndex].setVisible(visible);
    };
    SlotGridTable.prototype.dataChanged = function (tableIndex) {
        this.scrollTo(this.limitRightX[tableIndex], tableIndex);
    };
    SlotGridTable.prototype.doInertiaMovement = function (v, tableIndex) {
        var _this = this;
        // console.log('doInertiaMovement', tableIndex)
        var steps = 0;
        if (this.smoothMovTimer != undefined)
            this.smoothMovTimer.remove();
        var velocity = v;
        this.smoothMovTimer = this.scene.time.addEvent({
            delay: 10,
            callback: function () {
                velocity -= velocity / 10;
                _this.stack[tableIndex].x += velocity;
                if (velocity < 1 && velocity > -1)
                    _this.smoothMovTimer.remove();
                if (_this.stack[tableIndex].x > _this.limitLeftX[tableIndex]) {
                    _this.smoothMovTimer.remove();
                    _this.correctPosition(tableIndex);
                }
                else if (_this.stack[tableIndex].x < _this.limitRightX[tableIndex]) {
                    _this.smoothMovTimer.remove();
                    _this.correctPosition(tableIndex);
                }
                else {
                    if (_this.itemW && _this.config.width) {
                        var gapIndex = Math.round((_this.stack[tableIndex].x - (_this.config.x + _this.overlapWidth)) / _this.itemW[tableIndex]);
                        if (_this.currentSloteIndex !== gapIndex && tableIndex === 0) {
                            _this.currentSloteIndex = Math.abs(gapIndex);
                            _this.cloudBoard.is_shared = _this.sloteItemsInfo[_this.currentSloteIndex].is_shared;
                            var _a = _this.sloteItemsInfo[_this.currentSloteIndex], download = _a.download, like = _a.like;
                            console.log('this.currentSloteIndex', _this.currentSloteIndex, download, like);
                            _this.cloudBoard.counter = { download: download, like: like };
                            _this.cloudBoard.updateButton();
                        }
                        var posX = _this.itemW[tableIndex] * gapIndex + _this.config.x;
                        _this.scrollTo(posX, tableIndex);
                    }
                }
                steps++;
            },
            loop: true
        });
    };
    SlotGridTable.prototype.sortSharedSlotes = function (sort_indexs) {
        var _this = this;
        sort_indexs.forEach(function (e, i) {
            _this.containerAry[e.index].x = Math.floor(i / _this.config.table.columns) * _this.itemW[1];
        });
    };
    SlotGridTable.prototype.getSortedSharedSloteIndexs = function (sort_flag) {
        var sort_indexs = this.sloteItemsInfo.map(function (e, index) {
            return __assign({ index: index }, e);
        });
        switch (sort_flag) {
            case "random":
                return sort_indexs.sort(function (a, b) { return !a.is_shared && b.is_shared ? 1 : -1; });
            case "like":
                return sort_indexs.sort(function (a, b) {
                    if (b.is_shared) {
                        if (a.is_shared) {
                            return a.like < b.like ? 1 : -1;
                        }
                        else {
                            return 1;
                        }
                    }
                    else {
                        return -1;
                    }
                });
            case "download":
                return sort_indexs.sort(function (a, b) {
                    if (b.is_shared) {
                        if (a.is_shared) {
                            return a.download < b.download ? 1 : -1;
                        }
                        else {
                            return 1;
                        }
                    }
                    else {
                        return -1;
                    }
                });
            case "date":
                return sort_indexs.sort(function (a, b) {
                    if (b.is_shared) {
                        if (a.is_shared) {
                            return a.shared_date < b.shared_date ? 1 : -1;
                        }
                        else {
                            return 1;
                        }
                    }
                    else {
                        return -1;
                    }
                });
            default:
                return sort_indexs.sort(function (a, b) { return !a.is_shared && b.is_shared ? 1 : -1; });
        }
    };
    return SlotGridTable;
}());
var TmenuGridTable = /** @class */ (function () {
    function TmenuGridTable(scene, config) {
        this.table = config.background;
        this.table.setPosition(config.x, config.y);
        this.table.displayHeight = config.height;
        this.table.displayWidth = config.width;
        this.table.setOrigin(0, 0).setDepth(22);
        this.stack = scene.add.container(config.x, config.y).setDepth(22);
        this.bindEvent(scene);
        this.scene = scene;
        this.config = config;
        this.tableMovement = false;
    }
    TmenuGridTable.prototype.bindEvent = function (scene) {
        var _this = this;
        var isDown = false;
        var lastPosX = 0;
        var travelDistance = 0;
        var velocityX = 0;
        this.table.setInteractive().on('pointerdown', function (pointer, localX, localY, event) {
            isDown = true;
        }).on('pointermove', function (pointer, localX, localY, event) {
            if (isDown) {
                if (lastPosX == 0) {
                    lastPosX = pointer.x;
                }
                _this.stack.x += pointer.x - lastPosX;
                travelDistance += Math.abs(pointer.x - lastPosX);
                lastPosX = pointer.x;
                if (travelDistance > 10) {
                    _this.tableMovement = true;
                }
            }
        }).on('pointerup', function (pointer, localX, localY, event) {
            lastPosX = 0;
            if (isDown) {
                isDown = false;
                _this.correctPosition();
                _this.detectEvent(pointer, event);
            }
            if (_this.tableMovement) {
                _this.doInertiaMovement(velocityX);
            }
            travelDistance = 0;
            _this.tableMovement = false;
        }).on('pointerout', function (pointer, localX, localY, event) {
            lastPosX = 0;
            if (isDown) {
                isDown = false;
                _this.correctPosition();
                _this.detectEvent(pointer, event);
            }
            travelDistance = 0;
            _this.tableMovement = false;
        });
        scene.events.on("velocity", function (vx, vy) {
            velocityX = vx;
        });
    };
    TmenuGridTable.prototype.setPosition = function (pos) {
        if (pos.x != undefined) {
            this.stack.x = pos.x;
            this.table.x = pos.x;
        }
        if (pos.y != undefined) {
            this.stack.y = pos.y;
            this.table.y = pos.y;
        }
    };
    TmenuGridTable.prototype.setItems = function (items) {
        var _this = this;
        this.items = items;
        var columns = this.config.table.columns;
        var width = this.config.width;
        var height = this.config.height;
        // get first element of array        
        var firstelement = null;
        items.forEach(function (element) {
            firstelement = element;
            return;
        });
        var itemH = firstelement.getDisplayHeight();
        var itemW = firstelement.getDisplayWidth();
        var paddingTop = 0;
        var marginLeft = 0;
        var index = 0;
        items.forEach(function (element) {
            element.index = index;
            var col = index % columns;
            var x = Math.floor(index / columns) * itemW + itemW / 2;
            var y = col * itemH + itemH / 2;
            var cellContainer = _this.scene.add.container(x, y);
            cellContainer.add(element.stack);
            _this.stack.add(cellContainer);
            index++;
        });
        if (this.config.scrollMode == 1) {
            this.table.displayWidth = Math.ceil(itemW * index / this.config.table.columns);
            this.limitLeftX = this.config.x;
            this.limitRightX = -Math.max(this.table.displayWidth - this.config.width, 0) + this.config.x;
            this.table.displayWidth = LayoutContants.getInstance().SCREEN_WIDTH;
        }
    };
    TmenuGridTable.prototype.detectEvent = function (pointer, event) {
        var _this = this;
        if (!this.tableMovement) {
            this.items.forEach(function (element) {
                if (element.image.getBounds().contains(pointer.x, pointer.y)) {
                    _this.table.emit("cell.click", element.bodyPart.modelPart.tMenuOrder);
                }
            });
        }
    };
    TmenuGridTable.prototype.correctPosition = function () {
        if (this.config.scrollMode == 1) {
            if (this.stack.x > this.limitLeftX) {
                this.scrollTo(this.limitLeftX);
            }
            if (this.stack.x < this.limitRightX) {
                this.scrollTo(this.limitRightX);
            }
        }
    };
    TmenuGridTable.prototype.scrollTo = function (targetPosX) {
        // var frames = 15;
        // const unit = (targetPosX - this.stack.x)/frames;
        // if(unit == 0){
        //     return;
        // }
        // var duration = 100;
        // var delay = duration/frames;
        // if(this.timer) this.timer.destroy();
        // this.timer = this.scene.time.addEvent({
        //     delay: delay,
        //     callback: ()=>{
        //         if(duration > 0){
        //             this.stack.x += unit;
        //             duration -= delay;
        //         }
        //     },
        //     repeat: frames
        // });
        if (this.tween)
            this.tween.remove();
        this.tween = this.scene.tweens.add({
            targets: this.stack,
            x: targetPosX,
            // alpha: { start: 0, to: 1 },
            // alpha: 1,
            // alpha: '+=1',
            ease: 'Back.easeIn',
            duration: 300,
            repeat: 0,
            yoyo: false
        });
    };
    TmenuGridTable.prototype.moveByX = function (dx) {
        this.stack.x += dx;
        this.table.x += dx;
    };
    TmenuGridTable.prototype.setVisible = function (visible) {
        this.stack.setVisible(visible);
        this.table.setVisible(visible);
    };
    TmenuGridTable.prototype.setMoveByY = function (dy) {
        this.stack.y += dy;
        this.table.y += dy;
    };
    TmenuGridTable.prototype.getTableY = function () {
        return this.stack.y;
    };
    TmenuGridTable.prototype.doInertiaMovement = function (v) {
        var _this = this;
        var steps = 0;
        if (this.smoothMovTimer != undefined)
            this.smoothMovTimer.remove();
        var velocity = v;
        // if(this.tween) this.tween.remove();
        // this.tween = this.scene.tweens.add({
        //     targets: this.stack,
        //     x: '+'+v,
        //     // alpha: { start: 0, to: 1 },
        //     // alpha: 1,
        //     // alpha: '+=1',
        //     ease: 'Back.easeOut',       // 'Cubic', 'Elastic', 'Bounce', 'Back'
        //     duration: 300,
        //     repeat: 0,            // -1: infinity
        //     yoyo: false
        // });
        this.smoothMovTimer = this.scene.time.addEvent({
            delay: 8,
            callback: function () {
                velocity -= velocity / 9;
                _this.stack.x += velocity;
                if (velocity < 1 && velocity > -1)
                    _this.smoothMovTimer.remove();
                if (_this.stack.x > _this.limitLeftX) {
                    _this.smoothMovTimer.remove();
                    _this.correctPosition();
                }
                if (_this.stack.x < _this.limitRightX) {
                    _this.smoothMovTimer.remove();
                    _this.correctPosition();
                }
                steps++;
            },
            loop: true
        });
    };
    return TmenuGridTable;
}());
var ColorsRange = /** @class */ (function () {
    function ColorsRange(xPos, yPos, red, green, blue) {
        this.xPos = xPos;
        this.yPos = yPos;
        this.red = red;
        this.green = green;
        this.blue = blue;
    }
    return ColorsRange;
}());
var Global = /** @class */ (function () {
    function Global() {
        this.sloteItemCountSet = new Map();
        this.modelCharacteristics = new ModelCharacteristics();
        // do something construct...
    }
    Global.getInstance = function () {
        if (!Global.instance) {
            Global.instance = new Global();
            // ... any one time initialization goes here ...
        }
        return Global.instance;
    };
    Global.prototype.setScreenModelArr = function (arr) {
        this.modelArr = arr;
    };
    Global.prototype.getModelArr = function () {
        return this.modelArr;
    };
    Global.prototype.getModelByName = function (modelName) {
        for (var i = 0; i < this.modelArr.length; i++) {
            if (this.modelArr[i].modelName === modelName) {
                return this.modelArr[i];
            }
        }
        return null;
    };
    Global.prototype.setModel = function (model) {
        this.selectedModel = model;
    };
    Global.prototype.getModel = function () {
        return this.selectedModel;
    };
    Global.prototype.setCurrentModel = function (model) {
        this.savedModel = model;
    };
    Global.prototype.saveCurrentModel = function (model) {
        this.savedModel.savetoJson(this.selectedModel.modelName);
    };
    Global.prototype.getSavedModel = function () {
        return this.savedModel;
    };
    Global.prototype.setSeletectedModelParts = function (modelParts) {
        this.modelParts = modelParts;
    };
    Global.prototype.getSeletectedModelParts = function () {
        return this.modelParts;
    };
    Global.prototype.setSloteItemsCount = function (modelName, sloteItemsCount) {
        this.sloteItemCountSet.set(modelName, sloteItemsCount);
    };
    Global.prototype.getSloteItemsCount = function (modelName) {
        if (this.sloteItemCountSet.has(modelName)) {
            return this.sloteItemCountSet.get(modelName);
        }
        else {
            return 0;
        }
    };
    Global.prototype.getModelCharacteristics = function () {
        return this.modelCharacteristics;
    };
    Global.prototype.setResetModelFlag = function (flag) {
        this.resetModel = flag;
    };
    Global.prototype.getResetModelFlag = function () {
        return this.resetModel;
    };
    Global.prototype.setCurrentScene = function (curScene) {
        this.currentScene = curScene;
    };
    Global.prototype.getCurrentScene = function () {
        return this.currentScene;
    };
    Global.prototype.setNavigation = function (_scene) {
        // window.onhashchange = () =>{
        //     // this.routeNavigation();
        // }
    };
    Global.prototype.checkLogoView = function () {
        //get current url
        var url = window.location.href;
        // convert url string to URL object
        var currentURL = new URL(url);
        // parse hash
        var urlHash = currentURL.hash;
        var urlSegments = [null, null];
        if (urlHash)
            urlSegments = urlHash.replace('#', '').split('/');
        /**
         * Route Navigations
         * [0] - Scene Name
         * [1] - Model Name
         */
        switch (urlSegments[0]) {
            case CST.SCENES.SAVE_SLOTE:
                if (urlSegments[1]) {
                    //let curModel = Global.getInstance().getModelByName(urlSegments[1]);
                    //if(curModel){
                    return false;
                    //}
                }
            case CST.SCENES.MODELING:
                if (urlSegments[1]) {
                    var curModel = Global.getInstance().getModelByName(urlSegments[1]);
                    //if(curModel){                        
                    return false;
                    //}
                }
            case CST.SCENES.MAIN:
                return false;
            default:
                return true;
        }
    };
    Global.prototype.setHelpersImages = function (arr) {
        this.helperImageArr = arr;
    };
    Global.prototype.getHelperImages = function () {
        return this.helperImageArr;
    };
    Global.prototype.routeNavigation = function () {
        var _scene = this.currentScene;
        //get current url
        var url = window.location.href;
        // convert url string to URL object
        var currentURL = new URL(url);
        // parse hash
        var urlHash = currentURL.hash;
        var urlSegments = [null, null];
        if (urlHash)
            urlSegments = urlHash.replace('#', '').split('/');
        /**
         * Route Navigations
         * [0] - Scene Name
         * [1] - Model Name
         */
        switch (urlSegments[0]) {
            case CST.SCENES.SAVE_SLOTE:
                if (urlSegments[1]) {
                    var curModel = Global.getInstance().getModelByName(urlSegments[1]);
                    if (curModel) {
                        Global.getInstance().setModel(curModel);
                        _scene.start(CST.SCENES.SAVE_SLOTE, { initSound: true });
                        break;
                    }
                }
            case CST.SCENES.MODELING:
                if (urlSegments[1]) {
                    var curModel = Global.getInstance().getModelByName(urlSegments[1]);
                    if (curModel) {
                        Global.getInstance().setModel(curModel);
                        // _scene.start(CST.SCENES.MODELING);
                        _scene.start(CST.SCENES.SAVE_SLOTE, { initSound: true });
                        // Global.getInstance().setModel(curModel);
                        // _scene.start(CST.SCENES.SAVE_SLOTE,{direct:true});
                        break;
                    }
                }
            case CST.SCENES.MAIN:
                if (Global.getInstance().getCurrentScene().name == CST.SCENES.MAIN)
                    break;
                _scene.start(CST.SCENES.MAIN);
                break;
            default:
                window.location.hash = CST.SCENES.MAIN;
                _scene.start(CST.SCENES.MAIN);
        }
    };
    return Global;
}());
var ColorPositionPair = /** @class */ (function () {
    function ColorPositionPair(palletNumber, buttonIndex) {
        this.palletNumber = palletNumber;
        this.buttonIndex = buttonIndex;
    }
    return ColorPositionPair;
}());
var Feature = /** @class */ (function () {
    function Feature(partName, palleteSection, partLink, partToggle, tMenuButton, tabLink, colorLink, colorLinkV2, colorModifier, paralax, part_id, parent_id) {
        this.partName = partName;
        this.palleteSection = palleteSection;
        this.partLink = partLink;
        this.partToggle = partToggle;
        this.tMenuButton = tMenuButton;
        this.tabLink = tabLink;
        this.colorLink = colorLink;
        this.colorLinkV2 = colorLinkV2;
        this.colorModifier = +colorModifier;
        this.paralax = paralax;
        this.part_id = part_id;
        this.parent_id = parent_id || 0;
        this.isChildren = this.parent_id > 0 ? true : false;
    }
    return Feature;
}());
var FileNames = /** @class */ (function () {
    function FileNames(models, atlasFront, atlasFrames, song, partsCsv, featuresCsv, saveFile) {
        this.models = models;
        this.atlasFront = atlasFront;
        this.atlasFrames = atlasFrames;
        this.song = song;
        this.partsCsv = partsCsv;
        this.featuresCsv = featuresCsv;
        this.saveFile = saveFile;
    }
    FileNames.prototype.generateFileNames = function (selectedModel, isHd) {
        this.atlasFront = selectedModel.modelName + "_Pack0.json";
        this.atlasFrames = selectedModel.modelName + "_Pack1.json";
        this.atlasSDFront = selectedModel.modelName + "-SD" + "_Pack0.json";
        this.atlasSDFrames = selectedModel.modelName + "-SD" + "_Pack1.json";
        // this.atlasFront = selectedModel.modelName + "_Pack0.json";
        // this.atlasFrames = selectedModel.modelName + "_Pack1.json";
        this.song = selectedModel.modelName + "_Song.mp3";
        this.partsCsv = selectedModel.modelName + "_Parts.csv";
        this.featuresCsv = selectedModel.modelName + "_Features.csv";
        this.saveFile = selectedModel.modelName + ".sav";
        return this;
    };
    return FileNames;
}());
var Finish;
(function (Finish) {
    Finish["NORMAL"] = "Natural";
    Finish["GLOSS"] = "Gloss";
})(Finish || (Finish = {}));
var SwitchStatus;
(function (SwitchStatus) {
    SwitchStatus[SwitchStatus["LEFT"] = -1] = "LEFT";
    SwitchStatus[SwitchStatus["STOP"] = 0] = "STOP";
    SwitchStatus[SwitchStatus["RIGHT"] = 1] = "RIGHT";
})(SwitchStatus || (SwitchStatus = {}));
var LayoutContants = /** @class */ (function () {
    function LayoutContants() {
        this.SCREEN_WIDTH = 720;
        this.SCREEN_HEIGHT = 1280;
        this.FLOAT_COLOR_BUTTON_POSITIONS = [210, 188, 166, 142, 120, 97, 73, 50, 28];
        this.COLOR_ADJUST_DOT_POSITIONS = [210, 188, 166, 142, 120, 97, 73, 50, 28];
        this.EDIT_BOX_BASE_WIDTH = 720;
        this.EDIT_BOX_BASE_HEIGHT = 1280;
        this.SCREEN_SIZE_COEF = this.EDIT_BOX_BASE_WIDTH / this.SCREEN_WIDTH;
        this.SCREEN_HEIGHT_COEF = this.EDIT_BOX_BASE_HEIGHT / this.SCREEN_HEIGHT;
        this.BASE_FPS = 60;
        this.FADE_SPRITE_ALPHA = 0.5;
        this.COLOR_ADJUST_BUTTON_SCALE = 0.40;
        this.COLOR_ADJUST_DOT_SCALE = 0.80;
        this.COLOR_ADJUST_SCALE = 0.455;
        this.SCALE = 0.49;
        this.BOOT_PROGRESSBAR_SCALE = 1.5;
        this.SETTINGS_BUTTONS_SCALE = 0.7;
        this.BUY_BUTTON_SCALE = 0.6;
        this.SELECT_BUTTON_SCALE = 0.5;
        this.SMALL_COLOR_SELECTION_SCALE = 0.09;
        this.MEDIUM_COLOR_SELECTION_SCALE = 0.18;
        this.MAIN_COLOR_SELECTION_SCALE = 0.27;
        this.LARGE_COLOR_SELECTION_SCALE = 0.36;
        this.LOADING_IMAGE_SCALE = 0.75;
        this.NET_WARNING_SCALE = 0.75;
        this.RATE_IMAGE_SCALE = 0.75;
        this.PRIVACY_SCALE = 0.4;
        this.lABEL_SCALE = 1.6;
        this.lOADING_BITMAP_FONT_EXTRA_WIDTH = 100;
        this.BUTTONS_SIZE_BASE = 85;
        this.BODY_PART_BUTTON_WIDTH = 80;
        this.BASIC_ASPECT_RATIO = 0.75;
        this.BUTTONS_SIZE = 85;
        this.COLOR_SLIDER_X_OFFSET = 22;
        this.FLOAT_COLOR_SLIDER_X_OFFSET = 20;
        this.X_OFFSET_BETWEEN_SLIDERS = 62;
        this.FLOAT_SLIDER_BG_OFFSET_X = 49;
        this.SLOTS_UPPER_OFFSET = 85 * 0.6;
        this.SLIDER_BG_OFFSET_Y = 109;
        this.SLIDER_Y_OFFSET = 27;
        this.SLIDER_BUTTON_Y_OFFSET = 19;
        this.SAVESLOT_OFFSET_Y_COEF = 0.0897;
        this.BUY_NOW_OFFSET = 45;
        this.SIZE_TEXT_OFFSET = 75;
        this.ORDER_NOW_TEXT_SIZE = 40;
        this.SIZE_TEXT_SIZE = 30;
        this.PERCENTAGE_SIZE = 25;
        this.MODEL_ICON_WIDTH_COEF = 1.99;
        this.MODEL_ICON_HEIGHT_COEF = 1.5;
        this.DICE_OFFSET_X = 0;
        this.DICE_OFFSET_Y = 100;
        this.SIZE_BUTTON_OFFSET = 110;
        this.DICE_SIZE_IN_CM = 1.6;
        this.root_scale = 1;
        // do something construct...
    }
    LayoutContants.getInstance = function () {
        if (!LayoutContants.instance) {
            LayoutContants.instance = new LayoutContants();
            // ... any one time initialization goes here ...
        }
        return LayoutContants.instance;
    };
    LayoutContants.prototype.setRootScale = function (scale) {
        this.root_scale = scale;
    };
    LayoutContants.prototype.setScreenBounds = function (width, height) {
        this.NEW_COEF = (this.SCREEN_HEIGHT - this.BUTTONS_SIZE_BASE) / (height - this.BUTTONS_SIZE_BASE);
        this.SCREEN_WIDTH = width;
        this.SCREEN_HEIGHT = height;
        this.SCREEN_SIZE_COEF = Math.max(this.EDIT_BOX_BASE_WIDTH / this.SCREEN_WIDTH, 1);
        this.ORIGIN_SCREEN_SIZE_COEF = this.EDIT_BOX_BASE_WIDTH / this.SCREEN_WIDTH;
        // this.SCREEN_HEIGHT_COEF =  Math.max(this.EDIT_BOX_BASE_HEIGHT/this.SCREEN_HEIGHT, 1);
        this.SCREEN_HEIGHT_COEF = this.EDIT_BOX_BASE_HEIGHT / this.SCREEN_HEIGHT;
        this.COLOR_ADJUST_BUTTON_SCALE = 0.4 / this.SCREEN_SIZE_COEF;
        this.COLOR_ADJUST_DOT_SCALE = 0.80 / this.SCREEN_SIZE_COEF;
        this.COLOR_ADJUST_SCALE = 0.455 / this.SCREEN_SIZE_COEF;
        this.SCALE = 0.49 / this.SCREEN_SIZE_COEF;
        this.HEIGHT_SCALE = 0.49 / this.SCREEN_HEIGHT_COEF;
        this.BOOT_PROGRESSBAR_SCALE = 1.5 / this.SCREEN_SIZE_COEF;
        this.SETTINGS_BUTTONS_SCALE = 0.7 / this.SCREEN_SIZE_COEF;
        this.BUY_BUTTON_SCALE = 0.6 / this.SCREEN_SIZE_COEF;
        this.SELECT_BUTTON_SCALE = 0.5 / this.SCREEN_SIZE_COEF;
        this.SMALL_COLOR_SELECTION_SCALE = 0.09 / this.SCREEN_SIZE_COEF;
        this.MEDIUM_COLOR_SELECTION_SCALE = 0.18 / this.SCREEN_SIZE_COEF;
        this.MAIN_COLOR_SELECTION_SCALE = 0.27 / this.SCREEN_SIZE_COEF;
        this.LARGE_COLOR_SELECTION_SCALE = 0.36 / this.SCREEN_SIZE_COEF;
        this.LOADING_IMAGE_SCALE = 0.75 / this.SCREEN_SIZE_COEF;
        this.NET_WARNING_SCALE = 0.75 / this.SCREEN_SIZE_COEF;
        this.RATE_IMAGE_SCALE = 0.75 / this.SCREEN_SIZE_COEF;
        this.PRIVACY_SCALE = 0.4 / this.SCREEN_SIZE_COEF;
        this.lABEL_SCALE = 1.6 / this.SCREEN_SIZE_COEF;
        this.lOADING_BITMAP_FONT_EXTRA_WIDTH = 100 / this.SCREEN_SIZE_COEF;
        this.BUTTONS_SIZE_BASE = 85;
        this.BODY_PART_BUTTON_WIDTH = 80;
        this.BASIC_ASPECT_RATIO = 0.75;
        this.ACTUAL_ASPECT_RATIO = this.SCREEN_WIDTH / this.SCREEN_HEIGHT;
        this.ASPECT_RATIO_COEF = this.ACTUAL_ASPECT_RATIO / this.BASIC_ASPECT_RATIO;
        this.BACKGROUND_OFFSET_COEF = 1 - this.ASPECT_RATIO_COEF;
        this.BACKGROUND_OFFSET = this.SCREEN_HEIGHT * this.BACKGROUND_OFFSET_COEF * 0.5;
        this.MODEL_OFFSET_Y = 60 * (1 - this.SCREEN_HEIGHT_COEF);
        this.BUTTONS_SIZE = this.BUTTONS_SIZE_BASE / this.SCREEN_SIZE_COEF;
        this.COLOR_SLIDER_X_OFFSET = 22 / this.SCREEN_SIZE_COEF;
        this.FLOAT_COLOR_SLIDER_X_OFFSET = 20 / this.SCREEN_SIZE_COEF;
        this.X_OFFSET_BETWEEN_SLIDERS = 62 / this.SCREEN_SIZE_COEF;
        this.FLOAT_SLIDER_BG_OFFSET_X = 49 / this.SCREEN_SIZE_COEF;
        this.SLOTS_UPPER_OFFSET = this.BUTTONS_SIZE * 0.6;
        this.SLIDER_BG_OFFSET_Y = 109 / this.SCREEN_SIZE_COEF;
        this.SLIDER_Y_OFFSET = 27 / this.SCREEN_SIZE_COEF;
        this.SLIDER_BUTTON_Y_OFFSET = 19 / this.SCREEN_SIZE_COEF;
        this.SAVESLOT_OFFSET_Y_COEF = 0.0897;
        this.BUY_NOW_OFFSET = 45 / this.SCREEN_SIZE_COEF;
        this.SIZE_TEXT_OFFSET = 75 / this.SCREEN_SIZE_COEF;
        this.ORDER_NOW_TEXT_SIZE = 40 / this.SCREEN_SIZE_COEF;
        this.SIZE_TEXT_SIZE = 30 / this.SCREEN_SIZE_COEF;
        this.PERCENTAGE_SIZE = 25 / this.SCREEN_SIZE_COEF;
        this.MODEL_ICON_WIDTH_COEF = 1.99;
        this.MODEL_ICON_HEIGHT_COEF = 1.5;
        this.DICE_OFFSET_X = 0 / this.SCREEN_SIZE_COEF;
        this.DICE_OFFSET_Y = 100 / this.SCREEN_HEIGHT_COEF;
        this.SIZE_BUTTON_OFFSET = 110 / this.SCREEN_SIZE_COEF;
        this.DICE_SIZE_IN_CM = 1.6;
        this.isLandScape = (width > height) ? true : false;
    };
    LayoutContants.prototype.setModelMoving = function (flag) {
        this.modelMoving = flag;
    };
    LayoutContants.prototype.getScreenBounds = function () {
        return {
            width: this.SCREEN_WIDTH,
            height: this.SCREEN_WIDTH
        };
    };
    return LayoutContants;
}());
var Material = /** @class */ (function () {
    function Material(clayRegion, glossRegion, glossAlpha, clayButtonRegion, glossButtonRegion, glossButtonAlpha) {
        this.clayRegion = clayRegion;
        this.glossRegion = glossRegion;
        this.glossAlpha = +glossAlpha;
        this.clayButtonRegion = clayButtonRegion;
        this.glossButtonRegion = glossButtonRegion;
        this.glossButtonAlpha = +glossButtonAlpha;
    }
    return Material;
}());
var MaterialButton = /** @class */ (function () {
    function MaterialButton(scene, uiGenerator) {
        this.BACKGROUND_INDEX = 0;
        this.scene = scene;
        this.uiGenerator = uiGenerator;
        this.bodyPartsManager = scene.bodyPartsManager;
        this.stack = new Phaser.GameObjects.Container(scene);
    }
    MaterialButton.prototype.setImage = function (tabRegion, atlas, tabIndex) {
        var self = this;
        if (tabRegion.includes("tmp_Nada")) {
            this.stack.removeAll();
            return;
        }
        if (this.bodyPartsManager.selectedBodyPartIndex == this.BACKGROUND_INDEX && tabIndex > 0) {
            atlas = "new_tab_bg";
        }
        this.tabImage = new Phaser.GameObjects.Image(this.scene, 0, 0, atlas, tabRegion + ".png");
        this.tabImage.setScale(LayoutContants.getInstance().BUTTONS_SIZE / this.tabImage.displayHeight);
        this.stack.removeAll();
        this.stack.add(this.tabImage);
        this.tabIndex = tabIndex;
    };
    MaterialButton.prototype.setMaterial = function (tabIndex) {
        // debugger;
        if (this.bodyPartsManager.selectedBodyPart.isChild()) {
            // this.bodyPartsManager.setSelectedBodyPart();
            var parentBodyPart = this.bodyPartsManager.selectedBodyPart.getParnetBodyPart();
            this.uiGenerator.chooseTMenuButton(parentBodyPart.bodyPartIndex);
            this.uiGenerator.chooseTabButton(parentBodyPart.selectedTabIndex);
            this.uiGenerator.chooseColorButton(new ColorPositionPair(parentBodyPart.palleteNumber, parentBodyPart.colorButton.buttonIndex));
        }
        this.bodyPartsManager.setMaterial(tabIndex); // set materail
        this.uiGenerator.createUserAction(); // create action
        this.scene.getModelCharacteristics().setFinish(tabIndex);
        this.uiGenerator.setColorSelectors();
    };
    MaterialButton.prototype.dispose = function () {
        this.stack = null;
    };
    MaterialButton.prototype.getContainer = function () {
        return this.stack;
    };
    MaterialButton.prototype.clearStack = function () {
        this.stack.removeAll();
    };
    MaterialButton.prototype.onTapButton = function () {
        this.scene.playSound(CST.SOUND.COLOR_GENERAL_SOUND);
        this.setMaterial(this.tabIndex);
    };
    return MaterialButton;
}());
var Model = /** @class */ (function () {
    function Model(modelName, iconName, isOrbit, data8HD, dataHD, data8SD, dataSD, extraSmallSize, smallSize, mediumSize, largeSize, shopID, frameSetup, category, shopText, shopPrice, shopImage, shopLink, shopSkip, zoomAdjust, modeOff) {
        this.modelName = modelName;
        this.iconName = iconName;
        this.data8HD = +data8HD;
        this.dataHD = +dataHD;
        this.data8SD = +data8SD;
        this.dataSD = +dataSD;
        this.extraSmallSize = extraSmallSize;
        this.smallSize = smallSize;
        this.mediumSize = mediumSize;
        this.largeSize = largeSize;
        this.shopID = shopID;
        this.frameSetup = frameSetup;
        this.isOrbit = isOrbit;
        this.category = category;
        this.shopText = shopText;
        this.shopPrice = shopPrice;
        this.shopImage = shopImage;
        this.shopLink = shopLink;
        this.shopSkip = (shopSkip == "1") ? true : false;
        this.zoomAdjust = parseFloat(zoomAdjust);
        this.modeOff = (modeOff == "1") ? true : false;
    }
    Model.prototype.getExtraSmallSizeInCm = function () {
        return +this.extraSmallSize.split("-")[0].replace("cm", "");
    };
    Model.prototype.getSmallSizeInCm = function () {
        return +this.smallSize.split("-")[0].replace("cm", "");
    };
    Model.prototype.getMediumSizeInCm = function () {
        return +this.mediumSize.split("-")[0].replace("cm", "");
    };
    Model.prototype.getLargeSizeInCm = function () {
        return +this.largeSize.split("-")[0].replace("cm", "");
    };
    Model.prototype.getExtraSmallSizeInCmOfString = function () {
        return this.extraSmallSize.split("-")[0];
    };
    Model.prototype.getSmallSizeInCmOfString = function () {
        return this.smallSize.split("-")[0];
    };
    Model.prototype.getMediumSizeInCmOfString = function () {
        return this.mediumSize.split("-")[0];
    };
    Model.prototype.getLargeSizeInCmOfString = function () {
        return this.largeSize.split("-")[0];
    };
    return Model;
}());
var ModelCharacteristics = /** @class */ (function () {
    function ModelCharacteristics() {
    }
    ModelCharacteristics.prototype.getSize = function () {
        return this.size;
    };
    ModelCharacteristics.prototype.setSize = function (size) {
        this.size = size;
    };
    ModelCharacteristics.prototype.getFinish = function () {
        return this.finish;
    };
    ModelCharacteristics.prototype.setFinish = function (tabIndex) {
        if (tabIndex > 0) {
            this.finish = Finish.GLOSS;
            return;
        }
        this.finish = Finish.NORMAL;
    };
    ModelCharacteristics.prototype.getName = function () {
        return this.name;
    };
    ModelCharacteristics.prototype.setName = function (name) {
        this.name = name;
    };
    ModelCharacteristics.prototype.getShopId = function () {
        return this.shopId;
    };
    ModelCharacteristics.prototype.setShopId = function (shopId) {
        this.shopId = shopId;
    };
    return ModelCharacteristics;
}());
var ModelPart = /** @class */ (function () {
    function ModelPart(partName, sortOrder, tMenuOrder, partSizeX, partSizeY, partPosX, partPosY, scaleAdjust, xAdjust, yAdjust, partBaseImage, partGlossImage, glossAlpha, colorButtonBaseImage, colorButtonGlossImage, buttonGlossAlpha, tabRegions, materials, feature) {
        this.partName = partName;
        this.sortOrder = sortOrder;
        this.tMenuOrder = +tMenuOrder;
        this.partSizeX = +partSizeX;
        this.partSizeY = +partSizeY;
        this.partPosX = +partPosX;
        this.partPosY = +partPosY;
        this.scaleAdjust = +scaleAdjust;
        this.xAdjust = +xAdjust;
        this.yAdjust = +yAdjust;
        this.partBaseImage = partBaseImage;
        this.partGlossImage = partGlossImage;
        this.glossAlpha = glossAlpha;
        this.colorButtonBaseImage = colorButtonBaseImage;
        this.colorButtonGlossImage = colorButtonGlossImage;
        this.buttonGlossAlpha = buttonGlossAlpha;
        this.tabRegions = tabRegions;
        this.materials = materials;
        this.feature = feature;
    }
    ModelPart.prototype.getTabRegions = function () {
        var result = [];
        this.tabRegions.forEach(function (element) {
            if (element != "") {
                result.push(element);
            }
        });
        return result;
    };
    return ModelPart;
}());
// interface Map {
//     [key: string]: "";
//  } 
var SavedModel = /** @class */ (function () {
    function SavedModel(json) {
        this.bloomData = this.objToStrMap(json.bloomData);
        this.activeTabs = this.objToStrMap(json.activeTabs);
        this.partColors = this.objToStrMap(json.partColors);
        this.sliderValues = this.objToStrMap(json.sliderValues);
        this.buttonsIndex = this.objToStrMap(json.buttonsIndex);
        this.backgroundIndex = json.backgroundIndex;
        this.bgImgOrientation = json.bgImgOrientation;
        this.filterIntensity = json.filterIntensity;
        this.filterIntensity = json.filterIntensity;
        this.palletIndex = json.palletIndex;
        this.showMaster = json.showMaster;
    }
    SavedModel.prototype.setIndex = function (index) {
        this.index = index;
    };
    SavedModel.prototype.getBloomData = function () {
        return this.bloomData;
    };
    SavedModel.prototype.setBloomData = function (bloomData) {
        this.bloomData = bloomData;
    };
    SavedModel.prototype.getImage = function () {
        return this.image;
    };
    SavedModel.prototype.setImage = function (image) {
        this.image = image;
    };
    SavedModel.prototype.getBgImg = function () {
        return this.bgImg;
    };
    SavedModel.prototype.setBgImg = function (bgImg) {
        this.bgImg = bgImg;
    };
    SavedModel.prototype.getBgImgOrientation = function () {
        return this.bgImgOrientation;
    };
    SavedModel.prototype.setBgImgOrientation = function (bgImgOrientation) {
        this.bgImgOrientation = bgImgOrientation;
    };
    SavedModel.prototype.getSlotName = function () {
        return this.slotName;
    };
    SavedModel.prototype.setSlotName = function (slotName) {
        this.slotName = slotName;
    };
    SavedModel.prototype.getFilterName = function () {
        return this.filterName;
    };
    SavedModel.prototype.setFilterName = function (filterName) {
        this.filterName = filterName;
    };
    SavedModel.prototype.getFilterIntensity = function () {
        return this.filterIntensity;
    };
    SavedModel.prototype.setFilterIntensity = function (filterIntensity) {
        this.filterIntensity = filterIntensity;
    };
    SavedModel.prototype.getBackgroundIndex = function () {
        return this.backgroundIndex;
    };
    SavedModel.prototype.setBackgroundIndex = function (backgroundIndex) {
        this.backgroundIndex = backgroundIndex;
    };
    SavedModel.prototype.getPalletMode = function () {
        return this.palletMode;
    };
    SavedModel.prototype.setPalletMode = function (palletMode) {
        this.palletMode = palletMode;
    };
    SavedModel.prototype.getPalletIndex = function () {
        return this.palletIndex;
    };
    SavedModel.prototype.setPalletIndex = function (palletIndex) {
        this.palletIndex = palletIndex;
    };
    SavedModel.prototype.getShowMaster = function () {
        return this.showMaster;
    };
    SavedModel.prototype.setShowMaster = function (showMaster) {
        this.showMaster = showMaster;
    };
    SavedModel.prototype.getActiveTabs = function () {
        return this.activeTabs;
    };
    SavedModel.prototype.setActiveTabs = function (activeTabs) {
        this.activeTabs = activeTabs;
    };
    SavedModel.prototype.getPartColors = function () {
        return this.partColors;
    };
    SavedModel.prototype.setPartColors = function (partColors) {
        this.partColors = partColors;
    };
    SavedModel.prototype.getSliderValues = function () {
        return this.sliderValues;
    };
    SavedModel.prototype.setSliderValues = function (sliderValues) {
        this.sliderValues = sliderValues;
    };
    SavedModel.prototype.getButtonsIndex = function () {
        return this.buttonsIndex;
    };
    SavedModel.prototype.setButtonsIndex = function (buttonsIndex) {
        this.buttonsIndex = buttonsIndex;
    };
    SavedModel.prototype.objToStrMap = function (obj) {
        var strMap = new Map();
        for (var _i = 0, _a = Object.keys(obj); _i < _a.length; _i++) {
            var k = _a[_i];
            strMap.set(k, obj[k]);
        }
        return strMap;
    };
    SavedModel.prototype.savetoJson = function (modelName) {
        var jsonData = this.getJSONData();
        console.log("call savetoJson");
        var event = new CustomEvent('upload_data', { detail: { model_name: modelName, index: this.index, saved_data: jsonData } });
        document.dispatchEvent(event);
        localStorage.setItem("" + modelName + this.index, jsonData);
    };
    SavedModel.prototype.getJSONData = function () {
        var bloomData = this.strMapToObj(this.bloomData);
        var activeTabs = this.strMapToObj(this.activeTabs);
        var partColors = this.strMapToObj(this.partColors);
        var sliderValues = this.strMapToObj(this.sliderValues);
        var buttonsIndex = this.strMapToObj(this.buttonsIndex);
        var backgroundIndex = this.backgroundIndex;
        var bgImgOrientation = this.bgImgOrientation;
        var filterIntensity = this.filterIntensity;
        var palletIndex = this.palletIndex;
        var showMaster = this.showMaster;
        var jsonObj = {
            "bloomData": bloomData,
            "activeTabs": activeTabs,
            "partColors": partColors,
            "sliderValues": sliderValues,
            "buttonsIndex": buttonsIndex,
            "backgroundIndex": backgroundIndex,
            "bgImgOrientation": bgImgOrientation,
            "filterIntensity": filterIntensity,
            "palletIndex": palletIndex,
            "showMaster": showMaster
        };
        return JSON.stringify(jsonObj);
    };
    SavedModel.prototype.strMapToJson = function (strMap) {
        return JSON.stringify(this.strMapToObj(strMap));
    };
    SavedModel.prototype.strMapToObj = function (strMap) {
        var object = {};
        strMap.forEach(function (value, key) {
            object[key] = value;
        });
        return object;
    };
    return SavedModel;
}());
var UserAction = /** @class */ (function () {
    function UserAction(uiGenerator, bodyPartIndex, colorPositionPair, tabIndex) {
        this.uiGenerator = uiGenerator;
        this.bodyPartIndex = bodyPartIndex;
        this.colorPositionPair = colorPositionPair;
        this.tabIndex = tabIndex;
    }
    UserAction.prototype.performAction = function () {
        // this.uiGenerator.setColorSelection(this.uiGenerator.getSelectedColorButton().buttonIndex);
        this.uiGenerator.chooseTMenuButton(this.bodyPartIndex);
        this.uiGenerator.chooseTabButton(this.tabIndex);
        this.uiGenerator.chooseColorButton(this.colorPositionPair);
        // if(this.childColorLink){
        //     this.uiGenerator.chooseChildColorLink(this.childColorLink);
        // }
    };
    return UserAction;
}());
var LaunchScene = /** @class */ (function (_super) {
    __extends(LaunchScene, _super);
    function LaunchScene() {
        var _this = _super.call(this, { key: CST.SCENES.LAUNCH }) || this;
        _this.models_Arr = new Array();
        _this.LAYOUT_CONSTANT = LayoutContants.getInstance();
        _this.name = CST.SCENES.LAUNCH;
        console.log('start lanuchscene');
        return _this;
    }
    LaunchScene.prototype.preload = function () {
        this.createImagesFromAssets();
        this.loadSounds();
        Global.getInstance().setNavigation(this.scene);
        this.loadModels();
    };
    LaunchScene.prototype.init = function () {
        Global.getInstance().setCurrentScene(this.scene);
    };
    LaunchScene.prototype.create = function () {
        // this.helperContainer = new HelperContainer(this, this.helpersArr);
        // this.timer = this.scene.time.addEvent({
        //     delay: 1000,                // ms
        //     callback: ()=>{
        //         this.updatePingPong();
        //     },
        //     //args: [],
        //     loop: true
        // });
    };
    LaunchScene.prototype.update = function () {
        var _this = this;
        if (this.fullloaded) {
            this.sound.play("button-2");
            this.fullloaded = false;
            this.time.addEvent({
                delay: 2000,
                callback: function () {
                    _this.startMainScene();
                    // this.startModelingScene();
                },
                loop: false
            });
        }
    };
    LaunchScene.prototype.startMainScene = function () {
        var _this = this;
        if (this.models_Arr.length > 1) {
            Global.getInstance().setScreenModelArr(this.models_Arr);
            // this.scene.start(CST.SCENES.MAIN);
            Global.getInstance().routeNavigation();
        }
        else {
            this.time.addEvent({
                delay: 1000,
                callback: function () {
                    _this.startMainScene();
                },
                loop: false
            });
        }
    };
    // startModelingScene(){
    //     if(this.models_Arr.length > 1){
    //         Global.getInstance().setModel(this.models_Arr[0]);
    //         this.removeTextures();
    //         window.location.hash = CST.SCENES.MODELING + '/' + this.models_Arr[0].modelName;
    //         // CsvParser.parsePartsCsv(
    //         //     `${CST.HOST_ADDRESS}/assets/img/models/23-004_SM-HD/23-004_SM_Parts.csv`, 
    //         //     `${CST.HOST_ADDRESS}/assets/img/models/23-004_SM-HD/23-004_SM_Features.csv`, 
    //         //     (result)=>{
    //         //         Global.getInstance().setSeletectedModelParts(result);
    //         //         //this.scene.start(CST.SCENES.MODELING, this.models_Arr[0]);
    //         // });
    //     }else{
    //         this.time.addEvent({
    //             delay: 1000,
    //             callback: ()=>{
    //                 this.startModelingScene();
    //             },
    //             loop: false
    //         })
    //     }
    // }
    LaunchScene.prototype.createImagesFromAssets = function () {
        this.load.image("EditBox_Model_1", "assets/img/EditBox_Model_1.png");
        this.load.image("BoxBGFade", "assets/img/BoxBGFade.png");
        this.load.image("BoxFull", "assets/img/BoxFull.png");
        // this.load.multiatlas('individual_image','assets/img/individual_image.json', 'assets/img');
        this.load.image("shelf_adjusted", 'assets/img/shelf_adjusted.png');
        this.load.image("back_button", 'assets/img/back_button.png');
        this.load.image("StoreButton", 'assets/img/StoreButton.png');
        this.load.image("Button_AddPage", 'assets/img/Button_AddPage.png');
        //modeling scene asset
        this.load.image("EditBox_ModelMask", "assets/img/EditBox_ModelMask.png");
        this.load.image("close_rate", "assets/img/close_rate.png");
        this.load.image("rate", "assets/img/rate.png");
        this.load.image("Menu_SelectsBG", "assets/img/Menu_SelectsBG.png");
        this.load.image("Menu_SelectsBG", "assets/img/Menu_SelectsBG.png");
        this.load.image("SelectHoop", "assets/img/SelectHoop.png");
        this.load.image("FiguromoBox_BG_extension", "assets/img/FiguromoBox_BG_extension.png");
        this.load.image("BG_ValueNew_Gradient", "assets/img/BG_ValueNew_Gradient.png");
        this.load.image("BG_ValueNew", "assets/img/BG_ValueNew.png");
        this.load.image("Button_ValueNew", "assets/img/Button_ValueNew.png");
        this.load.image("ColorAdjust_Button_Dot", "assets/img/ColorAdjust_Button_Dot.png");
        this.load.image("Button_NewPreviewTab", "assets/img/Button_NewPreviewTab.png");
        this.load.image("FloatSlider_BGTrans", "assets/img/FloatSlider-BGTrans.png");
        this.load.image("FloatSlider_Strip2", "assets/img/FloatSlider-Strip2.png");
        this.load.image("ColorAdjust_Button", "assets/img/ColorAdjust_Button.png");
        this.load.image("TMenu_Selection", "assets/img/TMenu_Selection.png");
        this.load.image("TMenu_SaveON", "assets/img/TMenu_SaveON.png");
        this.load.image("Button_BackNEW", "assets/img/Button_BackNEW.png");
        this.load.image("Button_SaveNEW", "assets/img/Button_SaveNEW.png");
        this.load.image("NewSettings_BG1", "assets/img/NewSettings_BG1.png");
        this.load.image("dice", "assets/img/dice.png");
        this.load.image("back_button", "assets/img/back_button.png");
        this.load.image("solid_grey_8", "assets/img/solid_grey_8.png");
        this.load.image("TMenu_Back", "assets/img/TMenu_Back.png");
        this.load.image("Button_Undo", "assets/img/Button_Undo.png");
        this.load.image("Button_redo", "assets/img/Button_redo.png");
        this.load.image("Button_SlideGroove", "assets/img/Button_SlideGroove.png");
        this.load.image("Button_FrameSlideNew", "assets/img/Button_FrameSlideNew.png");
        this.load.image("AutoOn", "assets/img/AutoOn.png");
        //  Cloud Community Buttons
        // 
        //  Date, Like, Random, Sahre, Download
        // 
        //  * added by asset *
        this.load.image("Button_CloudFeatured", "assets/img/Button_CloudFeatured.png");
        this.load.image("Button_CloudRandom", "assets/img/Button_CloudRandom.png");
        this.load.image("Button_CloudDownload", "assets/img/Button_CloudDownload.png");
        this.load.image("Button_CloudLike", "assets/img/Button_CloudLike.png");
        this.load.image("Button_CloudLike_Red", "assets/img/Button_CloudLike_Red.png");
        this.load.image("Button_CloudDate", "assets/img/Button_CloudDate.png");
        this.load.image("Button_CloudShare", "assets/img/Button_CloudShare.png");
        this.load.image("Button_CloudFeatured_Blue", "assets/img/Button_CloudFeatured_Blue.png");
        this.load.image("Button_CloudRandom_Blue", "assets/img/Button_CloudRandom_Blue.png");
        this.load.image("Button_CloudDownload_Blue", "assets/img/Button_CloudDownload_Blue.png");
        this.load.image("Button_CloudLike_Blue", "assets/img/Button_CloudLike_Blue.png");
        this.load.image("Button_CloudDate_Blue", "assets/img/Button_CloudDate_Blue.png");
        this.load.image("Button_CloudShare_Blue", "assets/img/Button_CloudShare_Blue.png");
        // this.load.image("Button_Download", "assets/img/Button_Download.png");
        this.load.image("Model_Title", "assets/img/SM_Title.png");
        // margin saveslots
        this.load.image("UISaveslotBottom", "assets/img/UISaveslotBottom.png");
        this.load.image("UISaveslotMiddle", "assets/img/UISaveslotMiddle.png");
        this.load.image("UISaveslotTop", "assets/img/UISaveslotTop.png");
        this.load.image("UISaveslotCommunity", "assets/img/UISaveslotCommunity.png");
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        this.load.image("Button_Size1", "assets/img/Button-Size1.png");
        this.load.image("Button_Size1SEL", "assets/img/Button-Size1SEL.png");
        this.load.image("Button_ShopClose", "assets/img/Button_ShopClose.png");
        this.load.image("Button_ShopBuy", "assets/img/Button_ShopBuy.png");
        this.load.image("AutoOff", "assets/img/AutoOff.png");
        this.load.image("AutoOn", "assets/img/AutoOn.png");
        this.load.image('Fade0', 'assets/img/Fade0.png');
        this.load.image('Fade1', 'assets/img/Fade1.png');
        this.load.image('Fade2', 'assets/img/Fade2.png');
        this.load.image('Fade3', 'assets/img/Fade3.png');
        this.load.image('Fade4', 'assets/img/Fade4.png');
        this.load.image('Fade5', 'assets/img/Fade5.png');
        this.load.image('Fade6', 'assets/img/Fade6.png');
        this.load.image('Fade7', 'assets/img/Fade7.png');
        this.load.image('Fade8', 'assets/img/Fade8.png');
        this.load.image('solid', 'assets/img/solid.jpg');
        // this.load.image('solid-2','assets/img/solid-2.jpg');
        this.load.image('solid-new', 'assets/img/solid-new.png');
        this.load.multiatlas('new_tab_bg', 'assets/img/new_tab_bg.json', 'assets/img');
        this.load.multiatlas('proof', 'assets/img/proof.json', 'assets/img');
        this.load.image('ModeRainbow', 'assets/img/ModeRainbow.png');
        this.load.image('ModePalette', 'assets/img/ModePalette.png');
        this.load.image('ModeGlow', 'assets/img/ModeGlow.png');
        this.load.image('ModeBG', 'assets/img/ModeBG.png');
        this.load.image('PaletteArrowL', 'assets/img/PaletteArrowL.png');
        this.load.image('PaletteArrowR', 'assets/img/PaletteArrowR.png');
        this.load.image('BG_PaletteNew', 'assets/img/BG_PaletteNew.png');
        //setttings
        this.load.image('Button_GoToSettings', 'assets/img/Button_GoToSettings.png'); //setting off
        this.load.image('Button_GoToSettingsON', 'assets/img/Button_GoToSettingsON.png'); //setting on
        this.load.image('Button_RateMe02', 'assets/img/Button_RateMe02.png'); //rate app icon
        this.load.image('GDRP', 'assets/img/GDRP.png'); //privacypolicy
        this.load.image('ButtonSwitch_OFF', 'assets/img/ButtonSwitch_OFF.png'); //switch off
        this.load.image('ButtonSwitch_ON', 'assets/img/ButtonSwitch_ON.png'); //switch on
        this.load.image('NewSettings_BG2', 'assets/img/NewSettings_BG2.png'); //setting background
        this.load.json("categoryData", "assets/img/category/Categories.json");
        this.load.image("IAP-Button-Category", "assets/img/category/IAP-Button-Category.png");
        this.load.image("empty", "assets/img/empty.png");
        //load image helper images.
        var arr = [];
        for (var i = 1; i <= 3; i++) {
            this.load.image("helper_" + i, "assets/img/helpers/helper_" + i + ".png"); //setting background
            arr.push("helper_" + i);
        }
        Global.getInstance().setHelpersImages(arr);
    };
    LaunchScene.prototype.loadModels = function () {
        var self = this;
        self.models_Arr = [];
        Papa.parse(CST.HOST_ADDRESS + "/assets/cvs/Real3d_V2.csv", {
            download: true,
            complete: function (results) {
                var i = 0;
                results.data.forEach(function (entry) {
                    if (i < results.data.length - 1 && i > 0) {
                        var model = new Model(entry[CST.CSVPARSER.MODEL_NAME_COL], entry[CST.CSVPARSER.ICON_NAME_COL], entry[CST.CSVPARSER.IS_ORBIT_COL], entry[CST.CSVPARSER.DATA_8_HD_COL], entry[CST.CSVPARSER.DATA_HD_COL], entry[CST.CSVPARSER.DATA_8_SD_COL], entry[CST.CSVPARSER.DATA_SD_COL], entry[CST.CSVPARSER.EXTRA_SMALL_SIZE_COL], entry[CST.CSVPARSER.SMALL_SIZE_COL], entry[CST.CSVPARSER.MEDIUM_SIZE_COL], entry[CST.CSVPARSER.LARGE_SIZE_COL], entry[CST.CSVPARSER.SHOP_ID_COL], entry[CST.CSVPARSER.FRAME_SETUP_COL], entry[CST.CSVPARSER.CATEGORY_COL], entry[CST.CSVPARSER.SHOP_TEXT_COL], entry[CST.CSVPARSER.SHOP_PRICE_COL], entry[CST.CSVPARSER.SHOP_IMAGE_COL], entry[CST.CSVPARSER.SHOP_lINK_COL], entry[CST.CSVPARSER.SHOP_SKIP_COL], entry[CST.CSVPARSER.ZOOM_ADJUST_COL], entry[CST.CSVPARSER.MADE_OFF_COL]);
                        self.models_Arr.push(model);
                    }
                    i++;
                });
                Global.getInstance().setScreenModelArr(self.models_Arr);
                self.loadColors(self);
            }
        });
    };
    LaunchScene.prototype.startSaveSlote = function () {
        var _this = this;
        if (this.models_Arr.length > 1) {
            window.location.hash = CST.SCENES.SAVE_SLOTE + '/' + this.models_Arr[0].modelName;
        }
        else {
            this.time.addEvent({
                delay: 1000,
                callback: function () {
                    _this.startSaveSlote();
                },
                loop: false
            });
        }
    };
    LaunchScene.prototype.loadColors = function (scene) {
        var _this = this;
        CsvParser.parseColorCsv(CST.HOST_ADDRESS + "/assets/cvs/Table_Pallets-ALL.csv", function (result) {
            scene.game.global.colorsRanges = result;
            if (Global.getInstance().checkLogoView()) {
                var loadImage;
                var loadProcessImage;
                var pgoregress = 0;
                var virProgress = 0;
                loadProcessImage = _this.add.image(_this.LAYOUT_CONSTANT.SCREEN_WIDTH / 2, _this.LAYOUT_CONSTANT.SCREEN_HEIGHT / 2, "logo", "AnimFill_2.png");
                loadProcessImage.setOrigin(0.5, 0.5);
                loadImage = _this.add.image(_this.LAYOUT_CONSTANT.SCREEN_WIDTH / 2, _this.LAYOUT_CONSTANT.SCREEN_HEIGHT / 2, "logoFrame1");
                _this.load.on('progress', function (value) {
                    if (loadProcessImage) {
                        if (value == 1) {
                            pgoregress = 5;
                        }
                        else if (value > 0.8) {
                            pgoregress = 4;
                        }
                        else if (value > 0.6) {
                            pgoregress = 3;
                        }
                        else if (value > 0.4) {
                            pgoregress = 2;
                        }
                        else if (value > 0.2) {
                            pgoregress = 1;
                        }
                    }
                }, _this);
                _this.timer = setInterval(function () {
                    if (pgoregress == 5) {
                        if (virProgress == 4) {
                            virProgress++;
                            loadProcessImage.setTexture("logo", "AnimFill_7.png");
                            loadImage.setTexture("logoFrame2");
                            _this.fullloaded = true;
                            clearInterval(_this.timer);
                        }
                    }
                    if (pgoregress >= 4) {
                        if (virProgress == 3) {
                            virProgress++;
                            loadProcessImage.setTexture("logo", "AnimFill_6.png");
                        }
                    }
                    if (pgoregress >= 3) {
                        if (virProgress == 2) {
                            virProgress++;
                            loadProcessImage.setTexture("logo", "AnimFill_5.png");
                        }
                    }
                    if (pgoregress >= 2) {
                        if (virProgress == 1) {
                            virProgress++;
                            loadProcessImage.setTexture("logo", "AnimFill_4.png");
                        }
                    }
                    if (pgoregress >= 1) {
                        if (virProgress == 0) {
                            virProgress++;
                            loadProcessImage.setTexture("logo", "AnimFill_3.png");
                        }
                    }
                }, 500);
                // this.load.on('fileprogress', function (file) {
                // });
            }
            else {
                _this.load.on('complete', function () {
                    _this.startMainScene();
                });
            }
        });
    };
    LaunchScene.prototype.loadSounds = function () {
        this.load.audio('backgroundSong', ['assets/sounds/00-000_UI_Song.mp3']);
        this.load.audio('backsound', ['assets/sounds/button-6.mp3']);
        this.load.audio('colorchangesound', ['assets/sounds/button-2.mp3']);
        this.load.audio('generalsound', ['assets/sounds/button-1.mp3']);
        this.load.audio('swoosh', ['assets/sounds/swoosh.mp3']);
        this.load.audio('slideOpen', ['assets/sounds/slideOpen.mp3']);
    };
    return LaunchScene;
}(Phaser.Scene));
/// <reference path='../../phaser.d.ts'/>
var Random = Phaser.Math.Between;
var COLOR_PRIMARY = 0xCCCCCC;
var COLOR_LIGHT = 0xCCCCCC;
var COLOR_DARK = 0xe2e2e2;
var MainScene = /** @class */ (function (_super) {
    __extends(MainScene, _super);
    function MainScene() {
        var _this = _super.call(this, { key: CST.SCENES.MAIN }) || this;
        _this.models_Arr = new Array();
        _this.LAYOUT_CONSTANT = LayoutContants.getInstance();
        _this.category_Arr = new Array();
        _this.name = CST.SCENES.MAIN;
        _this.loaded = false;
        return _this;
    }
    MainScene.prototype.preload = function () {
        this.loadModels();
    };
    MainScene.prototype.removeTextures = function () {
        var _this = this;
        return;
        this.models_Arr.forEach(function (model) {
            _this.textures.remove(model.iconName);
        });
        this.textures.remove("IAPicon_BGBlue");
        this.textures.remove("IAPicon_BGGrey");
        this.textures.remove("IAP-Button-Category");
        this.category_Arr.forEach(function (element) {
            _this.textures.remove(element);
        });
        this.category_Arr = [];
    };
    MainScene.prototype.loadModels = function () {
        var _this = this;
        this.models_Arr.forEach(function (model) {
            _this.load.image(model.iconName, CST.HOST_ADDRESS + "/assets/img/icons/" + model.iconName + ".png");
        });
        this.load.image("IAPicon_BGBlue", CST.HOST_ADDRESS + "/assets/img/icons/IAPicon_BGBlue.png");
        this.load.image("IAPicon_BGGrey", CST.HOST_ADDRESS + "/assets/img/icons/IAPicon_BGGrey.png");
        this.load.image("IAP-Button-Category", CST.HOST_ADDRESS + "/assets/img/category/IAP-Button-Category.png");
        this.models_Arr.forEach(function (element) {
            if (_this.category_Arr.indexOf(element.category) == -1) {
                _this.category_Arr.push(element.category);
                _this.load.image(element.category, CST.HOST_ADDRESS + "/assets/img/category/" + element.category + ".png");
            }
        });
    };
    MainScene.prototype.init = function () {
        Global.getInstance().setCurrentScene(this.scene);
        this.models_Arr = Global.getInstance().getModelArr();
        // Global.getInstance().setNativgation(this.scene);
    };
    MainScene.prototype.create = function () {
        this.load.start();
        this.initElements();
        this.addEventListner();
        var event = new CustomEvent('onScreenChange', { detail: CST.SCENES.MAIN });
        document.dispatchEvent(event);
    };
    MainScene.prototype.update = function () {
        this.playBackgroundSong();
        var pointer = this.input.activePointer;
        if (pointer.isDown) {
            if (!this.lastPosY)
                this.lastPosY = pointer.y;
            if (!this.lastPosX)
                this.lastPosX = pointer.x;
            //const time = 1/this.game.loop.actualFps;
            var vy = (pointer.y - this.lastPosY);
            var vx = (pointer.x - this.lastPosX);
            this.lastPosY = pointer.y;
            this.lastPosX = pointer.x;
            this.events.emit("velocity", vx, vy);
        }
        else {
            this.lastPosY = undefined;
            this.lastPosX = undefined;
        }
    };
    MainScene.prototype.initElements = function () {
        this.helperContainer = new HelperContainer(this, 0);
        this.initSetting();
        this.initModelTable();
        this.initialSelectCategory();
        this.defaultHeaderBar = this.add.graphics();
        this.defaultHeaderBar.fillStyle(0xdddddd);
        this.defaultHeaderBar.fillRect(0, 0, this.LAYOUT_CONSTANT.SCREEN_WIDTH, this.settingPanel.visibleBarHeight);
        this.defaultHeaderBar.setDepth(1000);
    };
    MainScene.prototype.initialSelectCategory = function () {
        this.categorlSelector = this.add.graphics();
        this.selectedCategoryItem = this.categoryItems[0];
        this.markSelectedCategory();
    };
    MainScene.prototype.initSetting = function () {
        var _this = this;
        this.settingPanel = new SettingPanel(this, 1000 * this.LAYOUT_CONSTANT.HEIGHT_SCALE, "NewSettings_BG2", function (isOn) {
            _this.toggleSettingPanel(isOn);
        });
        this.settingPanel.setDepth(100);
    };
    MainScene.prototype.initModelTable = function () {
        var _this = this;
        var scrollMode = 1; // 0:vertical, 1:horizontal
        var scrollMode = 0;
        var cellWidth = 300;
        var y = this.settingPanel.getHeight() + this.settingPanel.availableDistance + this.settingPanel.visibleBarHeight;
        var columns = Math.floor(this.LAYOUT_CONSTANT.SCREEN_WIDTH / cellWidth);
        this.modelItemTable = new ModelGridTable(this, {
            x: 0,
            y: y,
            width: this.LAYOUT_CONSTANT.SCREEN_WIDTH,
            height: this.LAYOUT_CONSTANT.SCREEN_HEIGHT - y,
            background: this.add.image(0, 0, ""),
            scrollMode: scrollMode,
            table: {
                cellWidth: cellWidth,
                cellHeight: 400,
                columns: columns,
            },
            toggleSettingPannel: function (isOn) {
                _this.toggleSettingPanel(isOn);
            },
            settingPannel: this.settingPanel
        });
        // listner when clicking model
        this.modelItemTable.table.on("cell.click", function (cellIndex) {
            _this.modelItems[cellIndex].updateBackground(true);
            Global.getInstance().setModel(_this.modelItems[cellIndex].model);
            _this.startSloteScene(_this.modelItems[cellIndex].model.modelName);
        });
        var self = this;
        var scrollMode = 1; // 0:vertical, 1:horizontal
        this.categoryGridTable = new CategoryGridTable(this, {
            x: 0,
            y: this.settingPanel.getHeight() - 140 * LayoutContants.getInstance().HEIGHT_SCALE,
            width: LayoutContants.getInstance().SCREEN_WIDTH,
            height: 100 * LayoutContants.getInstance().HEIGHT_SCALE,
            background: this.add.image(0, 0, "TMenu_Back"),
            scrollMode: scrollMode
        });
        var categoryItems = this.getCategoryItems();
        this.categoryGridTable.setItems(categoryItems);
        this.settingPanel.stack.add(this.categoryGridTable.stack);
        this.categoryGridTable.table.on("cell.click", function (cellIndex) {
            var tempItem = _this.categoryItems[cellIndex];
            if (_this.selectedCategoryItem.index != tempItem.index) {
                _this.selectedCategoryItem = tempItem;
                _this.markSelectedCategory();
            }
        });
    };
    MainScene.prototype.markSelectedCategory = function () {
        var _this = this;
        this.toggleSettingPanel(false, true);
        this.settingPanel.settingButton.setActive(false);
        this.categorlSelector.clear();
        this.categorlSelector.fillStyle(0x57aafb);
        this.selectedCategoryItem.addSelectector(this.categorlSelector);
        var height = this.selectedCategoryItem.getOriginDisplayHeight();
        var width = this.selectedCategoryItem.getOriginDisplaywidth() + 30;
        var frames = 10;
        var totalSec = 200;
        if (this.selectorTimer)
            this.selectorTimer.destroy();
        var index = 0;
        this.selectorTimer = this.time.addEvent({
            delay: totalSec / frames,
            callback: function () {
                var newWidth = index * (width - height) / frames + height;
                _this.categorlSelector.fillRoundedRect(-newWidth / 2, -height / 2, newWidth, height, height / 2);
                index++;
            },
            repeat: frames
        });
        this.modelItems = this.getItems(1);
        this.modelItemTable.setItems(this.modelItems);
    };
    MainScene.prototype.getItems = function (scrollMode) {
        var _this = this;
        var result = [];
        this.models_Arr.forEach(function (element) {
            if (element.category == _this.selectedCategoryItem.category) {
                result.push(new ModelItem(_this, scrollMode, element));
            }
        });
        return result;
    };
    MainScene.prototype.getCategoryItems = function () {
        var _this = this;
        var result = [];
        this.category_Arr.forEach(function (element) {
            result.push(new CategoryItem(_this, element));
        });
        this.categoryItems = result;
        return result;
    };
    MainScene.prototype.startSloteScene = function (modelName) {
        //this.removeTextures();
        //window.location.hash =  CST.SCENES.SAVE_SLOTE + '/' + modelName; 
        window.location.href = CST.HOST_ADDRESS + '#' + CST.SCENES.SAVE_SLOTE + '/' + modelName;
        this.scene.start(CST.SCENES.SAVE_SLOTE, { initSound: true });
    };
    MainScene.prototype.startModelingScene = function (index) {
        //this.removeTextures();
        //window.location.href = CST.SCENES.MODELING + '/' + this.models_Arr[index].modelName;
        window.location.href = CST.HOST_ADDRESS + '#' + CST.SCENES.SAVE_SLOTE + '/' + this.models_Arr[index].modelName;
    };
    MainScene.prototype.playBackgroundSong = function () {
        if ((localStorage.getItem("music") || 'on') === 'on') {
            if (!this.isMusicPlay) {
                this.sound.stopAll();
                this.sound.play("backgroundSong", { volume: 0.2, loop: true });
                this.isMusicPlay = true;
            }
        }
        else {
            this.isMusicPlay = false;
            this.sound.stopAll();
        }
    };
    MainScene.prototype.addEventListner = function () {
        document.addEventListener('web.exit.shoping-cart', function (e) {
        });
    };
    MainScene.prototype.toggleSettingPanel = function (isOn, onlySettingPannel) {
        var _this = this;
        if (onlySettingPannel === void 0) { onlySettingPannel = false; }
        var availableDistance = this.settingPanel.availableDistance;
        //const settingPanelHeight = this.settingPanel.getHeight();
        this.settingPanel.setOn(isOn);
        //const bottomY = availableDistance + this.settingPanel.visibleBarHeight;
        var topY = this.settingPanel.visibleBarHeight;
        var bottomY = this.settingPanel.availableDistance + this.settingPanel.visibleBarHeight;
        if (this.timer)
            this.timer.destroy();
        this.timer = this.time.addEvent({
            delay: 10,
            callback: function () {
                if (isOn) {
                    _this.settingPanel.stack.y += 10;
                    if (!onlySettingPannel)
                        _this.modelItemTable.increaseDY(10);
                    _this.modelItemTable.increaseTableDY(10);
                    if (_this.settingPanel.stack.y > topY) {
                        _this.settingPanel.stack.y = topY;
                        _this.timer.destroy();
                    }
                }
                else {
                    _this.settingPanel.stack.y -= 10;
                    if (!onlySettingPannel)
                        _this.modelItemTable.increaseDY(-10);
                    _this.modelItemTable.increaseTableDY(-10);
                    if (_this.settingPanel.stack.y < bottomY) {
                        _this.settingPanel.stack.y = bottomY;
                        _this.timer.destroy();
                    }
                }
            },
            loop: true
        });
    };
    return MainScene;
}(Phaser.Scene));
/// <reference path='../../phaser.d.ts'/>
var ModelingScene = /** @class */ (function (_super) {
    __extends(ModelingScene, _super);
    function ModelingScene() {
        var _this = _super.call(this, { key: CST.SCENES.MODELING }) || this;
        _this.SOLID_GREY_SPRITE = "solidGreySprite";
        _this.FADE_SPRITE = "fadeSprite";
        _this.BACKGROUND_SPRITE = "backgroundSprite";
        _this.NUMBER_OF_POOFS = 24;
        _this.COLOR_SELECTOR_ALPHA_CHANGE = 0.02;
        _this.moveDistance = 0;
        _this.startPosX = 0;
        _this.startPosY = 0;
        _this.LAYOUT_CONSTANT = LayoutContants.getInstance();
        _this.resolution = "SD";
        _this.config = {
            showingFPS: false
        };
        _this.name = CST.SCENES.MODELING;
        _this.animationDirection = SwitchStatus.RIGHT;
        _this.animationRate = 30;
        _this.loadingStarted = true;
        _this.screenMode = CST.LAYOUT.EDIT_MODE;
        return _this;
    }
    ModelingScene.prototype.init = function () {
        this.resolution = "SD";
        this.isFileLoaded = false;
        // Global.getInstance().setCurrentScene(this.scene);
        // Global.getInstance().setNavigation(this.scene);
        this.initButtonSounds();
        var selectedModel = Global.getInstance().getModel();
        console.log("selectedModel -------------> ", selectedModel);
        this.model = selectedModel;
        this.fileNames = new FileNames().generateFileNames(selectedModel, true);
        this.atlasFront = this.model.modelName + "-front";
        this.atlasFrames = "" + this.model.modelName;
        this.frameSetup();
        this.modelCharacteristics = new ModelCharacteristics();
        this.modelCharacteristics.setName(this.model.modelName);
        this.modelCharacteristics.setShopId(this.model.shopID);
        if (this.checkPreviewMode()) {
            console.log('previewmode');
            this.screenMode = CST.LAYOUT.PREVIEW_MODE;
        }
        else {
            this.screenMode = CST.LAYOUT.EDIT_MODE;
        }
        this.animationDirection = CST.ANIMATION.DIRECTION.RIGHT;
        this.modelParts = Global.getInstance().getSeletectedModelParts();
        //this.cache.destroy();
    };
    ModelingScene.prototype.preload = function () {
        this.load.multiatlas(this.atlasFront + "-HD", CST.HOST_ADDRESS + "/assets/img/models/" + this.model.modelName + "/HD/" + this.fileNames.atlasFront, CST.HOST_ADDRESS + "/assets/img/models/" + this.model.modelName + "/HD");
        // this.load.multiatlas(`${this.atlasFront}-SD`,`${CST.HOST_ADDRESS}/assets/img/models/${this.model.modelName}/SD/${this.fileNames.atlasSDFront}`, `${CST.HOST_ADDRESS}/assets/img/models/${this.model.modelName}/SD`);        
    };
    ModelingScene.prototype.resetScreen = function () {
        this.savedModel = Global.getInstance().getSavedModel();
        this.uiGenerator.resetElementPositionByScreenMode(this.screenMode);
        if (this.atlasFrames != null) {
            this.startOnFramesLoaded(this.atlasFrames);
            this.bodyPartsManager.setSavedProperties(this.savedModel);
            this.uiGenerator.setInitialUserAction();
        }
        Global.getInstance().setResetModelFlag(false);
    };
    ModelingScene.prototype.create = function () {
        this.helperContainer = new HelperContainer(this, 1);
        this.buildStage();
        this.startTimer();
        this.resetScreen();
        this.bindLoadEvent();
        this.bodyPartsManager.resetSprite();
        this.loadModels("SD");
        // this.uiGenerator.materialButtons[this.bodyPartsManager.selectedBodyPart.selectedTabIndex].onTapButton();
        // debugger
        this.showFPS();
    };
    ModelingScene.prototype.checkPreviewMode = function () {
        var url = window.location.href;
        console.log('modelingscenepreview', url);
        if (url.includes('sharemodelview')) {
            return true;
        }
        else {
            return false;
        }
    };
    ModelingScene.prototype.loadModels = function (resol) {
        var _this = this;
        if (resol == "SD") {
            this.uiGenerator.bottomSwitchBar.setVisible(false);
            this.load.on("progress", function (value) {
                _this.uiGenerator.progressBar.increasePercent();
            }, this);
        }
        this.load.on("complete", function () {
            _this.uiGenerator.progressBar.setVisible(false);
            if (_this.screenMode == CST.LAYOUT.EDIT_MODE) {
                _this.uiGenerator.bottomSwitchBar.setVisible(true);
            }
            if (resol == "SD") {
                _this.events.emit("loadedSD");
            }
            if (resol == "HD") {
                _this.events.emit("loadedHD");
            }
        }, this)
            .on("customfileloaded", function (file) {
            console.log(file);
        });
        if (resol == "HD") {
            this.load.customLoad = true;
            this.load.multiatlas(this.model.modelName + "-" + resol, CST.HOST_ADDRESS + "/assets/img/models/" + this.model.modelName + "/" + resol + "/" + this.fileNames.atlasFrames, CST.HOST_ADDRESS + "/assets/img/models/" + this.model.modelName + "/" + resol);
        }
        else {
            this.load.multiatlas(this.model.modelName + "-" + resol, CST.HOST_ADDRESS + "/assets/img/models/" + this.model.modelName + "/" + resol + "/" + this.fileNames.atlasSDFrames, CST.HOST_ADDRESS + "/assets/img/models/" + this.model.modelName + "/" + resol);
        }
        this.load.start();
    };
    ModelingScene.prototype.bindLoadEvent = function () {
        var _this = this;
        this.events.once("loadedSD", function () {
            _this.resolution = "SD";
            _this.isFileLoaded = true;
            _this.bodyPartsManager.resetSprite();
            _this.loadModels("HD");
        });
        this.events.once("loadedHD", function () {
            // this.resolution = "HD";
            // if(this.timer) clearInterval(this.timer)
            // //this.textures.remove(`${this.model.modelName}-SD`);
            // this.bodyPartsManager.resetSprite();
            // this.startTimer();
        });
        document.addEventListener('customfileloaded', function (e) {
            _this.HDFile = e.detail;
        });
        /**
         * Keyboard Bind functions...
         */
        this.input.keyboard.on("keyup_CTRL", function () {
            _this.panStart = false;
            _this.startPosX = 0;
            _this.startPosY = 0;
        });
        this.input.keyboard.on("keydown_CTRL", function () {
            if (_this.screenMode == CST.LAYOUT.EDIT_MODE || _this.screenMode == CST.LAYOUT.PREVIEW_MODE) {
                _this.panStart = true;
                _this.startPosX = 0;
                _this.startPosY = 0;
            }
        });
        this.input.keyboard.on("keyup_SHIFT", function () {
            _this.pinchStart = false;
            _this.lastPinchVector = undefined;
            //this.resetCenterPointer();
        });
        this.input.keyboard.on("keydown_SHIFT", function () {
            if (_this.screenMode == CST.LAYOUT.EDIT_MODE || _this.screenMode == CST.LAYOUT.PREVIEW_MODE) {
                _this.lastPinchVector = undefined;
                if (_this.input.activePointer.isDown) {
                    _this.bodyPartsManager.tCenterX = _this.input.activePointer.x;
                    _this.bodyPartsManager.tCenterY = _this.input.activePointer.y;
                }
                if (!_this.pinchStart) {
                    _this.selectedCenterY = _this.input.activePointer.x;
                    _this.selectedCenterX = _this.input.activePointer.y;
                    _this.pinchStart = true;
                }
            }
        });
    };
    ModelingScene.prototype.startTimer = function () {
        var _this = this;
        if (this.timer)
            clearInterval(this.timer);
        this.timer = setInterval(function () {
            _this.updateFrame();
        }, 2000 / 29);
    };
    ModelingScene.prototype.updateFrame = function () {
        // debugger
        var _this = this;
        if (this.resolution == "SD") {
            if (this.HDFile && this.bodyPartsManager.frame == this.firstFrame) {
                this.helperContainer.setHelperIndex(2);
                if (this.HDFile.multiFile)
                    this.HDFile.multiFile.addToCache();
                this.resolution = "HD";
                this.bodyPartsManager.resetSprite();
            }
        }
        if (this.bodyPartsManager && this.isFileLoaded) {
            if (this.model.isOrbit == "0") {
                // this.uiGenerator.bottomSwitchBar.updatePingPong();
                // const frame = this.uiGenerator.bottomSwitchBar.getPingPongFrame();
                // if(this.uiGenerator.bottomSwitchBar.isPingPong){
                //     this.bodyPartsManager.setFrame(frame);
                // }else{
                //     // this.uiGenerator.bottomSwitchBar.updateThumbButtonPositionByFrame(this.bodyPartsManager.frame);
                // }
            }
            else {
                if (this.animationDirection == SwitchStatus.RIGHT) {
                    this.bodyPartsManager.rotateRight();
                }
                if (this.animationDirection == SwitchStatus.LEFT) {
                    this.bodyPartsManager.rotateLeft();
                }
            }
            this.bodyPartsManager.updateModel();
            // this.uiGenerator.bottomSwitchBar.setFrame(this.bodyPartsManager.frame);
        }
        if (this.bodyPartsManager) {
            if (this.blinkingEnabled) {
                if (this.bodyPartsManager.selectedBodyPart.modelPart.feature.isChildren) {
                    this.uiGenerator.selectedChildBodyPartButton.blink(function () {
                        _this.stopBlinking();
                    });
                }
                else {
                    this.uiGenerator.selectedTMenuButton.blink(function () {
                        _this.stopBlinking();
                    });
                }
                // this.setBlinkingTimerStarted(false);
            }
        }
    };
    /**
     * Model Scene Update
     * This method is called once per game step while the scene is running
     */
    ModelingScene.prototype.update = function () {
        // console.log(this.frame,"===================================")
        // console.log('colorMode', this.uiGenerator.colorMode);
        // console.log('palleteNumber', this.uiGenerator.palleteNumber);
        // console.log('bodycolorMode', this.bodyPartsManager.selectedBodyPart.colorModeNumber);
        // console.log('bodypalleteNumber', this.bodyPartsManager.selectedBodyPart.palleteNumber);
        // console.log("===================================",this.frame)
        if (this.config.showingFPS) {
            this.fpsText.setText("FPS:" + this.game.loop.actualFps);
        }
        if (this.isFileLoaded && this.bodyPartsManager) {
            if (this.screenMode == CST.LAYOUT.EDIT_MODE || this.screenMode == CST.LAYOUT.PREVIEW_MODE) {
                // && Math.abs(deltaPointer2Dis)>1
                if (this.pinchP1X == undefined && this.input.pointer1.isDown && this.input.pointer2.isDown) {
                    if (this.time.now - this.lastRenderTime > 20) {
                        this.touchCenterX = (this.input.pointer1.x + this.input.pointer2.x) / 2;
                        this.touchCenterY = (this.input.pointer1.y + this.input.pointer2.y) / 2;
                        this.pinchP1X = this.input.pointer1.x;
                        this.pinchP2X = this.input.pointer2.x;
                        this.pinchP1Y = this.input.pointer1.y;
                        this.pinchP2Y = this.input.pointer2.y;
                    }
                    this.secondClick = false;
                }
                else if (this.pinchP1X && this.input.pointer1.isDown && this.input.pointer2.isDown) {
                    // moving center pos while performing pinch
                    var dx = (this.input.pointer1.x + this.input.pointer2.x) / 2 - this.touchCenterX;
                    var dy = (this.input.pointer1.y + this.input.pointer2.y) / 2 - this.touchCenterY;
                    // Moving
                    this.bodyPartsManager.moveByX(dx);
                    this.bodyPartsManager.moveByY(dy);
                    this.LAYOUT_CONSTANT.setModelMoving(true);
                    var currentPinchDis = Phaser.Math.Distance.Between(this.input.pointer1.x, this.input.pointer1.y, this.input.pointer2.x, this.input.pointer2.y);
                    var lastPinchDis = Phaser.Math.Distance.Between(this.pinchP1X, this.pinchP1Y, this.pinchP2X, this.pinchP2Y);
                    // Scaling
                    //let distance = Math.abs(currentPinchDis - lastPinchDis);
                    var scale = currentPinchDis / lastPinchDis;
                    this.bodyPartsManager.setScaleMulti(scale, this.touchCenterX, this.touchCenterY);
                    this.uiGenerator.setScaleDiceImageByDScale();
                    this.LAYOUT_CONSTANT.setModelMoving(true);
                    this.pinchP1X = this.input.pointer1.x;
                    this.pinchP2X = this.input.pointer2.x;
                    this.pinchP1Y = this.input.pointer1.y;
                    this.pinchP2Y = this.input.pointer2.y;
                    this.touchCenterX = (this.input.pointer1.x + this.input.pointer2.x) / 2;
                    this.touchCenterY = (this.input.pointer1.y + this.input.pointer2.y) / 2;
                }
                else {
                    //console.log('undefined')
                    this.pinchP1X = undefined;
                    this.pinchP2X = undefined;
                    this.pinchP1Y = undefined;
                    this.pinchP2Y = undefined;
                    this.touchCenterX = undefined;
                    this.touchCenterY = undefined;
                    this.secondClick = true;
                    this.lastRenderTime = this.time.now;
                }
            }
        }
        var pointer = this.input.activePointer;
        if (pointer.isDown) {
            if (!this.lastPosY)
                this.lastPosY = pointer.y;
            if (!this.lastPosX)
                this.lastPosX = pointer.x;
            var time = 1 / this.game.loop.actualFps;
            var vy = (pointer.y - this.lastPosY);
            var vx = (pointer.x - this.lastPosX);
            this.lastPosY = pointer.y;
            this.lastPosX = pointer.x;
            this.events.emit("velocity", vx, vy);
        }
        else {
            this.lastPosY = undefined;
            this.lastPosX = undefined;
        }
    };
    ModelingScene.prototype.frameSetup = function () {
        var frameSetup = this.model.frameSetup.split(",");
        this.minFrame = +frameSetup[0];
        this.maxFrame = +frameSetup[1];
        this.firstFrame = +frameSetup[2];
    };
    ModelingScene.prototype.getColorFromRange = function (i, colorSliderPosition) {
        var colorRange = this.game.global.colorsRanges[i];
        var tempColor = new Phaser.Display.Color(255, 255, 255);
        tempColor.red = colorRange.red[colorSliderPosition] * 255;
        tempColor.green = colorRange.green[colorSliderPosition] * 255;
        tempColor.blue = colorRange.blue[colorSliderPosition] * 255;
        tempColor.alpha = 0;
        return tempColor;
    };
    ModelingScene.prototype.callColorPoofer = function () {
        var selectedbodyPart = this.bodyPartsManager.selectedBodyPart;
        if (!selectedbodyPart.modelPart.partName.includes("Background")) {
            var pos = this.bodyPartsManager.getCurrentSelectedPartPos();
            if (pos) {
                this.colorProofSprite.startColorProof(selectedbodyPart.color, pos.x, pos.y);
            }
        }
    };
    ModelingScene.prototype.stopBlinking = function () {
        if (this.blinkingEnabled) {
            this.setBlinkingEnabled(false);
        }
    };
    ModelingScene.prototype.setBlinkingEnabled = function (blinkingEnabled) {
        this.blinkingEnabled = blinkingEnabled;
    };
    ModelingScene.prototype.buildStage = function () {
        this.loadingStarted = false;
        this.isSavesSlotsScreen = false;
        this.uiGenerator = new UIGenerator(this);
        this.uiGenerator.createImagesFromAssets();
        this.uiGenerator.setImagesVisibility();
        this.bodyPartsManager = new BodyPartsManager(this, this.modelParts);
        this.bodyPartsManager.setAtlasFront(this.getAtlasfront());
        this.setFirstFrame();
        this.uiGenerator.createTables();
        this.uiGenerator.setActorsLocation();
        this.uiGenerator.populateColorsTable();
        this.bodyParts = this.bodyPartsManager.createBodyParts(this.getAtlasfront(), this.frame);
        this.bodyPartsManager.setInitialSelectedBodyPart();
        var material = this.bodyPartsManager.selectedBodyPart.material;
        this.bodyPartsManager.setColorButtonImages(material);
        this.uiGenerator.setColorsToButtons();
        this.uiGenerator.setColorSelection(0);
        this.uiGenerator.getSelectedColorButton().addToColoredBodyPartsCount();
        this.uiGenerator.setBackground(this.bodyPartsManager.backgroundTexture, this.bodyPartsManager.fadeTexture);
        this.uiGenerator.populateBodyPartTable();
        this.uiGenerator.setTMenuSelector();
        this.uiGenerator.populateMaterialTable();
        this.uiGenerator.setMaterialButtons("" + this.getHDAtalasFront());
        this.bodyPartsManager.setUtmostSprites(this.screenMode);
        this.isEditBoxScreen = true;
        this.uiGenerator.populateStage();
        this.setUpEventHandler();
        this.bodyPartsManager.renderBodyPart();
        this.colorProofSprite = new ColorProoferSprite(this, "proof", "Poof.01.png");
    };
    ModelingScene.prototype.setFirstFrame = function () {
        this.frame = this.firstFrame;
        this.checkFrame();
        this.frameSliderMoveRight = true;
    };
    ModelingScene.prototype.checkFrame = function () {
        if (this.frame == this.firstFrame) {
            this.atlas = this.atlasFront;
        }
        else {
            this.atlas = this.atlasFrames;
        }
    };
    ModelingScene.prototype.setColorSelectorsAlfa = function (colorSelectorsAlfa) {
        this.colorSelectorsAlfa = colorSelectorsAlfa;
    };
    ModelingScene.prototype.getModelCharacteristics = function () {
        return this.modelCharacteristics;
    };
    ModelingScene.prototype.startOnFramesLoaded = function (atlasFrames) {
        this.setAtlasFrames(atlasFrames);
        this.bodyPartsManager.setLinkedBodyParts();
    };
    ModelingScene.prototype.setAtlasFrames = function (atlasFrames) {
        this.bodyPartsManager.setAtlasFrames(atlasFrames);
        this.atlasFrames = atlasFrames;
        for (var i = this.minFrame; i < this.maxFrame + 1; i++) {
            if (i != this.firstFrame) {
                this.bodyPartsManager.createFramesSprites(i);
            }
        }
    };
    ModelingScene.prototype.setUpEventHandler = function () {
        var _this = this;
        //color adjust frame:
        var colorFrame = this.uiGenerator.colorAdjustFrame;
        var colorButton = this.uiGenerator.colorAdjustButton;
        var dot = this.uiGenerator.colorAdjustDot;
        var self = this;
        colorFrame.isDrag = false;
        colorFrame.setName("colorAdjust");
        var LastPosX = 0;
        var modelScene = this;
        colorFrame.setInteractive({ hitAreaCallback: function () {
            } }).on('pointerdown', function (pointer, localX, localY, event) {
            modelScene.stopBlinking();
            colorFrame.isDrag = true;
        }).on('pointerup', function (pointer, localX, localY, event) {
            colorFrame.isDrag = false;
            LastPosX = 0;
            self.uiGenerator.correctColorAdjustbutton(colorFrame);
        });
        this.input.on("pointerdown", function (event, target) {
            _this.lastPinchVector = undefined;
            LayoutContants.getInstance().setModelMoving(false);
        }, this);
        this.input.on("pointermove", function (event, targets) {
            if (targets.length > 0) {
                var target = targets[0];
                if (target.name == "colorAdjust") {
                    if (target.isDrag) {
                        if (LastPosX == 0) {
                            LastPosX = event.x;
                        }
                        if (event.x > _this.uiGenerator.colorAdjustFrame.x - _this.uiGenerator.colorAdjustFrame.displayWidth + 38 / LayoutContants.getInstance().SCREEN_SIZE_COEF && event.x < _this.uiGenerator.colorAdjustFrame.x - 20 / LayoutContants.getInstance().SCREEN_SIZE_COEF) {
                            colorButton.x = event.x;
                            self.uiGenerator.onChangeColorAdjustment(colorButton);
                        }
                        dot.x = colorButton.x;
                        LastPosX = event.x;
                    }
                }
            }
            else {
                if (colorFrame.isDrag) {
                    colorFrame.isDrag = false;
                    LastPosX = 0;
                    self.uiGenerator.correctColorAdjustbutton(colorButton);
                }
            }
        }, this);
        //bodypart drag
        var lastTime = 0;
        var isDown = false;
        this.uiGenerator.touchZone.setInteractive()
            .on('pointerdown', function (pointer, localX, localY, event) {
            if (pointer.button == 1) {
                if (_this.screenMode == CST.LAYOUT.EDIT_MODE || _this.screenMode == CST.LAYOUT.PREVIEW_MODE) {
                    _this.panStart = true;
                    _this.startPosX = 0;
                    _this.startPosY = 0;
                }
            }
            isDown = true;
            _this.touchZonePointerDown(pointer);
        })
            .on('pointermove', function (pointer, localX, localY, event) {
            if (pointer.isDown) {
                _this.touchZonePointerMove(pointer, localX);
            }
        })
            .on('pointerup', function (pointer, localX, localY, event) {
            if (isDown) {
                if (pointer.button == 1) {
                    _this.panStart = false;
                    _this.startPosX = 0;
                    _this.startPosY = 0;
                }
                _this.touchZonePointerUp(pointer);
                if (_this.secondClick) {
                    var clickDelay = _this.time.now - lastTime;
                    lastTime = _this.time.now;
                    if (clickDelay < 300) {
                        _this.uiGenerator.fadeInBackgroundSprite();
                    }
                }
            }
            else {
                isDown = false;
            }
        })
            .on('pointerout', function () {
        });
        this.uiGenerator.touchZone.on('wheel', function (pointer, dx, dy, dz, event) {
            self.bodyPartsManager.setScaleBy(2 * dy / 1000);
        });
        //drag buttonPreview
        this.isPrevBtnDown = false;
        var btnPrevLX = 0;
        var btnPrevLY = 0;
        var buttonPreview = this.uiGenerator.buttonPreview;
        var originX = buttonPreview.x;
        var originY = buttonPreview.y;
        this.magnetStatus = 0;
        var isTopTrend = false;
        // it causes this event when press down preview button - Modified by SY
        buttonPreview.setInteractive().on("pointerdown", function (pointer, localX, localY, event) {
            _this.isPrevBtnDown = true;
            btnPrevLX = 0;
            btnPrevLY = 0;
        });
        // it causes this event when moving preview button under pressed down - Modified by SY
        this.input.on("pointermove", function (event, target) {
            if (_this.isPrevBtnDown && event.x > originX - buttonPreview.displayWidth && buttonPreview.x <= LayoutContants.getInstance().SCREEN_WIDTH) {
                if (btnPrevLX == 0)
                    btnPrevLX = event.x;
                if (btnPrevLY == 0)
                    btnPrevLY = event.y;
                var deltaX = event.x - btnPrevLX;
                var deltaY = event.y - btnPrevLY;
                buttonPreview.x += deltaX;
                buttonPreview.y += deltaY;
                if (buttonPreview.x > LayoutContants.getInstance().SCREEN_WIDTH) {
                    buttonPreview.x = LayoutContants.getInstance().SCREEN_WIDTH;
                    deltaX = 0;
                }
                if (buttonPreview.x < originX) {
                    buttonPreview.x = originX;
                    deltaX = 0;
                }
                ;
                if (buttonPreview.y < 0) {
                    buttonPreview.y = 0;
                    deltaY = 0;
                }
                ;
                if (buttonPreview.y > _this.LAYOUT_CONSTANT.BUTTONS_SIZE * 1.2) {
                    buttonPreview.y = _this.LAYOUT_CONSTANT.BUTTONS_SIZE * 1.2;
                    deltaY = 0;
                }
                ;
                if (buttonPreview.x < originX + 0.5 * LayoutContants.getInstance().BUTTONS_SIZE) {
                    _this.magnetStatus = 0;
                }
                else if (buttonPreview.x < originX + 1 * LayoutContants.getInstance().BUTTONS_SIZE) {
                    _this.magnetStatus = 1;
                }
                else if (buttonPreview.x < originX + (1.8 + 0.5) * LayoutContants.getInstance().BUTTONS_SIZE) {
                    _this.magnetStatus = 2;
                }
                else {
                    _this.magnetStatus = 3;
                }
                if (buttonPreview.y < originY - 0.6 * LayoutContants.getInstance().BUTTONS_SIZE) {
                    isTopTrend = true;
                }
                else {
                    isTopTrend = false;
                }
                // this.events.emit("prevbutton.pontermove", deltaX);
                btnPrevLX = event.x;
                btnPrevLY = event.y;
                _this.uiGenerator.updateGridTablesBasedOnPreviewBtn(deltaX);
            }
        }, this);
        // it causes this event when pressing up preview button - Modified by SY
        this.input.on("pointerup", function (event, targets) {
            var deltaX = 0;
            var deltaY = 0;
            if (_this.isPrevBtnDown) {
                _this.isPrevBtnDown = false;
                if (_this.magnetStatus == 0) {
                    _this.screenMode = CST.LAYOUT.EDIT_MODE;
                    deltaX = originX - buttonPreview.x;
                }
                else if (_this.magnetStatus == 1) {
                    deltaX = originX + 0.8 * LayoutContants.getInstance().BUTTONS_SIZE - buttonPreview.x;
                }
                else if (_this.magnetStatus == 2) {
                    deltaX = originX + 1.8 * LayoutContants.getInstance().BUTTONS_SIZE - buttonPreview.x;
                }
                else {
                    deltaX = LayoutContants.getInstance().SCREEN_WIDTH - buttonPreview.x;
                    _this.screenMode = CST.LAYOUT.PREVIEW_MODE;
                }
                if (isTopTrend) {
                    deltaY = -buttonPreview.y;
                }
                else {
                    deltaY = originY - buttonPreview.y;
                }
                if (_this.magnetStatus == 3) {
                    _this.uiGenerator.colorAdjustContainer.setVisible(false);
                }
                else {
                    _this.uiGenerator.colorAdjustContainer.setVisible(true);
                }
                buttonPreview.x += deltaX;
                buttonPreview.y += deltaY;
                // this.events.emit("prevbutton.pontermove", deltaX, isLeftTrend);
                _this.uiGenerator.updateGridTablesBasedOnPreviewBtn(deltaX);
            }
        }, this);
        //color adjust frame:
        var isSelected = false;
        this.input.on("pointermove", function (event, target) {
            isSelected = true;
            if (_this.uiGenerator.floatColorSliderBG.visible) {
                _this.uiGenerator.updateFloatingPosition(event.x);
                _this.uiGenerator.setFloatingColorAdjustPosition();
            }
        }, this);
        this.input.on("pointerup", function (event, target) {
            if (isSelected && _this.uiGenerator.floatColorSliderBG.visible) {
                _this.uiGenerator.selectedColorButton.onTapButton();
                isSelected = false;
            }
            setTimeout(function () {
                _this.uiGenerator.hideFloatColorSlider();
            }, 500);
        }, this);
    };
    ModelingScene.prototype.setFrame = function (frame) {
        // this.frame = frame;
        // this.checkFrame();
        // this.bodyParts = this.bodyPartsManager.setFrame(frame);
        // this.bodyPartsManager.setUtmostSprites(frame);
    };
    ModelingScene.prototype.setUndoRedoCooldowned = function (undoRedoCooldowned) {
        this.isUndoRedoCooldowned = undoRedoCooldowned;
    };
    ModelingScene.prototype.playSound = function (soundId) {
        if (soundId == CST.SOUND.BACKGROUND_SOUND) {
            this.backgrondSong.play();
        }
        if ((localStorage.getItem("sound") || 'on') === 'on') {
            if (soundId == CST.SOUND.COLOR_CHANGE_SOUND) {
                this.swoosh.play();
            }
            if (soundId == CST.SOUND.COLOR_GENERAL_SOUND) {
                this.generalsound.play();
            }
            if (soundId == CST.SOUND.CANCEL_SOUND) {
                this.backsound.play();
            }
        }
    };
    ModelingScene.prototype.goMainScene = function () {
        clearInterval(this.timer);
        //window.location.hash = CST.SCENES.MAIN ;
        window.location.href = CST.HOST_ADDRESS + '#' + CST.SCENES.MAIN;
    };
    ModelingScene.prototype.goSloteScene = function () {
        clearInterval(this.timer);
        // console.log('here')
        // window.location.reload();
        window.location.hash = CST.SCENES.SAVE_SLOTE + '/' + Global.getInstance().getModel().modelName;
        this.scene.start(CST.SCENES.SAVE_SLOTE, { initSound: false });
        // var event = new CustomEvent('update_slote', { detail: true });
        // document.dispatchEvent(event);
        // this.scene.switch(CST.SCENES.SAVE_SLOTE);
    };
    ModelingScene.prototype.resetCenterPointer = function () {
        // this.bodyPartsManager.resetCenterOffY();
    };
    ModelingScene.prototype.setScreeMode = function (screenMode) {
        this.screenMode = screenMode;
        this.bodyPartsManager.resetDXDY();
        this.bodyPartsManager.setUtmostSprites(screenMode);
    };
    ModelingScene.prototype.addEventListner = function () {
        document.addEventListener('web.exit.shoping-cart', function (e) {
        });
    };
    ModelingScene.prototype.initButtonSounds = function () {
        this.backgrondSong = this.sound.add("backgroundSong", { volume: 0.2 });
        this.swoosh = this.sound.add("swoosh");
        this.generalsound = this.sound.add("generalsound");
        this.backsound = this.sound.add("backsound");
    };
    ModelingScene.prototype.getResolution = function () {
        return this.resolution || "SD";
    };
    ModelingScene.prototype.getAtlasfront = function () {
        return this.atlasFront + "-" + this.getResolution();
    };
    ModelingScene.prototype.getHDAtalasFront = function () {
        return this.atlasFront + "-HD";
    };
    ModelingScene.prototype.touchZonePointerDown = function (pointer) {
        if (this.isFileLoaded) {
            this.startPosX = pointer.x;
            this.startPosY = pointer.y;
            this.animationDirection = SwitchStatus.STOP;
            this.uiGenerator.bottomSwitchBar.setSwtichOn(SwitchStatus.STOP, true);
        }
    };
    ModelingScene.prototype.touchZonePointerMove = function (pointer, localX) {
        if (this.isPrevBtnDown)
            return;
        this.currentPointX = pointer.x;
        if (this.startPosX == 0) {
            this.startPosX = pointer.x;
        }
        if (this.startPosY == 0) {
            this.startPosY = pointer.y;
        }
        if (this.panStart) {
            var moveX = pointer.x - this.startPosX;
            var moveY = pointer.y - this.startPosY;
            this.bodyPartsManager.moveByX(moveX);
            this.bodyPartsManager.moveByY(moveY);
            this.startPosX = pointer.x;
            this.startPosY = pointer.y;
            this.LAYOUT_CONSTANT.setModelMoving(true);
        }
        else if (this.pinchStart) {
            //console.log('pinchStart', this.bodyPartsManager.tCenterX, this.bodyPartsManager.tCenterY)
            // if(this.bodyPartsManager.tCenterX){
            // }
            var currentVetor = new Phaser.Math.Vector2(pointer.x, pointer.y);
            var ox = this.bodyPartsManager.bodyPartStack.x + this.bodyPartsManager.bodyPartInnerStack.x;
            var oy = this.bodyPartsManager.bodyPartStack.y + this.bodyPartsManager.bodyPartInnerStack.y;
            var originVector = new Phaser.Math.Vector2(ox, oy);
            if (this.lastPinchVector == undefined) {
                this.lastPinchVector = currentVetor;
            }
            // let currentPinchDis = Phaser.Math.Distance.Between(pointer.x, pointer.y, this.bodyPartsManager.tCenterX, this.bodyPartsManager.tCenterY);
            // let lastPinchDis = Phaser.Math.Distance.Between(this.pinchP1X, this.pinchP1Y, this.pinchP2X, this.pinchP2Y);
            // Scaling
            //let distance = Math.abs(currentPinchDis - lastPinchDis);
            //let scale = currentPinchDis/lastPinchDis;
            var scale = currentVetor.distanceSq(originVector) / this.lastPinchVector.distanceSq(originVector);
            if (scale > 0.1) {
                this.bodyPartsManager.setScaleMulti(scale, this.selectedCenterX, this.selectedCenterY);
                this.uiGenerator.setScaleDiceImageByDScale();
            }
            this.lastPinchVector = currentVetor;
            this.LAYOUT_CONSTANT.setModelMoving(true);
        }
        else {
            if (!this.input.pointer2.isDown && this.isFileLoaded) {
                if (pointer.x > this.startPosX + 0.01 * this.uiGenerator.touchZone.scaleX) {
                    this.LAYOUT_CONSTANT.setModelMoving(true);
                    this.bodyPartsManager.rotateRight();
                    this.startPosX = pointer.x;
                }
                if (pointer.x < this.startPosX - 0.01 * this.uiGenerator.touchZone.scaleX) {
                    this.bodyPartsManager.rotateLeft();
                    this.LAYOUT_CONSTANT.setModelMoving(true);
                    this.startPosX = pointer.x;
                }
                this.uiGenerator.bottomSwitchBar.setFrame(this.bodyPartsManager.frame);
            }
        }
    };
    ModelingScene.prototype.touchZonePointerUp = function (pointer) {
        this.bodyPartsManager.checkPixelPerfectPos(pointer.x, pointer.y);
    };
    ModelingScene.prototype.showFPS = function () {
        if (localStorage.getItem("fps") == 'on') {
            this.fpsText = this.add.text(100, 500, "FPS:");
            this.fpsText.setColor("0x000000");
            this.fpsText.setFontFamily("Nevis");
            this.fpsText.depth = 1000;
            this.config.showingFPS = true;
        }
    };
    return ModelingScene;
}(Phaser.Scene));
/// <reference path='../../phaser.d.ts'/>
var PreloadScene = /** @class */ (function (_super) {
    __extends(PreloadScene, _super);
    function PreloadScene() {
        var _this = _super.call(this, { key: CST.SCENES.PRELOAD }) || this;
        _this.models_Arr = new Array();
        _this.LAYOUT_CONSTANT = LayoutContants.getInstance();
        return _this;
    }
    PreloadScene.prototype.preload = function () {
        //if(Global.getInstance().checkLogoView())
        this.loadLogoImages();
    };
    PreloadScene.prototype.create = function () {
        this.scene.start(CST.SCENES.LAUNCH);
    };
    PreloadScene.prototype.loadLogoImages = function () {
        this.load.image("logoFrame1", "assets/img/bootscreen/logo/Anim_Top1.png");
        this.load.image("logoFrame2", "assets/img/bootscreen/logo/Anim_Top2.png");
        this.load.image("tab-splitter", "assets/img/TabSplitter.png");
        this.load.multiatlas('logo', 'assets/img/bootscreen/logo/logo.json', 'assets/img/bootscreen/logo');
        this.load.audio('button-2', ['assets/sounds/button-2.mp3']);
    };
    return PreloadScene;
}(Phaser.Scene));
var SaveSloteScene = /** @class */ (function (_super) {
    __extends(SaveSloteScene, _super);
    function SaveSloteScene() {
        var _this = _super.call(this, { key: CST.SCENES.SAVE_SLOTE }) || this;
        _this.LAYOUT_CONSTANT = LayoutContants.getInstance();
        _this.initSound = true;
        _this.resolution = "SD";
        _this.name = CST.SCENES.SAVE_SLOTE;
        return _this;
    }
    SaveSloteScene.prototype.init = function (data) {
        if (data === void 0) { data = { initSound: true }; }
        Global.getInstance().setCurrentScene(this.scene);
        Global.getInstance().setNavigation(this.scene);
        var selectedModel = Global.getInstance().getModel();
        this.isFileLoaded = false;
        this.model = selectedModel;
        this.fileNames = new FileNames().generateFileNames(selectedModel, false);
        this.atlasFront = this.model.modelName + "-front";
        this.atlasFrames = "" + this.model.modelName;
        this.frameSetup();
        this.modelCharacteristics = new ModelCharacteristics();
        this.initSound = data.initSound;
        this.apiData = { 'savedData': [], 'sharedData': [] };
        this.sloteNum = 60;
        this.sloteItemsData = [];
        this.isLoggedIn = false;
        this.user_id = "";
        this.communityItemData = [];
        this.sloteItems = [];
        this.communityModelIndex = 0;
        this.isDebug = false;
        this.checkUserLoggedIn();
    };
    SaveSloteScene.prototype.preload = function () {
        this.load.multiatlas(this.model.modelName + "-front-SD", "assets/img/models/" + this.model.modelName + "/SD/" + this.fileNames.atlasSDFront, "assets/img/models/" + this.model.modelName + "/SD");
        this.load.json(this.model.modelName + "-save", "assets/img/models/" + this.model.modelName + "/" + this.fileNames.saveFile);
        this.load.audio(this.model.modelName, ["assets/img/models/" + this.model.modelName + "/" + this.fileNames.song]);
        if (!this.model.shopSkip && this.model.shopID == "") {
            this.load.image(this.model.shopImage, "assets/img/models/" + this.model.modelName + "/" + this.model.shopImage); //shop image
        }
        this.load.image(this.model.modelName + "_Title", "assets/img/models/" + this.model.modelName + "/" + this.model.modelName + "_Title.png"); //title image
        this.loadModels();
    };
    SaveSloteScene.prototype.create = function () {
        return __awaiter(this, void 0, void 0, function () {
            var event, event;
            var _this = this;
            return __generator(this, function (_a) {
                this.playModelBackgroundSong();
                CsvParser.parsePartsCsv(CST.HOST_ADDRESS + "/assets/img/models/" + this.model.modelName + "/" + this.fileNames.partsCsv, CST.HOST_ADDRESS + "/assets/img/models/" + this.model.modelName + "/" + this.fileNames.featuresCsv, function (result) {
                    _this.modelParts = result;
                    Global.getInstance().setSeletectedModelParts(result);
                    _this.isFileLoaded = true;
                });
                // We need to show the initial model when user didn't login...
                this.initialData = this.cache.json.get(this.model.modelName + "-save") || [];
                event = new CustomEvent('web.getToken', { detail: "event test" });
                document.dispatchEvent(event);
                event = new CustomEvent('onScreenChange', { detail: CST.SCENES.SAVE_SLOTE });
                document.dispatchEvent(event);
                this.bindEventLisner(0);
                this.bindEventLisner(1);
                if (this.isLoggedIn) {
                    this.getSavedDataFromAPI();
                    this.getSharedDataFromAPI();
                    console.log('savedData', this.apiData['savedData']);
                }
                return [2 /*return*/];
            });
        });
    };
    SaveSloteScene.prototype.loadModels = function () {
    };
    SaveSloteScene.prototype.checkUserLoggedIn = function () {
        var _this = this;
        var event = new CustomEvent('checkUserLoggedIn', { detail: { callback: function (res) {
                    console.log("checkUserLoggedIn", res);
                    var loginFlag = res[0];
                    _this.isLoggedIn = loginFlag;
                    _this.user_id = _this.isLoggedIn ? res[1] : _this.user_id;
                } } });
        document.dispatchEvent(event);
    };
    SaveSloteScene.prototype.removeTextures = function () {
        this.textures.remove(this.model.modelName + "-front-SD");
        this.textures.remove(this.model.modelName + "-save");
        this.textures.remove(this.model.modelName);
    };
    SaveSloteScene.prototype.update = function () {
        if (this.isDebug) {
            if (this.isFileLoaded) {
                this.initElements();
                this.isFileLoaded = false;
            }
        }
        else {
            if (this.isFileLoaded && this.apiData['savedData'] && this.apiData['sharedData']) {
                if (this.isLoggedIn) {
                    if (this.apiData['savedData'].length) {
                        this.initElements();
                        this.isFileLoaded = false;
                    }
                }
                else {
                    this.initElements();
                    this.isFileLoaded = false;
                }
                // console.log('update render')
            }
        }
        var pointer = this.input.activePointer;
        if (pointer.isDown) {
            if (!this.lastPosY)
                this.lastPosY = pointer.y;
            3000;
            if (!this.lastPosX)
                this.lastPosX = pointer.x;
            var time = 1 / this.game.loop.actualFps;
            var vy = (pointer.y - this.lastPosY);
            var vx = (pointer.x - this.lastPosX);
            this.lastPosY = pointer.y;
            this.lastPosX = pointer.x;
            this.events.emit("velocity", vx, vy);
        }
        else {
            this.lastPosY = undefined;
            this.lastPosX = undefined;
        }
    };
    SaveSloteScene.prototype.initElements = function () {
        var _this = this;
        var scrollMode = 1;
        var marginLeft = 100;
        var marginTop = 50;
        var cellHeight = (this.LAYOUT_CONSTANT.SCREEN_HEIGHT - marginTop) / 2;
        var cellWidth = 400;
        var columns = Math.round((this.LAYOUT_CONSTANT.SCREEN_HEIGHT - 100) / (cellHeight * 2));
        this.sloteItems = [];
        this.sloteItems[0] = this.getItems(cellWidth, cellHeight, scrollMode, 0, Math.max(this.sloteNum, Global.getInstance().getSloteItemsCount(this.model.modelName)));
        this.sloteItems[1] = this.getItems(cellWidth, cellHeight, scrollMode, 0, Math.max(this.sloteNum, Global.getInstance().getSloteItemsCount(this.model.modelName)), true);
        console.log('slotitemsinfo', this.sloteItems[0], this.sloteItems[1]);
        this.sloteGridTable = new SlotGridTable(this, {
            x: marginLeft,
            y: marginTop,
            width: this.LAYOUT_CONSTANT.SCREEN_WIDTH - marginLeft,
            height: this.LAYOUT_CONSTANT.SCREEN_HEIGHT - marginTop,
            background: [this.add.image(0, 0, "solid"), this.add.image(0, 0, "solid")],
            scrollMode: scrollMode,
            modelName: this.model.modelName,
            sloteItemsData: this.sloteItemsData,
            table: {
                cellWidth: cellWidth,
                cellHeight: cellHeight,
                columns: columns,
            },
            updateSloteItemData: this.updateSloteItemData,
        });
        var tableIndex = 0;
        for (var i = 0; i < 2; i++) {
            tableIndex = i;
            if (this.sloteItems[tableIndex] && this.sloteItems[tableIndex].length) {
                this.sloteGridTable.setItems(this.sloteItems[tableIndex], tableIndex);
            }
        }
        if (this.sloteItems[1].length) {
            // listner when clicking model in slote pannel
            this.sloteGridTable.table[1].on("cell.click", function (cellIndex) {
                _this.communityModelBoard.setVisible(true);
                _this.changeCommunityModelBoardFlag(true);
                _this.communityModelIndex = cellIndex;
                console.log('community slot cell click', _this.communityModelIndex);
            });
        }
        // listner when clicking model in slote pannel
        this.sloteGridTable.table[0].on("cell.click", function (cellIndex) {
            _this.editCommunityModel(cellIndex);
        });
        this.exitButton = new CustomImageButton(this, 0, 0, "back_button", function () {
            _this.goMainScene();
        });
        this.exitButton.setTopLeftOrigin();
        console.log("sloteItems", this.sloteItems[0]);
        this.communityModel = new CommunityModel(this, this.LAYOUT_CONSTANT.SCREEN_WIDTH, this.LAYOUT_CONSTANT.SCREEN_HEIGHT, this.model, this.sloteItems[0][0].savedModel);
        this.communityModelBoard = new CommunityModelBoard(this, {
            x: 0,
            y: 0,
            width: this.LAYOUT_CONSTANT.SCREEN_WIDTH,
            height: this.LAYOUT_CONSTANT.SCREEN_HEIGHT,
            background: this.add.image(0, 0, "Fade8"),
            modelName: this.model.modelName,
            changeCommunityModelBoardFlag: this.changeCommunityModelBoardFlag,
            editCommunityModel: function () {
                _this.editCommunityModel();
            }
        });
        this.communityModelBoard.setItems(this.communityModel);
        this.communityModelBoard.setVisible(false);
        this.changeCommunityModelBoardFlag(false);
        this.communityModelIndex = 0;
    };
    SaveSloteScene.prototype.changeCommunityModelBoardFlag = function (flag) {
        this.isCommunityModelBoardShown = flag;
    };
    SaveSloteScene.prototype.editCommunityModel = function (cellIndex) {
        if (cellIndex === void 0) { cellIndex = undefined; }
        if (cellIndex !== undefined) {
            if (!this.isCommunityModelBoardShown) {
                this.sloteItems[0][cellIndex].savedModel.setIndex(cellIndex);
                this.selectedIndex = cellIndex;
                this.startModelingScene(this.sloteItems[0][cellIndex].savedModel);
            }
        }
        else {
            if (this.sloteItems[1].length) {
                this.sloteItems[1][this.communityModelIndex].savedModel.setIndex(this.communityModelIndex);
                this.selectedIndex = this.communityModelIndex;
                this.startModelingScene(this.sloteItems[1][this.communityModelIndex].savedModel);
            }
        }
    };
    // starting ModelingScene
    SaveSloteScene.prototype.startModelingScene = function (savedModel) {
        Global.getInstance().setCurrentModel(savedModel);
        if (this.isCommunityModelBoardShown) {
            window.location.hash = CST.SCENES.MODELING + '/' + Global.getInstance().getModel().modelName + '?' + 'sharemodelview';
        }
        else {
            window.location.hash = CST.SCENES.MODELING + '/' + Global.getInstance().getModel().modelName;
        }
        this.scene.start(CST.SCENES.MODELING);
    };
    SaveSloteScene.prototype.frameSetup = function () {
        var frameSetup = this.model.frameSetup.split(",");
        this.firstFrame = +frameSetup[2];
        this.minFrame = +frameSetup[0];
        this.maxFrame = +frameSetup[1];
    };
    SaveSloteScene.prototype.getModelCharacteristics = function () {
        return this.modelCharacteristics;
    };
    SaveSloteScene.prototype.getItems = function (width, height, scrollMode, offset, count, shareFlag) {
        var _this = this;
        if (shareFlag === void 0) { shareFlag = false; }
        var result = [];
        if (offset + count > this.initialData.length)
            return result;
        for (var i = offset; i < offset + count; i++) {
            var jsonData = this.initialData[i];
            var itemData = jsonData;
            result.push(itemData);
        }
        console.log('initial_item_datas', result);
        var itemArr = [];
        result.forEach(function (element, i) {
            var itemData = element;
            if (_this.isDebug) {
                if (localStorage.getItem("" + _this.model.modelName + (i + offset))) {
                    var jsonStr = localStorage.getItem("" + _this.model.modelName + (i + offset));
                    var localJSONOBJp = JSON.parse(jsonStr);
                    itemData = localJSONOBJp;
                }
            }
            else {
                var apiJSON = _this.findSavedModel(i + offset, shareFlag);
                if (apiJSON) {
                    console.log("exist slote info", apiJSON, i);
                    itemData = apiJSON;
                }
            }
            var sloteItem;
            if (itemData.hasOwnProperty('slote_info')) {
                var _a = itemData || {}, id = _a.id, user_id = _a.user_id, model_name = _a.model_name, slot_index = _a.index, slote_info = _a.slote_info;
                var sloteItemInfo = __assign({ id: id, user_id: user_id, model_name: model_name, slot_index: slot_index }, slote_info);
                var sloteItemInfos = {
                    sloteItemInfo: sloteItemInfo,
                    tabIndex: i + offset,
                    shareFlag: shareFlag
                };
                sloteItem = new SlotItem(_this, width, height, scrollMode, _this.model, itemData.saved_data, sloteItemInfos);
            }
            else {
                var slote_info = {
                    like_count: 0,
                    download_count: 0,
                    slot_index: 0,
                    is_shared: false,
                    shared_date: "",
                    feature: 0
                };
                var sloteItemInfo = __assign({ id: 0, user_id: _this.user_id, model_name: _this.model.modelName, slot_index: i }, slote_info);
                var sloteItemInfos = {
                    sloteItemInfo: sloteItemInfo,
                    tabIndex: i + offset,
                    shareFlag: shareFlag
                };
                sloteItem = new SlotItem(_this, width, height, scrollMode, _this.model, itemData, sloteItemInfos);
            }
            itemArr.push(sloteItem);
            // if (shareFlag) {
            //     this.communityItemData.push(itemData)
            // }
        });
        return itemArr;
    };
    SaveSloteScene.prototype.goMainScene = function () {
        //this.removeTextures();
        // window.location.hash = CST.SCENES.MAIN;
        window.location.href = CST.HOST_ADDRESS + "#MainScene";
        window.location.reload();
        //window.location.href = `${CST.HOST_ADDRESS}?selected_model=${0}`;
        // this.scene.start(CST.SCENES.MAIN);
    };
    SaveSloteScene.prototype.playModelBackgroundSong = function () {
        if ((localStorage.getItem("music") || 'on') === 'on') {
            if (this.initSound) {
                this.sound.stopAll();
                this.sound.play(this.model.modelName, { volume: 0.2, loop: true });
            }
        }
        else {
            this.initSound = true;
            this.sound.stopAll();
        }
    };
    SaveSloteScene.prototype.bindEventLisner = function (tableIndex) {
        var _this = this;
        document.addEventListener('update_slote', function (e) {
            var index = _this.selectedIndex;
            _this.sloteItems[tableIndex][index].updateData(Global.getInstance().getSavedModel());
        });
        document.addEventListener('upload_data', function (e) {
            _this.copyStringToClipboard(e.detail.saved_data);
        });
    };
    SaveSloteScene.prototype.copyStringToClipboard = function (str) {
        // Create new element
        var el = document.createElement('textarea');
        // Set value (string to be copied)
        el.value = str;
        // Set non-editable to avoid focus and move outside of view
        el.setAttribute('readonly', '');
        el.style.position = "absolute";
        el.style.left = '-9999px';
        document.body.appendChild(el);
        // Select text inside element
        el.select();
        // Copy text to clipboard
        document.execCommand('copy');
        // Remove temporary element
        document.body.removeChild(el);
    };
    SaveSloteScene.prototype.getSavedDataFromAPI = function () {
        var _this = this;
        var event = new CustomEvent('getSavedData', { detail: { model_name: this.model.modelName, shareFlag: false, callback: function (res) {
                    _this.apiData['savedData'] = res;
                    console.log('getSavedDataFromAPI', res);
                } } });
        document.dispatchEvent(event);
    };
    SaveSloteScene.prototype.getSharedDataFromAPI = function () {
        var _this = this;
        var event = new CustomEvent('getSavedData', { detail: { model_name: this.model.modelName, shareFlag: true, callback: function (res) {
                    _this.apiData['sharedData'] = res;
                    console.log('getSharedDataFromAPI', res);
                } } });
        document.dispatchEvent(event);
    };
    SaveSloteScene.prototype.findSavedModel = function (index, shareFlag) {
        var result;
        if (shareFlag) {
            this.apiData['sharedData'].forEach(function (element) {
                if (parseInt(element["index"]) == index) {
                    result = element;
                }
            });
        }
        else {
            this.apiData['savedData'].forEach(function (element) {
                if (parseInt(element["index"]) == index) {
                    result = element;
                }
            });
        }
        return result;
    };
    SaveSloteScene.prototype.getSloteItemData = function (slote_index) {
        var _this = this;
        if (slote_index === void 0) { slote_index = undefined; }
        var promise = new Promise(function (resolve, reject) {
            var event = new CustomEvent('getSloteData', { detail: { model_name: _this.model.modelName, slote_index: slote_index, callback: function (res) {
                        _this.sloteItemsData = res;
                        resolve(res);
                        console.log('this.sloteItemsData', _this.sloteItemsData);
                    } } });
            document.dispatchEvent(event);
        });
        return promise;
    };
    SaveSloteScene.prototype.addSloteItemData = function (model_data) {
        var promise = new Promise(function (resolve, reject) {
            var event = new CustomEvent('addSloteData', { detail: { model_data: model_data, callback: function (res) {
                        resolve(res);
                    } } });
            document.dispatchEvent(event);
        });
        return promise;
    };
    SaveSloteScene.prototype.updateSloteItemData = function (mid, model_data) {
        var event = new CustomEvent('setSloteData', { detail: { mid: mid, model_data: model_data, callback: function (res) {
                    console.log("update success", res);
                } } });
        document.dispatchEvent(event);
    };
    SaveSloteScene.prototype.deleteSloteItemData = function (mid) {
        var event = new CustomEvent('delSloteData', { detail: { mid: mid, callback: function (res) {
                } } });
        document.dispatchEvent(event);
    };
    SaveSloteScene.prototype.delay = function (ms) {
        return new Promise(function (resolve) { return setTimeout(resolve, ms); });
    };
    return SaveSloteScene;
}(Phaser.Scene));
var TestScene = /** @class */ (function (_super) {
    __extends(TestScene, _super);
    function TestScene() {
        var _this = _super.call(this, { key: "test" }) || this;
        _this.modelScale = 0.2;
        _this.pinchStart = false;
        _this.userScale = 1;
        return _this;
    }
    TestScene.prototype.preload = function () {
        this.load.image("BoxFull1", "assets/BoxFull.png");
        this.load.image("BoxFull2", "assets/BoxFull.png");
    };
    TestScene.prototype.create = function () {
        this.cam1 = this.cameras.main.setSize(512, 384).setName('Camera 1');
        this.testImage1 = this.add.image(LayoutContants.getInstance().SCREEN_WIDTH / 2, LayoutContants.getInstance().SCREEN_HEIGHT / 2, "BoxFull1");
        this.testImage1.setScale(this.modelScale).setTint(0xdddddd).setAlpha(0.3);
        this.testImage1.displayWidth = 500;
        this.testImage1.displayHeight = 200;
        this.outContainer = this.add.container(LayoutContants.getInstance().SCREEN_WIDTH / 2, LayoutContants.getInstance().SCREEN_HEIGHT / 2);
        this.testImage = this.add.image(0, 0, "BoxFull2").setScale(this.modelScale);
        this.outContainer.add(this.testImage);
        this.bindEvent();
        this.tCenterX = LayoutContants.getInstance().SCREEN_WIDTH / 2;
        this.tCenterY = LayoutContants.getInstance().SCREEN_HEIGHT / 2;
    };
    TestScene.prototype.update = function () {
        var pointer = this.input.activePointer;
    };
    TestScene.prototype.setScaleMulti = function (scale, posX, posY) {
        var ox0 = this.testImage1.originX;
        var oy0 = this.testImage1.originY;
        if (this.testImage1.getBounds().contains(posX, posY)) {
            var x = posX - this.testImage1.x + this.testImage1.displayWidth * ox0;
            var y = posY - this.testImage1.y + this.testImage1.displayWidth * oy0;
            var newOx = x / this.testImage1.displayWidth;
            var newOy = y / this.testImage1.displayHeight;
            var dx = this.testImage1.displayWidth * (newOx - ox0);
            var dy = this.testImage1.displayHeight * (newOy - oy0);
            this.testImage1.x += dx;
            this.testImage1.y += dy;
            this.testImage1.setOrigin(newOx, newOy);
            this.outContainer.setPosition(this.testImage1.x - dx, this.testImage1.y - dy);
        }
        this.testImage1.scale *= scale;
        var ux = this.testImage1.x - this.testImage1.displayWidth * this.testImage1.originX + this.testImage1.displayWidth / 2;
        var uy = this.testImage1.y - this.testImage1.displayWidth * this.testImage1.originY + this.testImage1.displayHeight / 2;
        this.outContainer.scale *= scale;
        this.outContainer.x = ux;
        this.outContainer.y = uy;
    };
    TestScene.prototype.refreshScale = function (startPosX, startPosY, startTargetX, startTargetY) {
        // var dx = startPosX - startTargetX;
        // var dy = startPosY - startTargetY;
        // this.outContainer.x = LayoutContants.getInstance().SCREEN_WIDTH/2;
        // this.outContainer.y =  LayoutContants.getInstance().SCREEN_HEIGHT/2;
        // console.log("scale end dex", this.innerContainer.x);
        // this.innerContainer.x += dx/this.userScale; 
        // this.innerContainer.y += dy/this.userScale;
    };
    TestScene.prototype.bindEvent = function () {
        var _this = this;
        this.input.keyboard.on("keyup_SHIFT", function () {
            _this.pinchStart = false;
        });
        this.input.keyboard.on("keydown_SHIFT", function () {
            _this.lastPinchVector = undefined;
            if (_this.input.activePointer.isDown && !_this.pinchStart) {
                _this.pinchStart = true;
                _this.tCenterX = _this.input.activePointer.x;
                _this.tCenterY = _this.input.activePointer.y;
            }
        });
        this.input.on("pointermove", function (events, targets) {
            if (_this.pinchStart && events.isDown) {
                var currentVetor = new Phaser.Math.Vector2(events.x, events.y);
                var originVector = new Phaser.Math.Vector2(_this.outContainer.x, _this.outContainer.y);
                if (_this.lastPinchVector == undefined) {
                    _this.lastPinchVector = currentVetor;
                }
                _this.setScaleMulti(currentVetor.distanceSq(originVector) / _this.lastPinchVector.distanceSq(originVector), _this.tCenterX, _this.tCenterY);
                _this.lastPinchVector = currentVetor;
            }
        });
        this.input.on("pointerdown", function (events, targets) {
            _this.tCenterX = events.x;
            _this.tCenterY = events.y;
        });
        this.input.on("pointerup", function (events, targets) {
            _this.refreshScale(_this.tCenterX, _this.tCenterY, _this.startTargetX, _this.startTargetY);
        });
    };
    return TestScene;
}(Phaser.Scene));
//# sourceMappingURL=output.js.map