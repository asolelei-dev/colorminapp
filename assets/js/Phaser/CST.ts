var TOKEN;
const CST = {
    FRONT_FRAME:15,
    HOST_ADDRESS:"./",
    API_URL:"https://colorcloud.figuromomodels.com/api/",
    AUTH:{
        TOKEN_URL:"https://colorcloud.figuromomodels.com/api-token-auth/",
        USERNAME:"figuro5",
        PASSWORD:"figuro5pass1"
    },
    SHOPIFY:{
        PRIVATE_API_KEY:"cb4a7d42d28224712805f137d204ecb5",
        PRIVATE_API_KEY_PASSWORD:"e9d4504fcfbdbc06739d4b6f330964c7"
    },
    SCENES:{
        INITIAL:"InitialScene",
        PRELOAD:"PreloadScene",
        LAUNCH:"LaunchScene",
        MAIN:"MainScene",
        MODELING:"ModelingScene",
        SAVE_SLOTE:"Save_slote"
    },
    EVENTS:{
        LOADED:"loaded",
        CSV_LOADED:"csv_loaded"
    },
    ANIMATION:{
        DIRECTION:{
            RIGHT:1,
            LEFT:2,
            STOP:3
        },
        RATE:3
    },
    IMAGE:{
        LOGO: "assets/img/bootscreen/logo/AnimFill_2.png"
    },
    MODEL_TYPE:{
        GLOSS:1,
        CLATY:0
    },
    CSVPARSER:{
        PART_NAME_COL : 0,
        SORT_ORDER_COL : 2,
        TMENU_ORDER_COL : 3,
        PART_SIZE_X_COL : 4,
        PART_SIZE_Y_COL : 5,
        PART_POS_X_COL : 6,
        PART_POS_Y_COL : 7,
        SCALE_ADJUST_COL : 8,
        X_ADJUST_COL : 9,
        Y_ADJUST_COL : 10,
        FIRST_TAB_IMAGE_COL : 13,
        LAST_TAB_IMAGE_COL : 32,
        PART_BASE_IMAGE_COL : 34,
        PART_GLOSS_IMAGE_COL : 35,
        GLOSS_ALPHA_COL : 36,
        COLOR_BUTTON_BASE_IMAGE_COL : 39,
        COLOR_BUTTON_GLOSS_IMAGE_COL : 40,
        BUTTON_GLOSS_ALPHA_COL : 41,
        NUM_OF_TABS : 20,
        NUM_OF_COLUMNS_BETWEEN_CORRESPONDING_IMAGES_IN_CSV : 10,
        NUM_OF_PALLETES : 82,
        OFFSET_IN_COLORS_CSV : 4,
        MODEL_NAME_COL : 0,
        ICON_NAME_COL : 1,
        IS_ORBIT_COL : 6,
        DATA_8_HD_COL : 8,
        DATA_HD_COL : 9,
        DATA_8_SD_COL : 10,
        DATA_SD_COL : 11,
        EXTRA_SMALL_SIZE_COL : 12,
        SMALL_SIZE_COL : 13,
        MEDIUM_SIZE_COL : 14,
        LARGE_SIZE_COL : 15,
        SHOP_ID_COL : 16,
        FRAME_SETUP_COL : 29,
        CATEGORY_COL : 30,
        SHOP_TEXT_COL:31,
        SHOP_PRICE_COL:32,
        SHOP_IMAGE_COL:33,
        SHOP_lINK_COL:34,
        SHOP_SKIP_COL:35,
        ZOOM_ADJUST_COL:36,
        MADE_OFF_COL:37
    },

    LAYOUT:{
        FLOAT_COLOR_BUTTON_POSITIONS : [210, 188, 166, 142, 120, 97, 73, 50, 28],
        COLOR_ADJUST_DOT_POSITIONS : [210, 188, 166, 142, 120, 97, 73, 50, 28],
        EDIT_BOX_BASE_WIDTH : 720,
        EDIT_BOX_BASE_HEIGHT : 1280,
        SCREEN_SIZE_COEF : this.EDIT_BOX_BASE_WIDTH / this.SCREEN_WIDTH,
        SCREEN_HEIGHT_COEF : this.EDIT_BOX_BASE_HEIGHT / this.SCREEN_HEIGHT,
        BASE_FPS : 60,
        FADE_SPRITE_ALPHA : 0.5,

        COLOR_ADJUST_BUTTON_SCALE : 0.40,
        COLOR_ADJUST_DOT_SCALE : 0.80,
        COLOR_ADJUST_SCALE : 0.455,
        SCALE : 0.49,
        BOOT_PROGRESSBAR_SCALE : 1.5,
        SETTINGS_BUTTONS_SCALE : 0.7,
        BUY_BUTTON_SCALE : 0.6,
        SELECT_BUTTON_SCALE : 0.5,
        SMALL_COLOR_SELECTION_SCALE : 0.09,
        MEDIUM_COLOR_SELECTION_SCALE : 0.18,
        MAIN_COLOR_SELECTION_SCALE : 0.27,
        LARGE_COLOR_SELECTION_SCALE : 0.36,
        LOADING_IMAGE_SCALE : 0.75,
        NET_WARNING_SCALE : 0.75,
        RATE_IMAGE_SCALE : 0.75,
        PRIVACY_SCALE : 0.4,

        lABEL_SCALE : 1.6,
        lOADING_BITMAP_FONT_EXTRA_WIDTH : 100,
        BUTTONS_SIZE_BASE : 85,
        BODY_PART_BUTTON_WIDTH : 80,

        BASIC_ASPECT_RATIO : 0.75,
        BUTTONS_SIZE : 85,

        COLOR_SLIDER_X_OFFSET : 22,
        FLOAT_COLOR_SLIDER_X_OFFSET : 20,
        X_OFFSET_BETWEEN_SLIDERS : 29,
        FLOAT_SLIDER_BG_OFFSET_X : 49,
        SLOTS_UPPER_OFFSET : 85 * 0.6,

        SLIDER_BG_OFFSET_Y : 109,
        SLIDER_Y_OFFSET : 27,
        SLIDER_BUTTON_Y_OFFSET : 19,

        SAVESLOT_OFFSET_Y_COEF : 0.0897,

        BUY_NOW_OFFSET : 45,
        SIZE_TEXT_OFFSET : 75,
        ORDER_NOW_TEXT_SIZE : 40,
        SIZE_TEXT_SIZE : 30,
        PERCENTAGE_SIZE : 25,

        MODEL_ICON_WIDTH_COEF : 1.99,
        MODEL_ICON_HEIGHT_COEF : 1.5,

        DICE_OFFSET_X : 0,
        DICE_OFFSET_Y : 100,

        SIZE_BUTTON_OFFSET : 110,
        DICE_SIZE_IN_CM : 1.6,

        EDIT_MODE:0,
        PREVIEW_MODE:1,
        SAVE_PREVIEW_MODE:2,
        SHOP_MODE:3,
        
    },
    SOUND:{
        BACKGROUND_SOUND:0,
        COLOR_CHANGE_SOUND:1,
        COLOR_GENERAL_SOUND:2,
        CANCEL_SOUND:3,
    },
    COLOR_MODE:{
        DEFAULT:0,
        CUSTOME:1
    }
}
