var CURRENT_SCENE = CST.SCENES.MAIN;
$(function(){
    document.addEventListener('upload_data', e => {
        $.ajax({
            url:`${CST.HOST_ADDRESS}/v1/api/uploadSavedData`,
            method:"POST",
            data:{
                user_id: user_id,
                model_name:e.detail.model_name,
                saved_data:e.detail.saved_data,
                index:e.detail.index
            },
            success:(res)=>{
                console.log("uploadSavedData", res);
            }
        })
    });

    document.addEventListener('getSavedData', e => {
        const model_name = e.detail.model_name;
        const callback = e.detail.callback;
        const shareFlag = e.detail.shareFlag;
        // console.log("getSavedData api call", user_id)

        if (!shareFlag){
            if(parseInt(user_id) > 0){
                getSavedData(user_id, model_name, (result)=>{
                    plusSloteInfoData(result, user_id, model_name, (updatedResult) => {
                        callback(updatedResult);
                    });
                });
            }else{
                callback([]);
            }
        } else {
            getSharedData(model_name, (result)=>{
                callback(result);
            });
        }
        
    });

    document.addEventListener('checkUserLoggedIn', e => {
        const callback = e.detail.callback;
        
        if(parseInt(user_id) > 0){
            callback([true, user_id]);
        }else{
            callback([false, 0]);
        }
    });

    
    // added by asset
    /************************************************************************************************************** */
    document.addEventListener('setSloteData', e => {
        const mid = e.detail.mid;
        const model_data = e.detail.model_data;
        const callback = e.detail.callback;
        
        setSloteData(mid, model_data, (result)=>{
            callback(result);
        });
        
    });

    document.addEventListener('getSloteData', e => {
        console.log("addEventListener getSloteData")
        const { model_name, slot_index,callback } = e.detail 
       
        getSloteData( model_name, slot_index, (result)=>{
            callback(result);
        });
    });

    document.addEventListener('addSloteData', e => {
        const model_data = { "user_id": parseInt(user_id) > 0?user_id:"test-1", ...e.detail.model_data };
        const callback = e.detail.callback;
       
        addSloteData(model_data, (result)=>{
            callback(result);
        });
       
    });

    document.addEventListener('delSloteData', e => {
        const mid = e.detail.mid;
        const callback = e.detail.callback;
       
        delSloteData(mid, (result)=>{
            callback(result);
        });
       
    });
    /***************************************************************************************************************/
    document.addEventListener('onScreenChange', e => {
        CURRENT_SCENE = e.detail;
        if(e.detail == CST.SCENES.MAIN){
            $(".nav-bar-header").show();
            if(!localStorage.getItem("accepted")){
                $(".consent-layout").addClass("active");
                localStorage.setItem("bottom-height", $(".consent-layout").innerHeight());
            }
        }else{
            $(".nav-bar-header").hide();
        }
    });

    document.addEventListener('app-link', e => {

        if(e.detail == "rate"){
            if(getOS() == "IOS"){
                window.location.href = LINKS.RATE_IOS;
            }
    
            if(getOS() == "Android"){
                window.location.href = LINKS.RATE_GOOGLE;
            }
        }
        if(e.detail == "privacy"){
            window.location.href = LINKS.PRIVACY;
        }

        

    });
    
    function getSavedData(uid, model_name, callback){
        $.ajax({
            url: `${CST.HOST_ADDRESS}/v1/api/getSavedData?user_id=${uid}&model_name=${model_name}`,
            method:"get",
            success:(res)=>{
                var result = JSON.parse(res);
                result.forEach(element => {
                    element.saved_data = JSON.parse(element.saved_data);
                });
                // console.log("getSavedData",result);
                
                callback(result);
            }
        })
    };

    function getSharedData(model_name, callback){

            $.ajax({
                url: `https://figuro5:figuro5pass1@colorcloud.figuromomodels.com/socials/api/models/?is_shared=${true}&model_name=${model_name}`,
                method:"GET",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader("Authorization", "Basic "+btoa("figuro5"+':'+"figuro5pass1"));
                },
                success:(sloteInfos)=>{
                    const sharedSlotIndexs = sloteInfos.map(e => e.slot_index);
                   
                    $.ajax({
                        url: `${CST.HOST_ADDRESS}/v1/api/getSavedData?model_name=${model_name}&slot_index=${sharedSlotIndexs}`,
                        method:"get",
                        success:(res)=>{
                            var result = JSON.parse(res);
                            result.forEach(element => {
                                element.saved_data = JSON.parse(element.saved_data);
                            });
                            // console.log("getSavedData",result);
                            const updatedResult = result.map(item => {
                                const { id, saved_data, index } = item;
                                
                                const infoItem = sloteInfos.find(e => e.slot_index === index);
                                const { like_count = 0, donwload_count = 0, is_shared = false, shared_date = "1999-01-01T00:00:00.350993Z", feature = 0 } = infoItem || {};
                                return { 
                                    id,
                                    user_id,
                                    model_name,
                                    index,
                                    saved_data,
                                    slote_info: {
                                        like_count,
                                        donwload_count,
                                        is_shared,
                                        shared_date,
                                        feature,
                                    }
                                }
                            });
                            callback(updatedResult);
                        }
                    })
                },
                error: (err) => {
                    console.log(err);
                }
                
            })
        
        
    };

    // added by asset
    // 
    // we need to build new api for saving like number and download number 
    /************************************************************************************************************** */
    function setSloteData(mid, model_data, callback){
        
        $.ajax({
            url: `https://colorcloud.figuromomodels.com/socials/api/models/${mid}/`,
            method:"PUT",
            contentType: "application/json",
            data: JSON.stringify(model_data),
            beforeSend: function(xhr) {
                xhr.setRequestHeader("Authorization", "Basic "+btoa("figuro5"+':'+"figuro5pass1"));
            },
            success:(res)=>{
                console.log('set slotdata success')
            }
        })
    };

    function getSloteData(user_id, model_name, callback){
      
        $.ajax({
            url: `https://figuro5:figuro5pass1@colorcloud.figuromomodels.com/socials/api/models/?user_id=${user_id}&model_name=${model_name}`,
            method:"GET",
            beforeSend: function(xhr) {
                xhr.setRequestHeader("Authorization", "Basic "+btoa("figuro5"+':'+"figuro5pass1"));
            },
            success:(res)=>{
                callback(res)
            },
            error: (err) => {
                console.log(err);
            }
            
        })
    };

    function addSloteData(model_data, callback){
        $.ajax({
            url: `https://colorcloud.figuromomodels.com/socials/api/models/`,
            method:"POST",
            contentType: "application/json",
            data: JSON.stringify(model_data),
            beforeSend: function(xhr) {
                xhr.setRequestHeader("Authorization", "Basic "+btoa("figuro5"+':'+"figuro5pass1"));
            },
            success:(res)=>{
                callback("success")
                // console.log('add slotdata success')
            }
        }) 
    };

    function delSloteData(mid, callback){
        $.ajax({
            url: `https://colorcloud.figuromomodels.com/socials/api/models/${mid}/`,
            method:"DELETE",
            contentType: "application/json",
            data: JSON.stringify({id: mid}),
            beforeSend: function(xhr) {
                xhr.setRequestHeader("Authorization", "Basic "+btoa("figuro5"+':'+"figuro5pass1"));
            },
            success:(res)=>{
                console.log('delete slotdata success')
            }
        })
    };

    /************************************************************************************************************** */

    function getOS() {
        var userAgent = window.navigator.userAgent,
            platform = window.navigator.platform,
            macosPlatforms = ['Macintosh', 'MacIntel', 'MacPPC', 'Mac68K'],
            windowsPlatforms = ['Win32', 'Win64', 'Windows', 'WinCE'],
            iosPlatforms = ['iPhone', 'iPad', 'iPod'],
            os = null;

        if (macosPlatforms.indexOf(platform) !== -1) {
            os = 'Mac OS';
        } else if (iosPlatforms.indexOf(platform) !== -1) {
            os = 'iOS';
        } else if (windowsPlatforms.indexOf(platform) !== -1) {
            os = 'Windows';
        } else if (/Android/.test(userAgent)) {
            os = 'Android';
        } else if (!os && /Linux/.test(platform)) {
            os = 'Linux';
        }
        return os;
        
    }

    function plusSloteInfoData(slotedata, user_id, model_name, callback){

        $.ajax({
            url: `https://figuro5:figuro5pass1@colorcloud.figuromomodels.com/socials/api/models/?user_id=${user_id}&model_name=${model_name}`,
            method:"GET",
            beforeSend: function(xhr) {
                xhr.setRequestHeader("Authorization", "Basic "+btoa("figuro5"+':'+"figuro5pass1"));
            },
            success:(sloteInfos)=>{
                
                const updatedResult = slotedata.map(item => {
                    const { id, saved_data, index } = item;
                    
                    const infoItem = sloteInfos.find(e => e.slot_index === index);
                    const { like_count = 0, donwload_count = 0, is_shared = false, shared_date = "1999-01-01T00:00:00.350993Z", feature = 0 } = infoItem || {};
                    return { 
                        id,
                        user_id,
                        model_name,
                        index,
                        saved_data,
                        slote_info: {
                            like_count,
                            donwload_count,
                            is_shared,
                            shared_date,
                            feature,
                        }
                     }
                });
                console.log("plusSloteInfoData success", updatedResult)
                callback(updatedResult);
            },
            error: (err) => {
                console.log(err);
            }
            
        })
    }
});
