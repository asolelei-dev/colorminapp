<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="0" />
    <meta name="google-signin-client_id" content="527345867759-416ifafdbfiqnciigpo3iesm23r35k9q.apps.googleusercontent.com">

    <style media='screen' type='text/css'>
        @font-face {
            font-family: "Nevis";
            src: url('./assets/fonts/nevis.ttf');
            font-weight:400;
            font-weight:normal;
        }

        #game-canvas {
            display:flex;
        }
        canvas {
            margin:auto;
        }

        body {
            overflow:hidden !important;
        }

    </style>
    <link rel="stylesheet" href="./assets/css/style.css" crossorigin="anonymous">
    
    <div style="font-family: Nevis; position: absolute; top:100px; color: transparent">.</div>
    <script src="./assets/js/phaser.min.js"></script>
    <script src="./assets/js/papaparse.min.js"></script>
    <title>Color Minis</title>
    <script src="./output.js"></script>

    <link rel="stylesheet" href="./assets/fonts/pe/css/pe-icon-set-weather.min.css">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.4.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <?php require("./HTML/shoping-cart.php") ?>

</head>
<body>
    <div id = "game-canvas"></div>
</body>
</html>