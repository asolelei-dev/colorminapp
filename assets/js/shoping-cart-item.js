function getHashVal(str) {
    var hash = 0, i, chr;
    if (str.length === 0) return hash;
    for (i = 0; i < str.length; i++) {
        chr   = str.charCodeAt(i);
        hash  = ((hash << 5) - hash) + chr;
        hash |= 0; // Convert to 32bit integer
    }
    return hash;
};
class ShopingCartItem {
    constructor(variantId, imageId, productName, size, finish, oldPrice, newPrice, quantity, jsonData){
        this.variantId = variantId;
        this.imageId = imageId;
        this.size = size;
        this.finish = finish;
        this.productName = productName;
        this.oldPrice = oldPrice;
        this.newPrice = newPrice;
        this.quantity = quantity;
        this.jsonData = jsonData;
        this.hashValue = getHashVal(variantId+productName+jsonData);

    }

    increaseQuantity(){
        this.quantity ++;
    }

    decreaseQuantity(){
        this.quantity --;
        if(this.quantity < 0) this.quantity = 0;
    }

    

}