const client = ShopifyBuy.buildClient({
    domain: 'colorminis-shop.myshopify.com',
    storefrontAccessToken: '01476f01ed588cc19262797feb38096a'
});

var selectedProduct;
var shopingCartItems = [];
var selectedVariant;
var selectedModelCharacteristic;
var savedJSONdata;
var pendingFunc;
var timer;
var selectedProductImage;

$(function(){
    $(".btn-exit").click(()=>{
        var event = new CustomEvent('web.exit.shoping-cart', { detail: "event test" });
        document.dispatchEvent(event);
        hideShopingCart();
    });

    $(".btn-bucket").click(()=>{
        if(shopingCartItems.length > 0){
            $(".shoping-cart").hide();
            $(".shoping-cart-bucket").show();
        }
        
    });

    $('.collapse').on('show.bs.collapse', function () {
        $(this).siblings('.card-header').addClass('active');
    });

    $('.collapse').on('hide.bs.collapse', function () {
        $(this).siblings('.card-header').removeClass('active');
    });

    $("#product-options").on("changed.bs.select", 
        (e, clickedIndex, isSelected, previousValue) => {
        var selected = $(e.currentTarget).val();
        var variant = filterByVariantID(selectedProduct.variants, selected);
        updateShopingCartUIBySelectedVariant(variant);

    });

    $("#btn_add_to_cart").click(()=>{
        addItemtoCart();
        updateProductBucket();
    });

    $("#btn_buy_now").click(()=>{

        client.checkout.create().then((checkout) => {
            // Do something with the checkout
            var lineItemsToAdd = [];

            if(shopingCartItems.length > 0){
                shopingCartItems.forEach(element => {

                    var item = {
                        quantity:element.quantity,
                        variantId:element.variantId,
                        customAttributes: [{key: "hashCode", value: `hashcode${element.hashValue}`}]
                    };
                    lineItemsToAdd.push(item);
                });
    
            }else{

                var item = {
                    quantity:1,
                    variantId:selectedVariant.id
                };
                lineItemsToAdd.push(item);
                addItemtoCart();
                
            }

            client.checkout.addLineItems(checkout.id, lineItemsToAdd).then((final_checkout) => {
                console.log(final_checkout.lineItems); // Array with one additional line item
                location.href = final_checkout.webUrl;
                
            });

            

        });
    });

    $("#btn_check_out").click(()=>{
        $("#btn_buy_now").trigger("click");
    })

});

function getShopingCartItem(uid, callback){
    $.ajax({
        url: `${CST.HOST_ADDRESS}/v1/api/getSavedShopItems?user_id=${uid}`,
        method:"get",
        success:(res)=>{
            var result = JSON.parse(res);
            console.log(JSON.parse(result.jsonData));
            callback(JSON.parse(result.jsonData));
        }
    })
}


$(document).on("click", ".btn-add-item", (e)=>{
    var cardItem = $(e.target).parents(".cart-item");
    var shopingCartItem = getItemFromShopgingCarts(cardItem.data("hashVal"));
    shopingCartItem.increaseQuantity();
    saveShopingCartItem();
    initShopingCartItem();
    updateTotalCost();
});

$(document).on("click", ".btn-remove-item", (e)=>{
    var cardItem = $(e.target).parents(".cart-item");
    var shopingCartItem = getItemFromShopgingCarts(cardItem.data("hashVal"));
    remvoeItemFromCart(shopingCartItem);
    updateTotalCost();
});


document.addEventListener('web.start.shoping-cart', e => {

    console.log(e.detail);

        pendingFunc = ()=>{

            //check if it is login
            const modelCharacteristic = e.detail.modelCharacteristics;
            const savedmodel = e.detail.savedModel;
            savedJSONdata = savedmodel;
            startShopingCart(modelCharacteristic);
            localStorage.setItem("temp_data", savedJSONdata);

            if(parseInt(user_id) > 0){
                shopingCartItems = [];
                getShopingCartItem(user_id, (result)=>{
                    console.log(result);
                    result.forEach(element =>{
                        const name = element.productName;
                        const size = element.size;
                        const finish = element.finish;
                        const imageId = element.imageId;
                        const oldPrice = element.oldPrice;
                        const newPrice = element.newPrice;
                        const variantId = element.variantId;
                        const quantity = element.quantity;
                        const jsonData = element.jsonData;
                        var newItem = new ShopingCartItem(variantId, imageId, name, size, finish, oldPrice, newPrice, quantity, jsonData);
                        shopingCartItems.push(newItem);
                    });

                    if(shopingCartItems.length > 0){
                        initShopingCartItem();
                    }
                    
                });
            }
        }

        if(parseInt(user_id) == 0){
            $("#btn-login").trigger("click");
        }else{
            pendingFunc();
        }

    }
);

document.addEventListener('web.getToken', e => {
        console.log("web.getToken event dispatch")
        getToken();
    }
);

document.addEventListener('web.order_finish', e => {
        onFinishOrder(e.detail);
    }
);

function startShopingCart(modelCharacteristic){
    $("canvas").hide();
    // $(".shoping-cart").show();
    $(".shoping-cart-bucket").hide();
    $("#btn-bucket span").hide();
    fetchProductInfoFromShopify(modelCharacteristic);
}

function hideShopingCart(){
    $("canvas").show();
    $(".shoping-cart").hide();
    $(".shoping-cart-bucket").hide();
}

function getToken(){
    $.post(`${CST.AUTH.TOKEN_URL}`, {username:CST.AUTH.USERNAME, password:CST.AUTH.PASSWORD}, (res)=>{
        TOKEN = `Token ${res.token}`;
        localStorage.setItem("temp_token", TOKEN);
        getFAQList();
    });
}

function getFAQList(){
    $.ajax({
        "url":`https://colorcloud.figuromomodels.com/api/faqs`,
        "method": "GET",
        "headers": {
            "Authorization": TOKEN,
            "Content-Type": "application/json",
        }
    }).done((res)=>{
        initFAQList(res);
    });
}

function initFAQList(faqlist){
    const faqlistHTML = `
            ${faqlist.map(obj => `<h5>${obj.question}</h5><p>${obj.answer}</p>`).join("")}
        `;
        $("#faq-list-body").html(faqlistHTML);
}

function saveShopingCartItem(){
    if(timer) clearTimeout(timer);
    console.log(shopingCartItems);
    timer = setTimeout(() => {
        $.ajax({
            url:`${CST.HOST_ADDRESS}/v1/api/addShopingCart`,
            method: "POST",
            data: {
                jsonData:JSON.stringify(shopingCartItems)
            },
        }).done((res)=>{
        });
    }, 500);
}

function initShopingCartItem(){
    
    const html = `
        ${shopingCartItems.map(obj => `
            <div class = "row cart-item" data-hash-val = "${obj.hashValue}">
                <div class = "col-8">
                    <div>
                        <img src = "${obj.imageId}"/>
                        <div>${obj.size}cm ${obj.finish}</div>
                    </div>
                    <div><h4>${obj.productName}</h4></div>
                    <div>CUSTOM COLOR FIGURE</div>
                </div>
                <div class = "col-4">
                    <div>$${obj.oldPrice}</div>
                    <div>$${obj.newPrice}</div>
                    <div>${obj.quantity}</div>
                    <div>
                        <button type="button" class="btn btn-secondary btn-add-item">+</button>
                        <button type="button" class="btn btn-secondary btn-remove-item">-</button>
                    </div>
                </div>
            </div>
        `).join("")}
        `;
    $("#item-list").html(html);

}

function initSelectOptions(options){
    const html = `
            ${options.map(obj => `<option title = "${obj.sku}" value="${obj.id}">${obj.sku}</option>`).join("")}
        `;
        $("#product-options").html(html);
        $('.selectpicker').selectpicker('refresh');
}

function fetchProductInfoFromShopify(modelCharacteristic){

    selectedModelCharacteristic = modelCharacteristic;
    localStorage.setItem("temp_name", selectedModelCharacteristic.name);

    // Build a custom products query using the unoptimized version of the SDK
    var encodedID = btoa(`gid://shopify/Product/${modelCharacteristic.getShopId()}`);

    client.product.fetch(encodedID).then((product) => {
        selectedProduct = product;
        console.log(product);
        updateShopingCartUI(product, modelCharacteristic.getFinish(), modelCharacteristic.getSize());
    });
}

function filterBySKU(variants, finish, size){
    var result = undefined;
    variants.forEach(element => {
        console.log(element.sku, size);
        if(element.sku.includes(finish) && element.sku.includes(size)) result = element;
    });
    return result;
}

function filterByVariantID(variants, id){
    var result = undefined;
    variants.forEach(element => {
        if(element.id == id) result = element;
    });
    return result;
}

function getItemFromShopgingCarts(hashVal){
    var result = undefined;
    shopingCartItems.forEach(element => {
        if(element.hashValue == hashVal) result = element;
    });
    return result;
}

function updateShopingCartUI(productData, finish, size){
        
    $("#product-title h4").html(productData.title);
    initSelectOptions(productData.variants);
    var variant = filterBySKU(productData.variants, finish, size);
    updateShopingCartUIBySelectedVariant(variant);
    updateProductBucket();
    //update image
    const productImage = variant.image.src;
    selectedProductImage = productImage;
    if(productImage != ""){
        $(".banner-image img").attr({"src":productImage});
    }
    $(".shoping-cart").show();
}

function updateShopingCartUIBySelectedVariant(variant){
    selectedVariant = variant;
    const sku = variant.sku;
    var arr = sku.replace("cm", "").split("_");
    selectedModelCharacteristic.setSize((arr[2].split("-")[0]));
    selectedModelCharacteristic.finish = arr[1];

    $("#cost-div #old-cost").html(`${variant.compareAtPrice}$`);
    $("#cost-div #new-cost").html(`${variant.price}$`);
    $("#product-options").val(variant.id);
    $("#product-options").selectpicker("refresh");
}

function getShpingCartItemBySelectedModelCharacteristic(){
    var result;
    shopingCartItems.forEach(element => {
        if(element.hashValue == getHashVal(selectedVariant.id+selectedModelCharacteristic.getName()+savedJSONdata)){
            result = element;
        }
    });
    return result;
}

function addItemtoCart(){
    selectedShopingCartItem = getShpingCartItemBySelectedModelCharacteristic();

    if(selectedShopingCartItem == undefined){
        const name = selectedModelCharacteristic.name;
        const size = selectedModelCharacteristic.size;
        const finish = selectedModelCharacteristic.finish;
        const imageId = selectedVariant.image.src;
        const oldPrice = selectedVariant.compareAtPrice;
        const newPrice = selectedVariant.price;
        const variantId = selectedVariant.id;
        var newItem = new ShopingCartItem(variantId, imageId, name, size, finish, oldPrice, newPrice, 1, savedJSONdata);
        shopingCartItems.push(newItem);
    }else{
        selectedShopingCartItem.increaseQuantity();
    }
    saveShopingCartItem();
    initShopingCartItem();

}

function remvoeItemFromCart(selectedShopingCartItem){

    selectedShopingCartItem.decreaseQuantity();
    if(selectedShopingCartItem.quantity == 0){
        shopingCartItems = shopingCartItems.filter(function(ele){
            return (ele.quantity > 0);
        });
    }
    saveShopingCartItem();
    initShopingCartItem();

}

function updateProductBucket(){

    var totalQuantity = 0;

    shopingCartItems.forEach(element => {
        totalQuantity += element.quantity;
    });

    if(totalQuantity == 0){
        $("#btn-bucket span").hide();    
    }else{
        $("#btn-bucket span").show();    
    }
    $("#btn-bucket span").html(totalQuantity);
    updateTotalCost();

}

function updateTotalCost(){
    var totalCost = 0;
    shopingCartItems.forEach(element => {
        totalCost += parseFloat(element.newPrice) * element.quantity;
    });

    $("#sub-total").html(`Subtotal $${totalCost.toFixed(2)}`);

}

function uploadSavedModel(shopifyurl, callback){

    const temp_token = localStorage.getItem("temp_token");

    var sendData = {
        // data:temp_data,
        device_id:"web",
        // series:temp_name.split("_")[0],
        items:[],
        shopify_status_url:shopifyurl
    };

    if(parseInt(user_id) > 0){
        var items = [];
        getShopingCartItem(user_id, (result)=>{
            result.forEach(element =>{
                const name = element.productName;
                const jsonData = element.jsonData;
                items.push({
                    series:name.split("_")[0],
                    data:jsonData
                });
            });
            sendData.items = items;

            console.log("sendData ====> ", sendData);
            ////////////////////////////
            $.ajax({
                url:`https://colorcloud.figuromomodels.com/api/buy/`,
                method: "POST",
                headers: {
                    "Authorization": temp_token,
                    "Content-Type": "application/json",
                },
                data: JSON.stringify(sendData),
            }).done((res)=>{
                window.history.pushState("", "", `${CST.HOST_ADDRESS}`);
                callback();
            });
        });
    }
}

function resetShopingCart(callback){
    $.ajax({
        url:`${CST.HOST_ADDRESS}/v1/api/restShoppingCart`,
        method:"POST",
        data:{
            user_id: user_id
        },
        success:(res)=>{
            callback();
        }
    });
}

function onFinishOrder(order_status_url){

    uploadSavedModel(order_status_url,()=>{
        resetShopingCart(()=>{
        })
    });
}


